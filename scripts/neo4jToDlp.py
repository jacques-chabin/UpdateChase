import sys
import json
import pprint

PROP_MAP = {}
NB_NULLS = 0

def mapNodeProps(node_data):
    p_pred = node_data.get('labels')[0]
    p_props = PROP_MAP.get(p_pred, [])
    p_props = set(p_props).union(node_data.get('properties').keys())

    PROP_MAP[p_pred] = sorted(p_props)

def mapRelProps(node_data):
    p_pred = node_data.get('type')
    p_props = PROP_MAP.get(p_pred, [])
    p_props = set(p_props).union(node_data.get('properties').keys())

    PROP_MAP[p_pred] = sorted(p_props)

def extractPredProps(pred, data):
    props = []
    global NB_NULLS
    for prop_name in PROP_MAP.get(pred):
        p = data.get(prop_name)

        if type(p) in (list, tuple):
            p = p[0] if len(p) else None
        elif type(p) is set:
            p = p.pop() if len(p) else None
        elif type(p) is dict:
            p = list(p.values())[0] if len(p) else None
        
        if not p:
            NB_NULLS += 1
            p = f'_N{NB_NULLS}'
        
        props.append(str(p).replace(',', '').replace("'", ' ').replace('"', '').replace('(', '').replace(')', ''))
    
    return props

def extractNodePred(node_data):
    p_id = node_data.get('identity')
    p_pred = node_data.get('labels')[0]
    p_props = [str(p_id)] + extractPredProps(p_pred, node_data.get('properties'))
    return p_id, (f'{p_pred}{p_props}.').replace('[', '(').replace(']', ')')

def extractRelPred(node_data):
    p_id = node_data.get('identity')
    p_start = node_data.get('start')
    p_end = node_data.get('end')
    p_pred = node_data.get('type')
    p_props = [str(p_start), str(p_end)] + extractPredProps(p_pred, node_data.get('properties'))
    return p_id, (f'{p_pred}{p_props}.').replace('[', '(').replace(']', ')')

if __name__ == "__main__":
    filename = sys.argv[1] if len(sys.argv) > 1 else input('file> ')
    output = filename.replace('json', 'dlp')

    with open(filename, 'r') as f:
        data = json.load(f)
    
    # Generate prop map
    for record in data:
        mapNodeProps(record.get('a'))
        mapNodeProps(record.get('b'))
        mapRelProps(record.get('r'))
    
    pprint.pprint(PROP_MAP)
        
    # Get terms
    preds = {}

    for record in data:
        # Convert A
        pred_id, pred = extractNodePred(record.get('a'))
        preds[pred_id] = pred
        # Convert B
        pred_id, pred = extractNodePred(record.get('b'))
        preds[pred_id] = pred
        # Convert R
        pred_id, pred = extractRelPred(record.get('r'))
        preds["r" + str(pred_id)] = pred
    
    print(f'Nb nulls: {NB_NULLS}')

    with open(output, 'w') as f:
        for atom in sorted(preds.values()):
            f.write(atom + '\n')

