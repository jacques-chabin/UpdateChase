import sys
import json

NB_NULLS = 50

if __name__ == "__main__":
    filename = sys.argv[1] if len(sys.argv) > 1 else input('file> ')
    output = filename.replace('json', 'dlp')

    nb_nulls = int(sys.argv[2] if len(sys.argv) > 2 else input('NB nulls (default: ' + str(NB_NULLS) + ')> ') or NB_NULLS)

    with open(filename, 'r') as f:
        data = json.load(f)
    
    # Get terms
    terms = {}
    for v in data.get('vertices'):
        v_id = v.get('_id')
        v_name = v.get('short') or v.get('category') or v.get('iid')
        v_class = v.get('xlabel')
        terms[v_id] = (v_name, v_class)
    
    all_terms = list(set(v[0] for v in terms.values()))
    nulls_candidates = all_terms[:nb_nulls]

    # Convert some constants to nulls values
    for i in range(len(nulls_candidates)):
        for k, v in terms.items():
            if v[0] == nulls_candidates[i]:
                terms[k] = ("_N" + str(i), v[1])

    all_preds = set()
    all_class = set()
    with open(output, 'w') as f:
        for v in terms.values():
            v_name, v_class = v
            if v_class:
                all_class.add(v_class)
                f.write("{pred}('{x}').\n".format(
                    pred=v_class,
                    x=v_name
                ))

        for e in data.get('edges'):
            all_preds.add(e.get('_label'))
            f.write("{pred}('{x}', '{y}').\n".format(
                pred=e.get('_label'),
                x=terms.get(e.get('_outV'))[0],
                y=terms.get(e.get('_inV'))[0]
            ))

    print("NB constants:", len(all_terms) - len(nulls_candidates))
    print("NB nulls:", len(nulls_candidates))
    print("NB preds:", len(all_preds) + len(all_class))
    print("Preds:", sorted(list(all_preds)))
    print("Class:", sorted(list(all_class)))
