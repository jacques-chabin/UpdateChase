-- creation de la base de donnees issue du papier
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8;
USE `test`;

drop table if exists conf;
drop table if exists journal;
drop table if exists ranking;
drop table if exists univ;
drop table if exists cnrs;
drop table if exists aff;
drop table if exists prod;


create table prod(titre varchar(50),
				  auteur varchar(30),
				  yearp varchar(4),
				  location varchar(50),
				  labo varchar(30));
				  
create table aff(auteur varchar(30),
				 labo varchar(30),
				 yearp varchar(4));
				
create table cnrs(labo varchar(30));

create table univ(labo varchar(30));

create table ranking(location varchar(50),
					 rank varchar(1));
					 
create table journal(location varchar(50));

create table conf(location varchar(50));

create table res(auteur varchar(30),
				 yearp varchar(4),
				 ville varchar(30),
				 classe varchar(1));
				 
create table prof(auteur varchar(30),
				 yearp varchar(4),
				 ville varchar(30));


/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* S1 */

insert into prod values	('T1','Bob','2014','TODS','L1'),
	('T2','Bob','2015','ODBASE','L2'),
	('T2','Tom','2015','ODBASE','L2'),
	('T5','Tom','2015','TLDKS','L2'),
	('T6','Sue','2016','TLDKS','L2');
	
insert into aff values ('Bob','L2','2015');
insert into cnrs values('L2');
insert into univ values('L3'),('L1');

insert into res values('Sue', '2016', 'Orleans', '1'), ('Alice', '2016','Paris','1');

/* Ajoutée par inférence : */
insert into aff values ('Sue', 'Lnull1', '2016');
insert into aff values ('Alice', 'Lnull2', '2016');

/* S2 */

insert into ranking values('TODS','a'),('ICEIS','c'),('TLDKS','a');
insert into journal values('TODS'),('TLDKS');
insert into conf values('ICEIS'),('ODBASE');

/* S3 */

insert into prod values	('T1','Bob','2014','TODS','L1'),
	('T3','Mary','2016','ICEIS','L3'),
    ('T4','Joe','2016','ICEIS','L3') ;
	
insert into aff values ('Bob','L1','2014'),
	('Tom','L2','2015'),
	('Mary','L3','2016'),
	('Mary','L1','2016');
	
insert into cnrs values('L1');

insert into prof values('Mary','2014','Orleans'),('Ann','2015','Tours');

/* Ajoutée par inférence : */
insert into aff values('Mary', 'Lnull3', '2014'), ('Ann', 'Lnull4', '2015');

/* S4 */

/* insert into ranking values ('TODS','b'),('ODBASE','b'),('ICEIS','c'),('TLDKS','c'); */
  
  
create user 'userpaper'@'localhost' identified by 'userpaper';
grant select on test.* to 'userpaper'@'localhost';

