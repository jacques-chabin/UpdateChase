-- creation de la base de donnees issue du papier

drop table if exists conf;
drop table if exists journal;
drop table if exists ranking;
drop table if exists univ;
drop table if exists cnrs;
drop table if exists aff;
drop table if exists prod;


create table prod(titre varchar(50),
				  auteur varchar(30),
				  yearp varchar(4),
				  location varchar(50),
				  labo varchar(30));
				  
create table aff(auteur varchar(30),
				 labo varchar(30),
				 yearp varchar(4));
				
create table cnrs(labo varchar(30));

create table univ(labo varchar(30));

create table ranking(location varchar(50),
					 rank varchar(1));
					 
create table journal(location varchar(50));

create table conf(location varchar(50));


/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* S1 */

insert into prod values	('t1','a1','y1','p1','l1'),
	('t2','a1','y2','p2','l2'),
	('t2','a2','y2','p2','l2'),
	('t5','a2','y2','p5','l2');
	
insert into aff values ('a1','l2','y2');
insert into cnrs values('l2');
insert into univ values('l3'),('l1');


/* S2 */

insert into ranking values('p1','a'),('p3','c'),('p5','a');
insert into journal values('p1'),('p5');
insert into conf values('p3'),('p2');

/* S3 */

insert into prod values	('t3','a3','y3','p3','l3'),
	('t4','a4','y3','p3','l3');
/*  prod('t1','a1','y1','p1','l1') */
	
insert into aff values ('a1','l1','y1'),
	('a2','l2','y2'),
	('a3','l3','y3'),
	('a3','l1','y3');
	
insert into cnrs values('l1');

/* S4 */

insert into ranking values ('p2','b');
/*  ranking('p1','b'),
  ranking('p2','b'),
  ranking('p3','c'),
  ranking('p5','c');  */
