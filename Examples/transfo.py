from random import *

def createAtome(pred, nba, c1, nb1, c2, nb2):
	nbp = 0
	res = set()
	while(len(res) < nba):
		a=pred+"("+c1+str(randint(0,nb1))+", "+c2+str(randint(0,nb2))+").\n"
		res.add(a)
	return res
	
def createDB(nbp, nbi):
	name = 'data'+str(nbp)+'-'+str(nbi)+'.dlp'
	fichier = open(name,'w')
	for i in range(nbp):
		pred = 'A'+str(i)
		c1 ='a'+str(i)+'-'
		c2 = 'a'+str(i+1)+'-'	
		res = createAtome(pred, nbi, c1, nbi, c2, nbi)
		for a in res:
			fichier.write(a)
	fichier.close()

def createBDTest2(nbp, nbi):
	name = 'data'+str(nbp)+'-'+str(nbi)+'.dlp'
	fichier = open(name,'w')
	for i in range(nbp):
		predA = 'A'+str(i)
		predB = 'B'+str(i)
		predC = 'C'+str(i)
		predD = 'D'+str(i)
		cx ='a'+str(i)+'-'
		cy = 'a'+str(i+1)+'-'
		cz = 'b'+str(i)+'-'
		cu = 'c'+str(i)+'-'
		cv = 'd'+str(i)+'-'
		for j in range(nbi):
			fin1=str(j)
			fin2=str(randint(0,nbi))
			a=predA+'('+cx+fin1+','+cy+fin2+").\n"
			fichier.write(a)
			b=predB+'('+cx+fin1+','+cz+fin1+").\n"
			fichier.write(b)
			c=predC+'('+cx+fin1+','+cz+fin1+','+cu+fin1+").\n"
			fichier.write(c)
			d=predD+'('+cx+fin1+','+cz+fin1+','+cu+fin1+','+cv+fin1+").\n"
			fichier.write(d)
	fichier.close()			
						
def createConstraint1(nbp):
	name = "T1posConst1-"+str(nbp)+".dlp"
	fichier=open(name,"w")
	for i in range(nbp):
		c = "A"+str(i)+ "(XX,XY) -> B"+str(i)+"(XY,XZ) .\n"
		fichier.write(c)
	fichier.close()

def createConstraint2(nbp):
	name = "T2posConst2-"+str(nbp)+".dlp"
	name1 = "T1posConst1-"+str(nbp)+".dlp"
	ficold = open(name1, 'r')
	const1 = ficold.read()
	ficold.close()
	fichier=open(name,"w")
	fichier.write(const1)
	for i in range(nbp):
		#c = "A"+str(i)+ "(XX,XY) -> B"+str(i)+"(XY,XZ) .\n"
		#fichier.write(c)
		c = "B"+str(i)+"(XX,XY)  -> C"+str(i)+"(XX,XY,XZ) .\n"
		fichier.write(c)
	fichier.close()

def createConstraint3(nbp):
	name = "T3posConst3-"+str(nbp)+".dlp"
	name1 = "T2posConst2-"+str(nbp)+".dlp"
	ficold = open(name1, 'r')
	const1 = ficold.read()
	ficold.close()
	fichier=open(name,"w")
	fichier.write(const1)
	for i in range(nbp):
		#c = "A"+str(i)+ "(XX,XY) -> B"+str(i)+"(XY,XZ) .\n"
		#fichier.write(c)
		c = "C"+str(i)+"(XX,XY,XZ)  -> D"+str(i)+"(XX,XY,XZ,XU) .\n"
		fichier.write(c)
	fichier.close()

def createConstraint2_0(nbp):
	# nbp contraintes 2 atomes vers 1 atomes sans Null
	name = "T4posConst2_0-"+str(nbp)+".dlp"
	fichier=open(name,"w")
	for i in range(nbp):
		c = "A"+str(i)+ "(XX,XY), A"+str(i+1)+ "(XY,XZ) -> AA"+str(i)+"(XX,XZ) .\n"
		fichier.write(c)
	fichier.close()

def createConstraint2_1(nbp):
	# nbp contraintes 2 atomes vers 1 atomes une null
	name = "T5posConst2_1-"+str(nbp)+".dlp"
	ficold = open("T4posConst2_0-"+str(nbp)+".dlp", 'r')
	const1 = ficold.read()
	ficold.close()
	fichier=open(name,"w")
	fichier.write(const1)	
	for i in range(nbp):
		c = "A"+str(i)+ "(XX,XY), A"+str(i+1)+ "(XY,XZ) -> AB"+str(i)+"(XX,XU) .\n"
		fichier.write(c)
	fichier.close()

def createConstraint2_2(nbp):
	# nbp contraintes 2 atomes vers 1 atomes une null propagée
	name = "T6posConst2_2-"+str(nbp)+".dlp"
	ficold = open("T4posConst2_0-"+str(nbp)+".dlp", 'r')
	const1 = ficold.read()
	ficold.close()
	fichier=open(name,"w")
	fichier.write(const1)	

	for i in range(nbp):
		c = "A"+str(i)+ "(XX,XY), A"+str(i+1)+ "(XY,XZ) -> AB"+str(i)+"(XX,XU) .\n"
		fichier.write(c)
		c = "AA"+str(i)+ "(XX,XY), AB"+str(i)+ "(XX,XZ) -> AC"+str(i)+"(XX,XZ,XU) .\n"
		fichier.write(c)
	fichier.close()	


def fusionAllConstraints(nbp):
	# regroupe les ctr3 et et 2_2 en un fichier
	name = "allPosConst-"+str(nbp)+".dlp"
	ficold = open("T3posConst3-"+str(nbp)+".dlp", 'r')
	const1 = ficold.read()
	ficold.close()
	fichier=open(name,"w")
	fichier.write(const1)
	ficold = open("T6posConst2_2-"+str(nbp)+".dlp", 'r')
	const1 = ficold.read()
	ficold.close()
	fichier.write(const1)	

def createInsert(nbp, nbi):
	name = "insertAtom-"+str(nbp)+".dlp"
	fichier = open(name, 'w')
	for i in range(nbp):
		a='A'+str(i)+"("+'a'+str(i)+'-'+str(randint(0,nbi))+", "+'a'+str(i+1)+'-'+str(randint(0,nbi))+").\n"
		fichier.write(a)
	fichier.close()	

def createAll(nbp):
	createBDTest2(nbp, nbp)
	createInsert(nbp, nbp)
	createConstraint1(nbp)
	createConstraint2(nbp)
	createConstraint3(nbp)
	createConstraint2_0(nbp)
	createConstraint2_1(nbp)
	createConstraint2_2(nbp)
	fusionAllConstraints(nbp)
	
	
def txtToDlg(name):
	fichier = open(name, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	i=1
	name = name.split('.')
	nameRes=name[0]+"Pred"+'.dlp'
	ficRes = open(nameRes, "w")
	for s in lignes :
		#print(s)
		i=i+1
		pos = s.find('CI(')
		if pos != -1 :
			s=s[pos+3:len(s)-2]
			#print(s)
			ls = s.split(',')
			atome = ls[1]+'('+ls[0]+').\n'
			ficRes.write(atome)
		else :
			pos = s.find('PI(')
			if pos != -1 :
				s=s[pos+3:len(s)-2]
				#print(s)
				ls = s.split(',')
				atome = ls[2]+'('+ls[0]+','+ls[1]+').\n'
				ficRes.write(atome)	
						
	ficRes.close()
	return i
	
def sqlToDlg(name):
	fichier = open(name, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	i=0
	name = name.split('.')
	nameRes=name[0]+'.dlp'
	ficRes = open(nameRes, "w")
	ficAllRes = open("allData.dlp", "a")
	nullNb=0
	for s in lignes :
		#print(s)
		pos1 = s.find('INSERT INTO ')
		pos2 = s.find('VALUES ')
		if pos1 != -1 and pos2 != -1:
			predicat=s[pos1+13:pos2-2]
			#print(predicat)
			value=s[pos2+7:len(s)-1]
			lvalues=value.split(')')
			for val in lvalues:
				if val!="":
					if val[0] == ',':
						val=val[1:]
					lval = val.split(',')
					atome = predicat
					for v in lval:
						if v == 'null' : #and nullNb < 10:
							nullNb +=1
							v='_N'+str(nullNb)
						atome += v+','
					atome = atome[:len(atome)-1]+').\n'
					i+=1
					ficRes.write(atome)		
	ficRes.close()
	return i, nullNb

def allSqlToDlg(lname):		
	nullNb=0
	ficAllRes = open("allData2.dlp", "w")
	i=0
	for name in lname :
		fichier = open(name, "r");
		texte = fichier.read()
		fichier.close()
		lignes = texte.split("\n")
		name = name.split('.')
		nameRes=name[0]+'.dlg'
		ficRes = open(nameRes, "w")
		for s in lignes :
			#print(s)
			pos1 = s.find('INSERT INTO ')
			pos2 = s.find('VALUES ')
			if pos1 != -1 and pos2 != -1:
				predicat=s[pos1+13:pos2-2]
				#print(predicat)
				value=s[pos2+7:len(s)-1]
				lvalues=value.split(')')
				for val in lvalues:
					if val!="":
						if val[0] == ',':
							val=val[1:]
						lval = val.split(',')
						atome = predicat
						for v in lval:
							if v == 'null' : # and nullNb < 0:
								nullNb +=1
								v='_N'+str(nullNb)
							atome += v+','
						atome = atome[:len(atome)-1]+').\n'
						i+=1
						ficRes.write(atome)	
						ficAllRes.write(atome)	
		ficRes.close()
	ficAllRes.close()
	return i, nullNb

lesFichiers1000SQL=['S01ProductFeature.sql', 'S02ProductType.sql', 'S03Producer.sql',  'S04Product.sql', 'S05ProductTypeProduct.sql', 'S06ProductFeatureProduct.sql', 'S07Vendor.sql', 'S08Offer.sql', 'S09Person.sql', 'S10Review.sql']
lesFichiers100SQL=['01ProductFeature.sql', '02ProductType.sql', '03Producer.sql',  '04Product.sql', '05ProductTypeProduct.sql', '06ProductFeatureProduct.sql', '07Vendor.sql', '08Offer.sql', '09Person.sql', '10Review.sql']

def suppVirgulesEtParentheses(ch):
	#transforme aussi les X (réservé aux variables) en x
	res=""
	for c in ch:
		if c==',' or c =='(' or c == ')':
			c=' '
		else :
			if c=='X':
				c='x'
		res += c
	return res
	
def flavioCvsCIToDlp():
	nullNb=0
	ficAllRes = open("allData.dlp", "w")
	i=0
	# transformation classe instance
	name = "ci.csv"
	fichier = open(name, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	
	for s in lignes :
		if(s!="" and s.find("%") == -1):
			lval = s.split(';')
			atome = ""
			for v in lval[1:]: 
				v = suppVirgulesEtParentheses(v)
				if atome!="":
					atome+=','
				atome +=v
			atome = 'CI('+atome+').\n'
			i+=1
			ficAllRes.write(atome)
	
	name = "pi.csv"
	fichier = open(name, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	
	for s in lignes :
		if(s!="" and s.find("%") == -1):
			lval = s.split(';')
			atome = ""
			for v in lval[1:]: 
				v = suppVirgulesEtParentheses(v)
				if atome!="":
					atome+=','
				atome +=v
			atome = 'PI('+atome+').\n'
			i+=1
			ficAllRes.write(atome)
		
	ficAllRes.close()
	return i

def flavioCvsIndToDlp():
	nullNb=0
	ficAllRes = open("ind.dlp", "w")
	i=0
	# transformation indinv
	name = "ind.csv"
	fichier = open(name, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	
	for s in lignes :
		if(s!="" and s.find("%") == -1):
			lval = s.split(';')
			v=lval[0]
			v = suppVirgulesEtParentheses(v)
			atome = 'IND('+v+').\n'
			i+=1
			ficAllRes.write(atome)

	ficAllRes.close()
	return i

def transformNullsConstantes(bdUn):		
	nullNb=0
	ficRes = open("bddinit2.dlp", "w")
	fichier = open(bdUn, "r");
	texte = fichier.read()
	fichier.close()
	lignes = texte.split("\n")
	for s in lignes :
		#print(s)
		pos1 = s.find('_')
		pos2 = s.find('New')
		pos3 = s.find('_',pos1+1)
		pos4 = s.find('#')
		cpt=0
		while pos1 != -1 and pos2 != -1 and pos3 !=-1 :
			res = s[:pos1]+'cste'+s[pos1+1:pos2]+s[pos2+3:pos3]+'-'
			if s[pos4-1]=='*':
				res +=s[pos3+1:pos4-1]
			else:
				res +=s[pos3+1:pos4]
			if s[pos4+2]=='R':
				res += s[pos4+3:]
			else:
				res += s[pos4+2:]


			print(s)
			print(res)
			cpt+=1
			if(cpt >5):
				entree = input("Entrez un char")
			s = res
			pos1 = s.find('_')
			pos2 = s.find('New')
			pos3 = s.find('_',pos1+1)
			pos4 = s.find('#')	
				
		ficRes.write(s+"\n")	
	ficRes.close()
