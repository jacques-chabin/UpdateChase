%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% queries and traduction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

first query
?(Xartist, Xmuseum, Xpays) :- Museum(Xmuseum), Author(Xartwork, Xartist), Birthplace(Xartist, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays).


query completed :
?(Xartist, Xmuseum, Xcountry) :- Museum(Xmuseum), Author(Xartwork, Xauthor), Birthplace(Xauthor, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays), Artist(Xauthor), Artwork(Xartwork)

--SQL Hive traduction avec contraintes positives
SELECT m.museum, e.artwork, l.place, c2.country, a.artist, b.place, c1.country
FROM museum m
JOIN exibit e ON (e.museum = m.museum)
JOIN location l ON (l.museum = m.museum)
JOIN country c2 ON (l.place = c2.place)
JOIN author a ON (a.artwork = e.artwork)
JOIN birthplace b ON (b.artist = a.artist)
JOIN country c1 ON (c1.place = b.place)
WHERE c1.country = c1.country;

--SQL Hive traduction avec contraintes positives et une contrainte négative
SELECT m.museum, e.artwork, l.place, c2.country, a.artist, b.place, c1.country
FROM museum m
JOIN exibit e ON (e.museum = m.museum)
JOIN location l ON (l.museum = m.museum)
JOIN country c2 ON (l.place = c2.place)
JOIN author a ON (a.artwork = e.artwork)
JOIN birthplace b ON (b.artist = a.artist)
JOIN country c1 ON (c1.place = b.place)
WHERE c1.country = c1.country 
And a.artist NOT IN (SELECT politician from Politician);

--SQL Hive traduction avec contraintes positives et une contrainte négative
SELECT m.museum, e.artwork, l.place, c2.country, a.artist, b.place, c1.country
FROM Museum m
JOIN Exibit e ON (e.museum = m.museum)
JOIN Location l ON (l.museum = m.museum)
JOIN Country c2 ON (l.place = c2.place)
JOIN Author a ON (a.artwork = e.artwork)
JOIN Birthplace b ON (b.artist = a.artist)
JOIN Country c1 ON (c1.place = b.place)
WHERE c1.country = c1.country 
and a.artist IN (SELECT a2.artist from Author a2 GROUP BY a2.artwork HAVING count(*)=1);

sparql traduction

"SELECT ?author ?birthplace ?artwork ?museum ?ville ?pays WHERE { "
				+ " ?author rdf:type dbo:Artist."
				+ " ?artwork dbo:author ?author."
				+ " ?author dbo:birthPlace ?birthplace."
				+ " ?birthplace dbo:country ?pays."
				+ " ?artwork rdf:type dbo:Artwork."
				+ " ?artwork dbo:museum ?museum."
				+ " ?museum rdf:type dbo:Museum."
				+ " ?museum dbo:location ?ville."
				+ " ?ville dbo:country ?pays."
				+ " }";


// Exécution sous eclipse

Key Constraints [[Author(Xp, Xa), Author(Yp, Xa)] -> equal(Xp, Yp), [Exibit(Xa, Xm), Exibit(Xa, Ym)] -> equal(Xm, Ym)]
Positives Constraints [[Author(X, Y)] -> Artist(Y), [Author(X, Y)] -> Artwork(X)]
Negatives Constraints [[Artist(X), Politician(X)] -> !, [Artist(X), Scientist(X)] -> !]
Queries : [[Museum(Xmuseum), Author(Xartwork, Xartist), Birthplace(Xartist, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays)] -> ?(Xartist, Xmuseum, Xpays)]
Inference Rules [[Painter(X)] -> Artist(X)]
Schema : [Painting(painting), Artwork(artwork), Painter(painter), Actor(actor), Photographer(photographer), Artist(artist), Person(person), Museum(museum), EducationalInstitution(educationalInstitution), Place(place), MuseumType(museumType), PaintingType(paintingType), Date(date), BirthDate(birthDate), DeathDate(deathDate), CompletionDate(completionDate), PaintingTechnique(paintingTechnique), Scientist(scientist), Politician(politician), LocatedIn(place, country), HasType(museum, museumType), HasNationality(person, country), HasGraduated(person, educationalInstitution), WasBornIn(person, birthDate), WasBornWhere(person, place), DiedIn(person, deathDate), CreatedIn(artwork, completionDate), HasTechnique(painting, paintingTechnique), ExhibitAt(artwork, museum), HasArtwork(artist, artwork), madeBy(artwork, artist), Author(artwork, artist), Birthplace(person, place), Country(place, country), Exibit(artwork, museum), Location(museum, place)]
SchemaSparql : [Museum(dbo:Museum), Author(dbo:author), Birthplace(dbo:birthPlace), Country(dbo:country), Exibit(dbo:museum), Location(dbo:location), Artist(dbo:Artist), Artwork(dbo:Artwork), Politician(dbo:Politician), Scientist(dbo:Scientist)]
query before algo1 : [Museum(Xmuseum), Author(Xartwork, Xartist), Birthplace(Xartist, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays)] -> ?(Xartist, Xmuseum, Xpays)
query after algo1 : [Museum(Xmuseum), Author(Xartwork, Xartist), Birthplace(Xartist, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays), Artist(Xartist), Artwork(Xartwork)] -> ?(Xartist, Xmuseum, Xpays)
No problem with negative constraints
Sparql Query to answer : SELECT ?Xartist ?Xmuseum ?Xpays ?Xartwork ?Xplace ?Xville  WHERE { ?Xmuseum rdf:type dbo:Museum. ?Xartwork dbo:author ?Xartist. ?Xartist dbo:birthPlace ?Xplace. ?Xplace dbo:country ?Xpays. ?Xartwork dbo:museum ?Xmuseum. ?Xmuseum dbo:location ?Xville. ?Xville dbo:country ?Xpays. ?Xartist rdf:type dbo:Artist. ?Xartwork rdf:type dbo:Artwork. }
Done.
reponse requete effectuée en: 3.223 secondes. essais1
reponse requete effectuée en: 3.1 secondes. essais2
reponse requete effectuée en: 1.53 secondes. essais3
reponse requete effectuée en: 1.551 secondes. essais4
reponse requete effectuée en: 1.51 secondes. essais5
reponse requete effectuée en: 1.614 secondes. essais6

Number : 556 answers of the query to validade with algo2 
validation de 1878 requetes effectuée en: 192.987 secondes. essais1
validation de 1878 requetes effectuée en: 80.476 secondes.  essais2
validation de 1878 requetes effectuée en: 84.846 secondes. essais3
validation de 1878 requetes effectuée en: 77.484 secondes. essais4
validation de 1878 requetes effectuée en: 71.202 secondes. essais5
validation de 1878 requetes effectuée en: 72.96 secondes. essais6

Number of respons : 68

 Validate respons for query [Museum(Xmuseum), Author(Xartwork, Xartist), Birthplace(Xartist, Xplace), Country(Xplace, Xpays), Exibit(Xartwork, Xmuseum), Location(Xmuseum, Xville), Country(Xville, Xpays), Artist(Xartist), Artwork(Xartwork)] -> ?(Xartist, Xmuseum, Xpays) 
 are 
?(http://dbpedia.org/resource/Brose_Partington, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Brose_Partington, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Laura_Molina_(artist), http://dbpedia.org/resource/National_Museum_of_Mexican_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Laura_Molina_(artist), http://dbpedia.org/resource/National_Museum_of_Mexican_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Jan_Mostaert, http://dbpedia.org/resource/Rijksmuseum, http://dbpedia.org/resource/Netherlands)
?(http://dbpedia.org/resource/Lippo_Memmi, http://dbpedia.org/resource/Uffizi, http://dbpedia.org/resource/Italy)
?(http://dbpedia.org/resource/Jeffrey_Smart, http://dbpedia.org/resource/National_Gallery_of_Victoria, http://dbpedia.org/resource/Australia)
?(http://dbpedia.org/resource/Jeffrey_Smart, http://dbpedia.org/resource/National_Gallery_of_Victoria, http://dbpedia.org/resource/Australia)
?(http://dbpedia.org/resource/William_Eggleston, http://dbpedia.org/resource/Getty_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/William_Eggleston, http://dbpedia.org/resource/Getty_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Hilda_Rix_Nicholas, http://dbpedia.org/resource/National_Gallery_of_Australia, http://dbpedia.org/resource/Australia)
?(http://dbpedia.org/resource/Hilda_Rix_Nicholas, http://dbpedia.org/resource/National_Gallery_of_Australia, http://dbpedia.org/resource/Australia)
?(http://dbpedia.org/resource/Nicolai_Abildgaard, http://dbpedia.org/resource/National_Gallery_of_Denmark, http://dbpedia.org/resource/Denmark)
?(http://dbpedia.org/resource/George_Luks, http://dbpedia.org/resource/Museum_of_Fine_Arts,_Boston, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/George_Luks, http://dbpedia.org/resource/Museum_of_Fine_Arts,_Boston, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Joaquin_Mir_Trinxet, http://dbpedia.org/resource/Museu_Nacional_d'Art_de_Catalunya, http://dbpedia.org/resource/Spain)
?(http://dbpedia.org/resource/Truman_Lowe, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Truman_Lowe, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Truman_Lowe, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Truman_Lowe, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Christopher_R._W._Nevinson, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Christopher_R._W._Nevinson, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Christopher_R._W._Nevinson, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Christopher_R._W._Nevinson, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Charles_Demuth, http://dbpedia.org/resource/Metropolitan_Museum_of_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Charles_Demuth, http://dbpedia.org/resource/Metropolitan_Museum_of_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Robert_Stackhouse, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Robert_Stackhouse, http://dbpedia.org/resource/Indianapolis_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Jeff_Koons, http://dbpedia.org/resource/Hirshhorn_Museum_and_Sculpture_Garden, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Jeff_Koons, http://dbpedia.org/resource/Hirshhorn_Museum_and_Sculpture_Garden, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Władysław_Podkowiński, http://dbpedia.org/resource/National_Museum,_Kraków, http://dbpedia.org/resource/Poland)
?(http://dbpedia.org/resource/Johannes_Cornelisz_Verspronck, http://dbpedia.org/resource/Rijksmuseum, http://dbpedia.org/resource/Netherlands)
?(http://dbpedia.org/resource/Henry_Bond, http://dbpedia.org/resource/Tate_Modern, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Henry_Bond, http://dbpedia.org/resource/Tate_Modern, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Richard_Artschwager, http://dbpedia.org/resource/Art_Institute_of_Chicago, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Richard_Artschwager, http://dbpedia.org/resource/Art_Institute_of_Chicago, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Laura_Knight, http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Arthur_Streeton, http://dbpedia.org/resource/National_Gallery_of_Australia, http://dbpedia.org/resource/Australia)
?(http://dbpedia.org/resource/John_Quidor, http://dbpedia.org/resource/Smithsonian_American_Art_Museum, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/John_Quidor, http://dbpedia.org/resource/Smithsonian_American_Art_Museum, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Francesco_Hayez, http://dbpedia.org/resource/Pinacoteca_di_Brera, http://dbpedia.org/resource/Italy)
?(http://dbpedia.org/resource/Nicolaas_Pieneman, http://dbpedia.org/resource/Rijksmuseum, http://dbpedia.org/resource/Netherlands)
?(http://dbpedia.org/resource/Paja_Jovanović, http://dbpedia.org/resource/National_Museum_of_Serbia, http://dbpedia.org/resource/Serbia)
?(http://dbpedia.org/resource/Paulus_Potter, http://dbpedia.org/resource/Mauritshuis, http://dbpedia.org/resource/Netherlands)
?(http://dbpedia.org/resource/George_Caleb_Bingham, http://dbpedia.org/resource/Metropolitan_Museum_of_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/George_Caleb_Bingham, http://dbpedia.org/resource/Metropolitan_Museum_of_Art, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Kay_Sage, http://dbpedia.org/resource/Walker_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Kay_Sage, http://dbpedia.org/resource/Walker_Art_Center, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Asher_Brown_Durand, http://dbpedia.org/resource/Walters_Art_Museum, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Asher_Brown_Durand, http://dbpedia.org/resource/Walters_Art_Museum, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Asher_Brown_Durand, http://dbpedia.org/resource/Walters_Art_Museum, http://dbpedia.org/resource/United_States)
?(http://dbpedia.org/resource/Mikhail_Nesterov, http://dbpedia.org/resource/Tretyakov_Gallery, http://dbpedia.org/resource/Russia)
?(http://dbpedia.org/resource/Adam_Elsheimer, http://dbpedia.org/resource/Alte_Pinakothek, http://dbpedia.org/resource/Germany)
?(http://dbpedia.org/resource/Julio_González_(sculptor), http://dbpedia.org/resource/Museu_Nacional_d'Art_de_Catalunya, http://dbpedia.org/resource/Spain)
?(http://dbpedia.org/resource/Paul_Nash_(artist), http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Paul_Nash_(artist), http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Paul_Nash_(artist), http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Paul_Nash_(artist), http://dbpedia.org/resource/Imperial_War_Museum, http://dbpedia.org/resource/United_Kingdom)
?(http://dbpedia.org/resource/Gerard_ter_Borch, http://dbpedia.org/resource/Rijksmuseum, http://dbpedia.org/resource/Netherlands)
?(http://dbpedia.org/resource/Ivan_Shishkin, http://dbpedia.org/resource/Tretyakov_Gallery, http://dbpedia.org/resource/Russia)
?(http://dbpedia.org/resource/Georg_Friedrich_Kersting, http://dbpedia.org/resource/Alte_Nationalgalerie, http://dbpedia.org/resource/Germany)
Number of respons : 68
