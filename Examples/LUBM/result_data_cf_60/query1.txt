====== DATALOG ======
?(Xpubli, Xauthor1, Xauthor2) :- Publication(Xpubli), 
publicationAuthor(Xpubli, Xauthor1), publicationAuthor(Xpubli,Xauthor2),
FullProfessor(Xauthor1), GraduateStudent(Xauthor2), 
worksFor(Xauthor1, Xorg), memberOf(Xauthor2, Xorg).


====== SPARQL ======
SELECT ?Xpubli ?Xauthor1 ?Xauthor2 ?Xorg  
FROM <lubm>
WHERE { 
?Xpubli rdf:type ub:Publication. 

?Xpubli ub:publicationAuthor ?Xauthor1. 
?Xpubli ub:publicationAuthor ?Xauthor2. 

?Xauthor1 rdf:type ub:FullProfessor. 
?Xauthor2 rdf:type ub:GraduateStudent. 

?Xauthor1 ub:worksFor ?Xorg. 
?Xauthor2 ub:memberOf ?Xorg. 
} 

--> 234559 results

===== HiveQL ======
SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization  
FROM Publication p
JOIN publicationAuthor pa1 ON (p.idPublication = pa1.publication)
JOIN publicationAuthor pa2 ON (p.idPublication = pa2.publication)

JOIN FullProfessor fprof ON (pa1.person = fprof.idFullProfessor)
JOIN GraduateStudent gstu ON (pa2.person = gstu.idGraduateStudent)

JOIN worksFor w ON (pa1.person = w.person)
JOIN memberOf m ON (pa2.person = m.person)

WHERE w.organization=m.organization


===== Tables ======
FullProfessor(idFullProfessor)
GraduateStudent(idGraduateStudent)
Publication(idPublication)
publicationAuthor(publication,person)
memberOf(person,organization)
worksFor(person,organization)
