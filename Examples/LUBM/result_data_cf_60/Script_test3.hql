

Drop table execution_time_rep;
CREATE TABLE IF NOT EXISTS execution_time_rep (out_table_result STRING, starttime timestamp, nbrep int);

-- Test1 validation des 46 réponses 35 requêtes à évaluer

set out_table_result=result_T1_Q2_1;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime)  select 'result_T1_Q2_1',from_unixtime(unix_timestamp()) FROM essai; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent10' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_2;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_2',from_unixtime(unix_timestamp()), nbrep FROM result_T1_Q2_1; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent10' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_3;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_3',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_2; 
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor7'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor7/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};


set out_table_result=result_T1_Q2_4;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_4',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_3; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent10'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor7/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_5;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_5',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_4; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor7'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_6;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_6',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_5; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent16'  And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_7;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_7',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_6; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent16' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_8;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_8',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_7; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor6'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor6/Publication12' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_9;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_9',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_8; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent16'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor6/Publication12' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_10;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_10',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_9; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor6'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_11;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_11',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_10; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent25'  And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_12;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_12',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_11; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent25' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_13;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_13',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_12; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_14;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_14',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_13; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_15;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_15',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_14; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_16;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_16',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_15; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication6' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_17;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_17',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_16; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication6' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_18;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_18',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_17; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_19;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_19',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_18; 
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_20;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_20',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_19; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_21;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_21',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_20; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent101' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_22;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_22',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_21; 
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent101' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_23;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_23',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_22; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication11' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_24;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_24',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_23; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent101'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication11' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_25;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_25',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_24; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication5' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_26;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_26',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_25; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent101'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication5' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_27;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_27',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_26; 
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent107' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_28;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_28',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_27; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent107' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_29;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_29',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_28; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_30;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_30',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_29; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent107'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_31;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_31',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_30; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_32;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_32',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_31; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent31' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_33;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_33',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_32; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent31' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_34;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_34',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_33; 

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication15' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_35;
set factor=85;
insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_35',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_34; 
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent31'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication15' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};


insert into  execution_time_rep (out_table_result,starttime,nbrep)  select 'result_T1_Q2_36',from_unixtime(unix_timestamp()) , nbrep FROM result_T1_Q2_35; 
 
DROP table execution_time;
CREATE TABLE IF NOT EXISTS execution_time (out_table_result STRING, starttime timestamp,endtime timeStamp);

 --Test2_1 2pos 0neg 0key taus85
 set out_table_result=result_T2_Q1_85;
 set factor=85;

 insert into  execution_time (out_table_result,starttime)  select 'result_T2_Q1_85',from_unixtime(unix_timestamp()) FROM essai; 
 
 set hive.auto.convert.join=false; 
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor} ;

 insert into  execution_time (out_table_result,endtime)  select 'result_T2_Q1_85',from_unixtime(unix_timestamp()) FROM essai;

 
 --Test2_2 2pos(1tgd) 0neg 0key taus60
 set out_table_result=result_T2_Q1_60;
 set factor=60;
 insert into  execution_time (out_table_result,starttime)  select 'result_T2_Q1_60',from_unixtime(unix_timestamp()) FROM essai; 

 set hive.auto.convert.join=false; 
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor} ;


 
 insert into  execution_time (out_table_result,endtime)  select 'result_T2_Q1_60',from_unixtime(unix_timestamp()) FROM essai;


 --Test3_1 3pos(2tgd) 0neg 0key taus85
 set out_table_result=result_T3_Q1_85;
 set factor=85;
 insert into  execution_time (out_table_result,starttime)  select 'result_T3_Q1_85',from_unixtime(unix_timestamp()) FROM essai; 
 
 set hive.auto.convert.join=false; 

 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor} ;

 insert into  execution_time (out_table_result,endtime)  select 'result_T3_Q1_85',from_unixtime(unix_timestamp()) FROM essai;

 --Test3_2 3pos(2tgd) 0neg 0key taus85
 set out_table_result=result_T3_Q1_60;
 set factor=60;
 insert into  execution_time (out_table_result,starttime)  select 'result_T3_Q1_60',from_unixtime(unix_timestamp()) FROM essai; 

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor} ;

 insert into  execution_time (out_table_result,endtime)  select 'result_T3_Q1_60',from_unixtime(unix_timestamp()) FROM essai;
 

 --Test4_1 3pos(2tgd) 0neg 1key taus85
 set out_table_result=result_T4_Q1_85;
 set factor=85;
 insert into  execution_time (out_table_result,starttime)  select 'result_T4_Q1_85',from_unixtime(unix_timestamp()) FROM essai;  


 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And fprof.idFullProfessor IN (SELECT pa3.person FROM  publicationAuthor_p pa1 GROUP BY pa3.person HAVING Count(*)=1);

 insert into  execution_time (out_table_result,endtime)  select 'result_T4_Q1_85',from_unixtime(unix_timestamp()) FROM essai; 
  


 --Test4_2 3pos(2tgd) 0neg 1key taus60
 set out_table_result=result_T4_Q1_60;
 set factor=60;
 insert into  execution_time (out_table_result,starttime)  select 'result_T4_Q1_60',from_unixtime(unix_timestamp()) FROM essai; 


 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And fprof.idFullProfessor IN(SELECT pa3.person FROM  publicationAuthor_p pa1 GROUP BY pa3.person HAVING Count(*)=1);

 insert into  execution_time (out_table_result,endtime)  select 'result_T4_Q1_60',from_unixtime(unix_timestamp()) FROM essai; 

 --Test4_3 3pos(2tgd) 0neg 1key taus85
 set out_table_result=result_T4b_Q1_85;
 set factor=85;
 insert into execution_time (out_table_result,starttime)  select 'result_T4b_Q1_85',from_unixtime(unix_timestamp()) FROM essai; 

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor};
 
 insert into  execution_time (out_table_result,endtime)  select 'result_T4b_Q1_85',from_unixtime(unix_timestamp()) FROM essai;
 
 

 --Test5_1 3pos(2tgd) 1neg 0key taus85
 set out_table_result=result_T5_Q1_85;
 set factor=85;
 insert into  execution_time (out_table_result,starttime)  select 'result_T5_Q1_85',from_unixtime(unix_timestamp()) FROM essai; 

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And te.course <> tc.course;

 insert into  execution_time (out_table_result,endtime)  select 'result_T5_Q1_85',from_unixtime(unix_timestamp()) FROM essai;


 
 --Test5_2 3pos(2tgd) 1neg 0key taus85
 set out_table_result=result_T5_Q1_60;
 set factor=60;
 insert into  execution_time (out_table_result,starttime)  select 'result_T5_Q1_60',from_unixtime(unix_timestamp()) FROM essai; 

 set hive.auto.convert.join=false;
 
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And te.course <> tc.course;

 insert into  execution_time (out_table_result,endtime)  select 'result_T5_Q1_60',from_unixtime(unix_timestamp()) FROM essai;



 select * from   execution_time_rep order by starttime;

 select * from   execution_time     order by starttime;  
  