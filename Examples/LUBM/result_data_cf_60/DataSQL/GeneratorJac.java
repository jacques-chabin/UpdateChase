import java.util.ArrayList;
import java.io.IOException;
import java.util.Scanner;
import java.io.*;
import java.util.*;

class GeneratorJac{
	
	public static FileWriter createFile(String name){
		FileWriter fw=null;
		try {
			// open the file named name.sql in the current directory
			fw = new FileWriter(name+".sql");
			fw.write("LOCK TABLES `"+name+"` WRITE;\n");
			fw.write("ALTER TABLE `"+name+"` DISABLE KEYS;\n");
		}catch (IOException e3){
			System.out.println("Can't create file "+e3.getMessage());
		}
		return fw;
	}
	public static void instance1File(FileWriter fw, int nbInst, String name){
		Random rand = new Random();
		int nombreAleatoire = rand.nextInt(10 - 5 + 1) + 5;
		try {
			fw.write("INSERT INTO `"+name+"` VALUES ");
			for (int i=0; i<nbInst; ++i){
				fw.write("('"+name+i+"', "+nombreAleatoire*10+"),\n");
				nombreAleatoire = rand.nextInt(10 - 5 + 1) + 5;
			}
			fw.write("('"+name+nbInst+"', "+nombreAleatoire*10+");\n");
		}catch (IOException e3){
			System.out.println("Can't write in file "+e3.getMessage());
		}
	}
	
	
	public static void closeFile(FileWriter fw, String name){
		try {
			fw.write("ALTER TABLE `"+name+"` ENABLE KEYS;\n");
			fw.write("UNLOCK TABLES;\n");
			fw.close();
		}catch (IOException e3){
			System.out.println("Can't close file "+e3.getMessage());
		}
	}
	
	public static void main(String[] args) throws IOException{
		int factor = 1;
		int NbFullProfessor = 100*factor;
		int NbGraduateStudent= 50*factor;
		int NbOrganization = 20*factor;
		int NbResearchAssistant = 20*factor;
		int NbTeachingAssistant = 20*factor;
		
		FileWriter ffp=createFile("FullProfessor");
		instance1File(ffp, NbFullProfessor, "FullProfessor");
		closeFile(ffp, "FullProfessor");
		
		ffp=createFile("GraduateStudent");
		instance1File(ffp, NbGraduateStudent, "GraduateStudent");
		closeFile(ffp, "GraduateStudent");
		
		ffp=createFile("Organization");
		instance1File(ffp, NbOrganization, "Organization");
		closeFile(ffp, "Organization");

		ffp=createFile("ResearchAssistant");
		instance2File(ffp, NbResearchAssistant, "GraduateStudent");
		closeFile(ffp, "ResearchAssistant");
		
		ffp=createFile("TeachingAssistant");
		instance2File(ffp, NbTeachingAssistant, "GraduateStudent");
		closeFile(ffp, "TeachingAssistant");
	
	}
}
