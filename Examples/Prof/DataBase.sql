-- creation de la base de donnees pour tester la sémentique des contraintes
DROP DATABASE IF EXISTS `test`;

CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8;
USE `test`;

drop table if exists Studies;
drop table if exists Teaches;
drop table if exists Prof;
drop table if exists Student;
drop table if exists Course;
drop table if exists ProposedBy;

create table Course(nameCourse varchar(50));
create table Student(nameStudent varchar(50));
create table Prof(nameProf varchar(50));
				  
create table Teaches(nameProf varchar(50), nameCourse varchar(50));
create table Studies(nameStudent varchar(50), nameCourse varchar(50));
create table ProposedBy(nameCourse varchar(50), nameDept varchar(50));

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */


insert into Prof values	('Bob');
insert into Student values('Bob');
insert into Course values('BD'), ('Maths'), ('Algo');

insert into Teaches values('Bob', 'BD'), ('Bob', 'Maths');
insert into Studies values('Bob', 'BD'), ('Bob', 'Algo');

/*
insert into Teaches values('Bob', 'BD');
insert into Studies values('Bob', 'BD');
*/

insert into ProposedBy values('BD', 'IUT'), ('Algo', 'COST'), ('Maths', 'Lycee');
