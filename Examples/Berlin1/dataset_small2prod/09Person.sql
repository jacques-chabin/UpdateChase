CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `nr` int(11) primary key,
  `name` varchar(30) character set utf8 collate utf8_bin default NULL,
  `mbox_sha1sum` char(40) character set utf8 collate utf8_bin default NULL,
  `country` char(2) character set utf8 collate utf8_bin default NULL,
  `publisher` int(11),
  `publishDate` date
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `person` WRITE;
ALTER TABLE `person` DISABLE KEYS;

INSERT INTO `person` VALUES (1,'Ruggiero-Delane','fb3efd92e3c7a8d775a895ba476e11a3e8f3fac','US',1,'2008-09-05'),(2,'Eyana-Aurelianus','df1cf8e68d49e5b65f1507dbecec6b61e9dc98','JP',1,'2008-08-07');

ALTER TABLE `person` ENABLE KEYS;
UNLOCK TABLES;