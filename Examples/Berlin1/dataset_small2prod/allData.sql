source 01ProductFeature.sql
source 02ProductType.sql
source 03Producer.sql
source 04Product.sql
source 05ProductTypeProduct.sql
source 06ProductFeatureProduct.sql
source 07Vendor.sql
source 08Offer.sql
source 09Person.sql
source 10Review.sql

drop view if exists isSold;
create view isSold as (select product, vendor from offer);

drop view if exists make;
create view make as (select nr as product, producer from product);

create user 'berlin'@'localhost' identified by 'berlin';
grant select on benchmark.* to berlin;

