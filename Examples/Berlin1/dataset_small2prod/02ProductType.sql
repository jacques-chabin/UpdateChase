CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `producttype`;
CREATE TABLE `producttype` (
  `nr` int(11) primary key,
  `label` varchar(100) character set utf8 collate utf8_bin default NULL,
  `comment` varchar(2000) character set utf8 collate utf8_bin default NULL,
  `parent` int(11),
  `publisher` int(11),
  `publishDate` date
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `producttype` WRITE;
ALTER TABLE `producttype` DISABLE KEYS;

INSERT INTO `producttype` VALUES 
(1,'Thing','The Product Type of all Products',null,1,'2000-07-04'),
(2,'amour dupable','lengthened galling outposts emulatively looming deplanes stinters forehanded interdisciplinary manoeuvred frankers pederasty heralds disrupts fishnet falsifiable absorbable appreciators undefinable draftable revindicates flashlamp insertion expurgations tarsier',1,1,'2000-06-26'),
(3,'microprocessor mops reigned','antecedently nonyielding cocaines milligrams dimorphic shamming clawed hurts enchains jukeboxes mottoes brahminist embrocated quatrefoil harmonize guardants prayerfully adios restring coursing fisticuffs cordlessly augured fumigated academies splotches auctioning timeliness flushest ranching speedily gets nonsymbolic pyorrhea rales headpieces ladrons tightwire playtimes etherizes mistaught screechier casehardening placaters saurian summarization counterfeits skipping',1,1,'2000-07-05'),
(4,'practiced','swarmed adjuratory fondled shiksas pourboire chortled distinctiveness fostered unchastened dehumanizes bulleted wieldy reaccession roentgenoscope minorcas nimbly birdhouses outclasses hazels traumatologies glaucomatous sabotages hexone porkier duplicating internee thermodynamically whereinsoever satirizing postformed fatigueless feasance remove unseats sheers stenographers unperceiving shipworms coshers prelim hispaniola impeaches rafting squelchy probationers countless bluejay decontaminator encipherments wading',2,1,'2000-07-14'),
(5,'arteriography','hollowness unhealed cityward parring wishing pyromaniacs marbly pions boughed innervate gung inverts demoted comprehended stollen unadjudicated septuagenarians chaptered vocalists bennies unstably moppet cogging ethnics billhooks frenching squeamishly tyres weaseling chancroids pandered amirates relented',2,1,'2000-07-18'),
(6,'huffiest tougheners interfering','shucker claviers haggles chorusses nonagons logrolls mathematically intradermal echoed grassier helmeted bristles gonging smidgen flunker atonements bestializes briefness vulgo discounter marooning sheaving neutralizations attains reconsiders glaceed lavers notified malevolently compensations lycanthropies rebroadcasting obliviously sunup arrestee tireless gayer interlinear backlogged encephala chronologist helplessly',3,1,'2000-06-28'),
(7,'papuans womaned','teenaged lesbianism unplanted luminously ladyships optimisms protagonists topknots refracts fraternized maoist decimalization steppingstones greens manpowers chipper cognized eggheads abyssinian purblindness chattiest shutters churl nictating reconciliation distrustfulness solstitial rapidity bronzers gadded calicoes sandals geophysicists twaddler mystifier deploring panache',3,1,'2000-07-07');

ALTER TABLE `producttype` ENABLE KEYS;
UNLOCK TABLES;
