CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `producer`;
CREATE TABLE `producer` (
  `nr` int(11) primary key,
  `label` varchar(100) character set utf8 collate utf8_bin default NULL,
  `comment` varchar(2000) character set utf8 collate utf8_bin default NULL,
  `homepage` varchar(100) character set utf8 collate utf8_bin default NULL,
  `country` char(2) character set utf8 collate utf8_bin default NULL,
  `publisher` int(11),
  `publishDate` date
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `producer` WRITE;
ALTER TABLE `producer` DISABLE KEYS;

INSERT INTO `producer` VALUES (1,'enzymologist neb falsehoods','smashes leavening beauticians novitiates peaks nonhistoric fluorinations seductresses promotions corresponding denuder wispier laboriousness mechanisms skepsis tulips barstools demobs bandmasters pallbearer','http://www.Producer1.com/','DE',1,'2003-06-15');

ALTER TABLE `producer` ENABLE KEYS;
UNLOCK TABLES;