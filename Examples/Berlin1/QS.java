import java.util.ArrayList;
import java.io.IOException;
import java.util.Scanner;

class QS{
	static int verbose = 3;
	public static void main(String[] args) throws IOException{
		
		/* String directory = "Examples/Paper2/";
		String dataBase = "paper";
		String user = "userpaper";
		String mdp="userpaper"; 	*/
		
		
		String directory = "Examples/Berlin1/";
		String dataBase = "benchmark";
		String user = "berlin";
		String mdp = "berlin";
		
		// String directory = "Examples/DBPedia/";
	

		/* String directory = "Examples/Paper/";
		String dataBase = "test";
		String user = "userpaper";
		String mdp = "userpaper"; */
		
		/* initialisation of the sets of constraints */
		LinkedListRules keyConstraintsSet = LinkedListRules.initFromFile(directory+"KeyConstraint");
		System.out.println("Key Constraints " + keyConstraintsSet);
		LinkedListRules posConstraintsSet = LinkedListRules.initFromFile(directory+"PosConstraint");
		System.out.println("Positives Constraints " + posConstraintsSet);
		LinkedListRules negConstraintsSet = LinkedListRules.initFromFile(directory+"NegConstraint");
		System.out.println("Negatives Constraints " + negConstraintsSet);
		
		/* initialisation of inference rules */
		LinkedListRules infRulesSet = LinkedListRules.initFromFile(directory+"InfRule");
		System.out.println("Inference Rules " + infRulesSet);		
		
		/* initialisation of queries */
		LinkedListRules queriesSet = LinkedListRules.initFromFile(directory+"Queries");
		System.out.println("Queries : " + queriesSet);

		/* initialisation of the graph schema liste of predicate(attributs) */
		Schema sch = new Schema();
		sch.initFromFile(directory+"Schema");
		System.out.println("Schema : "+sch);
		
		/* initialisation of the sparql schema liste of predicate(type) */
		/* used for sparql translation */
		Schema schSp = new Schema();
		schSp.initFromFile(directory+"SchemaSparql");
		System.out.println("SchemaSparql : "+schSp);

		/* We consider for instance only the first query */
		Query q1 = (Query)queriesSet.get(0);
		
		/* Query is completed with positive constraints and we check if there is no inconsistance with neg constraints */
		long tempsDebut = System.currentTimeMillis();
		if(verbose>=1) System.out.println("query before algo1 : " + q1);
        int ncpb = q1.algo1(posConstraintsSet, negConstraintsSet);
		if(verbose>=1) System.out.println("query after algo1 : " + q1);		
        if(ncpb != -1){
            System.out.println("STOP  query maps the negative constraint number  " + ncpb);
            return;
        }
        else{
            if(verbose>=1) System.out.println("No problem with negative constraints");
        }         
        long tempsFin1 = System.currentTimeMillis();
        float seconds = (tempsFin1 - tempsDebut) / 1000F;
        System.out.println("tranfo requete effectuée en: "+ Float.toString(seconds) + " secondes.");
 
		/* ans of the query is obtained from datalog file : */
        //LinkedListRules ansQuerySet = LinkedListRules.initFromFile(directory+"AnsQuery");

		/* ans of the query is obtained from MySQL evaluator : */        
        // LinkedListRules ansQuerySet = MySQL.answer(q1, sch, dataBase, user, mdp);
        
		/* ans of the query is obtained from SPARQL end point : */        
        //LinkedListRules ansQuerySet = Sparql.answer(q1, sch);

		/* ans of the query is obtained from Raqueline answer file : */                
        LinkedListRules ansQuerySet = LinkedListRules.initFromAnsSparqlFile(directory+"Raque_Ans_Total", q1);
       
        long tempsFin2 = System.currentTimeMillis();
		seconds = (tempsFin2 - tempsFin1) / 1000F;
        System.out.println("reponse requete effectuée en: "+ Float.toString(seconds) + " secondes.");

		/* Pourquoi la suite : juste voir une traduction vers sparql ??? */
        /* Query qc = q1.completeAllVariables();
        String querySparql = qc.queryToSparql(schSp);
        System.out.println("Sparql Query to answer : " + querySparql); */

        
		if(verbose>=1) System.out.println("Answers of a query used fro algo2 " + ansQuerySet);

        // Algo2 check if constraints are valid over answers
        
        System.out.println("Number of answers after algo1 : "+ansQuerySet.size());
        
        LinkedListRules ansQueryValidSet = new LinkedListRules();
        int cptQV=0;
        int cptQAV=0;

		//Cache queriesChecked = new Cache();
		Cache queriesChecked = Cache.initFromAns2File(directory+"Raque_Ans2");
		
		int nombreEntier;
		System.out.println("Rentrez un nombre positif ou nul :");
		Scanner lectureClavier = new Scanner(System.in);
		nombreEntier = lectureClavier.nextInt();
		
        for(Rule r2: ansQuerySet){
			Query q2 = (Query)r2;
			LinkedListRules newQueries = q2.algo2(negConstraintsSet, keyConstraintsSet, posConstraintsSet);
			if(verbose >=2) System.out.println("\n\n For the answer : " + q2);
			if(verbose >=2) System.out.println("Queries to check  " + newQueries);
			
			boolean ok = true;
			int i = 0;
			Query q3 = null;
			while(i<newQueries.size() && ok){
				q3 = (Query) newQueries.get(i);
				Integer checked = queriesChecked.isInCache(q3);
				// si la requete n'est pas dans le cache
				//Pour ne pas utiliser le cache
				//checked = null;
				if(checked == null){ 
				    cptQV++;
					ok = MySQL.checkBQ(q3, sch, dataBase, user, mdp);

					//ok = Sparql.checkQuery(q3, schSp);
					Integer ans = null;
					if(ok){
						++i;
						ans = 1;
					}
					else{
						ans = -1;
					}
					// on ajoute la requete dans le cache avec sa reponse
					queriesChecked.addInCache(q3, ans);
				}
				else{
					cptQAV++;
					// la requete est dans le cache, on prend la réponse
					if(checked==-1){
						ok = false;
					}
					else{
						ok = true;
						++i;
					}
				}
			}
			
			if(ok){
				if(verbose >=2) System.out.println(" answer is valide \n");
				ansQueryValidSet.add(q2);
			}
			else{
				if(verbose >=2) {
					if(q3.getHead().getName()=="?"){
						System.out.println(" answer is not valide because  " + q3 + " has answers in BDD \n");
					}
					else{
						System.out.println(" answer is not valide because  " + q3 + " has no answers in BDD \n");
					}
				}
			}
		}
		System.out.println("Cache after validation :\n" + queriesChecked);
		long tempsFin3 = System.currentTimeMillis();
		seconds = (tempsFin3 - tempsFin2) / 1000F;
        System.out.println("validation effectuée en: "+ Float.toString(seconds) + " secondes.");
        System.out.println("Number of new queries for validation : " + cptQV);
		System.out.println("Number of queries allready validated " + cptQAV);
		System.out.println("Number of respons : "+ansQueryValidSet.size());
		System.out.println(" Validate respons for query " + q1 + " \n are ");
		//for(Rule rs : ansQueryValidSet){
		//	System.out.println(rs.getHead());
		//}
	}
}
