CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `nr` int(11) primary key,
  `label` varchar(100) character set utf8 collate utf8_bin default NULL,
  `comment` varchar(2000) character set utf8 collate utf8_bin default NULL,
  `homepage` varchar(100) character set utf8 collate utf8_bin default NULL,
  `country` char(2) character set utf8 collate utf8_bin default NULL,
  `publisher` int(11),
  `publishDate` date
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `vendor` WRITE;
ALTER TABLE `vendor` DISABLE KEYS;

INSERT INTO `vendor` VALUES (1,'cambia','smelled fucking spectacularly','http://www.vendor1.com/','GB',1,'2008-05-31'),(2,'commissariats','nonpareil etherification reelected','http://www.vendor2.com/','US',2,'2008-06-18'),(3,'opined rebinding musingly','ghats sargassos doctors','http://www.vendor3.com/','CN',3,'2008-05-29'),(4,'quintuplet','synchronizes drachmae strayers','http://www.vendor4.com/','US',4,'2008-05-09'),(5,'supremacist','regulative waspily impassively','http://www.vendor5.com/','KR',5,'2008-06-03'),(6,'arbours pruners','lifesaving fawny heathy','http://www.vendor6.com/','CN',6,'2008-04-22'),(7,'mixer','cryptically beautifies casehardens','http://www.vendor7.com/','US',7,'2008-05-13'),(8,'finis monadism inscribes','repaying bawlers leadoff','http://www.vendor8.com/','JP',8,'2008-05-26'),(9,'casements clonked knighthood','listening axemen southed','http://www.vendor9.com/','US',9,'2008-06-15'),(10,'plopping medusoid doctrinally','handcuffed propitiously predesignates','http://www.vendor10.com/','RU',10,'2008-05-04'),(11,'tells pasties shapers','millrun humidities fruited','http://www.vendor11.com/','GB',11,'2008-06-01'),(12,'boppers indicts','trochaic convexo conspirer','http://www.vendor12.com/','US',12,'2008-04-08');

ALTER TABLE `vendor` ENABLE KEYS;
UNLOCK TABLES;
