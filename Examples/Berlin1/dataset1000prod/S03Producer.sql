CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `producer`;
CREATE TABLE `producer` (
  `nr` int(11) primary key,
  `label` varchar(100) character set utf8 collate utf8_bin default NULL,
  `comment` varchar(2000) character set utf8 collate utf8_bin default NULL,
  `homepage` varchar(100) character set utf8 collate utf8_bin default NULL,
  `country` char(2) character set utf8 collate utf8_bin default NULL,
  `publisher` int(11),
  `publishDate` date
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `producer` WRITE;
ALTER TABLE `producer` DISABLE KEYS;

INSERT INTO `producer` VALUES (1,'velcro shying enticements','hothouses gnawing warblers','http://www.Producer1.com/','DE',1,'2003-06-15'),(2,'episcopally expectations','sneeringly discernment snot','http://www.Producer2.com/','US',2,'2001-10-12'),(3,'suckled flintlocks','cosmonaut channels patness','http://www.Producer3.com/','KR',3,'2002-08-26'),(4,'redeveloper looper nosily','radioisotopic assimilation voicelessly','http://www.Producer4.com/','AT',4,'2002-07-19'),(5,'antisubmarine','bandana fashionableness interferometry','http://www.Producer5.com/','US',5,'2005-03-25'),(6,'cashmeres prearranges','hunches judgeships engram','http://www.Producer6.com/','US',6,'2002-06-14'),(7,'stript','thatching hothouses foists','http://www.Producer7.com/','US',7,'2004-06-25'),(8,'assignat disrobe','collaterals noctambulist maskable','http://www.Producer8.com/','GB',8,'2005-03-03'),(9,'flams eternise barite','syncopates decreers templars','http://www.Producer9.com/','US',9,'2000-10-21'),(10,'swigger spectrographies sniffily','collaborated nonaligned groundling','http://www.Producer10.com/','US',10,'2005-05-21'),(11,'named','imploded cedars cavitates','http://www.Producer11.com/','US',11,'2001-04-06'),(12,'geodesics poorhouses','prevailers overrode madnesses','http://www.Producer12.com/','US',12,'2001-05-16'),(13,'ethicists crusted upswollen','uncurls qualmy anthologize','http://www.Producer13.com/','FR',13,'2001-09-11'),(14,'baptismally','henhouse operably radiotelemetry','http://www.Producer14.com/','US',14,'2005-01-07'),(15,'whips','hawkshaws diatribes birretta','http://www.Producer15.com/','US',15,'2000-12-22'),(16,'complicator','bicolor declensions profiteering','http://www.Producer16.com/','FR',16,'2003-04-23'),(17,'employability semiserious tanned','banditry precooling dishonest','http://www.Producer17.com/','KR',17,'2004-04-29'),(18,'fuehrer mover','oxidize sequencies nurtures','http://www.Producer18.com/','US',18,'2003-07-16'),(19,'stripped partings townships','motormen bullrings incurrable','http://www.Producer19.com/','GB',19,'2002-03-31'),(20,'vizirs shipwright hairstreak','occultly intermingles untucked','http://www.Producer20.com/','GB',20,'2002-01-29'),(21,'nondenominational','hogfish hefters spawns','http://www.Producer21.com/','FR',21,'2003-01-28'),(22,'vestibular unshakable','cretins undersheriff infuriated','http://www.Producer22.com/','DE',22,'2000-12-17');

ALTER TABLE `producer` ENABLE KEYS;
UNLOCK TABLES;
