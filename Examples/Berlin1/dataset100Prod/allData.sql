source 01ProductFeature.sql
source 02ProductType.sql
source 03Producer.sql
source 04Product.sql
source 05ProductTypeProduct.sql
source 06ProductFeatureProduct.sql
source 07Vendor.sql
source 08Offer.sql
source 09Person.sql
source 10Review.sql

drop view if exists isSold;
create view isSold as (select product, vendor from offer);

drop view if exists make;
create view make as (select nr as product, producer from product);

drop view if exists nameProduct;
create view nameProduct as (select nr as nrProduct, label from product);

drop view if exists offerProduct;
create view offerProduct as (select nr as nrOffer, product from offer);

drop view if exists offerVendor;
create view offerVendor as(select nr as nrOffer, vendor from offer); 

drop view if exists vendorName;
create view vendorName as(select nr as nrVendor, label as nameVendor from vendor);

drop view if exists producerName;
create view producerName as(select nr as nrProducer, label as nameProducer from producer);

drop view if exists vendorCountry;
create view vendorCountry as(select nr as nrVendor, country from vendor);

drop view if exists producerCountry;
create view producerCountry as(select nr as nrProducer, country from producer);

drop view if exists offerVCountry;
create view offerVCountry as (select nrOffer, country from offerVendor, vendorCountry where offerVendor.vendor = vendorCountry.nrVendor);

drop view if exists offerPrCountry;
create view offerPrCountry as (select nrOffer, country from offerProduct, make, producerCountry where offerProduct.product = make.product AND make.producer = producerCountry.nrproducer);


create user 'berlin'@'localhost' identified by 'berlin';
grant select on benchmark.* to 'berlin'@'localhost';

