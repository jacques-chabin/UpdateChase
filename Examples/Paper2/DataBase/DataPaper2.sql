-- creation de la base de donnees issue du papier
CREATE DATABASE IF NOT EXISTS `paper` DEFAULT CHARACTER SET utf8;
USE `paper`;

drop table if exists conf;
drop table if exists journal;
drop table if exists ranking;
drop table if exists univ;
drop table if exists cnrs;
drop table if exists aff;
drop table if exists prod;


create table prod(titre varchar(50),
				  auteur varchar(30),
				  yearp varchar(4),
				  location varchar(50),
				  labo varchar(30));
				  
create table aff(auteur varchar(30),
				 labo varchar(30),
				 yearp varchar(4));
				
create table cnrs(labo varchar(30));

create table univ(labo varchar(30));

create table ranking(location varchar(50),
					 rank varchar(1));
					 
create table journal(location varchar(50));

create table conf(location varchar(50));

source Insert.sql;
source InsertProd.sql;
source InsertAff.sql;

create user 'userpaper'@'localhost' identified by 'userpaper';
grant select on 'paper.*' to 'userpaper'@'localhost';
