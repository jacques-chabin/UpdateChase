-- creation de la base de donnees issue du papier 
drop table if exists conf; 
drop table if exists journal; 
drop table if exists ranking; 
drop table if exists univ; 
drop table if exists cnrs; 
drop table if exists aff; 
drop table if exists prod; 
create table prod(titre varchar(50), auteur varchar(30), yearp varchar(4), location varchar(50), labo varchar(30)); 
create table aff(auteur varchar(30), labo varchar(30), yearp varchar(4)); 
create table cnrs(labo varchar(30)); 
create table univ(labo varchar(30)); 
create table ranking(location varchar(50), rank varchar(1)); 
create table journal(location varchar(50)); 
create table conf(location varchar(50)); 
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% data 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */ 
insert into conf values('Conf0'),('Conf1'),('Conf2'),('Conf3'),('Conf4'),('Conf5'),('Conf6'),('Conf7'),('Conf8'),('Conf9'),('Conf10'),('Conf11'),('Conf12'),('Conf13'),('Conf14'),('Conf15'),('Conf16'),('Conf17'),('Conf18'),('Conf19'),('Conf20'),('Conf21'),('Conf22'),('Conf23'),('Conf24'),('Conf25'),('Conf26'),('Conf27'),('Conf28'),('Conf29'),('Conf30'),('Conf31'),('Conf32'),('Conf33'),('Conf34'),('Conf35'),('Conf36'),('Conf37'),('Conf38'),('Conf39'),('Conf40'),('Conf41'),('Conf42'),('Conf43'),('Conf44'),('Conf45'),('Conf46'),('Conf47'),('Conf48'),('Conf49'),('Journal14'),('Journal17'),('Journal47');
insert into journal values('Journal0'),('Journal1'),('Journal2'),('Journal3'),('Journal4'),('Journal5'),('Journal6'),('Journal7'),('Journal8'),('Journal9'),('Journal10'),('Journal11'),('Journal12'),('Journal13'),('Journal14'),('Journal15'),('Journal16'),('Journal17'),('Journal18'),('Journal19'),('Journal20'),('Journal21'),('Journal22'),('Journal23'),('Journal24'),('Journal25'),('Journal26'),('Journal27'),('Journal28'),('Journal29'),('Journal30'),('Journal31'),('Journal32'),('Journal33'),('Journal34'),('Journal35'),('Journal36'),('Journal37'),('Journal38'),('Journal39'),('Journal40'),('Journal41'),('Journal42'),('Journal43'),('Journal44'),('Journal45'),('Journal46'),('Journal47'),('Journal48'),('Journal49'),('Conf4'),('Conf12'),('Conf40'),('Conf48');
insert into cnrs values('LaboC0'),('LaboC1'),('LaboC2'),('LaboC3'),('LaboC4'),('LaboC5'),('LaboC6'),('LaboC7'),('LaboC8'),('LaboC9'),('LaboC10'),('LaboC11'),('LaboC12'),('LaboC13'),('LaboC14'),('LaboC15'),('LaboC16'),('LaboC17'),('LaboC18'),('LaboC19');
insert into univ values('LaboU0'),('LaboU1'),('LaboU2'),('LaboU3'),('LaboU4'),('LaboU5'),('LaboU6'),('LaboU7'),('LaboU8'),('LaboU9'),('LaboU10'),('LaboU11'),('LaboU12'),('LaboU13'),('LaboU14'),('LaboU15'),('LaboU16'),('LaboU17'),('LaboU18'),('LaboU19'),('LaboC1'),('LaboC14'),('LaboC18');
insert into ranking values('Conf0', 'C'),('Conf1', 'C'),('Conf2', 'A'),('Conf3', 'A')('Conf3', 'B'),('Conf4', 'A'),('Conf5', 'C'),('Conf6', 'B'),('Conf7', 'C'),('Conf8', 'B'),('Conf9', 'A'),('Conf10', 'A'),('Conf11', 'A'),('Conf12', 'A'),('Conf13', 'C'),('Conf14', 'B'),('Conf15', 'A'),('Conf16', 'B'),('Conf17', 'A')('Conf17', 'B'),('Conf18', 'B'),('Conf19', 'B'),('Conf20', 'C'),('Conf21', 'B'),('Conf22', 'B'),('Conf23', 'A'),('Conf24', 'A'),('Conf25', 'A'),('Conf26', 'C'),('Conf27', 'A'),('Conf28', 'C'),('Conf29', 'B'),('Conf30', 'C'),('Conf31', 'A'),('Conf32', 'A'),('Conf33', 'C'),('Conf34', 'C'),('Conf35', 'A')('Conf35', 'B'),('Conf36', 'C')('Conf36', 'A'),('Conf37', 'A'),('Conf38', 'B'),('Conf39', 'C'),('Conf40', 'C'),('Conf41', 'C'),('Conf42', 'C'),('Conf43', 'B'),('Conf44', 'C'),('Conf45', 'A'),('Conf46', 'A'),('Conf47', 'B'),('Conf48', 'A'),('Conf49', 'A'),('Journal0', 'C'),('Journal1', 'A'),('Journal2', 'A'),('Journal3', 'B'),('Journal4', 'C'),('Journal5', 'A'),('Journal6', 'B'),('Journal7', 'C'),('Journal8', 'C'),('Journal9', 'B'),('Journal10', 'A'),('Journal11', 'C'),('Journal12', 'B'),('Journal13', 'A'),('Journal14', 'B'),('Journal15', 'C'),('Journal16', 'A'),('Journal17', 'B'),('Journal18', 'B'),('Journal19', 'C'),('Journal20', 'C'),('Journal21', 'C'),('Journal22', 'A'),('Journal23', 'C'),('Journal24', 'A'),('Journal25', 'C')('Journal25', 'A'),('Journal26', 'B'),('Journal27', 'C'),('Journal28', 'B')('Journal28', 'C'),('Journal29', 'B')('Journal29', 'C'),('Journal30', 'A'),('Journal31', 'C'),('Journal32', 'B'),('Journal33', 'C'),('Journal34', 'B')('Journal34', 'C'),('Journal35', 'B'),('Journal36', 'B'),('Journal37', 'C'),('Journal38', 'C'),('Journal39', 'B')('Journal39', 'C'),('Journal40', 'B'),('Journal41', 'A'),('Journal42', 'B'),('Journal43', 'C'),('Journal44', 'B')('Journal44', 'C'),('Journal45', 'C'),('Journal46', 'B'),('Journal47', 'B'),('Journal48', 'B'),('Journal49', 'B');
