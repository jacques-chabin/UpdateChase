<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>fr.lifo</groupId>
  <artifactId>update-chase</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>Update Chase</name>
  <description>Chase for updating databases</description>
  <url>${env.CI_PROJECT_URL}</url>

  <organization>
    <name>LIFO</name>
    <url>https://www.univ-orleans.fr/lifo/</url>
  </organization>

  <issueManagement>
    <system>gitlab</system>
    <url>${env.CI_PROJECT_URL}</url>
  </issueManagement>

  <ciManagement>
    <system>gitlab</system>
    <url>${env.CI_PIPELINE_URL}</url>
  </ciManagement>

  <scm>
    <url>${env.CI_PROJECT_URL}</url>
    <connection>scm:git:${env.CI_REPOSITORY_URL}</connection>
    <developerConnection>scm:git:${env.CI_REPOSITORY_URL}</developerConnection>
    <tag>HEAD</tag>
  </scm>

  <repositories>
    <repository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
  </repositories>

  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>

    <neo4j-bolt.version>4.4.9</neo4j-bolt.version>
    <neo4j.version>4.0.6</neo4j.version>
    <mysql.version>8.0.31</mysql.version>
    <redis.version>2.5.1</redis.version>

    <junit.version>5.9.0</junit.version>
    <junit.platform.version>1.8.2</junit.platform.version>

    <jmh.version>1.36</jmh.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>com.github.freva</groupId>
      <artifactId>ascii-table</artifactId>
      <version>1.8.0</version>
    </dependency>
    <dependency>
      <groupId>org.neo4j</groupId>
      <artifactId>neo4j</artifactId>
      <version>4.4.15</version>
    </dependency>

    <!-- Database connectors -->
    <dependency>
      <groupId>org.neo4j.driver</groupId>
      <artifactId>neo4j-java-driver</artifactId>
      <version>${neo4j-bolt.version}</version>
    </dependency>
    <dependency>
      <groupId>org.neo4j</groupId>
      <artifactId>neo4j-jdbc-driver</artifactId>
      <version>${neo4j.version}</version>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>${mysql.version}</version>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>com.redislabs</groupId>
      <artifactId>jredisgraph</artifactId>
      <version>${redis.version}</version>
    </dependency>

    <!-- Tests dependency -->
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <version>${junit.version}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.openjdk.jmh</groupId>
      <artifactId>jmh-core</artifactId>
      <version>${jmh.version}</version>
      <!--            <scope>test</scope>-->
    </dependency>
    <dependency>
      <groupId>org.openjdk.jmh</groupId>
      <artifactId>jmh-generator-annprocess</artifactId>
      <version>${jmh.version}</version>
      <!--            <scope>test</scope>-->
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <version>3.4.0</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.3.0</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <version>3.4.2</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <descriptorRefs>
            <descriptorRef>jar-with-dependencies</descriptorRef>
          </descriptorRefs>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>3.2.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <phase>install</phase>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>3.4.1</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
        <version>3.12.1</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>3.4.1</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.2</version>
        <configuration>
          <properties>
            <configurationParameters>
              junit.platform.reporting.open.xml.enabled = true
              junit.platform.reporting.output.dir = target/surefire-reports
            </configurationParameters>
          </properties>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>3.2.0</version>
        <configuration>
          <configLocation>google_checks.xml</configLocation>
        </configuration>
        <executions>
          <execution>
            <id>validate</id>
            <phase>validate</phase>
            <goals>
              <goal>checkstyle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>com.gitlab.smueller18.gitlab</groupId>
        <artifactId>gitlab-codeclimate-maven-plugin</artifactId>
        <version>1.1.0</version>
        <executions>
          <execution>
            <id>validate</id>
            <phase>validate</phase>
            <goals>
              <goal>codeclimate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>com.google.cloud.tools</groupId>
        <artifactId>jib-maven-plugin</artifactId>
        <version>1.0.0</version>
        <configuration>
          <from>
            <image>eclipse-temurin:19-jre</image>
          </from>
          <to>
            <image>${env.CI_REGISTRY_IMAGE}</image>
            <tags>
              <tag>${project.version}</tag>
            </tags>
          </to>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <distributionManagement>
    <repository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </repository>
    <snapshotRepository>
      <id>gitlab-maven</id>
      <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>
</project>