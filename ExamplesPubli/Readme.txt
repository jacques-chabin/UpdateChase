Contains input for execute examples and tests from our paper : Consistent Updating of  Databases with Marked Nulls.

Description of files and how to use it.

BddInit.dlp --> initial set of facts : File -- Open initial database
CompleteBdd.dlp --> complete initial set of fact after chase and core : File -- Open initial database
Constraints.dlp  --> set of constraints : File -- Open set of constraints
InsertFacts.dlp --> set of fact we want to insert in database : File -- Open a set of atomes form insertion
SuppFacts.dlp --> set of fact we want to delete in database : File -- Open a set of atomes form deletion

For tests you need to insert atomes from InsertFacts before to delete atomes from SuppFacts.dlp. 
Bdd and InsertFact are the same for all tests. CompleteBdd is obtained from BddInit where we apply 
chase plus core with all constraints (Constraints.dlp from test7)
