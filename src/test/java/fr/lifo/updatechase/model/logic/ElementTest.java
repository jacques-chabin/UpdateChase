package fr.lifo.updatechase.model.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ElementTest {

  private final Constant constant = new Constant("a");
  private final Variable variable = new Variable("X", Element.VARIABLE);
  private final Variable variableNull = new Variable("N1", Element.NULL);
  private final Variable variableStarredNull = new Variable("N2", Element.STARED_NULL);

  @Test
  void stringToElement() {
    Element element = Element.stringToElement("a");
    assertEquals(new Constant("a"), element);

    element = Element.stringToElement("X");
    assertEquals(new Variable("X"), element);

    element = Element.stringToElement("_N1");
    assertEquals(new Variable("N1", Element.NULL), element);

    element = Element.stringToElement("_N1#3");
    assertEquals(new Variable("N1", Element.NULL, 3), element);

    element = Element.stringToElement("_N1#3R");
    assertEquals(new Variable("N1", Element.NULL, 3, true), element);
  }

  @Test
  void isVariable() {
    assertFalse(constant.isVariable());
    assertTrue(variable.isVariable());
    assertTrue(variableNull.isVariable());
    assertTrue(variableStarredNull.isVariable());
  }

  @Test
  void isConstant() {
    assertTrue(constant.isConstant());
    assertFalse(variable.isConstant());
    assertFalse(variableNull.isConstant());
    assertFalse(variableStarredNull.isConstant());
  }

  @Test
  void isNullValue() {
    assertFalse(constant.isNullValue());
    assertFalse(variable.isNullValue());
    assertTrue(variableNull.isNullValue());
    assertTrue(variableStarredNull.isNullValue());
  }

  @Test
  void isStaredNullValue() {
    assertFalse(constant.isStaredNullValue());
    assertFalse(variable.isStaredNullValue());
    assertFalse(variableNull.isStaredNullValue());
    assertTrue(variableStarredNull.isStaredNullValue());
  }

  @Test
  void equalsModuloNull() {
    Element e1 = new Constant("a");
    Element e2 = new Constant("a");
    assertTrue(e1.equalsModuloNull(e2));

    e1 = new Constant("a");
    e2 = new Constant("b");
    assertFalse(e1.equalsModuloNull(e2));

    e1 = new Constant("a");
    e2 = new Variable("a");
    assertFalse(e1.equalsModuloNull(e2));

    e1 = new Variable("N1", Element.NULL);
    e2 = new Variable("N2", Element.NULL);
    assertTrue(e1.equalsModuloNull(e2));
  }
}