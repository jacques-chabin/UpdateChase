package fr.lifo.updatechase.model.db;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Variable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class BddTest {

  private final static boolean LOG_ENABLE = false;
  private final static boolean STATS_REPORT = false;
  private final static boolean DIFF_REPORT = false;
  private final static boolean RESTRICTED = true;
  private final static int STOP_COND = 0;

  private final static String[] TESTS_DIRECTORIES = {
      //@formatter:off
      "Jac/Joueur",
      "neo4j/Example2_1",
      "neo4j/Example2_2",
      "neo4j/Example3_1",
      "neo4j/Example3_2",
      //"neo4j/Example4", // Loop
      "neo4j/Example4_2",
      "neo4j/Example5",
      "neo4j/Example6_1",
      "neo4j/Example6_2",
      "neo4j/Example7_1",
      "neo4j/Example7_2",
      "neo4j/Example7_3",
      "neo4j/Example8",
      "neo4j/Example10_1",
      "neo4j/Example10_2",
      "neo4j/Example10_3",
      "neo4j/Example11",
      "neo4j/Example12_1",
      "neo4j/Example12_2",
      "neo4j/Example14",
      "neo4j/Example15",
      "neo4j/Example17",
      "neo4j/ExampleJacN",
      "neo4j/ExampleJacN2",
      //"ExamplesPubli/Tests/neo4j/neo4j",
      "testChase/simpleChase",
      "testChase/nullChase",
      "testChase/multiNullChase",
      "testChase/deleteChase",
      "testChase/deleteChase2",
      "testChase/test1",
      "testChase/test2", // Use STOP_COND=0
      "paper/selfSimplification",
      //"broad-deletion"  // Not sure about this one
      //@formatter:on
  };

  public final static String ABORT_FILE = "abort.txt";
  public final static String INIT_FILE = "BddInit.dlp";
  public final static String RULE_FILE = "Constraints.dlp";
  public final static String SCHEMA_FILE = "Schema.dlp";
  public final static String INSERT_FILE = "InsertFacts.dlp";
  public final static String DELETE_FILE = "SuppFacts.dlp";
  public final static String INSERT_RESULT_FILE = "InsertResult.dlp";
  public final static String DELETE_RESULT_FILE = "SuppResult.dlp";

  private final Element constA = new Constant("a");
  private final Element constB = new Constant("b");
  private final Element null1 = new Variable("N1", Element.NULL);
  private final Element null2 = new Variable("N2", Element.NULL, 3);

  private final Atom atom1 = new Atom("P1", new Element[]{null1, constA});
  private final Atom atom2 = new Atom("P1", new Element[]{null1, constB});
  private final Atom atom3 = new Atom("P2", new Element[]{constA, null2});

  private Bdd bdd;

  /**
   * Init a database connection
   *
   * @return a bdd or null if can't connect to the database
   */
  protected abstract Bdd getBdd();

  @BeforeAll
  public void beforeTests() {
    this.bdd = this.getBdd();
    assumeTrue(this.bdd.isConnected(), "No connection to database");
    Logger.getLogger("bdd").setLevel(Level.OFF);
  }

  @AfterAll
  public void afterTests() {
    Logger.getLogger("bdd").setLevel(Level.INFO);
  }

  @BeforeEach
  public void setUp() {
    this.bdd.initFromDB(List.of(atom1, atom2, atom3));
  }

  private static void log(String message) {
    if (LOG_ENABLE) {
      System.out.println(message);
    }
  }

  private static void logTime(String message, long start, long end) {
    log(String.format("%s (%fs)", message, (end - start) / 1000f));
  }

  private void printDiff(Bdd other) {
    if (DIFF_REPORT) {
      System.out.println("\nDiff report:");
      this.bdd.printDiff(other);
    }
  }

  private void printStats(BddStats stats) {
    if (STATS_REPORT) {
      System.out.println("\nStats report:");
      System.out.println(stats.getSummary());
    }
  }

  private LinkedListRules init_tests(File testDirectory, List<String> fileNames)
      throws IOException, SyntaxError {
    long startTime, endTime;
    Bdd localDB = null;
    File file;

    log("Start test");

    // Clear database
    this.bdd.clear();
    log("Database cleared");

    // Init database
    if (fileNames.contains(INIT_FILE)) {
      file = new File(testDirectory, INIT_FILE);
      localDB = BddMemoire.fromFile(file.getAbsolutePath());
      log("File loaded");
    }

    if (localDB != null) {
      startTime = System.currentTimeMillis();
      this.bdd.initFromDB(localDB);
      endTime = System.currentTimeMillis();
      logTime("INIT", startTime, endTime);
    }

    // Init rules
    LinkedListRules rules;
    if (fileNames.contains(RULE_FILE)) {
      file = new File(testDirectory, RULE_FILE);
      rules = LinkedListRules.fromFile(file.getAbsolutePath());
    } else {
      rules = new LinkedListRules();
    }
    log(rules.size() + " rules loaded");

    return rules;
  }

  private void check_test_results(File testDirectory, String fileName)
      throws IOException, SyntaxError {
    File file = new File(testDirectory, fileName);
    Bdd localDB = BddMemoire.fromFile(file.getAbsolutePath());
    this.printDiff(localDB);
    assertEquals(localDB, this.bdd);
  }

  private static Stream<String> getTestBase() {
    ClassLoader classLoader = BddTest.class.getClassLoader();

    return Arrays.stream(TESTS_DIRECTORIES).map((dir) -> {
      URL url = classLoader.getResource(dir);
      return new File(url != null ? url.getFile() : dir).getAbsolutePath();
    });
  }

  @ParameterizedTest
  @MethodSource("getTestBase")
  public void addWithConstraints(File testDirectory) throws IOException, SyntaxError {
    assumeTrue(testDirectory.list() != null, "Path not exist");

    List<String> fileNames = Arrays.asList(Objects.requireNonNull(testDirectory.list()));
    assumeFalse(fileNames.contains(ABORT_FILE), "Skip test because of 'abort.txt'");
    assumeTrue(fileNames.contains(INSERT_FILE) && fileNames.contains(INSERT_RESULT_FILE),
        "Skip test because it's not an insertion test");

    LinkedListRules rules = this.init_tests(testDirectory, fileNames);

    // Insert test
    File file = new File(testDirectory, INSERT_FILE);
    Bdd localDB = BddMemoire.fromFile(file.getAbsolutePath());
    log("Initial size: " + this.bdd.size());
    log("Insert size: " + localDB.size());

    long startTime = System.currentTimeMillis();
    BddStats stats = this.bdd.addWithConstraints(localDB, rules, STOP_COND, new BddMemoire(),
        RESTRICTED);
    long endTime = System.currentTimeMillis();

    logTime("INSERT", startTime, endTime);
    log("Final size: " + this.bdd.size());

    // Check result
    this.check_test_results(testDirectory, INSERT_RESULT_FILE);
    this.printStats(stats);
  }

  @ParameterizedTest
  @MethodSource("getTestBase")
  public void removeWithConstraints(File testDirectory) throws IOException, SyntaxError {
    assumeTrue(testDirectory.list() != null, "Path not exist");

    List<String> fileNames = Arrays.asList(Objects.requireNonNull(testDirectory.list()));
    assumeFalse(fileNames.contains(ABORT_FILE), "Skip test because of 'abort.txt'");
    assumeTrue(fileNames.contains(DELETE_FILE) && fileNames.contains(DELETE_RESULT_FILE),
        "Skip test because it's not a deletion test");

    LinkedListRules rules = this.init_tests(testDirectory, fileNames);

    // Delete test
    File file = new File(testDirectory, DELETE_FILE);
    Bdd localDB = BddMemoire.fromFile(file.getAbsolutePath());
    log("Initial size: " + this.bdd.size());
    log("Delete size: " + localDB.size());

    long startTime = System.currentTimeMillis();
    BddStats stats = this.bdd.removeWithConstraints(localDB, rules, STOP_COND, RESTRICTED);
    long endTime = System.currentTimeMillis();
    logTime("DELETE", startTime, endTime);
    log("Final size: " + this.bdd.size());

    // Check result
    this.check_test_results(testDirectory, DELETE_RESULT_FILE);
    this.printStats(stats);
  }

  @Test
  public void initFromDB() {
    assertEquals(bdd, this.bdd);
  }

  @Test
  public void add() throws SyntaxError {
    Atom atom = new Atom("P2", new Element[]{new Constant("a"), new Variable("N3", Element.NULL)});

    this.bdd.add(atom);
    assertEquals(Set.of(atom1, atom2, atom3, atom), Set.copyOf(this.bdd));
  }

  @Test
  public void remove() {
    this.bdd.remove(atom1);
    assertEquals(Set.of(atom2, atom3), Set.copyOf(this.bdd));
  }

  @Test
  public void getNulls() {
    assertEquals(Set.of(null1, null2), bdd.getNulls());
  }

  @Test
  public void getNbNulls() {
    assertEquals(2, bdd.getNbNulls());
  }

  @Test
  public void getNbConst() {
    assertEquals(2, bdd.getNbConst());
  }

  @Test
  public void getAllPredicates() {
    assertEquals(Set.of("P1", "P2"), Set.copyOf(bdd.getAllPredicates()));
  }

  @Test
  public void getAtomsWithPredicate() {
    assertEquals(Set.of(atom1, atom2), Set.copyOf(bdd.getAtomsWithPredicate("P1")));
    assertEquals(Set.of(atom3), Set.copyOf(bdd.getAtomsWithPredicate("P2")));
  }

  @Test
  public void getMaxProf() {
    assertEquals(3, bdd.getMaxProf());
  }

  @Test
  public void containsPredicate() {
    assertTrue(bdd.containsPredicate("P1"));
    assertTrue(bdd.containsPredicate("P2"));
    assertFalse(bdd.containsPredicate("P3"));
  }

  @Test
  public void setNullsDegree() {
    this.bdd.setNullsDegree(List.of(null2), 0);
    assertEquals(0, this.bdd.getMaxProf());
  }

  @Test
  public void getNullBucket() {
    Bdd atoms = new BddMemoire(List.of(atom1));
    assertEquals(Set.of(null1), this.bdd.getNullBucket(atoms));
  }

  @Test
  public void getNullBucket_multiAtomsSamePartition() {
    Bdd atoms = new BddMemoire(List.of(atom1, atom2));
    assertEquals(Set.of(null1), this.bdd.getNullBucket(atoms));
  }

  @Test
  public void getNullBucket_multiAtomsDifferentPartition() {
    Bdd atoms = new BddMemoire(List.of(atom1, atom3));
    assertEquals(Set.of(null1, null2), this.bdd.getNullBucket(atoms));
  }

  @Test
  public void getNullBucket_lessSpecific() {
    Atom atom = new Atom("P2", new Element[]{null1, null2});
    this.bdd.add(atom);
    assertEquals(Set.of(null1, null2), this.bdd.getNullBucket(new BddMemoire(List.of(atom))));

  }

  @Test
  public void getNullBucket_nullCompatible() {
    Element null3 = new Variable("N3", Element.NULL);
    Atom atom = new Atom("P1", new Element[]{null3, constB});
    this.bdd.add(atom);
    assertEquals(Set.of(null1, null3), this.bdd.getNullBucket(new BddMemoire(List.of(atom))));
  }

  @Test
  public void getNullBucket_constCompatible() {
    Atom atom = new Atom("P1", new Element[]{constA, constB});
    this.bdd.add(atom);
    assertEquals(Set.of(null1), this.bdd.getNullBucket(new BddMemoire(List.of(atom))));
  }

  @Test
  public void getPartitions() {
    Map<Set<Element>, Set<Atom>> partitions = this.bdd.getPartitions(List.of(null1, null2));
    assertEquals(2, partitions.size());
    assertEquals(Set.of(atom1, atom2), partitions.get(Set.of(null1)));
    assertEquals(Set.of(atom3), partitions.get(Set.of(null2)));
  }

  @Test
  public void getPartitions_extendPartition() {
    Atom atom = new Atom("P2", new Element[]{null1, constB});
    this.bdd.add(atom);

    Map<Set<Element>, Set<Atom>> partitions = this.bdd.getPartitions(List.of(null1, null2));
    assertEquals(2, partitions.size());
    assertEquals(Set.of(atom1, atom2, atom), partitions.get(Set.of(null1)));
    assertEquals(Set.of(atom3), partitions.get(Set.of(null2)));
  }

  @Test
  public void getPartitions_joinedPartition() {
    Atom atom = new Atom("P2", new Element[]{null1, null2});
    this.bdd.add(atom);

    Map<Set<Element>, Set<Atom>> partitions = this.bdd.getPartitions(List.of(null1, null2));
    assertEquals(1, partitions.size());
    assertEquals(Set.of(atom1, atom2, atom3, atom), partitions.get(Set.of(null1, null2)));
  }

  @Test
  public void chaseInsert_noRules() {
    LinkedListRules rules = new LinkedListRules();

    this.bdd.chaseForInsertion(List.of(atom1), rules, 10, false, new BddStats());
    assertEquals(Set.of(atom1, atom2, atom3), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseInsert_oneRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constA}),
            new Atom("P1", new Element[]{new Variable("X"), constB})
        )
    ));
    //@formatter:on

    this.bdd.chaseForInsertion(List.of(atom1), rules, 10, false, new BddStats());

    Atom newAtom = new Atom("P3", new Element[]{null1});
    assertEquals(Set.of(atom1, atom2, atom3, newAtom), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseInsert_sequentialRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constA})
        )
    ));
    rules.add(new Rule(
        new Atom("P4", new Element[]{new Variable("X")}),
        Set.of(
            new Atom("P3", new Element[]{new Variable("X")}),
            new Atom("P1", new Element[]{new Variable("X"), constB})
        )
    ));
    //@formatter:on

    this.bdd.chaseForInsertion(List.of(atom1), rules, 10, false, new BddStats());

    Atom newAtom1 = new Atom("P3", new Element[]{null1});
    Atom newAtom2 = new Atom("P4", new Element[]{null1});
    assertEquals(Set.of(atom1, atom2, atom3, newAtom1, newAtom2), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseInsert_parallelRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constA})
        )
    ));
    rules.add(new Rule(
        new Atom("P4", new Element[]{new Variable("X")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constB})
        )
    ));
    //@formatter:on

    this.bdd.chaseForInsertion(List.of(atom1, atom2), rules, 10, false, new BddStats());

    Atom newAtom1 = new Atom("P3", new Element[]{null1});
    Atom newAtom2 = new Atom("P4", new Element[]{null1});
    assertEquals(Set.of(atom1, atom2, atom3, newAtom1, newAtom2), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseInsert_cyclicRules() {
    assumeFalse(this.bdd instanceof BddMemoire, "Cyclic rules does not works with bdd memoire");

    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X1"), new Variable("X3")}),
        Set.of(
            new Atom("P2", new Element[]{new Variable("X1"), new Variable("X2")})
        )
    ));
    rules.add(new Rule(
        new Atom("P2", new Element[]{new Variable("X1"), new Variable("X2")}),
        Set.of(
            new Atom("P3", new Element[]{new Variable("X1"), new Variable("X3")})
        )
    ));
    //@formatter:on

    this.bdd.chaseForInsertion(List.of(atom3), rules, 10, false, new BddStats());
    assertEquals(4, this.bdd.size());
  }

  @Test
  public void chaseDelete_noRules() {
    LinkedListRules rules = new LinkedListRules();

    this.bdd.remove(atom2);
    this.bdd.chaseForDeletion(List.of(atom2), rules, 10, false, new BddStats());
    assertEquals(Set.of(atom1, atom3), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseDelete_oneRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P1", new Element[]{new Variable("X"), constB}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constA})
        )
    ));
    //@formatter:on

    this.bdd.remove(atom2);
    this.bdd.chaseForDeletion(List.of(atom2), rules, 10, false, new BddStats());

    assertEquals(Set.of(atom3), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseDelete_sequentialRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P1", new Element[]{new Variable("X"), constA}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X"), constB})
        )
    ));
    rules.add(new Rule(
        new Atom("P2", new Element[]{constA, new Variable("X2")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X1"), constA})
        )
    ));
    //@formatter:on

    this.bdd.remove(atom3);
    this.bdd.chaseForDeletion(List.of(atom3), rules, 10, false, new BddStats());

    assertEquals(Set.of(), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseDelete_parallelRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P2", new Element[]{constA, new Variable("X2")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X1"), constB})
        )
    ));
    rules.add(new Rule(
        new Atom("P2", new Element[]{constA, new Variable("X2")}),
        Set.of(
            new Atom("P1", new Element[]{new Variable("X1"), constA})
        )
    ));
    //@formatter:on

    this.bdd.remove(atom3);
    this.bdd.chaseForDeletion(List.of(atom3), rules, 10, false, new BddStats());

    assertEquals(Set.of(), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseDelete_cyclicRules() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X1"), new Variable("X3")}),
        Set.of(
            new Atom("P2", new Element[]{new Variable("X1"), new Variable("X2")})
        )
    ));
    rules.add(new Rule(
        new Atom("P2", new Element[]{new Variable("X1"), new Variable("X2")}),
        Set.of(
            new Atom("P3", new Element[]{new Variable("X1"), new Variable("X3")})
        )
    ));
    //@formatter:on

    Atom deletedAtom = new Atom("P3", new Element[]{constA, null1});
    this.bdd.chaseForDeletion(List.of(deletedAtom), rules, 10, false, new BddStats());

    assertEquals(Set.of(atom1, atom2), Set.copyOf(this.bdd));
  }

  @Test
  public void chaseDelete_broadDeletion() {
    LinkedListRules rules = new LinkedListRules();
    //@formatter:off
    rules.add(new Rule(
        new Atom("P3", new Element[]{new Variable("X1"), new Variable("X3")}),
        Set.of(
            new Atom("P2", new Element[]{new Variable("X1"), new Variable("X2")})
        )
    ));
    //@formatter:on

    Atom ruleValidator = new Atom("P3", new Element[]{constA, constA});
    Atom deletedAtom = new Atom("P3", new Element[]{constA, constB});

    this.bdd.add(ruleValidator);
    this.bdd.chaseForDeletion(List.of(deletedAtom), rules, 10, false, new BddStats());

    assertEquals(Set.of(atom1, atom2, atom3, ruleValidator), Set.copyOf(this.bdd));
  }
}
