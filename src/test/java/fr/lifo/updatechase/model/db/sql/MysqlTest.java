package fr.lifo.updatechase.model.db.sql;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;

public class MysqlTest extends BddTest {

  private final static String DB_HOST = "mysql";
  private final static int DB_PORT = 3306;

  private final static String DB_NAME = "test_database";
  private final static String DB_USER = "test_user";
  private final static String DB_PASS = "test_pass";

  @Override
  protected Bdd getBdd() {
    return new BddMySQL(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
  }

}
