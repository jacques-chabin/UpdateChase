package fr.lifo.updatechase.model.db.mem;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;

public class MemoireTest extends BddTest {

  @Override
  protected Bdd getBdd() {
    return new BddMemoire();
  }

}
