package fr.lifo.updatechase.model.db.cypher.neo;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.cypher.BddNeo4j;
import fr.lifo.updatechase.model.db.cypher.query.Neo4JQueryBuilderInterface;

public abstract class Neo4JTest extends BddTest {

  protected final static String DB_ADDR = "neo4j";
  protected final static int DB_PORT = 7687;

  protected final static String DB_USER = "neo4j";
  protected final static String DB_PASS = "root";

  @Override
  protected Bdd getBdd() {
    BddNeo4j bdd = new BddNeo4j(DB_ADDR, DB_PORT, DB_USER, DB_PASS);
    bdd.setQueryBuilder(this.getBuilder());

    return bdd;
  }

  protected abstract Neo4JQueryBuilderInterface getBuilder();

}
