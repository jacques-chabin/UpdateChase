package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.logic.Schema;

public class Neo4JV2Test extends BddTest {

  protected final static String DB_ADDR = "localhost";
  protected final static int DB_PORT = 7687;

  protected final static String DB_USER = "neo4j";
  protected final static String DB_PASS = "root";

  @Override
  protected Bdd getBdd() {
    return new BddNeo4jV2(DB_ADDR, DB_PORT, DB_USER, DB_PASS, new Schema());
  }

}
