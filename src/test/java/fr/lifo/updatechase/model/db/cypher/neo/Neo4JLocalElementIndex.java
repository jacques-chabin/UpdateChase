package fr.lifo.updatechase.model.db.cypher.neo;

import fr.lifo.updatechase.model.db.cypher.query.LocalElementIndexQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.query.Neo4JQueryBuilderInterface;

public class Neo4JLocalElementIndex extends Neo4JTest {

  @Override
  protected Neo4JQueryBuilderInterface getBuilder() {
    return new LocalElementIndexQueryBuilder();
  }
}
