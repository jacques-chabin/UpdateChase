package fr.lifo.updatechase.model.db.cypher.neo;

import fr.lifo.updatechase.model.db.cypher.query.Neo4JQueryBuilderInterface;
import fr.lifo.updatechase.model.db.cypher.query.NoIndexQueryBuilder;

public class Neo4JNoIndex extends Neo4JTest {

  @Override
  protected Neo4JQueryBuilderInterface getBuilder() {
    return new NoIndexQueryBuilder();
  }
}
