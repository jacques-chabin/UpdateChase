package fr.lifo.updatechase.model.db.cypher.neo;

import fr.lifo.updatechase.model.db.cypher.query.LocalElementTypedRelsQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.query.Neo4JQueryBuilderInterface;

public class Neo4JLocalElementIndexTypedRels extends Neo4JTest {

  @Override
  protected Neo4JQueryBuilderInterface getBuilder() {
    return new LocalElementTypedRelsQueryBuilder();
  }
}
