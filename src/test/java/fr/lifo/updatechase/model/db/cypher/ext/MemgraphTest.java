package fr.lifo.updatechase.model.db.cypher.ext;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.cypher.BddNeo4j;

public class MemgraphTest extends BddTest {

  protected final static String DB_ADDR = "localhost";
  protected final static int DB_PORT = 7688;

  @Override
  protected Bdd getBdd() {
    return new BddNeo4j(DB_ADDR, DB_PORT);
  }

}
