package fr.lifo.updatechase.model;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import fr.lifo.updatechase.model.logic.Element;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestChase {

  private final static String DIR = "benchs/ldbc_nulls/ldbc-1000N/";
  private final static String INIT_FILE = DIR + BddTest.INIT_FILE;
  private final static String CONSTRAINTS_FILE = DIR + BddTest.RULE_FILE;
  private final static String SCHEMA_FILE = DIR + BddTest.SCHEMA_FILE;
  private final static String INSERT_FILE = DIR + BddTest.INSERT_FILE;
  private final static String INSERT_RESULT_FILE = DIR + BddTest.INSERT_RESULT_FILE;

  private final static String OUT_FILE = "BddInit-out.dlp";

  private final static int BATCH_SIZE = 1000;
  private final static int DELTA_MAX = 0;
  private final static boolean RESTRICTED = false;

  public static void main(String[] args) throws IOException, SyntaxError {
    //Bdd init = Bdd.fromFile(INIT_FILE);
    //LinkedListRules rules = LinkedListRules.fromFile(CONSTRAINTS_FILE);
    //Schema schema = Schema.fromFile(SCHEMA_FILE);

    //Bdd insert = Bdd.fromFile(INSERT_FILE);
    //Bdd insertResult = Bdd.fromFile(INSERT_RESULT_FILE);

    //Bdd bdd = new BddMemoire();
    //Bdd bdd = new BddNeo4j("localhost", "7687", "neo4j", "root");
    //Bdd bdd = new BddNeo4jV2("localhost", "7687", "neo4j", "root", schema);
    //Bdd bdd = new BddMySQL("localhost", 3307, "test_database", "test_user", "test_pass");
    System.out.println("Load file at " + INIT_FILE);
    Bdd bdd = Bdd.fromFile(INIT_FILE);

    System.out.println(bdd.getNulls().size());

    long i = 0;
    Map<String, String> elementMap = new HashMap<>();
    for (Element element : bdd.getNulls()) {
      elementMap.put(element.getName(), "const:" + i++);
      if (bdd.getNbNulls() - i <= 1000) {
        break;
      }
    }

    Bdd newBdd = new BddMemoire(bdd.parallelStream().map(atom -> {
      List<Element> elements = atom.stream().map(element -> {
        if (elementMap.containsKey(element.getName())) {
          return new Constant(elementMap.get(element.getName()));
        } else {
          return element;
        }
      }).collect(Collectors.toList());

      return new Atom(atom.getName(), elements);
    }).collect(Collectors.toList()));

    System.out.println(newBdd.getNulls().size());

        /*for (Map.Entry<String, List<Atom>> entry : insert.asMap().entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().size());
        }*/

        /*System.out.println("First Core");
        System.out.println("\n\nBefore: " + insert.size() + " atoms");
        insert.core();
        System.out.println("After: " + insert.size() + " atoms");*/

    //bdd.initFromDB(init);
    //bdd.core();

        /*System.out.println("Load...");
        List<Atom> atoms = DBFileIO.readFromFile(INIT_FILE, new LinkedList<>(), Atom::stringToAtom);
        int size = atoms.size();

        System.out.println("\nStart chase + core");
        //List<Atom> atoms = new ArrayList<>(insert);
        for (int i = 0; i < atoms.size(); i += BATCH_SIZE) {
            List<Atom> batch = atoms.subList(i, Math.min(i + BATCH_SIZE, size));
            bdd.addWithConstraints(batch, rules, DELTA_MAX, new BddMemoire(), RESTRICTED);
        }
        /*BddStats stats = bdd.addWithConstraints(insert, rules, DELTA_MAX, new BddMemoire(), RESTRICTED);
        System.out.println(stats.getSummary());*/

    System.out.println("\n\nSave DB");
    newBdd.toFile(OUT_FILE);

        /*System.out.println("\n\nBefore: " + init.size() + " atoms");
        System.out.println("After: " + bdd.size() + " atoms");
        bdd.printDiff(init);

        System.out.println("\n\nCompare to result");
        bdd.printDiff(insertResult);*/
  }
}
