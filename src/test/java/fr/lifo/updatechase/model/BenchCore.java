package fr.lifo.updatechase.model;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.DBFileIO;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.db.sql.BddMySQL;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class BenchCore {

  private final static String OUT_DIR = "output/";
  private final static String[] DIRS = {
      "benchs/ldbc_size/ldbc-1k/",
      "benchs/ldbc_size/ldbc-10k/",
      "benchs/ldbc_size/ldbc-50k/",
      "benchs/ldbc_size/ldbc-100k/",
      "benchs/ldbc_size/ldbc-200k/",
      "benchs/ldbc_size/ldbc-500k/",
      "benchs/ldbc_size/ldbc-1M/"
  };

  private final static String INIT_FILE = BddTest.INIT_FILE;
  private final static String CONSTRAINTS_FILE = BddTest.RULE_FILE;
  private final static String OUT_FILE = "BddInit-out.dlp";

  private final static int BATCH_SIZE = 1000;
  private final static int DELTA_MAX = 0;
  private final static boolean RESTRICTED = false;

  public static void main(String[] args) throws IOException, SyntaxError {
    Bdd bdd = new BddMySQL("mysqlN", 3306, "test_database", "test_user", "test_pass");
    //Bdd bdd = new BddMemoire();

    for (String dir : DIRS) {
      bdd.clear();

      System.out.println("Load...");
      LinkedListRules rules = LinkedListRules.fromFile(dir + CONSTRAINTS_FILE);
      List<Atom> atoms = DBFileIO.readFromFile(dir + INIT_FILE, new LinkedList<>(),
          Atom::stringToAtom);
      int size = atoms.size();

      System.out.println("\nStart chase + core");
      for (int i = 0; i < atoms.size(); i += BATCH_SIZE) {
        List<Atom> batch = atoms.subList(i, Math.min(i + BATCH_SIZE, size));
        bdd.addWithConstraints(batch, rules, DELTA_MAX, new BddMemoire(), RESTRICTED);
      }

      File output = new File(OUT_DIR + dir + OUT_FILE);
      System.out.println("\n\nSave DB to " + output.getAbsolutePath());
      output.getParentFile().mkdirs();
      bdd.toFile(output.getAbsolutePath());
    }
  }
}
