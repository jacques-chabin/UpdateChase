package fr.lifo.updatechase.benchs;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.DBFileIO;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Schema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public abstract class FileBenchmark extends Benchmarks {

  private static List<Atom> INIT = null;
  private static LinkedListRules RULES = null;
  private static Schema SCHEMA = null;

  @Override
  public Collection<Atom> generateDatabase(int db_scale) {

    if (INIT == null) {
      try {
        INIT = DBFileIO.readFromFile(this.getInitFile(), new LinkedList<>(), Atom::stringToAtom);

      } catch (IOException | SyntaxError exception) {
        exception.printStackTrace();
        return new ArrayList<>();
      }
    }

    try (Stream<Atom> atomStream = INIT.parallelStream()) {
      return atomStream.flatMap(atom -> IntStream.range(0, db_scale).parallel().mapToObj(i -> {
        Atom newAtom = (Atom) atom.clone();
        newAtom.forEach(element -> element.setName(element.getName() + "__" + i));
        return newAtom;
      })).collect(Collectors.toList());
    }
  }

  @Override
  public LinkedListRules generateRules(int nb_constraints) {
    if (RULES == null) {
      try {
        RULES = LinkedListRules.fromFile(this.getConstraintsFile());

      } catch (IOException | SyntaxError exception) {
        exception.printStackTrace();
        return new LinkedListRules();
      }
    }

    LinkedListRules generated = new LinkedListRules();
    generated.addAll(RULES.subList(0, Math.min(nb_constraints, RULES.size())));
    return generated;
  }

  @Override
  public Collection<Atom> generatePosUpdate(Bdd bdd, int update_size) {
    final AtomicInteger atomIndex = new AtomicInteger();
    List<Atom> atoms = new ArrayList<>(INIT);
    Collections.shuffle(atoms);

    return atoms.stream()
        .limit(update_size)
        .peek(atom -> {
          int j = atomIndex.incrementAndGet();
          for (int i = 0; i < atom.size(); i++) {
            Element element = atom.get(i);
            if (element.isConstant()) {
              element.setName("bench" + (i + j * atom.size()));
            } else {
              element.setName("_bench" + (i + j * atom.size()));
            }
          }
        })
        .collect(Collectors.toList());
  }

  @Override
  public Collection<Atom> generateNegUpdate(Bdd bdd, int update_size) {
    List<Atom> atoms = new ArrayList<>(bdd);
    Collections.shuffle(atoms);

    return atoms.subList(0, update_size);
  }

  @Override
  public Schema getSchema() {
    String schemaFilePath = this.getSchemaFile();

    if (SCHEMA == null && schemaFilePath != null) {
      try {
        SCHEMA = Schema.fromFile(schemaFilePath);

      } catch (IOException | SyntaxError exception) {
        exception.printStackTrace();
        return new Schema();
      }
    }

    return SCHEMA;
  }

  protected abstract String getConstraintsFile();

  protected abstract String getInitFile();

  protected String getSchemaFile() {
    return null;
  }

}
