package fr.lifo.updatechase.benchs;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.cypher.BddNeo4j;
import fr.lifo.updatechase.model.db.cypher.BddRedis;
import fr.lifo.updatechase.model.db.cypher.query.LocalElementIndexQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.query.LocalElementTypedRelsQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.query.NoIndexQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.query.TypedRelsQueryBuilder;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.db.sql.BddMySQL;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Schema;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.AuxCounters;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode({Mode.AverageTime/*, Mode.Throughput*/})
@Timeout(time = 1, timeUnit = TimeUnit.HOURS)
@Threads(value = 1)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = {"-Xms10G", "-Xmx10G"})
@Warmup(iterations = 2)
@Measurement(iterations = 15, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public abstract class Benchmarks {

  private final static Logger logger = Logger.getLogger("JMH");

  private final static int CHUNK_SIZE = 1000;
  private final static int DELTA_MAX = 0;
  private final static boolean RESTRICTED = false;

  @Param({/*"0",*/ "1", "2", "5", /*"10", "15", "20"*/})
  private int db_scale;

  @Param({"1", "5", "10", "15", "20"})
  private int update_size;

  @Param({/*"0", "1", "5", "10", "15", "20"*/ "200"})
  private int nb_constraints;

  @Param({
      "memory",
      "mysql",
      "neo4j no_index",
      "neo4j local_index",
      "neo4j no_index_typed",
      "neo4j local_index_typed",
      "redis"
  })
  private String dbms;

  private Bdd bdd;
  private Bdd init;
  private BddStats stats;
  private LinkedListRules rules;

  private Collection<Atom> posUpdate;
  private Collection<Atom> negUpdate;

  //// Aux counters
  @AuxCounters(AuxCounters.Type.EVENTS)
  @State(Scope.Thread)
  public static class Counter {

    public long nbRules;
    public long nbFacts;
    public long nbNulls;
    public long nbQueries;
    public long chaseTime;
    public long nullBucketTime;
    public long partitionTime;
    public long simplificationTime;
  }

  public static void main(String[] args) throws IOException, RunnerException {
    Options opt = new OptionsBuilder()
        .detectJvmArgs()
        .include(".*")
        .result("benchmarkResults.csv")
        .resultFormat(ResultFormatType.CSV)
        .shouldDoGC(true)
        .shouldFailOnError(false)
        .build();
    new Runner(opt).run();
  }

  //// Benchmark setup

  @Setup(Level.Trial)
  public void setupTrial() {
    Logger.getLogger("bdd").setLevel(java.util.logging.Level.CONFIG);

    // Get database
    logger.info("Connect to database...");

    switch (this.dbms) {
      case "memoire":
        this.bdd = new BddMemoire();
        break;

      case "neo4j no_index":
        this.bdd = new BddNeo4j("neo4j", 7687, "neo4j", "root");
        ((BddNeo4j) this.bdd).setQueryBuilder(new NoIndexQueryBuilder());
        break;

      case "neo4j local_index":
        this.bdd = new BddNeo4j("neo4j", 7687, "neo4j", "root");
        ((BddNeo4j) this.bdd).setQueryBuilder(new LocalElementIndexQueryBuilder());
        break;

      case "neo4j no_index_typed":
        this.bdd = new BddNeo4j("neo4j", 7687, "neo4j", "root");
        ((BddNeo4j) this.bdd).setQueryBuilder(new TypedRelsQueryBuilder());
        break;

      case "neo4j local_index_typed":
        this.bdd = new BddNeo4j("neo4j", 7687, "neo4j", "root");
        ((BddNeo4j) this.bdd).setQueryBuilder(new LocalElementTypedRelsQueryBuilder());
        break;

      case "mysql":
        this.bdd = new BddMySQL("mysql", 3306, "test_database", "test_user", "test_pass");
        break;

      case "redis":
        this.bdd = new BddRedis("redis", 6379, "bench");
        break;
    }

    logger.info("Connected");

    // Generate database
    logger.info("Generate database...");
    this.init = new BddMemoire(this.generateDatabase(this.db_scale));

    // Generate rules
    logger.info("Generate rules...");
    this.rules = this.generateRules(this.nb_constraints);

    // Core
        /*logger.info("Start initial core on " + generated.size() + " atoms with " + this.rules.size() + " constraints...");
        List<Atom> atoms = new ArrayList<>(generated);
        for (int i = 0; i < atoms.size(); i += CHUNK_SIZE) {
            logger.info("BATCH " + i + "/" + atoms.size() / CHUNK_SIZE);
            List<Atom> batch = atoms.subList(i, Math.min(i + CHUNK_SIZE, atoms.size()));
            this.init.addWithConstraints(batch, this.rules, DELTA_MAX, new BddMemoire(), RESTRICTED);
        }
        logger.info(this.init.size() + " atoms in database");*/

    logger.info("-----------------------\n" +
        "Nb atoms: " + this.init.size() + "\n" +
        "Nb nulls: " + this.init.getNbNulls() + "\n" +
        "Nb rules: " + this.rules.size() + "\n" +
        "-----------------------\n");
  }

  @TearDown(Level.Trial)
  public void teardownTrial() {
    logger.info("Close database");
    this.bdd.close();
  }

  @Setup(Level.Invocation)
  public void setupInvocation() {
    Logger.getLogger("bdd").setLevel(java.util.logging.Level.OFF);

    this.bdd.initFromDB(init);
    logger.info("DB loaded");

    // Generate pos update
    logger.info("Generate pos update...");
    this.posUpdate = this.generatePosUpdate(this.bdd, update_size);
    logger.info(this.posUpdate.stream().map(Atom::toString).collect(Collectors.joining("\n")));

    // Generate neg update
    logger.info("Generate neg update...");
    this.negUpdate = this.generateNegUpdate(this.bdd, update_size);
    logger.info(this.negUpdate.stream().map(Atom::toString).collect(Collectors.joining("\n")));
  }

  @TearDown(Level.Invocation)
  public void teardownInvocation() {
    Logger.getLogger("bdd").setLevel(java.util.logging.Level.CONFIG);
    this.bdd.clear();
  }

  @Benchmark
  public BddStats add(Counter counter) {
    this.stats = this.bdd.addWithConstraints(this.posUpdate, this.rules, DELTA_MAX,
        new BddMemoire(), RESTRICTED);

    counter.nbRules = this.rules.size();
    counter.nbFacts = this.init.size();
    counter.nbNulls = this.init.getNbNulls();
    counter.nbQueries = this.stats.getNumberQueries();
    counter.chaseTime = this.stats.getChaseTime().toMillis();
    counter.nullBucketTime = this.stats.getNullBucketTime().toMillis();
    counter.partitionTime = this.stats.getPartitionTime().toMillis();
    counter.simplificationTime = this.stats.getSimplificationTime().toMillis();

    return this.stats;
  }

  @Benchmark
  public BddStats delete(Counter counter) {
    this.stats = this.bdd.removeWithConstraints(this.negUpdate, this.rules, DELTA_MAX, RESTRICTED);

    counter.nbRules = this.rules.size();
    counter.nbFacts = this.init.size();
    counter.nbNulls = this.init.getNbNulls();
    counter.nbQueries = this.stats.getNumberQueries();
    counter.chaseTime = this.stats.getChaseTime().toMillis();
    counter.nullBucketTime = this.stats.getNullBucketTime().toMillis();
    counter.partitionTime = this.stats.getPartitionTime().toMillis();
    counter.simplificationTime = this.stats.getSimplificationTime().toMillis();

    return this.stats;
  }

  public abstract Collection<Atom> generateDatabase(int db_scale);

  public abstract LinkedListRules generateRules(int nb_constraints);

  public abstract Collection<Atom> generatePosUpdate(Bdd bdd, int update_size);

  public abstract Collection<Atom> generateNegUpdate(Bdd bdd, int update_size);

  public abstract Schema getSchema();
}
