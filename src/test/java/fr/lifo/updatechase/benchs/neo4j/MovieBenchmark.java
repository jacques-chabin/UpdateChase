package fr.lifo.updatechase.benchs.neo4j;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class MovieBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "neo4j-examples/movies-reduced/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "neo4j-examples/movies-reduced/BddInit.dlp";
  }

  @Override
  protected String getSchemaFile() {
    return "neo4j-examples/movies-reduced/Schema.dlp";
  }
}
