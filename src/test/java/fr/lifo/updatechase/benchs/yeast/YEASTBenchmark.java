package fr.lifo.updatechase.benchs.yeast;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class YEASTBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/yeast/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/yeast/BddInit.dlp";
  }
}
