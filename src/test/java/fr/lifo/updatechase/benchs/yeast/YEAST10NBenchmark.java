package fr.lifo.updatechase.benchs.yeast;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class YEAST10NBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/yeast-10N/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/yeast-10N/BddInit.dlp";
  }
}
