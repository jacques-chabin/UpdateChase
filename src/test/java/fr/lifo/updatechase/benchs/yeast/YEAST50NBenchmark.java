package fr.lifo.updatechase.benchs.yeast;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class YEAST50NBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/yeast-50N/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/yeast-50N/BddInit.dlp";
  }
}
