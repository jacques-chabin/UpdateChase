package fr.lifo.updatechase.benchs.yeast;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class YEAST100NBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/yeast-100N/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/yeast-100N/BddInit.dlp";
  }
}
