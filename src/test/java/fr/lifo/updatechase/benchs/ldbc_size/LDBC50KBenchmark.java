package fr.lifo.updatechase.benchs.ldbc_size;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class LDBC50KBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/ldbc_size/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/ldbc_size/ldbc-50k/BddInit.dlp";
  }

  @Override
  protected String getSchemaFile() {
    return "benchs/ldbc-Schema.dlp";
  }
}
