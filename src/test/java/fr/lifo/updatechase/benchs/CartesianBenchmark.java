package fr.lifo.updatechase.benchs;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Schema;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CartesianBenchmark extends Benchmarks {

  private static final int NB_PRED = 4;
  private static final int NB_INST = 20;
  private static final int ARITY = 3;

  @Override
  public Collection<Atom> generateDatabase(int db_scale) {
    Collection<Atom> atoms = new ArrayList<>(NB_PRED * NB_INST * db_scale);

    List<List<Element>> cartesianProduct = new ArrayList<>();
    cartesianProduct.add(new ArrayList<>());

    for (int i = 0; i < ARITY; i++) {
      List<List<Element>> newCartesianProduct = new ArrayList<>();
      for (int j = 0; j < (NB_INST * db_scale); j++) {
        for (List<Element> elements : cartesianProduct) {
          List<Element> tmp = new ArrayList<>(elements);
          tmp.add(new Constant("e" + j));
          newCartesianProduct.add(tmp);
        }
      }
      cartesianProduct = newCartesianProduct;
    }

    for (List<Element> elements : cartesianProduct) {
      for (int i = 0; i < NB_PRED; i++) {
        atoms.add(new Atom("P" + i, elements));
      }
    }

    return atoms;
  }

  @Override
  public LinkedListRules generateRules(int nb_constraints) {
    return new LinkedListRules();
  }

  @Override
  public Collection<Atom> generatePosUpdate(Bdd bdd, int update_size) {
    return Collections.emptyList();
  }

  @Override
  public Collection<Atom> generateNegUpdate(Bdd bdd, int update_size) {
    return Collections.emptyList();
  }

  @Override
  public Schema getSchema() {
    Schema schema = new Schema();
    String[] attributes = new String[ARITY];

    for (int i = 0; i < ARITY; i++) {
      attributes[i] = "x" + i;
    }

    for (int i = 0; i < NB_PRED; i++) {
      schema.node("P" + i, attributes);
    }

    return schema;
  }
}
