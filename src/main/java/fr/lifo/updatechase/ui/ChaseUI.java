package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JTabbedPane;

/**
 * Cette classe est le coeur de l'application. C'est a partir d'elle qu'on accede aux different
 * elements de l'application.
 */
public class ChaseUI extends JFrame {

  // La barre des menus
  public JMenuBar menu;

  // L'onglet Chase
  public VueChase vueChase;

  // L'onglet Core
  public VueCore vueCore;

  //L'onglet Insertion
  public VueInsertion vueInsertion;

  // L'onglet Delete
  public VueDelete vueDelete;

  // Le JPanel qui contient les onglets
  public JTabbedPane panneaux;

  // Les modeles de liste communs a 2+ onglets
  public DefaultListModel<Atom> bddListModel;
  public DefaultListModel<Rule> reglesListModel;

  // les Bdd et regles communes a 2+ onglets
  public Bdd bddSource;
  public LinkedListRules rules;

  /**
   * Constructeur de la Frame principale
   */
  public ChaseUI() {

    // Window name
    super("Chase - Core - Update");

    //Quand clique sur la croix rouge, on termine le programme
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    // Menu
    menu = new Menu(this);
    this.setJMenuBar(menu);

    //init des listModel pour affichage des JList
    this.bddListModel = new DefaultListModel<>();
    this.reglesListModel = new DefaultListModel<>();

    //init du model
    bddSource = new BddMemoire();
    rules = new LinkedListRules();

    // Taille de la fenetre
    Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
    int width = (int) (dimension.getWidth() / 1.10);
    int height = (int) (dimension.getHeight() / 1.25);

    // Appliquer la taille de la fenetre
    this.setSize(width, height);

    Container cont = this.getContentPane();

    // affichage des onglets
    this.panneaux = new JTabbedPane();
    cont.add(this.panneaux);

    // initialisation des vues contenues dans les onglets
    this.vueChase = new VueChase(this);
    this.vueCore = new VueCore(this);
    this.vueInsertion = new VueInsertion(this);
    this.vueDelete = new VueDelete(this);

    // ajout des vues dans les onglets
    this.panneaux.addTab("Chase", null, this.vueChase, "Appliquer le Chase a une Bdd");
    this.panneaux.addTab("Core", null, this.vueCore, "Appliquer le Core a une Bdd");
    this.panneaux.addTab("Insertion", null, this.vueInsertion, "Inserer un ensemble d'elements");
    this.panneaux.addTab("Deletion", null, this.vueDelete, "Supprimer un ensemble d'elements");
    this.setVisible(true);

  }

  /**
   * Getteur du Modele des JList de la Bdd Source
   *
   * @return DefaultListModel<Atome>
   */
  public DefaultListModel<Atom> getListModelBddSource() {
    return this.bddListModel;
  }

  /**
   * set le Modele des JList de la Bdd Source
   */
  public void setListModelBddSource(Bdd otherBdd) {
    this.bddListModel.clear();
    for (Atom atom : otherBdd) {
      this.bddListModel.addElement(atom);
    }
  }

  /**
   * Getteur du Modele des JList des regles
   *
   * @return DefaultListModel<Rule>
   */
  public DefaultListModel<Rule> getListModelRegles() {
    return this.reglesListModel;
  }

  /**
   * Ajout suppression du model de liste bdd source.
   *
   * @param a Atome a ajoute au modele de la BddSource.
   */
  public void addAtomeToJListBddSource(Atom a) {
    if (!this.getListModelBddSource().contains(a)) {
      this.getListModelBddSource().addElement(a);
    }
  }

  /**
   * Supprime un Ensemble d'Atomes du DefaultListModel<Atome> de la Bdd Source.
   *
   * @param index int[] le tableau des positions d'Elements a supprimer.
   * @return la Bdd des elements supprimes pour les supprimer apres a la Bdd Source.
   */
  public Bdd removeAtomeFromJListBddSource(int[] index) {
    Atom a;
    Bdd aux = new BddMemoire();
    for (int i = index.length - 1; i > -1; i--) {
      a = this.bddListModel.getElementAt(index[i]);
      this.bddListModel.remove(index[i]);
      aux.add(a);
    }
    return aux;
  }

  /**
   * Supprime un Ensemble d'Atomes du DefaultListModel<Atome> de la Bdd Source.
   *
   * @param otherBdd Bdd. la base de données contenant les Elements à supprimer.
   * @return la Bdd des elements supprimes pour les supprimer apres à la Bdd Source.
   */
  public Bdd removeAtomeFromJListBddSource(Bdd otherBdd) {
    Bdd aux = new BddMemoire();
    for (Atom a : otherBdd) {
      this.bddListModel.removeElement(a);
      aux.add(a);
    }
    return aux;
  }

  /**
   * Ajout suppression du DefaultListModel<Rule> des JList de regles.
   *
   * @param r Rule La regle a supprimer.
   */
  public void addRegleToJListReglesModele(Rule r) {
    this.getListModelRegles().addElement(r);
  }

  /**
   * Supprime un Ensemble de Rules du DefaultListModel<Rule> des JList de regles.
   *
   * @param index int[] le tableau des positions d'Elements a supprimer.
   * @return la LinkedListRules des elements supprimes pour les supprimer apres a la LinkedListRules
   * utilisee par UpdateChase.
   */
  public LinkedListRules removeRulesFromJListModel(int[] index) {
    Rule a;
    LinkedListRules aux = new LinkedListRules();
    for (int i = index.length - 1; i > -1; i--) {
      a = this.reglesListModel.getElementAt(index[i]);
      this.reglesListModel.remove(index[i]);
      aux.add(a);
    }
    return aux;
  }

  /**
   * Getter de la Bdd Source de UpdateChase.
   *
   * @return Bdd le Bdd Source de UpdateChase.
   */
  public Bdd getBddSource() {
    return this.bddSource;
  }

  /**
   * Set la Bdd Source de UpdateChase par la Bdd passee en parametre.
   *
   * @param bd Bdd
   */
  public void setBddSource(Bdd bd) {
    this.bddSource = new BddMemoire(bd);
  }

  /**
   * Getter des regles d'inferences courantes utilisee par UpdateChase.
   *
   * @return LinkedListRules
   */
  public LinkedListRules getRegles() {
    return this.rules;
  }

  /**
   * Setter des regles d'inferences qui seront utilisees par UpdateChase.
   *
   * @param llr
   */
  void setRegles(LinkedListRules llr) {
    this.rules = llr;
  }

  /**
   * Set le modele de toutes les vues Bdd Source par le DefaultListModel<Atome> courant
   * 'bddListMdel'.
   */
  public void updateAllVueBddSource() {
    vueChase.vueBdd.bddDisplay.setModel(bddListModel);
    vueCore.vueBddSource.bddDisplay.setModel(bddListModel);
    vueInsertion.vueBddSource.bddDisplay.setModel(bddListModel);
    vueDelete.vueBddSource.bddDisplay.setModel(bddListModel);
  }

  /**
   * Met a jour tout les labels qui affichent la taille de la Bdd Source.
   */
  public void updateAllCounter() {
    vueChase.vueBdd.setCount(vueChase.vueBdd.getBddSize());
    vueCore.vueBddSource.setCount(vueCore.vueBddSource.getBddSize());
    vueInsertion.vueBddSource.setCount(vueInsertion.vueBddSource.getBddSize());
    vueDelete.vueBddSource.setCount(vueDelete.vueBddSource.getBddSize());
  }

  /**
   * Set le modele de toutes les vues de regles par le DefaultListModel<Rule> courant
   * 'reglesListModel'.
   */
  public void updateAllVueRegles() {
    vueChase.vueRegles.ruleDisplay.setModel(reglesListModel);
    vueInsertion.vueRegles.ruleDisplay.setModel(reglesListModel);
    vueDelete.vueRegles.ruleDisplay.setModel(reglesListModel);
  }

  /**
   * Met a jour tout les labels qui affichent le nombre de regles courantes.
   */
  public void updateAllRuleCounter() {
    vueChase.vueRegles.setCount(rules.size());
    vueInsertion.vueRegles.setCount(rules.size());
    vueDelete.vueRegles.setCount(rules.size());
  }

  /**
   * Ajoute tout les atomes presents dans la Bdd a la DefaultListModel<Atome> des JList qui
   * affichent la Bdd Source.
   *
   * @param db une Bdd
   */
  public void addAllAtomeToJList(Bdd db) {
    for (Atom atom : db) {
      this.bddListModel.addElement(atom);
    }
  }

  /**
   * Ajoute toutes les regles presentes dans la LinkedListRules a la DefaultListModel<Rule> des
   * JList qui affichent les regles.
   *
   * @param llr LinkedListRules
   */
  public void addAllRulesToJList(LinkedListRules llr) {
    for (Rule r : llr) {
      reglesListModel.addElement(r);
    }
  }

  /**
   * Commente/Decommente un ou plusieurs atomes. Ajoute/enleve des marquages visuels de commentaire
   * et ajoute/supprime les atomes selectionnés de la bdd source.
   *
   * @param index les indices des atomes selectionnes.
   */
  public void commentAtomeFromJListBddSource(int[] index) {
    Atom a;
    for (int i = index.length - 1; i > -1; i--) {
      a = this.bddListModel.getElementAt(index[i]);
      if (!a.getName().startsWith("%")) {
        bddSource.remove(a);
        a.setName("%" + a.getName());
        bddListModel.setElementAt(a, index[i]);
      } else {
        a.setName(a.getName().substring(1));
        bddSource.add(a);
        bddListModel.setElementAt(a, index[i]);
      }
    }
  }

  /**
   * Commente/Decommente une ou plusieurs regles. Ajoute/enleve des marquages visuels de commentaire
   * et ajoute/supprime les regles selectionnés de la LinkedListRule de regles.
   *
   * @param index
   */
  public void commentRuleFromJListBddSource(int[] index) {
    Rule r;
    for (int i = index.length - 1; i > -1; i--) {
      r = reglesListModel.getElementAt(index[i]);
      if (!r.getHeadNames().contains("%")) {
        rules.remove(r);
        r.setHeadNames("%" + r.getHeadNames());
        reglesListModel.setElementAt(r, index[i]);
      } else {
        r.setHeadNames(r.getHeadNames().replace("%", ""));
        rules.add(r);
        reglesListModel.setElementAt(r, index[i]);
      }
    }
  }

  /**
   * Efface toutes les vues Bdd de resultat.
   */
  public void clearAllVueRes() {
    vueInsertion.auxListModel.clear();
    vueChase.auxListModel.clear();
    vueCore.auxListModel.clear();

    vueInsertion.resBdd = new BddMemoire();
    vueChase.resBdd = new BddMemoire();
    vueCore.resBdd = new BddMemoire();

    vueInsertion.vueRes.setCount(vueInsertion.resBdd.size());
    vueChase.vueRes.setCount(vueChase.resBdd.size());
    vueCore.vueRes.setCount(vueCore.resBdd.size());
  }

  /**
   * Efface les vue annexes telle que la vue Bdd d'ajout.
   */
  public void clearAllVueAnnexes() {
    vueInsertion.addAtomeListModel.clear();
    vueInsertion.addAtomeBdd = new BddMemoire();
    vueInsertion.vueAdd.setCount(vueInsertion.addAtomeBdd.size());
  }

  public void resetDB() {
    this.bddListModel = new DefaultListModel<>();
    this.addAllAtomeToJList(this.bddSource);
    this.updateAllVueBddSource();
    this.updateAllCounter();

    this.clearAllVueRes();
    this.clearAllVueAnnexes();
  }

}
