package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurRunCore;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * @author stagiaire Affichage du bouton  pour lancer l'algorithme de Core.
 */
public class VueSetUpCore extends JPanel {

  JButton b_run;
  JLabel execTime;

  ChaseUI mainFrame;

  VueSetUpCore(ChaseUI mf) {
    super();
    this.setLayout(new FlowLayout());

    mainFrame = mf;
    b_run = new JButton("  Run Core Algorithm  ");
    execTime = new JLabel();
    b_run.addActionListener(new ControlleurRunCore(mf, this));
    b_run.setToolTipText("This button will apply the Core algorithm on the initial database.");
    this.add(b_run);
    this.add(execTime);

    TitledBorder bordure = BorderFactory.createTitledBorder("Set up :");
    this.setBorder(bordure);

  }

  public void setExecTime(float time) {
    execTime.setText("Execution time : " + time + "s");
  }
}
