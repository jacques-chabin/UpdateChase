package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurValiderChase;
import fr.lifo.updatechase.model.logic.Atom;
import javax.swing.DefaultListModel;

/**
 * @author stagiaire Classe d'affichage d'une vue de resultat specifique a l'algorithme de Chase.
 */
public class VueBddResChase extends VueBddRes {

  VueBddResChase(String label, ChaseUI mf, DefaultListModel<Atom> dlm, VueChase vc) {
    super(label, mf, dlm);
    b_accept.addActionListener(new ControlleurValiderChase(mainFrame, vc));
    this.bddDisplay.setCellRenderer(new JListResAddCustomCellRenderer(mainFrame.bddSource));
  }

}
