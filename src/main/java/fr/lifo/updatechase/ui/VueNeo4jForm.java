package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurLoadBddNeo4j;
import fr.lifo.updatechase.controller.ControlleurSaveConfig;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VueNeo4jForm extends JFrame {

  private final ChaseUI mainFrame;

  public JTextField addressTextField;
  public JTextField portTextField;
  public JTextField userTextField;
  public JPasswordField passwordTextField;
  public JCheckBox saveLogin;

  public VueNeo4jForm(ChaseUI mainFrame) {

    // Window name
    super("Connect to a Neo4j database");
    this.mainFrame = mainFrame;

    this.setSize(350, 220);

    JPanel panel = new JPanel();
    panel.setBorder(new EmptyBorder(10, 10, 10, 10));

    GridLayout layout = new GridLayout(0, 2);
    layout.setHgap(10);
    layout.setVgap(10);

    panel.setLayout(layout);
    this.addressTextField = new JTextField();
    this.portTextField = new JTextField();
    this.userTextField = new JTextField();
    this.passwordTextField = new JPasswordField();
    this.saveLogin = new JCheckBox("Save credentials");
    this.saveLogin.addActionListener(new ControlleurSaveConfig(this));

    this.loadConfig();

    JButton connectButton = new JButton("Connect");
    connectButton.addActionListener(new ControlleurLoadBddNeo4j(this.mainFrame, this));

    panel.add(new JLabel("Address"));
    panel.add(this.addressTextField);
    panel.add(new JLabel("Port"));
    panel.add(this.portTextField);
    panel.add(new JLabel("User"));
    panel.add(this.userTextField);
    panel.add(new JLabel("Password"));
    panel.add(this.passwordTextField);
    panel.add(connectButton);
    panel.add(this.saveLogin);

    this.add(panel);

    this.setVisible(true);

  }

  public void loadConfig() {
    try {

      String fileName = "neo4j.cfg";
      File file = new File(fileName);
      FileReader fr = new FileReader(file);
      BufferedReader br = new BufferedReader(fr);
      String line;
      int i = 0;
      while ((line = br.readLine()) != null) {
        if (line.isEmpty()) {
          continue;
        }
        switch (i) {
          case 0:
            this.addressTextField.setText(line);
            break;
          case 1:
            this.portTextField.setText(line);
            break;
          case 2:
            this.userTextField.setText(line);
            this.saveLogin.setSelected(true);
            break;
          case 3:
            this.passwordTextField.setText(line);
            break;
        }
        ++i;
      }
    } catch (FileNotFoundException e) {

    } catch (IOException e) {

    }
  }

  public void saveConfig() {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter("neo4j.cfg"));

      writer.write(this.addressTextField.getText());
      writer.newLine();
      writer.write(this.portTextField.getText());
      writer.newLine();

      if (this.saveLogin.isSelected()) {
        writer.write(this.userTextField.getText());
        writer.newLine();
        writer.write(this.passwordTextField.getPassword());
        writer.newLine();
      }
      writer.close();
    } catch (IOException e) {

    }
  }

}

