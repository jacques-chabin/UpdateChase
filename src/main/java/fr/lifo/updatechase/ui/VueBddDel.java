package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import javax.swing.DefaultListModel;

public class VueBddDel extends VueBdd {

  public VueAjoutAtomeBdd vueAjoutAtomeBdd;

  VueBddDel(String label, ChaseUI mf, DefaultListModel<Atom> dlm) {

    super(label, mf);
    bddDisplay.setModel(dlm);
    vueAjoutAtomeBdd = new VueAjoutAtomeBddDel(mainFrame, this);

    this.add(vueAjoutAtomeBdd, BorderLayout.SOUTH);

  }

  @Override
  public int getBddSize() {
    return mainFrame.vueDelete.delAtomeBdd.size();
  }

  @Override
  public void setCount(int i) {
    vueAjoutAtomeBdd.setCount(getBddSize());
  }

  public String getInputText() {
    return vueAjoutAtomeBdd.getInputText();
  }

  public void emptyInputText() {
    vueAjoutAtomeBdd.emptyTextField();
  }

}
