package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurValiderCore;
import fr.lifo.updatechase.model.logic.Atom;
import javax.swing.DefaultListModel;

/**
 * @author stagiaire Classe d'affichage d'une vue de resultat specifique a l'algorithme de Core.
 */
public class VueBddResCore extends VueBddRes {

  VueBddResCore(String label, ChaseUI mf, DefaultListModel<Atom> dlm, VueCore vc) {
    super(label, mf, dlm);

    b_accept.addActionListener(new ControlleurValiderCore(vc, mf));
  }

  @Override
  public int getBddSize() {
    return mainFrame.vueCore.resBdd.size();
  }

}
