package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurRunDelete;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class VueSetUpDelete extends JPanel {

  JButton b_run;
  JRadioButton restricted;
  JRadioButton unrestricted;
  JLabel execTime;
  ChaseUI mainFrame;
  public JTextField cycleC;

  VueSetUpDelete(ChaseUI mf) {
    super();
    this.setLayout(new FlowLayout());

    mainFrame = mf;

    ButtonGroup jbg = new ButtonGroup();
    restricted = new JRadioButton();
    restricted.setToolTipText("Chase will be RESTRICTED.");
    unrestricted = new JRadioButton();
    unrestricted.setToolTipText("Chase will be UNRESTRICTED.");
    b_run = new JButton("  Run Deletion Algorithm  ");
    b_run.addActionListener(new ControlleurRunDelete(mf, this));
    b_run.setToolTipText(
        "This button will apply deletion algorithm on initial database. The changes will be applied by clicking on the 'Accept'.");

    jbg.add(restricted);
    jbg.add(unrestricted);
    restricted.setSelected(true);

    JLabel rest = new JLabel("Restricted    ");
    rest.setToolTipText("Chase will be RESTRICTED.");
    JLabel unrest = new JLabel("Unrestricted     ");
    unrest.setToolTipText("Chase will be UNRESTRICTED.");

    JLabel cycle = new JLabel("Cycle condidtion :");
    cycleC = new JTextField();
    cycleC.setColumns(3);
    cycleC.setText("0");
    cycleC.setToolTipText(
        "<html><ul><li>0 No restriction.</li>" + "<li>n> 0 limits the search depth to n. </li>"
            + "<li>-1 not used here.</li>"
            + "<li>-2 not used here.</li></ul></html>");

    execTime = new JLabel();

    this.setLayout(new FlowLayout());

    this.add(restricted);
    this.add(rest);
    this.add(unrestricted);
    this.add(unrest);
    this.add(cycle);
    this.add(cycleC);
    this.add(b_run);
    this.add(execTime);

    TitledBorder bordure = BorderFactory.createTitledBorder("Set up :");
    this.setBorder(bordure);

  }

  public void setExecTime(float time) {
    execTime.setText("Execution time : " + time + "s");
  }

  public boolean isRestricted() {
    return restricted.isSelected();
  }
}
