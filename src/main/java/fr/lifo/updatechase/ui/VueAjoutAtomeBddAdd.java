package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurAjoutAtomeBddAdd;
import fr.lifo.updatechase.controller.ControlleurCommentAtomeBddAdd;
import fr.lifo.updatechase.controller.ControlleurSupprimerSelectionAtomeBddAdd;

/**
 * @author stagiaire Vue d'ajout d'atome a une Bdd specifique a la vue Bdd Add.
 */
public class VueAjoutAtomeBddAdd extends VueAjoutAtomeBdd {

  VueAjoutAtomeBddAdd(ChaseUI mf, VueBddAdd vueBdd) {
    super(vueBdd);
    ControlleurAjoutAtomeBddAdd action = new ControlleurAjoutAtomeBddAdd(mf, vueBdd);
    b_add.addActionListener(action);
    b_remove.addActionListener(new ControlleurSupprimerSelectionAtomeBddAdd(mf, vueBdd));
    b_comment.addActionListener(new ControlleurCommentAtomeBddAdd(vueBdd, mf));
    inputText.addActionListener(action);
  }
}
