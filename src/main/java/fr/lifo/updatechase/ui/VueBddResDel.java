package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurValiderDel;
import fr.lifo.updatechase.model.logic.Atom;
import javax.swing.DefaultListModel;

/**
 * @author Julien Revaud Classe d'affichage d'une vue de resultat specifique a l'algorithme de
 * suppression. ATTENTION: cette classe affiche la Base de donnée qui résulte de la suppression et
 * NON les atomes qui seront supprimé
 */
public class VueBddResDel extends VueBddRes {

  VueBddResDel(String label, ChaseUI mf, DefaultListModel<Atom> dlm, VueDelete vd) {
    super(label, mf, dlm);

    b_accept.addActionListener(new ControlleurValiderDel(mainFrame));
    this.bddDisplay.setCellRenderer(new JListResAddCustomCellRenderer(mainFrame.bddSource));
  }

  @Override
  public int getBddSize() {
    return mainFrame.vueDelete.resBdd.size();
  }

}
