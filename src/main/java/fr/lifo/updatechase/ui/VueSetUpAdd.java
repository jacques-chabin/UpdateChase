package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurRunAdd;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * @author stagiaire Affichage des parametres specifiques et modifiables de l'algorithme
 * d'insertion.
 */
public class VueSetUpAdd extends JPanel {

  ChaseUI mainFrame;
  JRadioButton restricted;
  JRadioButton unrestricted;
  JButton run;
  JLabel execTime;
  public JTextField cycleC;

  VueSetUpAdd(ChaseUI mf) {
    this.mainFrame = mf;

    ButtonGroup jbg = new ButtonGroup();
    restricted = new JRadioButton();
    restricted.setToolTipText("Chase will be RESTRICTED.");
    unrestricted = new JRadioButton();
    unrestricted.setToolTipText("Chase will be UNRESTRICTED.");
    run = new JButton(" Run Insert Algorithm ");
    run.addActionListener(new ControlleurRunAdd(mainFrame, this));

    jbg.add(restricted);
    jbg.add(unrestricted);
    restricted.setSelected(true);

    JLabel rest = new JLabel("Restricted    ");
    rest.setToolTipText("Chase will be RESTRICTED.");
    JLabel unrest = new JLabel("Unrestricted     ");
    unrest.setToolTipText("Chase will be UNRESTRICTED.");

    JLabel cycle = new JLabel("Cycle condition :");
    cycleC = new JTextField();
    cycleC.setColumns(3);
    cycleC.setText("0");
    cycleC.setToolTipText(
        "<html><ul><li>0 No restriction.</li>" + "<li>n> 0 limits the search depth to n. </li>"
            + "<li>-1 does not use the atoms containing the nulls to deduce new information.</li>"
            + "<li>-2 does not use the atoms containing stared nulls to deduce new information.</li></ul></html>");

    execTime = new JLabel();

    this.setLayout(new FlowLayout());

    this.add(restricted);
    this.add(rest);
    this.add(unrestricted);
    this.add(unrest);
    this.add(cycle);
    this.add(cycleC);
    this.add(run);
    this.add(execTime);

    TitledBorder bordure = BorderFactory.createTitledBorder("Set up :");
    this.setBorder(bordure);
  }

  public void setExecTime(float time) {
    execTime.setText("Execution time : " + time + "s");
  }

  public boolean isRestricted() {
    return restricted.isSelected();
  }

  boolean isUnrestricted() {
    return unrestricted.isSelected();
  }
}
