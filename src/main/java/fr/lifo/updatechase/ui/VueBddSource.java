package fr.lifo.updatechase.ui;

import java.awt.BorderLayout;

/**
 * @author stagiaire Classe d'affichage de la vue de la Bdd Source qui est utilisee pour lancer les
 * differents algorithmes..
 */
public class VueBddSource extends VueBdd {

  public VueAjoutAtomeBdd vueAjoutAtome;

  VueBddSource(String label, ChaseUI mf) {
    super(label, mf);
    this.bddDisplay.setModel(this.mainFrame.getListModelBddSource());
    this.vueAjoutAtome = new VueAjoutAtomeBddSource(this);
    this.add(vueAjoutAtome, BorderLayout.SOUTH);
  }

  public String getInputText() {
    return vueAjoutAtome.getInputText();
  }

  public void setCount(int i) {
    this.vueAjoutAtome.setCount(i);
  }

  public int getBddSize() {
    return this.mainFrame.getBddSource().size();
  }
}
