package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author stagiaire Classe d'affichage d'une Vue Bdd de Resultat generique. Contient un bouton
 * d'acceptation du resultat et un label qui affiche la taille du resultat.
 */
public class VueBddRes extends VueBdd {

  JLabel count;
  JLabel execTime;
  JButton b_accept;
  JPanel buttonFlow;

  VueBddRes(String label, ChaseUI mf, DefaultListModel<Atom> dlm) {
    super(label, mf);

    this.count = new JLabel("size : 0");
    this.bddDisplay.setModel(dlm);
    b_accept = new JButton("Accept");
    JPanel textPanel = new JPanel();
    buttonFlow = new JPanel();
    buttonFlow.setLayout(new FlowLayout());
    JPanel flow2 = new JPanel();
    flow2.setLayout(new FlowLayout());

    textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
    buttonFlow.add(b_accept);
    flow2.add(count);

    JPanel flow3 = new JPanel();
    flow3.setLayout(new FlowLayout());
    execTime = new JLabel();
    flow3.add(execTime);

    textPanel.add(buttonFlow);
    textPanel.add(flow2);
    textPanel.add(flow3);

    this.add(textPanel, BorderLayout.SOUTH);

  }

  public void setExecTime(float time) {
    execTime.setText("Execution time : " + time + "s");
  }

  public void setCount(int i) {
    this.count.setText("size : " + i);
  }

  @Override
  public int getBddSize() {
    return mainFrame.vueChase.resBdd.size();
  }
}
