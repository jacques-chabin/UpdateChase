package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 * @author stagiaire L'onglet d'affichage de Chase. contient l'affichage de la Bdd Source, des
 * regles et du resultat de l'algorithme de Chase.
 */
public class VueChase extends JPanel {

  public VueBddRes vueRes;
  public DefaultListModel<Atom> auxListModel;
  public Bdd resBdd;
  VueBdd vueBdd;
  VueRegles vueRegles;
  VueStat vueStat;
  VueSetUpChase setUp;
  ChaseUI mainFrame;

  VueChase(ChaseUI mf) {
    super();
    this.setLayout(new BorderLayout());

    this.mainFrame = mf;

    this.setUp = new VueSetUpChase(mf);

    this.vueBdd = new VueBddSource("Initial database : ", mf);

    this.vueRegles = new VueRegles("Constraints : ", mf);

    this.auxListModel = new DefaultListModel<>();

    this.vueRes = new VueBddResChase("Atom(s) that will be inserted : ", mf, auxListModel, this);

    this.vueStat = new VueStat();
    this.resBdd = new BddMemoire();

    JPanel centre = new JPanel();
    centre.setLayout(new GridLayout(1, 3));

    centre.add(this.vueBdd);
    centre.add(this.vueRegles);
    centre.add(this.vueRes);

    this.add(this.setUp, BorderLayout.NORTH);
    this.add(centre, BorderLayout.CENTER);
    this.add(this.vueStat, BorderLayout.SOUTH);

  }

  public void populateJList() {
    this.auxListModel.clear();
    for (Atom atom : this.resBdd) {
      this.auxListModel.addElement(atom);
    }
  }
}
