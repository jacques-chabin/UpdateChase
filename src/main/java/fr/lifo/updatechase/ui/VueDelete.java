package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

public class VueDelete extends JPanel {

  public VueBdd vueDel;
  public VueBddRes vueRes;
  public DefaultListModel<Atom> resListModel;
  public DefaultListModel<Atom> delAtomeListModel;
  public Bdd resBdd;
  public BddMemoire delAtomeBdd;
  VueBddSource vueBddSource;
  VueRegles vueRegles;
  VueSetUpDelete setUp;
  ChaseUI mainFrame;
  VueStat vueStat;

  VueDelete(ChaseUI mf) {
    super();
    this.setLayout(new BorderLayout());

    this.mainFrame = mf;

    //creation de la vue SetUp
    this.setUp = new VueSetUpDelete(mf);

    //creation de la vue Bdd Source
    this.vueBddSource = new VueBddSource("Initial database : ", mf);

    //creation de la vue Regles
    this.vueRegles = new VueRegles("Constraints : ", mf);

    this.resListModel = new DefaultListModel<>();
    this.delAtomeListModel = new DefaultListModel<>();

    //creation des vues des atomes que l'ont veut supprimer et de la base de données résultat
    this.vueDel = new VueBddDel("Atom(s) to delete : ", mf, delAtomeListModel);
    this.vueRes = new VueBddResDel("Database after deletion : ", mf, resListModel, this);

    this.vueStat = new VueStat();// NE SERT A RIEN ?

    this.resBdd = new BddMemoire();
    this.delAtomeBdd = new BddMemoire();

    JPanel centre = new JPanel();
    centre.setLayout(new GridLayout(1, 3));

    JPanel centreDroit = new JPanel();
    centreDroit.setLayout(new GridLayout(2, 1));

    centreDroit.add(vueDel);
    centreDroit.add(vueRes);

    centre.add(this.vueBddSource);
    centre.add(this.vueRegles);
    centre.add(centreDroit);

    this.add(this.setUp, BorderLayout.NORTH);
    this.add(centre, BorderLayout.CENTER);
    this.add(this.vueStat, BorderLayout.SOUTH);
  }

  public void addAtomeToJListBddDel(Atom a) {
    if (a != null && !delAtomeListModel.contains(a)) {
      delAtomeListModel.addElement(a);
    }
  }

  public void addAllAtomeToJListBddDel() {
    for (Atom atom : this.delAtomeBdd) {
      this.delAtomeListModel.addElement(atom);
    }
  }

  public Bdd removeAtomeFromJListBddDel(int[] index) {
    Atom a;
    Bdd aux = new BddMemoire();
    for (int i = index.length - 1; i > -1; i--) {
      a = this.delAtomeListModel.getElementAt(index[i]);
      this.delAtomeListModel.remove(index[i]);
      aux.add(a);
    }
    return aux;
  }

  public void populateJList() {
    this.resListModel.clear();
    for (Atom atom : this.resBdd) {
      this.resListModel.addElement(atom);
    }
  }

  public void commentAtomeFromJListBddDel(int[] index) {
    Atom a;
    for (int i = index.length - 1; i > -1; i--) {
      a = this.delAtomeListModel.getElementAt(index[i]);
      if (!a.getName().startsWith("%")) {
        delAtomeBdd.remove(a);
        a.setName("%" + a.getName());
        delAtomeListModel.setElementAt(a, index[i]);
      } else {
        a.setName(a.getName().substring(1));
        delAtomeBdd.add(a);
        delAtomeListModel.setElementAt(a, index[i]);
      }
    }
  }
}
