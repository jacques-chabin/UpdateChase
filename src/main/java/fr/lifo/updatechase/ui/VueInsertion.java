package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 * @author stagiaire L'onglet d'affichage de l'Insertion. Contient l'affichage de la Bdd Source, des
 * regles, de la Bdd a ajouter a la Bdd Source et du resultat de l'algorithme d'insertion.
 */
public class VueInsertion extends JPanel {

  public VueBdd vueBddSource;
  public VueRegles vueRegles;
  public VueBdd vueAdd;
  public VueBddRes vueRes;
  public VueStat vueStat;
  public VueSetUpAdd setUp;
  public DefaultListModel<Atom> auxListModel;
  public DefaultListModel<Atom> addAtomeListModel;
  public Bdd resBdd;
  public BddMemoire addAtomeBdd;
  ChaseUI mainFrame;

  VueInsertion(ChaseUI mf) {
    super();
    this.setLayout(new BorderLayout());

    this.mainFrame = mf;

    this.setUp = new VueSetUpAdd(mf);

    this.vueBddSource = new VueBddSource("Initial database : ", mf);

    this.vueRegles = new VueRegles("Constraints : ", mf);

    this.auxListModel = new DefaultListModel<>();
    this.addAtomeListModel = new DefaultListModel<>();

    this.vueAdd = new VueBddAdd("Atom(s) to insert : ", mf, addAtomeListModel);
    this.vueRes = new VueBddResAdd("Atom(s) that will be inserted : ", mf, auxListModel, this);

    this.vueStat = new VueStat();
    this.resBdd = new BddMemoire();
    this.addAtomeBdd = new BddMemoire();

    JPanel centre = new JPanel();
    centre.setLayout(new GridLayout(1, 3));

    JPanel centreDroit = new JPanel();
    centreDroit.setLayout(new GridLayout(2, 1));

    centreDroit.add(vueAdd);
    centreDroit.add(vueRes);

    centre.add(this.vueBddSource);
    centre.add(this.vueRegles);
    centre.add(centreDroit);

    this.add(this.setUp, BorderLayout.NORTH);
    this.add(centre, BorderLayout.CENTER);
    this.add(this.vueStat, BorderLayout.SOUTH);
  }

  public void addAtomeToJListBddAdd(Atom a) {
    if (a != null && !addAtomeListModel.contains(a)) {
      addAtomeListModel.addElement(a);
    }
  }

  public void addAllAtomeToJListBddAdd() {
    for (Atom atom : this.addAtomeBdd) {
      this.addAtomeListModel.addElement(atom);
    }
  }

  public Bdd removeAtomeFromJListBddAdd(int[] index) {
    Atom a;
    Bdd aux = new BddMemoire();
    for (int i = index.length - 1; i > -1; i--) {
      a = this.addAtomeListModel.getElementAt(index[i]);
      this.addAtomeListModel.remove(index[i]);
      aux.add(a);
    }
    return aux;
  }

  public void populateJList() {
    this.auxListModel.clear();
    for (Atom atom : this.resBdd) {
      this.auxListModel.addElement(atom);
    }
  }

  public void commentAtomeFromJListBddAdd(int[] index) {
    Atom a;
    for (int i = index.length - 1; i > -1; i--) {
      a = this.addAtomeListModel.getElementAt(index[i]);
      if (!a.getName().startsWith("%")) {
        addAtomeBdd.remove(a);
        a.setName("%" + a.getName());
        addAtomeListModel.setElementAt(a, index[i]);
      } else {
        a.setName(a.getName().substring(1));
        addAtomeBdd.add(a);
        addAtomeListModel.setElementAt(a, index[i]);
      }
    }
  }
}
