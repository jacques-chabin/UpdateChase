package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurAjoutAtomeBddDel;
import fr.lifo.updatechase.controller.ControlleurCommentAtomeBddDel;
import fr.lifo.updatechase.controller.ControlleurSupprimerSelectionAtomeBddDel;

/**
 * @author Julien Revaud Vue d'ajout d'atome a une Bdd specifique a la vue Bdd Del.
 */
public class VueAjoutAtomeBddDel extends VueAjoutAtomeBdd {

  VueAjoutAtomeBddDel(ChaseUI mf, VueBddDel vueBdd) {
    super(vueBdd);
    ControlleurAjoutAtomeBddDel action = new ControlleurAjoutAtomeBddDel(mf, vueBdd);
    b_add.addActionListener(action);
    b_remove.addActionListener(new ControlleurSupprimerSelectionAtomeBddDel(mf, vueBdd));
    b_comment.addActionListener(new ControlleurCommentAtomeBddDel(vueBdd, mf));
    inputText.addActionListener(action);
  }
}
