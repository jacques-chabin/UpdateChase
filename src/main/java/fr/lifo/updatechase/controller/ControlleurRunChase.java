package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueSetUpChase;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui recupere les parametres necessaire pour lancer le Chase, lance
 * le Chase et met a jour la vue de resultat.
 */
public class ControlleurRunChase implements ActionListener {

  ChaseUI mainFrame;
  VueSetUpChase vsu;

  public ControlleurRunChase(ChaseUI mf, VueSetUpChase vueSetUp) {
    super();
    mainFrame = mf;
    vsu = vueSetUp;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    Bdd db = mainFrame.getBddSource();
    LinkedListRules llr = mainFrame.getRegles();
    boolean restreint = vsu.isRestricted();
    int cycle;
    try {
      cycle = Integer.parseInt(vsu.cycleC.getText());
      if (cycle > -3) {
        if (!(db == null) && db.size() > 0) {
          if (!(llr == null) && llr.size() > 0) {
            if (!LinkedListRules.risqueBoucleInfinie(llr, cycle)) {
              long tempsChase = System.currentTimeMillis();
              mainFrame.vueChase.resBdd = QS.mainChaseTransaction(db, llr, cycle, restreint);
              long tempsFin = System.currentTimeMillis();

              float temps = (tempsFin - tempsChase) / 1000f;
              vsu.setExecTime(temps);

              mainFrame.vueChase.populateJList();
              mainFrame.vueChase.vueRes.setCount(mainFrame.vueChase.vueRes.getBddSize());
            } else {
              JOptionPane.showMessageDialog(vsu, "Un risque de boucle infinie a été détecté, "
                      + "veuillez définir une condition de boucle avant de continuer", "ATTENTION",
                  JOptionPane.WARNING_MESSAGE);
            }
          } else {
            JOptionPane.showMessageDialog(vsu, "Aucune regle a appliquer.");
          }
        } else {
          JOptionPane.showMessageDialog(vsu, "La base de donnee source est vide");
        }
      } else {
        JOptionPane.showMessageDialog(vsu, "Un entier N > -3 doit etre entre");
        vsu.cycleC.setText("0");
      }
    } catch (NumberFormatException e) {
      JOptionPane.showMessageDialog(vsu, "Un entier doit etre entre");
      vsu.cycleC.setText("0");
    }

  }

}
