package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueSetUpAdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui se charge de recuperer les parametre necessaire a l'algorithme
 * d'ajout, lance l'algorithme d'ajout et met a jour la vue de resultat dans Insertion.
 */
public class ControlleurRunAdd implements ActionListener {

  ChaseUI mainFrame;
  VueSetUpAdd vueSetUpAdd;

  public ControlleurRunAdd(ChaseUI mf, VueSetUpAdd vueSetUpAdda) {
    super();
    mainFrame = mf;
    vueSetUpAdd = vueSetUpAdda;

  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    Bdd db = mainFrame.getBddSource();

    LinkedListRules llr = mainFrame.getRegles();

    boolean restreint = vueSetUpAdd.isRestricted();
    int cycle;
    try {
      cycle = Integer.parseInt(vueSetUpAdd.cycleC.getText());
      if (cycle > -3) {
        if (!(db == null)) {
          if (!(llr == null) /*&& llr.size()>0*/) {
            if (!LinkedListRules.risqueBoucleInfinie(llr, cycle)) {
              Bdd bddToAdd = mainFrame.vueInsertion.addAtomeBdd;
              mainFrame.vueInsertion.addAtomeBdd = new BddMemoire();
              mainFrame.vueInsertion.addAtomeListModel.clear();
              mainFrame.vueInsertion.vueAdd.setCount(mainFrame.vueInsertion.addAtomeBdd.size());

              long tempsAdd = System.currentTimeMillis();
              mainFrame.vueInsertion.resBdd = QS.mainAdd(db, bddToAdd, llr, cycle, restreint);
              long tempsFin = System.currentTimeMillis();

              float temps = (tempsFin - tempsAdd) / 1000f;
              vueSetUpAdd.setExecTime(temps);

              mainFrame.vueInsertion.populateJList();
              mainFrame.vueInsertion.vueRes.setCount(mainFrame.vueInsertion.vueRes.getBddSize());
            } else {
              JOptionPane.showMessageDialog(vueSetUpAdd,
                  "Un risque de boucle infinie a été détecté, "
                      + "veuillez définir une condition de boucle avant de continuer", "ATTENTION",
                  JOptionPane.WARNING_MESSAGE);
            }
          } else {
            JOptionPane.showMessageDialog(vueSetUpAdd, "Aucune regle a appliquer.");
          }
        } else {
          JOptionPane.showMessageDialog(vueSetUpAdd,
              "La base de données source n'a pas été créé : \n"
                  + "Nous vous conseillons de redemarrer l'application. \n"
                  + "Pensez cependant à bien enregister votre travail !", "ERREUR",
              JOptionPane.ERROR_MESSAGE);
        }
      } else {
        JOptionPane.showMessageDialog(vueSetUpAdd, "Un entier N > -3 doit etre entre");
        vueSetUpAdd.cycleC.setText("0");
      }
    } catch (NumberFormatException e) {
      JOptionPane.showMessageDialog(vueSetUpAdd, "Un entier doit etre entre");
      vueSetUpAdd.cycleC.setText("0");
    }

  }

}
