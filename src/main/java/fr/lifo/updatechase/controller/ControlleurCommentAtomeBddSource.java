package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.VueBdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlleurCommentAtomeBddSource implements ActionListener {

  VueBdd vueBdd;

  public ControlleurCommentAtomeBddSource(VueBdd vb) {
    super();
    vueBdd = vb;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;

    index = vueBdd.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      vueBdd.mainFrame.commentAtomeFromJListBddSource(index);

      System.out.println("Bdd apres comment :" + vueBdd.mainFrame.bddSource);
      vueBdd.mainFrame.updateAllCounter();
      System.out.println("LISTMODEL APRES comment" + vueBdd.mainFrame.bddListModel);
    }

  }

}
