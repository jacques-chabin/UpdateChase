package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueCore;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui valide le resultat de l'algorithme de Core en remplacant la Bdd
 * Source par la Bdd resultat de Core, puis met a jour les affichages.
 */
public class ControlleurValiderCore implements ActionListener {

  VueCore vueCore;
  ChaseUI mainFrame;

  public ControlleurValiderCore(VueCore vueCore, ChaseUI mainFrame) {
    super();
    this.vueCore = vueCore;
    this.mainFrame = mainFrame;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (!vueCore.resBdd.isEmpty()) {

      int n = JOptionPane.showConfirmDialog(vueCore,
          "Attention !\nLa Bdd resultat remplacera la Bdd source qui est commune au chase, core et add !",
          "Voulez-vous continuer ?", JOptionPane.YES_NO_OPTION);
      if (n == JOptionPane.YES_OPTION) {

        long temps = System.currentTimeMillis();
        mainFrame.bddSource = vueCore.resBdd;
        long tempsFin = System.currentTimeMillis();

        vueCore.vueRes.setExecTime((tempsFin - temps) / 1000f);

        mainFrame.bddListModel = vueCore.auxListModel;
        mainFrame.updateAllVueBddSource();

        vueCore.resBdd = new BddMemoire();
        vueCore.auxListModel = new DefaultListModel<>();
        vueCore.vueRes.bddDisplay.setModel(vueCore.auxListModel);
        mainFrame.updateAllCounter();
        vueCore.vueRes.setCount(0);
      }
    } else {
      JOptionPane.showMessageDialog(vueCore,
          "Cette Bdd est vide. Auncun changement ne sera effectue");
    }

  }

}
