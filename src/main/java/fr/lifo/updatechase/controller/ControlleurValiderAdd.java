package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controleur valide le resultat de l'algorithme d'ajout en ajoutant les Atome du
 * resultat a la Bdd Source.
 */
public class ControlleurValiderAdd implements ActionListener {

  public static boolean WITH_CORE = true;
  public static boolean WITHOUT_CORE = false;

  ChaseUI mainFrame;
  boolean option;

  public ControlleurValiderAdd(ChaseUI mf, boolean opt) {
    mainFrame = mf;
    option = opt; //instancié avec WITH_CORE ou WITHOUT_CORE
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    if (!mainFrame.vueInsertion.resBdd.isEmpty()) {
      System.out.println(
          "CYCLE " + Integer.parseInt(mainFrame.vueInsertion.setUp.cycleC.getText()));
      if ((Integer.parseInt(mainFrame.vueInsertion.setUp.cycleC.getText())
          > mainFrame.vueInsertion.resBdd.getMaxProf()) || (
          Integer.parseInt(mainFrame.vueInsertion.setUp.cycleC.getText()) <= 0)) {
        int n = JOptionPane.showConfirmDialog(mainFrame.vueInsertion,
            "Attention !\nLa Bdd resultat sera ajoute a la Bdd source qui est commune au chase, core et add !",
            "Voulez-vous continuer ?", JOptionPane.YES_NO_OPTION);
        if (n == JOptionPane.YES_OPTION) {
          long tempsAdd = System.currentTimeMillis();
          mainFrame.bddSource.addAll(mainFrame.vueInsertion.resBdd);
          long tempsFinAdd = System.currentTimeMillis();
          long temps = tempsFinAdd - tempsAdd;
          mainFrame.addAllAtomeToJList(mainFrame.vueInsertion.resBdd);
          if (option) {
            long tempsCore = System.currentTimeMillis();
            Bdd newBdd = QS.mainCore(mainFrame.getBddSource());
            mainFrame.setBddSource(newBdd);
            long tempsCoreFin = System.currentTimeMillis();
            temps += tempsCoreFin - tempsCore;

            mainFrame.getListModelBddSource().clear();
            mainFrame.addAllAtomeToJList(newBdd);
          }

          mainFrame.vueInsertion.vueRes.setExecTime(temps / 1000f);

          mainFrame.updateAllVueBddSource();

          mainFrame.vueInsertion.resBdd = new BddMemoire();
          mainFrame.vueInsertion.auxListModel = new DefaultListModel<>();
          mainFrame.vueInsertion.vueRes.bddDisplay.setModel(mainFrame.vueInsertion.auxListModel);
          mainFrame.updateAllCounter();
          mainFrame.vueInsertion.vueRes.setCount(0);
        }
      } else {
        JOptionPane.showMessageDialog(mainFrame.vueInsertion,
            "Insertion refusé, contraintes non respectées !", "ATTENTION",
            JOptionPane.WARNING_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(mainFrame.vueInsertion,
          "Cette Bdd est vide. Auncun changement ne sera effectue");
    }

  }

}
