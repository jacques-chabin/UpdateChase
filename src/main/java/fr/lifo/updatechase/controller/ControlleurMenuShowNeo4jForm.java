package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueNeo4jForm;
import java.awt.event.ActionEvent;

public class ControlleurMenuShowNeo4jForm extends ControlleurMenu {

  public ControlleurMenuShowNeo4jForm(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    new VueNeo4jForm(this.mainFrame);
  }

}
