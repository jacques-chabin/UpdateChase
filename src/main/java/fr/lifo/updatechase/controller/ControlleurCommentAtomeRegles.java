package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.VueRegles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlleurCommentAtomeRegles implements ActionListener {

  VueRegles vueRegles;

  public ControlleurCommentAtomeRegles(VueRegles vr) {
    super();
    vueRegles = vr;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;

    index = vueRegles.ruleDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      vueRegles.mainFrame.commentRuleFromJListBddSource(index);

      System.out.println("Regles apres comment :" + vueRegles.mainFrame.rules);
      vueRegles.mainFrame.updateAllRuleCounter();
      System.out.println("LISTMODEL APRES comment" + vueRegles.mainFrame.reglesListModel);
    }

  }

}
