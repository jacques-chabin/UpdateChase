package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.VueNeo4jForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlleurSaveConfig implements ActionListener {

  private final VueNeo4jForm neo4jForm;

  public ControlleurSaveConfig(VueNeo4jForm neo4jForm) {
    this.neo4jForm = neo4jForm;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    this.neo4jForm.saveConfig();
  }
}
