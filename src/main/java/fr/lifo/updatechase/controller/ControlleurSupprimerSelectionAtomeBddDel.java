package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBddDel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author stagiaire Controlleur qui effectue la suppression des atomes selectionnes dans la JList
 * de la Bdd Add et la BddAdd courante.
 */
public class ControlleurSupprimerSelectionAtomeBddDel implements ActionListener {

  VueBddDel vueBddDel;
  ChaseUI mainFrame;

  public ControlleurSupprimerSelectionAtomeBddDel(ChaseUI mf, VueBddDel vbd) {
    super();
    vueBddDel = vbd;
    mainFrame = mf;

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;
    Bdd aux = new BddMemoire();

    index = vueBddDel.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      //suppression des atomes selectionné dans la liste des atomes à supprimer.
      //retourne une Bdd contenant les atomes supprimés de la liste
      aux = mainFrame.vueDelete.removeAtomeFromJListBddDel(index);

      //suppression des atomes de la Bdd aux dans la Bdd des atomes à supprimer
      mainFrame.vueDelete.delAtomeBdd.destroyIntersectionWith(aux);

      System.out.println("Bdd apres suppression :" + mainFrame.vueDelete.delAtomeBdd);
      //mise à jour du compteur dans la partie d'ajout des atomes à supprimer
      mainFrame.vueDelete.vueDel.setCount(mainFrame.vueDelete.delAtomeBdd.size());
      System.out.println("LISTMODEL APRES SUPPRESSION" + mainFrame.vueDelete.delAtomeListModel);
    }

  }

}
