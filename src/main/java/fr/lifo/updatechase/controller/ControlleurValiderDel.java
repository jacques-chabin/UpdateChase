package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controleur valide le resultat de l'algorithme d'ajout en ajoutant les Atome du
 * resultat a la Bdd Source.
 */
public class ControlleurValiderDel implements ActionListener {

  ChaseUI mainFrame;

  public ControlleurValiderDel(ChaseUI mf) {
    mainFrame = mf;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    int n = JOptionPane.showConfirmDialog(mainFrame.vueInsertion,
        "Attention !\nLa Bdd resultat remplacera la Bdd source qui est commune au chase, core et add !",
        "Voulez-vous continuer ?",
        JOptionPane.YES_NO_OPTION);
    if (n == JOptionPane.YES_OPTION) {
      long temps = System.currentTimeMillis();
      mainFrame.setBddSource(mainFrame.vueDelete.resBdd);
      long tempsFin = System.currentTimeMillis();

      mainFrame.vueDelete.vueRes.setExecTime((tempsFin - temps) / 1000f);

      mainFrame.setListModelBddSource(mainFrame.vueDelete.resBdd);
      //met à jour les vues affichant la Bdd Source
      mainFrame.updateAllVueBddSource();

      mainFrame.vueDelete.resBdd = new BddMemoire();
      mainFrame.vueDelete.resListModel = new DefaultListModel<>();
      mainFrame.vueDelete.vueRes.bddDisplay.setModel(mainFrame.vueDelete.resListModel);
      mainFrame.updateAllCounter();
      mainFrame.vueDelete.vueRes.setCount(0);
    }
  }

}
