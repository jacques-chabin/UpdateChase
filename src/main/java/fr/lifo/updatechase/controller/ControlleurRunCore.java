package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueSetUpCore;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author stagiaire Controlleur qui s'occupe de recuperer lse parametre necessaires pour
 * l'algorithme de Core, lance l'algorithme de Core et met a jour la vue de resultat de Core.
 */
public class ControlleurRunCore implements ActionListener {

  ChaseUI mainFrame;
  VueSetUpCore vueSetUpCore;

  public ControlleurRunCore(ChaseUI mf, VueSetUpCore vsuc) {
    super();
    mainFrame = mf;
    vueSetUpCore = vsuc;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Bdd db = mainFrame.getBddSource();

    if (!(db == null)) {
      if (!db.isEmpty()) {
        long tempsCore = System.currentTimeMillis();
        mainFrame.vueCore.resBdd = QS.mainCore(db);
        long tempsFin = System.currentTimeMillis();

        float temps = (tempsFin - tempsCore) / 1000f;
        vueSetUpCore.setExecTime(temps);

        mainFrame.vueCore.populateJList();
        mainFrame.vueCore.vueRes.setCount(mainFrame.vueCore.vueRes.getBddSize());
      }
    }
  }

}
