package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui sauvegarde l'etat de la LinkedListRule courante dans un fichier
 * texte.
 */
public class ControlleurMenuSaveRules extends ControlleurMenu {

  public ControlleurMenuSaveRules(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Enregistrer Les Regles");
    try {
      fc.setCurrentDirectory(new File(".").getCanonicalFile());
    } catch (Exception e1) {
    }
    if (!mainFrame.rules.isEmpty()) {
      int retour = fc.showSaveDialog(mainFrame);
      if (retour == JFileChooser.APPROVE_OPTION) {
        // un fichier a été choisi (sortie par OK)
        // chemin absolu du fichier choisi

        try {
          String path = fc.getSelectedFile().getAbsolutePath();
          mainFrame.rules.toFile(path);
          JOptionPane.showConfirmDialog(mainFrame, "Les regles ont bien ete enregistrees", "Succes",
              JOptionPane.DEFAULT_OPTION);
        } catch (IOException e1) {
          JOptionPane.showConfirmDialog(mainFrame,
              "Erreur : Impossible d'enregistrer le Fichier : " + e1.getMessage(), "Erreur",
              JOptionPane.DEFAULT_OPTION);
        }
      }
    } else {
      JOptionPane.showConfirmDialog(mainFrame, "Erreur : Il n'y a aucune regle a enregistrer.",
          "Erreur", JOptionPane.DEFAULT_OPTION);
    }

  }

}
