package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.VueBdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author stagiaire Controlleur qui effectue la suppression des atomes selectionnes dans les JList
 * de la Bdd Source et la Bdd Source courante.
 */
public class ControlleurSupprimerSelectionAtomeBddSource implements ActionListener {

  VueBdd vb;

  public ControlleurSupprimerSelectionAtomeBddSource(VueBdd vueBdd) {
    super();
    this.vb = vueBdd;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    int[] index;
    Atom a;
    Bdd aux = new BddMemoire();

    index = vb.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      aux = vb.mainFrame.removeAtomeFromJListBddSource(index);

      System.out.println(aux);

      vb.mainFrame.bddSource.removeAll(aux);

      System.out.println("Bdd apres suppression :" + vb.mainFrame.bddSource);
      vb.mainFrame.updateAllCounter();
      System.out.println("LISTMODEL APRES SUPPRESSION" + vb.mainFrame.bddListModel);
    }

  }

}
