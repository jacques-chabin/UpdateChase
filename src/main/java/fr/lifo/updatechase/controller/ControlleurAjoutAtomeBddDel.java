package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBddDel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Le controlleur qui gere l'ajout d'un Atome a la Bdd d'Insertion et la mise a jour de
 * l'affichage.
 *
 * @author Julien Revaud
 */
public class ControlleurAjoutAtomeBddDel implements ActionListener {

  VueBddDel vueBddDel;
  ChaseUI mainFrame;

  public ControlleurAjoutAtomeBddDel(ChaseUI mf, VueBddDel vba) {
    super();
    vueBddDel = vba;
    mainFrame = mf;
  }

  /* (non-Javadoc)
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    Atom atom;
    String atomeString;

    atomeString = this.vueBddDel.getInputText();
    atomeString = atomeString.replaceAll(" ", ""); //suppression des espaces en trop

    try {
      atom = Atom.stringToAtom(atomeString);

      BddMemoire futuresAtomesToDel = new BddMemoire();
      futuresAtomesToDel.addAll(mainFrame.vueDelete.delAtomeBdd);
      futuresAtomesToDel.add(atom);
      if (!futuresAtomesToDel.atomesAreLinkedByNull()) {
        //ajoute l'atome dans la Bdd des atomes à supprimer
        mainFrame.vueDelete.delAtomeBdd.add(atom);
        System.out.println("Atome : " + atom + " a ete ajoute");

        //ajoute l'atome dans la liste des atomes à supprimer
        this.mainFrame.vueDelete.addAtomeToJListBddDel(atom);

        System.out.println("Bdd apres ajout " + mainFrame.vueDelete.delAtomeBdd);
        System.out.println("LISTMODEL " + mainFrame.vueDelete.delAtomeListModel);

        //set le compteur d'atome à jour dans la section d'ajout des atomes
        mainFrame.vueDelete.vueDel.setCount(mainFrame.vueDelete.delAtomeBdd.size());
        vueBddDel.emptyInputText();
        vueBddDel.vueAjoutAtomeBdd.inputText.requestFocus();
      } else {
        JOptionPane.showMessageDialog(this.vueBddDel,
            "Les Atomes à supprimer ne peuvent pas\n" + "être liés par des nuls",
            "ATTENTION", JOptionPane.WARNING_MESSAGE);
      }

    } catch (SyntaxError se) {
      JOptionPane.showMessageDialog(this.vueBddDel,
          "Erreur de syntaxe detectee\n" + "Syntaxe d'un atome :\n" + "nomPredicat('element').\n"
              + "  ou\n"
              + "nomPredicat('element1','element2').");
    }

  }

}
