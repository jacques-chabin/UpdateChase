package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueChase;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui valide le resultat de l'algorithme de Chase en remplacant la
 * Bdd Source par la Bdd resultat de Chase
 */
public class ControlleurValiderChase implements ActionListener {

  ChaseUI cui;
  VueChase vc;

  public ControlleurValiderChase(ChaseUI cui, VueChase vc) {
    super();
    this.cui = cui;
    this.vc = vc;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    if (!vc.resBdd.isEmpty()) {

      int n = JOptionPane.showConfirmDialog(vc,
          "Attention !\nLa Bdd resultat remplacera la Bdd source qui est commune au chase, core et add !",
          "Voulez-vous continuer ?", JOptionPane.YES_NO_OPTION);
      if (n == JOptionPane.YES_OPTION) {

        long temps = System.currentTimeMillis();
        cui.bddSource.addAll(vc.resBdd);
        long tempsFin = System.currentTimeMillis();

        vc.vueRes.setExecTime((tempsFin - temps) / 1000f);

        // pour conserver les atomes en commentaire
        DefaultListModel<Atom> aux = cui.bddListModel;
        for (int i = 0; i < aux.size(); i++) {
          if (aux.getElementAt(i).getName().startsWith("%")) {
            vc.auxListModel.addElement(aux.getElementAt(i));
          }
        }
        //--

        cui.addAllAtomeToJList(vc.resBdd);
        cui.updateAllVueBddSource();

        vc.resBdd = new BddMemoire();

        vc.auxListModel = new DefaultListModel<>();
        vc.vueRes.bddDisplay.setModel(vc.auxListModel);
        cui.updateAllCounter();
        vc.vueRes.setCount(0);
      }
    } else {
      JOptionPane.showMessageDialog(vc, "Cette Bdd est vide. Auncun changement ne sera effectue");
    }

  }

}
