package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.ui.VueRegles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author stagiaire Controlleur qui effectue la suppression des Rules selectionnees dans les JList
 * des Regles et la LinkedListRules courante.
 */
public class ControlleurSupprimerSelectionRegle implements ActionListener {

  VueRegles vr;

  public ControlleurSupprimerSelectionRegle(VueRegles v) {
    super();
    vr = v;

  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    int[] index;
    Rule r;
    LinkedListRules aux = new LinkedListRules();

    index = vr.ruleDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      aux = vr.mainFrame.removeRulesFromJListModel(index);

      System.out.println(aux);

      vr.mainFrame.rules.destroyIntersectionWith(aux);

      System.out.println("regles apres suppression :" + vr.mainFrame.rules);
      vr.mainFrame.updateAllRuleCounter();
      System.out.println("LISTMODEL APRES SUPPRESSION" + vr.mainFrame.reglesListModel);
    }

  }

}
