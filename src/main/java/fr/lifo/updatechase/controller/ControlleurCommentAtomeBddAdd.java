package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlleurCommentAtomeBddAdd implements ActionListener {

  VueBdd vueBdd;

  public ControlleurCommentAtomeBddAdd(VueBdd vb, ChaseUI mf) {
    super();
    vueBdd = vb;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;

    index = vueBdd.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      vueBdd.mainFrame.vueInsertion.commentAtomeFromJListBddAdd(index);

      System.out.println("Bdd apres comment :" + vueBdd.mainFrame.vueInsertion.addAtomeBdd);
      vueBdd.mainFrame.vueInsertion.vueAdd.setCount(
          vueBdd.mainFrame.vueInsertion.addAtomeBdd.size());
      System.out.println(
          "LISTMODEL APRES comment" + vueBdd.mainFrame.vueInsertion.addAtomeListModel);
    }

  }

}
