package fr.lifo.updatechase.model.db.cypher.strategy;

import fr.lifo.updatechase.model.logic.Atom;
import java.util.Collection;
import java.util.Comparator;

public class NullDegreeConstantNeoQueryStrategy implements NeoQueryStrategyInterface {

  @Override
  public Atom getBFSRoot(Collection<Atom> atoms) {
    return atoms.parallelStream().filter(Atom::containsConstantValues)
        .max(Comparator.comparingLong(Atom::getNbNulls)).orElse(null);
  }

}
