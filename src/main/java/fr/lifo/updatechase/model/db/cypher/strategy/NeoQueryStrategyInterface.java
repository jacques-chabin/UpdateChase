package fr.lifo.updatechase.model.db.cypher.strategy;

import fr.lifo.updatechase.model.logic.Atom;
import java.util.Collection;

public interface NeoQueryStrategyInterface {

  Atom getBFSRoot(Collection<Atom> atoms);
}
