package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.cypher.query.LocalElementIndexQueryBuilder;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@SuppressWarnings("ConstantExpression")
public class BddCypher extends Bdd {

  private final Connection connection;
  private int nbQueries;
  private String queryPrefix = "";

  public BddCypher(String uri) {
    this(uri, null, null);
  }

  public BddCypher(String uri, String user, String password) {
    this.nbQueries = 0;

    Connection connection;
    try {
      if (user != null && password != null) {
        connection = DriverManager.getConnection(uri, user, password);
      } else {
        connection = DriverManager.getConnection(uri);
      }

    } catch (SQLException e) {
      System.out.println("Echec de connexion!");
      System.out.println(e.getMessage());
      connection = null;
    }

    this.connection = connection;

    if (this.isConnected()) {
      // Init AgensGraph
      if (uri.startsWith("jdbc:agensgraph")) {
        System.out.println("Setup AgensGraph");
        try (Statement statement = connection.createStatement()) {
          statement.execute("CREATE GRAPH IF NOT EXISTS null_graph");
          statement.execute("SET GRAPH_PATH=null_graph");
        } catch (SQLException e) {
          System.err.println(e.getMessage());
        }
      } else if (uri.startsWith("jdbc:postgresql")) {
        this.queryPrefix = "{cypher} ";
      }

      try (Statement statement = connection.createStatement()) {
        this.postInit(statement);

      } catch (SQLException e) {
        System.err.println(e.getMessage());
      }
    }

  }

  protected void postInit(Statement statement) throws SQLException {
    if (this.queryPrefix.isEmpty()) {
      statement.execute(this.queryPrefix + "CREATE INDEX atomName FOR (a:Atom) ON (a.name)");
      statement.execute(
          this.queryPrefix + "CREATE CONSTRAINT constName ON (n:Element) ASSERT n.name IS UNIQUE");
      //statement.execute(this.queryPrefix + "CREATE INDEX atomRank FOR ()-[r:Contains]->() ON (r.rank)");
    }
  }

  public boolean isConnected() {
    try {
      return this.connection != null && !this.connection.isClosed();
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return false;
  }

  private static Atom resultSetToAtom(ResultSet resultSet) throws SQLException {
    String pred = resultSet.getString("a");
    List<Object> elementNames = Arrays.asList((Object[]) resultSet.getArray("e").getArray());
    return BddNeo4j.buildAtom(pred, elementNames);
  }

  private static Set<Atom> resultSetToListAtom(ResultSet resultSet) throws SQLException {
    Set<Atom> instance = new HashSet<>();

    for (int j = 0; j < resultSet.getMetaData().getColumnCount() / 3; ++j) {
      String pred = resultSet.getString("a" + j);
      List<Object> elementNames = Arrays.asList((Object[]) resultSet.getArray("e" + j).getArray());
      instance.add(BddNeo4j.buildAtom(pred, elementNames));
    }

    return instance;
  }

  ////// Closable ////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public void close() {
    try {
      this.connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  ////// Collection //////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean add(Atom atom) {
    //noinspection RedundantCollectionOperation
    return this.addAll(Collections.singleton(atom));
  }

  @Override
  public boolean addAll(Collection<? extends Atom> atoms) {
    if (atoms == null || atoms.isEmpty()) {
      return false;
    }

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms(atoms);

    // ---- Execute queries ----
    int updateSize;
    int nbNodesCreated = 0;
    this.nbQueries += 3;

    // Create constants
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS c MERGE (:Element:Constant {name: c})")) {
      Array constants = this.connection.createArrayOf("VARCHAR",
          ((Collection<?>) params.get("constants")).toArray());
      statement.setArray(1, constants);
      updateSize = statement.executeUpdate();
      nbNodesCreated += updateSize;
      System.out.println(updateSize + " constants added");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Create nulls
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS n MERGE (:Element:Null {name: n})")) {
      Array nulls = this.connection.createArrayOf("VARCHAR",
          ((Collection<?>) params.get("nulls")).toArray());
      statement.setArray(1, nulls);
      updateSize = statement.executeUpdate();
      nbNodesCreated += updateSize;
      System.out.println(updateSize + " nulls added");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Create predicates
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS a "

            // Check atom existence
            + "OPTIONAL MATCH (an:Atom {name:a.name}) "
            + "WHERE ALL(e IN a.elems WHERE (an)-[:Contains {rank:e.rank}]->(:Element {name: e.name})) "

            // Create atom if not exist
            + "WITH a, an " + "WHERE an IS null " + "CREATE (new:Atom {name:a.name}) "

            // Create links with elements
            + "WITH a, new " + "UNWIND a.elems AS e " + "MATCH (en:Element {name:e.name}) "
            + "CREATE (new)-[:Contains {rank:e.rank}]->(en);")) {
      Array atomArray = this.connection.createArrayOf("JAVA_OBJECT",
          ((Collection<?>) params.get("atoms")).toArray());
      statement.setArray(1, atomArray);
      updateSize = statement.executeUpdate();
      nbNodesCreated += updateSize;
      System.out.println(updateSize + " atoms and relations added");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return nbNodesCreated > 0;
  }

  @Override
  public boolean remove(Object o) {
    //noinspection RedundantCollectionOperation
    return this.removeAll(Collections.singleton(o));
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms((Collection<Atom>) c);
    int updateSize = 0;
    this.nbQueries++;

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS a " + "MATCH (an:Atom {name: a.name}) "
            + "WHERE ALL(e IN a.elems WHERE (an)-[:Contains {rank:e.rank}]->(:Element {name: e.name})) "
            + "DETACH DELETE an")) {
      Array atoms = this.connection.createArrayOf("JAVA_OBJECT",
          ((Collection<?>) params.get("atoms")).toArray());
      statement.setArray(1, atoms);
      updateSize = statement.executeUpdate();
      System.out.println(updateSize + " atoms and relations added");

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return updateSize > 0;
  }

  ////// Bdd /////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public int size() {
    this.nbQueries++;
    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (:Atom) RETURN COUNT(*) AS nbAtom LIMIT 1");
      resultSet.next();
      return resultSet.getInt("nbAtom");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return 0;
  }

  @Override
  public void clear() {
    this.nbQueries++;
    try (Statement statement = this.connection.createStatement()) {
      statement.execute(this.queryPrefix + "MATCH (n) DETACH DELETE (n)");

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean contains(Object o) {
    //noinspection RedundantCollectionOperation
    return this.containsAll(Collections.singleton(o));
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    this.nbQueries++;
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    boolean exist = false;
    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms((Collection<Atom>) c);

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS a " + "OPTIONAL MATCH (an:Atom {name:a.name}) "
            + "WHERE ALL(e IN a.elems WHERE (an)-[:Contains {rank:e.rank}]->(:Element {name: e.name})) "
            + "RETURN an IS NOT NULL AS isExist")) {

      Array atoms = this.connection.createArrayOf("JAVA_OBJECT",
          ((Collection<?>) params.get("atoms")).toArray());
      statement.setArray(1, atoms);
      ResultSet resultSet = statement.executeQuery();

      exist = true;
      while (resultSet.next()) {
        exist = exist && resultSet.getBoolean("isExist");
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return exist;
  }

  @Override
  public Iterator<Atom> iterator() {
    return this.stream().iterator();
  }

  /**
   * Get database as stream. It need to be close by calling close() or by using try-resource
   *
   * @return A stream of the database
   */
  @Override
  public Stream<Atom> stream() {
    try {
      Statement statement = this.connection.createStatement();
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (a:Atom)-[r:Contains]->(e:Element) "
              + "WITH a, collect(DISTINCT r.rank) as r, collect(e.name) as e "
              + "RETURN a.name as a, r, e");

      return StreamSupport.stream(new Spliterators.AbstractSpliterator<Atom>(Long.MAX_VALUE,
          Spliterator.ORDERED & Spliterator.IMMUTABLE & Spliterator.DISTINCT) {
        @Override
        public boolean tryAdvance(Consumer<? super Atom> action) {
          try {
            if (resultSet.next()) {
              action.accept(resultSetToAtom(resultSet));
              return true;
            }
          } catch (SQLException e) {
            e.printStackTrace();
          }

          return false;
        }
      }, false).onClose(() -> {
        try {
          statement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      });

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return Stream.empty();
  }

  @Override
  public void initFromDB(Collection<Atom> init) {
    this.clear();

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms(init);

    // ---- Execute queries ----
    int updateSize;
    this.nbQueries += 3;

    // Create constants
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS c CREATE (:Element:Constant {name: c})")) {
      Array constants = this.connection.createArrayOf("VARCHAR",
          ((Collection<?>) params.get("constants")).toArray());
      statement.setArray(1, constants);
      updateSize = statement.executeUpdate();
      System.out.println(updateSize + " constants added");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Create nulls
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS n CREATE (:Element:Null {name: n})")) {
      Array nulls = this.connection.createArrayOf("VARCHAR",
          ((Collection<?>) params.get("nulls")).toArray());
      statement.setArray(1, nulls);
      updateSize = statement.executeUpdate();
      System.out.println(updateSize + " nulls added");

    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Create predicates
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS a " + "CREATE (new:Atom {name:a.name}) " + "WITH a, new "
            + "UNWIND a.elems AS e "
            + "MATCH (en:Element {name: e.name}) "
            + "CREATE (new)-[:Contains {rank:e.rank}]->(en);")) {
      Array atoms = this.connection.createArrayOf("JAVA_OBJECT",
          ((Collection<?>) params.get("atoms")).toArray());
      statement.setArray(1, atoms);
      updateSize = statement.executeUpdate();
      System.out.println(updateSize + " atoms and relations added");

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Set<String> getAllPredicates() {
    this.nbQueries++;
    Set<String> predicates = new HashSet<>();

    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (a:Atom) RETURN DISTINCT a.name");
      while (resultSet.next()) {
        predicates.add(resultSet.getString(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return predicates;
  }

  @Override
  public boolean containsPredicate(String predicate) {
    this.nbQueries++;
    boolean ok = false;

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix
            + "OPTIONAL MATCH (an:Atom {name: ?}) RETURN an IS NOT NULL AS isExist LIMIT 1")) {
      statement.setString(1, predicate);
      ResultSet resultSet = statement.executeQuery();
      if (resultSet.next()) {
        ok = resultSet.getBoolean(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return ok;
  }

  @Override
  public Set<Atom> getAtomsWithPredicate(String predicate) {
    this.nbQueries++;
    Set<Atom> atoms = new HashSet<>();

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "MATCH (a:Atom {name: ?})-[r:Contains]->(e:Element) "
            + "WITH a, collect(DISTINCT r.rank) as r, collect(e.name) as e "
            + "RETURN a.name as a, r, e")) {
      statement.setString(1, predicate);
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        atoms.add(resultSetToAtom(resultSet));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return atoms;
  }

  @Override
  public boolean containsAnyNulls(Collection<? extends Element> nulls) {
    this.nbQueries++;
    boolean ok = false;

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS nullName " + "OPTIONAL MATCH (e:Element {name: nullName}) "
            + "RETURN e IS NOT NULL AS isExist")) {
      Object[] nullsArray = nulls.stream().map(el -> getNullName((Variable) el)).distinct()
          .toArray();
      Array constants = this.connection.createArrayOf("VARCHAR", nullsArray);
      statement.setArray(1, constants);
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next() && !ok) {
        ok = resultSet.getBoolean(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return ok;
  }

  @Override
  public Set<Element> getNulls() {
    this.nbQueries++;
    Set<Element> nulls = new HashSet<>();

    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (n:Null) RETURN DISTINCT n.name");

      while (resultSet.next()) {
        nulls.add(Element.stringToElement(resultSet.getString(1)));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return nulls;
  }

  @Override
  public int getNbNulls() {
    this.nbQueries++;
    int nbNulls = 0;

    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (:Null) RETURN count(*)");

      if (resultSet.next()) {
        nbNulls = resultSet.getInt(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return nbNulls;
  }

  @Override
  public int getNbConst() {
    this.nbQueries++;
    int nbConst = 0;

    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(
          this.queryPrefix + "MATCH (:Constant) RETURN count(*)");

      if (resultSet.next()) {
        nbConst = resultSet.getInt(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return nbConst;
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    this.nbQueries++;
    Object[] mappings = nulls.stream().map(element -> {
      Variable renamed = (Variable) element.clone();
      renamed.setProf(degree);

      return new String[]{getNullName((Variable) element), getNullName(renamed)};
    }).distinct().toArray();

    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS mapping " + "MATCH (e:Element {name: mapping[0]}) "
            + "SET e.name = mapping[1]")) {

      Array mappingsArray = this.connection.createArrayOf("JAVA_OBJECT", mappings);
      statement.setArray(1, mappingsArray);
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    //TODO: replace with parameter query (like add OPTIONAL MATCH clause)
    StringBuilder match = new StringBuilder(
        this.queryPrefix + "MATCH (an:Atom {name:'" + a.getName() + "'})");
    String end = "\nMATCH (an)-[r:Contains]->(e:Element)";
    end += "\nWITH an, an.name AS a, COLLECT(r.rank) AS r, COLLECT(e.name) AS e";
    if (delete) {
      end += "\nDETACH DELETE an";
    }
    end += "\nRETURN a, r, e";

    Element elem;
    Map<Element, Integer> variables = new HashMap<>();
    int varCount = 0;

    for (int i = 0; i < a.size(); i++) {
      elem = a.get(i);

      if (elem.isConstant()) {
        match.append(
            String.format(", (an)-[:Contains {rank:%d}]->(:Element:Constant {name:'%s'})", i,
                elem.getName()));

      } else {
        if (!variables.containsKey(elem)) {
          variables.put(elem, varCount++);
        }
        match.append(String.format(", (an)-[:Contains {rank:%d}]->(x%d:Element:Null)", i,
            variables.get(elem)));
      }
    }

    String query = match + end;
    //System.out.println("query to find atome to del in bdd " + query);

    Set<Atom> atoms = new HashSet<>();

    this.nbQueries++;
    try (Statement statement = this.connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        atoms.add(resultSetToAtom(resultSet));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return atoms;
  }

  @Override
  public boolean containsModuloNull(Atom a) {
    this.nbQueries++;
    boolean ok = false;

    try (Statement statement = this.connection.createStatement()) {
      String query = this.queryPrefix + new LocalElementIndexQueryBuilder().isomorphQuery(
          Collections.singleton(a), 1, null);
      ok = statement.executeQuery(query).next();

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return ok;
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    this.nbQueries = 0;
    BddStats bddStats = new BddStats();
    bddStats.start();

    try {
      // Start transaction
      bddStats.startCommit();
      this.connection.setAutoCommit(!this.queryPrefix.isEmpty());
      bddStats.stopCommit();

      // Chase
      Bdd sideEffects = this.chaseForInsertion(iRequest, rules, deltaMax, restricted, bddStats);
      thingsToAddAfterChase.initFromDB(sideEffects);

      // Get null bucket for core
      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(sideEffects);
      bddStats.stopNullBucket();

      // Do the core
      this.coreMaintenance(nullBucket, bddStats);

      // Check null degree
      bddStats.startNullDegree();
      Collection<Element> invalidNulls = sideEffects.getNulls().stream().map(x -> (Variable) x)
          .filter(
              x -> deltaMax > 0 && x.getProf() > deltaMax).collect(Collectors.toSet());
      boolean cantCommit = this.containsAnyNulls(invalidNulls);
      bddStats.stopNullDegree();

      if (cantCommit) {
        this.connection.rollback();

      } else {
        // Set null degree to 0
        bddStats.startNullDegree();
        this.setNullsDegree(sideEffects.getNulls(), 0);
        bddStats.stopNullDegree();

        // Commit
        bddStats.startCommit();
        this.connection.commit();
        bddStats.stopCommit();
      }

      this.connection.setAutoCommit(true);

    } catch (SQLException exception) {
      exception.printStackTrace();
    }

    bddStats.stop();
    bddStats.addQueries(this.nbQueries);
    return bddStats;
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    this.nbQueries = 0;
    BddStats bddStats = new BddStats();
    bddStats.start();

    try {
      // Start transaction
      bddStats.startCommit();
      this.connection.setAutoCommit(!this.queryPrefix.isEmpty());
      bddStats.stopCommit();

      // Get and delete request
      bddStats.startDelete();
      Collection<Atom> toDel = this.isomorphicAtom(dRequest, true);
      bddStats.stopDelete();
      bddStats.delAtoms(toDel.size());

      // Chase
      Bdd sideEffects = this.chaseForDeletion(toDel, rules, deltaMax, restricted, bddStats);

      // Get null bucket for core
      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(sideEffects);
      bddStats.stopNullBucket();

      // Do the core
      this.coreMaintenance(nullBucket, bddStats);

      // Commit
      bddStats.startCommit();
      this.connection.commit();
      this.connection.setAutoCommit(true);
      bddStats.stopCommit();

    } catch (SQLException exception) {
      exception.printStackTrace();
    }

    bddStats.stop();
    bddStats.addQueries(this.nbQueries);
    return bddStats;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {
    try (Statement statement = this.connection.createStatement()) {
      // Execute query
      this.nbQueries++;
      String query = this.queryPrefix + new LocalElementIndexQueryBuilder().chaseQuery(rule);
      ResultSet resultSet = statement.executeQuery(query);

      // Get substitutions
      while (resultSet.next()) {
        Substitution sub = new Substitution();
        Map<?, ?> subMap = resultSet.getObject("sub", Map.class);

        // Generate substitution
        for (Map.Entry<?, ?> entry : subMap.entrySet()) {
          sub.put((Variable) Element.stringToElement(entry.getKey().toString()),
              Element.stringToElement(entry.getValue().toString()));
        }

        // Apply substitution
        Rule r = sub.applySub(rule);
        Atom head = r.getHead();
        int prof = r.getMaxProf() + 1;

        // Set prof
        for (Variable v : head.getVariables()) {
          if (!r.getVariablesInBody().contains(v)) {
            v.setProf(prof);
          }
        }

        //System.out.println("RULE " + r);

        // Do chase step
        if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
          System.out.println("DEL " + r.getAtomesToDelInBody());
          newToDel.addAll(r.getAtomesToDelInBody());

        } else {
          System.out.println("ADD " + head);
          newToIns.add(head);
        }
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected Collection<Set<Atom>> getInstancesModuloNull(Collection<Atom> atoms) {
    List<Set<Atom>> atomList = new ArrayList<>();

    try (Statement statement = this.connection.createStatement()) {
      String query =
          this.queryPrefix + new LocalElementIndexQueryBuilder().isomorphQuery(atoms, null, null);
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        atomList.add(resultSetToListAtom(resultSet));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return atomList;
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    Collection<Substitution> substitutions = new HashSet<>();

    try (Statement statement = this.connection.createStatement()) {
      String query =
          this.queryPrefix + new LocalElementIndexQueryBuilder().isomorphQuery(partition, null,
              null);
      System.out.println(query);
      this.nbQueries++;

      Set<Variable> nullsElements = partition.stream().flatMap(a -> a.getNulls().stream())
          .collect(Collectors.toSet());

      ResultSet resultSet = statement.executeQuery(query);
      while (resultSet.next()) {
        Substitution sub = new Substitution();
        for (Variable nullElement : nullsElements) {
          String elementName = resultSet.getString(nullElement.getName());
          Element element = Element.stringToElement(elementName);
          sub.put(nullElement, element);
        }
        substitutions.add(sub);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return substitutions.stream();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    Set<Element> nullBucket = atoms.getNulls();

    List<Map<String, Object>> paramsList = new ArrayList<>();
    for (Atom a : atoms) {
      Map<String, Object> subParam = new HashMap<>();
      subParam.put("pred", a.getName());
      subParam.put("ref", a.stream().map(element -> element.isConstant() ? element.getName() : null)
          .collect(Collectors.toList()));
      paramsList.add(subParam);
    }

    this.nbQueries++;
    try (PreparedStatement statement = this.connection.prepareStatement(
        this.queryPrefix + "UNWIND ? AS x "
            + "MATCH (a:Atom {name: x.pred})-[r:Contains]->(e:Element) "
            + "WITH x, a, r, e ORDER BY r.rank " + "WITH a, COLLECT(e) AS elems, x.ref AS ref "
            + "WHERE size(elems) = size(ref) AND ALL(i IN range(0, size(elems)) WHERE elems[i]:Null OR ref[i] IS NULL OR elems[i].name = ref[i]) "
            + "UNWIND elems AS e " + "WITH e " + "WHERE e:Null " + "RETURN DISTINCT e.name")) {

      Array atomArray = this.connection.createArrayOf("JAVA_OBJECT", paramsList.toArray());
      statement.setArray(1, atomArray);
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        nullBucket.add(Element.stringToElement(resultSet.getString(1)));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return nullBucket;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    //A set of null values (partition)
    //is associated with a set of elements belonging to this partition
    Map<Set<Element>, Set<Atom>> partitions = new HashMap<>();

    List<Element> toCompute = new ArrayList<>(nullBucket);

    // Max path length (traversal of all null-atom pair)
    int maxPathLength = this.getNbNulls() * 2;

    //We match a combination of an atom (a) and all the nulls is is linked directly or indirectly to by other nulls (n).
    //(we are recursively traveling Contains relationships in any direction)
    String cypherQuery =
        this.queryPrefix + "UNWIND ? AS n\n" + "MATCH p = (x:Element:Null {name: n})-[:Contains*1.."
            + maxPathLength + "]-(y)\n"
            + "WHERE x <> y AND ALL(n IN nodes(p) WHERE NOT (n:Constant))\n"
            + "WITH x, COLLECT(DISTINCT y) AS nodes\n"
            + "WITH [n IN nodes WHERE (n:Atom)] AS atoms, [x.name] + [n IN nodes WHERE (n:Null) | n.name] AS partition\n"
            + "UNWIND atoms AS a\n" + "MATCH (a)-[r:Contains]->(e:Element)\n"
            + "WITH a, collect(r.rank) as r, collect(e.name) as e, partition\n"
            + "RETURN a.name as a, r, e, partition";

    try (PreparedStatement statement = this.connection.prepareStatement(cypherQuery)) {
      while (!toCompute.isEmpty()) {
        //Variable nullElement = (Variable) toCompute.get(0);
        //toCompute.remove(0);
        List<String> nulls = toCompute.stream().sequential().limit(50) // By chunk of 20
            .map(nullElement -> getNullName((Variable) nullElement)).collect(Collectors.toList());
        toCompute.removeAll(toCompute.stream().sequential().limit(50).collect(Collectors.toList()));

        // Prepare params
        //statement.setString(1, getNullName(nullElement));
        Array atomArray = this.connection.createArrayOf("JAVA_OBJECT", nulls.toArray());
        statement.setArray(1, atomArray);

        //We get all partitions in the database which corresponds to our null values in nullBucket
        System.out.println(
            "Run partition query for " + nulls + ": " + toCompute.size() + " remaining");
        this.nbQueries++;
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
          Atom atom = resultSetToAtom(resultSet);
          Object[] partition = (Object[]) resultSet.getArray("partition").getArray();

          Set<Element> foundNulls = Arrays.stream(partition).map(Object::toString)
              .map(Element::stringToElement).collect(
                  Collectors.toSet());

          if (!partitions.containsKey(foundNulls)) {
            partitions.put(foundNulls, new HashSet<>());
          }
          partitions.get(foundNulls).add(atom);
          toCompute.removeAll(foundNulls);
        }
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return partitions;
  }
}