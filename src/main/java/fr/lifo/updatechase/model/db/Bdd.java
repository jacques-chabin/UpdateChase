package fr.lifo.updatechase.model.db;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Bdd implements Collection<Atom>, AutoCloseable {

  protected static final Logger LOGGER = Logger.getLogger("bdd");

  public abstract boolean isConnected();

  /**
   * Gere l'affichage d'une Bdd.
   *
   * @return String l'affichage d'une Bdd.
   */
  @Override
  public String toString() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.map(Objects::toString).collect(Collectors.joining(".\n", "BDD: \n", "."));
    }
  }

  @Override
  public boolean equals(Object o) {
    boolean res = false;

    if (o instanceof Collection) {
      Collection<?> db = (Collection<?>) o;
      int dbSize = db.size();
      int thisSize = this.size();

      if (dbSize == thisSize) {
        try (Stream<?> stream = db.stream()) {
          Optional<Atom> atom = stream.filter(Atom.class::isInstance).map(Atom.class::cast).filter(
              x -> !this.containsModuloNull(x)).findAny();

          if (atom.isPresent()) {
            LOGGER.warning(atom.get() + " is not in BD");
          } else {
            res = true;
          }
        }

      } else {
        LOGGER.warning("Not the same number of atomes " + dbSize + "  " + thisSize);
      }
    }

    return res;
  }

  ////// Closable ////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Close the connection with a distant database
   */
  @Override
  public abstract void close();

  ////// Collection //////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  @Override
  public boolean contains(Object o) {
    try (Stream<Atom> stream = this.stream()) {
      return stream.anyMatch(o::equals);
    }
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    try (Stream<?> stream = c.stream()) {
      return stream.allMatch(this::contains);
    }
  }

  @Override
  public boolean addAll(Collection<? extends Atom> c) {
    boolean res = false;
    for (Atom a : c) {
      res = this.add(a) || res;
    }
    return res;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean res = false;
    for (Object a : c) {
      res = this.remove(a) || res;
    }
    return res;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    try (Stream<Atom> stream = this.stream()) {
      return stream.filter(x -> !c.contains(x)).map(this::remove).collect(Collectors.toSet())
          .contains(true);
    }
  }

  @Override
  public void clear() {
    this.removeAll(this);
  }

  @Override
  public Object[] toArray() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.toArray();
    }
  }

  @Override
  public <T> T[] toArray(T[] a) {
    throw new UnsupportedOperationException();
  }

  ////// Bdd /////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Permet de récupérer une copie de la base de données sous forme d'une Map associant chaque
   * prédicat à un emsemble de ses atomes
   *
   * @return le Map<String, Set<Atome>> de la Bdd
   */
  public Map<String, List<Atom>> asMap() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.collect(Collectors.groupingBy(Atom::getName));
    }
  }

  public int getMaxProf() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.mapToInt(Atom::getMaxProf).max().orElse(-1);
    }
  }

  public static BddMemoire fromFile(String path) throws SyntaxError, IOException {
    return DBFileIO.readFromFile(path, new BddMemoire(), Atom::stringToAtom);
  }

  public void printDiff(Bdd other) {
    LOGGER.info(this.getDiff(other));
  }

  /**
   * Init the database from another. No verification are done on this one (for performance), so make
   * sure to use only with already OK databases
   *
   * @param init The initial database to start with
   */
  public void initFromDB(Collection<Atom> init) {
    this.clear();
    this.addAll(init);
  }

  public String getDiff(Bdd other) {
    StringBuilder output = new StringBuilder();
    long nbDiff = 0;

    try (Stream<Atom> stream = this.stream()) {
      nbDiff += stream.filter(x -> !other.containsModuloNull(x))
          .peek(x -> output.append("+\t").append(x).append('\n')).count();
    }

    try (Stream<Atom> stream = other.stream()) {
      nbDiff += stream.filter(x -> !this.containsModuloNull(x))
          .peek(x -> output.append("-\t").append(x).append('\n')).count();
    }

    if (nbDiff > 0) {
      output.append(nbDiff).append(" differences");
    } else {
      output.append("Same database");
    }

    return output.toString();
  }

  /**
   * Sauvegarde la bdd dans un fichier texte, utilisable ultérieurement par le programme.
   *
   * @param path String. Le chemin complet avec le nom du fichier où la bdd sera sauvegardée.
   **/
  public void toFile(String path) throws IOException {
    DBFileIO.writeToFile(path, this);
  }

  public Collection<String> getAllPredicates() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.map(Atom::getName).collect(Collectors.toSet());
    }
  }

  public boolean containsPredicate(String pred) {
    try (Stream<Atom> stream = this.stream()) {
      return stream.map(Atom::getName).anyMatch(x -> x.equals(pred));
    }
  }

  public Collection<Atom> getAtomsWithPredicate(String pred) {
    try (Stream<Atom> stream = this.stream()) {
      return stream.filter(x -> x.getName().equals(pred)).collect(Collectors.toSet());
    }
  }

  public static String getNullName(Variable variable) {
    return '_' + variable.getName() + '#' + variable.getProf();
  }

  public Set<Element> getNulls() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.flatMap(Atom::stream).filter(Element::isNullValue).collect(Collectors.toSet());
    }
  }

  public int getNbNulls() {
    try (Stream<Atom> stream = this.stream()) {
      return (int) stream.flatMap(Atom::stream).filter(Element::isNullValue).map(Element::getName)
          .distinct().count();
    }
  }

  public int getNbConst() {
    try (Stream<Atom> stream = this.stream()) {
      return (int) stream.flatMap(Atom::stream).filter(Element::isConstant).map(Element::getName)
          .distinct().count();
    }
  }

  public boolean containsAnyNulls(Collection<? extends Element> nulls) {
    try (Stream<Atom> stream = this.stream()) {
      return stream.flatMap(Atom::stream).filter(Element::isNullValue).anyMatch(nulls::contains);
    }
  }

  /**
   * Return all atom in db which are isomorphic to atoms in dRequest
   *
   * @param dRequest A set of atoms we want to suppresse from the database
   * @param delete   A boolean if true delete atomes from Bdd if false keep atomes in Bdd
   */
  public Set<Atom> isomorphicAtom(Collection<? extends Atom> dRequest, boolean delete) {
    return dRequest.stream().flatMap(atom -> isomorphicAtom(atom, delete).stream())
        .collect(Collectors.toSet());
  }

  abstract public Set<Atom> isomorphicAtom(Atom a, boolean delete);

  /**
   * Méthode qui verifie si l'Atome 'a' est dans la Bdd sans prendre en compte les valeurs nulles.
   *
   * @param a Atome. l'Atome recherche.
   * @return boolean retourne un booleen indiquant si l'Atome a est present dans la Bdd.
   */
  public abstract boolean containsModuloNull(Atom a);

  /**
   * Ajoute un atome (ou un groupe d'atomes) à la base de données en prenant en compte les règles
   *
   * @param iRequest              Bdd. La base de données d'atomes que l'ont VEUT ajouter.
   * @param rules                 LinkedListRules. La liste des regles à prendre en compte.
   * @param deltaMax              int. La condition d'arret (profondeur...)
   * @param thingsToAddAfterChase Bdd. La base de données d'atomes que l'on DOIT ajouter (pour
   *                              respecter les règles).
   * @param restricted            boolean. Si le Chase doit etre lancé en mode restreint ou pas.
   * @return BddStats             les statistiques de l'insertion.
   */
  public abstract BddStats addWithConstraints(Collection<? extends Atom> iRequest,
      LinkedListRules rules, int deltaMax, Bdd thingsToAddAfterChase, boolean restricted);

  public abstract BddStats removeWithConstraints(Collection<? extends Atom> dRequest,
      LinkedListRules rules, int deltaMax, boolean restricted);

  public abstract void setNullsDegree(Collection<Element> nulls, int degree);

  protected Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsDeletedBefore = new BddMemoire(dRequest);  // Atoms deleted at previous step
    Bdd ToIns = new BddMemoire();                       // All atoms inserted
    Bdd ToDel = new BddMemoire(dRequest);               // All atoms deleted

    bddStats.startDelete();
    this.removeAll(dRequest);
    bddStats.stopDelete();

    while (!atomsDeletedBefore.isEmpty()) {
      Bdd atomsToDelete = new BddMemoire(); // Atoms to delete during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        // Get substitution candidates
        substitutionCandidates = rule.getHeadSubstitutions(atomsDeletedBefore);

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE DEL " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(r, ToIns, atomsToDelete, ToDel, deltaMax);
        }
      }
      bddStats.stopChase();

      ToIns = this.chaseForInsertion(ToIns, rules, deltaMax, restricted, bddStats);
      for (Atom a : new BddMemoire(ToIns)) {
        if ((deltaMax > 0 && a.getMaxProf() > deltaMax) || ToDel.containsModuloNull(a)) {
          atomsToDelete.add(a);
          ToIns.remove(a);
        }
      }

      LOGGER.info("CHASE DEL " + step + ": " + atomsToDelete);
      bddStats.delAtoms(atomsToDelete.size());
      bddStats.startDelete();
      this.removeAll(atomsToDelete);
      bddStats.stopDelete();

      ToDel.addAll(atomsToDelete);
      atomsDeletedBefore = atomsToDelete;

      ++step;
    }

    Bdd sideEffects = new BddMemoire(ToDel);
    sideEffects.addAll(ToIns);

    return sideEffects;
  }

  protected abstract void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax);

  protected Collection<Set<Atom>> getInstancesModuloNull(Collection<Atom> atoms) {
    try (Stream<Substitution> stream = this.getPartitionSubstitions(atoms)) {
      return stream.map(s -> s.applySub(atoms)).collect(Collectors.toSet());
    }
  }

  protected abstract Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition);

  protected abstract Set<Element> getNullBucket(Bdd atoms);

  public Map<Set<Element>, Set<Atom>> getPartitions() {
    return this.getPartitions(this.getNulls());
  }

  protected abstract Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket);

  /**
   * Simplification de la Bdd par l'algo Core
   *
   * @return BddStats
   */
  public boolean core() {
    return coreMaintenance(this.getNulls(), new BddStats());
  }

  public boolean coreMaintenance(Collection<Element> nullBucket, BddStats bddStats) {
    boolean simplification = false;

    bddStats.startPartition();
    Map<Set<Element>, Set<Atom>> partitions = this.getPartitions(nullBucket);
    bddStats.stopPartition();

    int i = 1;
    for (Map.Entry<Set<Element>, Set<Atom>> partition : partitions.entrySet()) {
      LOGGER.info("Search simplification for partition " + (i++) + "/" + partitions.size());
      bddStats.startSimplification();

      // Simplify the partition on itself in memory to reduce query complexity
      Bdd partSimp = new BddMemoire(partition.getValue());
      if (partSimp.coreMaintenance(partition.getKey(), new BddStats())) {
        LOGGER.info("\n\n=> SIMPLIFY PARTITION\n\n");
      }

      Substitution identity = new Substitution();
      for (Atom atom : partSimp) {
        for (Variable nullElem : atom.getNulls()) {
          identity.put(nullElem, nullElem);
        }
      }

      try (Stream<Substitution> substitutions = this.getPartitionSubstitions(partSimp)) {
        List<Substitution> substitutionsList = substitutions.collect(Collectors.toList());
        bddStats.startMoreSpecific();
        Substitution moreSpecSub = Substitution.getMostSpecific(substitutionsList, identity);
        bddStats.stopMoreSpecific();
        bddStats.stopSimplification();

        Collection<Atom> instance = moreSpecSub.applySub(partSimp);

        if (partition.getValue().size() != instance.size() || !partition.getValue()
            .containsAll(instance)) {
          LOGGER.info("**** Simplify " + partition + " with " + instance);
          bddStats.delAtoms(partition.getValue().size());
          bddStats.addAtoms(instance.size());

          bddStats.startDelete();
          this.removeAll(partition.getValue());
          bddStats.stopDelete();

          bddStats.startInsert();
          this.addAll(instance);
          bddStats.stopInsert();

          simplification = true;
        }
      }
    }

    return simplification;
  }

  /**
   * Applique une liste de regles de type "N atomes -> 1 atomes" a la Bdd et compte le nomrbre
   * d'Atome crees.
   *
   * @param rules      LinkedListRules. La liste des regles a appliquer.
   * @param deltaMax   int. La condition d'arret.
   * @param restricted boolean.
   * @param bddStats   bdd stats
   * @return int le nombre d'Atome crees.
   */
  protected Bdd chaseForInsertion(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsInsertedBefore = new BddMemoire(iRequest); // Atoms inserted at previous step
    Bdd ToIns = new BddMemoire(iRequest);               // All atoms inserted

    bddStats.addAtoms(iRequest.size());
    bddStats.startInsert();
    this.addAll(iRequest);
    bddStats.stopInsert();

    while (!atomsInsertedBefore.isEmpty()) {
      Bdd atomsToInsert = new BddMemoire(); // Atoms to insert during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        // Get substitution candidates
        substitutionCandidates = rule.getBodySubstitutions(atomsInsertedBefore);

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE INS " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(r, atomsToInsert, null, null, deltaMax);
        }
      }
      bddStats.stopChase();

      LOGGER.info("CHASE INS " + step + ": " + atomsToInsert);
      bddStats.addAtoms(atomsToInsert.size());
      bddStats.startInsert();
      this.addAll(atomsToInsert);
      bddStats.stopInsert();

      ToIns.addAll(atomsToInsert);

      atomsInsertedBefore.clear();
      for (Atom a : atomsToInsert) {
        if ((deltaMax < 1 || a.getMaxProf() < deltaMax) && (!restricted
            || a.containsConstantValues())) {
          atomsInsertedBefore.add(a);
        }
      }

      ++step;
    }

    return ToIns;
  }

}
