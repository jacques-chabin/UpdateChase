package fr.lifo.updatechase.model.db.sql;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Query;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Schema;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * MySQL gere le chargement du driver MySQL pour JAVA et de l'initialisation de la connexion a la
 * base de donee.
 */
public class MySQL {

  boolean connecte = false;
  Connection mysql;

  /**
   * Constructeur de MySQL. Si le chargement du driver ou la connexion echoue, l'attribut mysql est
   * set a null.
   *
   * @param db    le nom de la base de donne auquelle on veut se connecte.
   * @param login le login de connexion au serveur.
   * @param mdp   le mot de passe de connexion au serveur.
   */
  public MySQL(String db, String login, String mdp) {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      System.out.println("Driver MySQL non trouvé?");
      mysql = null;
      return;
    }

    try {
      String nomServeur = "localhost";
      //String nomBase = "test";
      mysql = DriverManager.getConnection("jdbc:mysql://" + nomServeur + ":3306/" + db, login, mdp);
      connecte = true;
    } catch (SQLException e) {
      System.out.println("Echec de connexion!");
      System.out.println(e.getMessage());
      mysql = null;
      //return;
    }

		/*if(connecte){
			//System.out.println("Suis connecté!");
		}*/
  }

  /**
   *
   */
  //static LinkedListRules answer(Query q1, Schema sch, String db, String login, String mdp){
  public static int answer(Query q1, Schema sch, String db, String login, String mdp) {
    // retourne le nombre de réponses à la requete Q1, place les réponses dans le fichier ansAlgo1.dlp
    // on crée une nouvelle requête qui a dans la tête toutes les variables du corps
    List<Element> nhead = new ArrayList<>();
    System.out.println("query dans ansswer : " + q1);
    if (!q1.getHead().isEmpty()) {
      nhead.addAll(q1.getHead());
    }
    for (Atom a : q1.getBody()) {
      for (Element e : a) {
        if (e.isVariable() && !nhead.contains(e)) {
          nhead.add(e);
        }
      }
    }

    Atom hq2 = new Atom(q1.getHeadNames(), nhead);
    Query q2 = new Query(hq2, q1.getBody());

    System.out.println("Query to answer : " + q2);
    String s2 = q2.queryToSQL(sch, false);
    System.out.println("SQL Query to answer : " + s2);

    LinkedListRules res = new LinkedListRules();
    //on ouvre une connexion
    MySQL connex = new MySQL(db, login, mdp);
    int cpt = 0;
    if (connex.connecte) {
      try {
        Statement s = connex.mysql.createStatement();
        ResultSet rs = s.executeQuery(s2);
				/* while(rs.next() && cpt<5000){
					cpt++;
					//System.out.println(cpt);
					// on crée une nouvelle substitution
					Substitution sub = new Substitution();
					for(int i=0; i<hq2.getTuple().size(); ++i){
						sub.putValue((Variable)hq2.getTuple().get(i), new Constant(rs.getString(i+1)));
					}
					Rule rep = sub.applySub(q1);
					res.add(rep);
					//System.out.println("Une réponse de plus"+rep);
					//System.out.println(rs.getString("nomLieu"));
				} */
        try {
          // open the file named ansAlgo1.dlp in the current directory
          FileWriter fw = new FileWriter("./ansAlgo1.dlp");

          while (rs.next()) {
            cpt++;
            //System.out.println(cpt);
            // on crée une nouvelle substitution
            Substitution sub = new Substitution();
            for (int i = 0; i < hq2.size(); ++i) {
              sub.put((Variable) hq2.get(i), new Constant(rs.getString(i + 1)));
            }
            Rule rep = sub.applySub(q1);
            fw.write(rep.toString() + ".\n");
            //System.out.println("Une réponse de plus"+rep);
            //System.out.println(rs.getString("nomLieu"));
          }
          fw.flush();
          fw.close();
        } catch (IOException e2) {
          System.out.println("Can't create file " + e2.getMessage());
        }
      } catch (SQLException e) {
        System.out.println("Pb " + e.getMessage());
      }

      // on ferme la connexion
      try {
        connex.mysql.close();
      } catch (SQLException e) {
        System.out.println("Pb close connexion" + e.getMessage());
      }
    }
    //return res;
    return cpt;
  }

  /**
   *
   */
  public static boolean checkBQ(Query q3, Schema sch, String db, String login, String mdp) {
    if (q3.getBody().get(0).getName().equals("!")) {
      return false;
    }

    if (QS.verbose >= 2) {
      System.out.println("Query to check : " + q3);
    }
    String s3 = q3.queryToSQL(sch, false);
    if (QS.verbose >= 2) {
      System.out.println("Query traduct : " + s3);
    }

    boolean res = false;
    MySQL connex = new MySQL(db, login, mdp);
    if (connex.connecte) {
      try {
        Statement s = connex.mysql.createStatement();
        ResultSet rs = s.executeQuery(s3);
        //System.out.println("sql version : "+s3);
        // si il s'agit d'une requete négative
        if (q3.getHeadNames().equals("?")) {
          // s'il y a au moins une réponse la requète n'est pas valide
          res = !rs.next();
        } else { // c'est une requète positive
          // s'il y a au moins une réponse la requète n'est pas valide
          res = !rs.next();
        }
      } catch (SQLException e) {
        System.out.println("Pb " + e.getMessage());
      }

      // on ferme la connexion
      try {
        connex.mysql.close();
      } catch (SQLException e) {
        System.out.println("Pb close connexion" + e.getMessage());
      }
    }
    return res;
  }
}
