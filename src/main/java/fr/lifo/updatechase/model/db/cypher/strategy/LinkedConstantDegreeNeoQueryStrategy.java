package fr.lifo.updatechase.model.db.cypher.strategy;

import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LinkedConstantDegreeNeoQueryStrategy implements NeoQueryStrategyInterface {

  @Override
  public Atom getBFSRoot(Collection<Atom> atoms) {
    Map<Constant, Long> constants = atoms.parallelStream().map(Atom::getConstants)
        .flatMap(Collection::parallelStream).collect(
            Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()));

    return atoms.parallelStream().max((atom1, atom2) -> {
      long count1 = 0;
      long count2 = 0;

      for (Constant constant : atom1.getConstants()) {
        count1 += constants.get(constant);
      }

      for (Constant constant : atom2.getConstants()) {
        count2 += constants.get(constant);
      }

      return Long.compare(count1, count2);
    }).orElse(null);
  }

}
