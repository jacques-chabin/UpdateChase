package fr.lifo.updatechase.model.db.mem;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Query;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Bdd est une classe representant une base de donnees. Bdd est caracterise par : numNewVarBd : le
 * numero d'identification des valeurs nulles. bdd : un HashMap<String, HashSet<Atome>> dont les
 * clef sont les noms des tables de la base de donnees, et les valeurs sont leur contenu.
 *
 * @see Atom
 * @see HashMap
 * @see HashSet
 */
public class BddMemoire extends Bdd {

  /* Valeur utilisee pour creer les nouvelles null values.*/
  private static int numNewVarBd = 1;

  /* la clef correspond au predicat de l'atome
   * la valeur est un hashset d'Atome.
   */
  private final Map<String, List<Atom>> bdd;

  /**
   * Constructeur Bdd Initialise l'attribut bdd avec un HashMap<String, HashSet<Atome>> vide.
   */
  public BddMemoire() {
    this.bdd = new ConcurrentHashMap<>();
  }

  public BddMemoire(Collection<? extends Atom> init) {
    this();
    this.addAll(init);
  }

  /**
   * Applique une regle sur une Bdd jusqu'a ce qu'aucune generation n'ai pu etre realisee.
   *
   * @param pc       Rule. La regle a appliquer.
   * @param stopCond int. L'Entier qui indique comment on traite les valeur nulles.
   * @param bd       Bdd. Une Bdd qui contiendra tout les Atomes generes.
   * @param oldList  Bdd. Une Bdd qui ne contiendra que les Atomes a traiter, pas deja traites,
   *                 generes sur le tour precedent.
   * @param newList  Bdd. Une Bdd qui contiendra les Atomes nouvellement generes sur une seule passe
   *                 de generation.
   * @return List<Atome> Un ArrayList d'Atome.
   * @deprecated
   */
  public static boolean update1WithOnePosConstraint1_1(Rule pc, int stopCond, BddMemoire bd,
      BddMemoire oldList, BddMemoire newList, boolean restricted) {
    //logger.info("Try to apply rule : " + pc +" on "+oldList);
    numNewVarBd++;
    pc = pc.renameVar(numNewVarBd);
    //logger.info("numNewVar = " + Bdd.numNewVarBd +"  "+ pc);
    List<Atom> res = new ArrayList<>();

    List<Atom> src = pc.getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    tabSub[0] = new Substitution(); // NE SERT A RIEN

    int i = 0;

    while (!fini) {
      //System.out.print(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      if (oldList.containsPredicate(src.get(i).getName())) {
        tabPos[i] = src.get(i)
            .mapAll(oldList.getAtomsWithPredicate(src.get(i).getName()), tabSub[i], tabPos[i],
                stopCond);
      } else {
        tabPos[i] = -1;
      }
      //logger.info(" "+src.get(i)+trg);
      //logger.info(" apres mapList tabi :"+ tabPos[i]);
      //logger.info(" apres mapList tabsi :"+ tabSub[i]);

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead());
        //logger.info("Atome instancie : "+a);
        long tempsDebutPut = System.currentTimeMillis();
        a.putNotRedOldNullVar(BddMemoire.numNewVarBd);
        if (!bd.containsNew(a, restricted) && !oldList.containsNew(a, restricted)
            && !Query.containsNew(a, res)
            && !newList.containsNew(a, restricted)) {
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);
          //logger.info("atome "+a);
        }
        long tempsFinPut = System.currentTimeMillis();
        QS.putRedOldNewVarTime += (tempsFinPut - tempsDebutPut) / 1000F;
      } else {
        i = i + 1;
      }
      if (fini) {
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(newList);
            newList.add(b);
          }
          //fini=false;
          res = new ArrayList<>();
        }
      }

    }
    //logger.info(result);
    return result;
  }

  /**
   * Applique une regle de type "N atomes -> 1 atones" a une Bdd oldList, genere une newList puis
   * rajoute OldList a bdd
   *
   * @param pc         Rule. la regle a appliquer
   * @param stopCond   int. represente la condition d'arret
   * @param bd         Bdd. la bdd deja traitee
   * @param oldList    Bdd. le bdd a traiter
   * @param newList    Bdd. le resultat du traitement de oldList
   * @param restricted boolean.
   * @return List<Atome> Un ArrayList d'Atome.
   * @deprecated
   */
  public static boolean updateAddWithOnePosConstraintn_1(Rule pc, int stopCond, int prof,
      BddMemoire bd, BddMemoire oldList, BddMemoire newList, boolean restricted) {
    LOGGER.info("Try to apply rule : " + pc + " on " + oldList);
    // si la rule contient au moins un atome unifiable avec un atome de oldList
    if (!pc.bodyContainsOne(oldList.getAllPredicates())) {
      LOGGER.info("Non");
      return false;
    }
    LOGGER.info("Oui");
    BddMemoire.numNewVarBd++;
    pc = pc.renameVar(BddMemoire.numNewVarBd);
    //logger.info("numNewVar = " + Bdd.numNewVarBd +"  "+ pc);
    List<Atom> res = new ArrayList<>();

    List<Atom> src = pc.getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    tabSub[0] = new Substitution();

    int i = 0;

    while (!fini) {
      //logger.info(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      if (bd.containsPredicate(src.get(i).getName())) {
        tabPos[i] = src.get(i)
            .mapAll(bd.getAtomsWithPredicate(src.get(i).getName()), tabSub[i], tabPos[i], stopCond);
      } else {
        tabPos[i] = -1;
      }
      //logger.info(" "+src.get(i)+trg);
      //logger.info(" apres mapList tabi :"+ tabPos[i]);
      //logger.info(" apres mapList tabsi :"+ tabSub[i]);
      //logger.info("  i = "+i+ " tabi " + tabPos[i]);

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead());
        // logger.info("Atome instancie : "+a);
        long tempsDebutPut = System.currentTimeMillis();
        a.putNotRedOldNullVar(BddMemoire.numNewVarBd);
        if (!bd.containsNew(a, restricted) && !oldList.containsNew(a, restricted)
            && !Query.containsNew(a, res)
            && !newList.containsNew(a, restricted)) {
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);

        }
        long tempsFinPut = System.currentTimeMillis();
        QS.putRedOldNewVarTime += (tempsFinPut - tempsDebutPut) / 1000F;
      } else {
        i = i + 1;
      }
      if (fini) {
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(newList);
            newList.add(b);
          }
          //fini=false;
          res = new ArrayList<>();
        }
      }

    }
    //logger.info(result);
    return result;
  }

  /**
   * Applique une regle de type "N atomes -> 1 atones" a une Bdd oldList, genere une newList puis
   * rajoute OldList a bdd
   *
   * @param pc            Rule. la regle a appliquer
   * @param stopCond      int. represente la condition d'arret
   * @param bd            Bdd. la bdd deja traitee
   * @param oldList       Bdd. le bdd a traiter
   * @param newList       Bdd. le resultat du traitement de oldList
   * @param restricted    boolean. si l'utilisateur à coché la case restricted ou non
   * @param useInDeletion boolean. si cette fonction est utilisé dans la fonction de suppression
   *                      d'atomes
   * @return List<Atome> Un ArrayList d'Atome.
   */
  public static boolean updateWithOnePosConstraintn_1(Rule pc, int stopCond, BddMemoire bd,
      BddMemoire oldList, BddMemoire newList, boolean restricted, int nbProf,
      boolean useInDeletion) {
    // logger.info("Try to apply rule : " + pc +" on "+oldList);
    // si la rule contient au moins un atome unifiable avec un atome de oldList
    if (!pc.bodyContainsOne(oldList.getAllPredicates())) {
      // logger.info("Non");
      return false;
    }
    // logger.info("Oui");
    numNewVarBd++;
    pc = pc.renameVar(numNewVarBd);
    //logger.info("numNewVar = " + Bdd.numNewVarBd +"  "+ pc);
    List<Atom> res = new ArrayList<>();

    List<Atom> src = pc.getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];

    int i = 0;

    while (!fini) {
      //logger.info(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      if (bd.containsPredicate(
          src.get(i)
              .getName())) { // ICI c'est utile car dans une règle il êut y avoir des atomes qui ne sont pas encore dans la BD
        //ATTENTION: modifie aussi tabSub[i] !
        //tabPos[i] augmente au fur et a mesure si l'atome trouvé est égal à l'atom Core(..) que l'on a deja
        tabPos[i] = src.get(i)
            .mapAll(bd.getAtomsWithPredicate(src.get(i).getName()), tabSub[i], tabPos[i], stopCond);
      } else {
        tabPos[i] = -1;
      }

      if (tabPos[i]
          == -1) { //si le nom ne correspond pas a ce qu'il y a dans la bd OU si aucun map ne fonctionne
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead(), nbProf);

        // logger.info("Atome instancie : "+a);
        long tempsDebutPut = System.currentTimeMillis();
        a.putNotRedOldNullVar(BddMemoire.numNewVarBd);
        if ((!bd.containsNew(a, restricted) && !oldList.containsNew(a, restricted)
            && !Query.containsNew(a, res)
            && !newList.containsNew(a, restricted)) || useInDeletion) {
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);

        }
        long tempsFinPut = System.currentTimeMillis();
        QS.putRedOldNewVarTime += (tempsFinPut - tempsDebutPut) / 1000F;
      } else {
        i = i + 1;
      }

      if (fini) { // Quand ce sera fini...
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(newList);
            newList.add(b);
          }
          //fini=false;
          res = new ArrayList<>();
        }
      }

    }

    //logger.info(result);
    return result;
  }

  /**
   * Applique une regle de type "N atomes -> 1 atones" a une Bdd oldList, genere une newList puis
   * rajoute OldList a bdd
   *
   * @param pc            Rule. la regle a appliquer
   * @param stopCond      int. represente la condition d'arret
   * @param bd            Bdd. la bdd deja traitee
   * @param oldList       Bdd. le bdd a traiter
   * @param newList       Bdd. le resultat du traitement de oldList
   * @param restricted    boolean. si l'utilisateur à coché la case restricted ou non
   * @param useInDeletion boolean. si cette fonction est utilisé dans la fonction de suppression
   *                      d'atomes
   * @return List<Atome> Un ArrayList d'Atome.
   */
  public static boolean updateWithOnePosConstraintn_1ForTransaction(Rule pc, int stopCond,
      BddMemoire bd, BddMemoire oldList, BddMemoire newList, boolean restricted, int nbProf,
      boolean useInDeletion, Substitution substitution) {
    // logger.info("Try to apply rule : " + pc +" on "+oldList);
    // si la rule contient au moins un atome unifiable avec un atome de oldList
        /*if(!pc.bodyContainsOne(oldList.asMap().keySet())){
            // logger.info("Non");
            return false;
        }*/
    //Modif pour transaction car il n'y plus de copie et à la première itération oldList est vide.
    for (Atom a : pc.getBody()) {
      if (!oldList.containsPredicate(a.getName()) && !bd.containsPredicate(a.getName())) {
        return false;
      }
    }
    //        logger.info("TEST at" + nbProf + " " + pc);
    //        logger.info(bd);
    //        logger.info(oldList);
    //        logger.info("Oui");
    numNewVarBd++;
    pc = pc.renameVar(numNewVarBd);
    //logger.info("numNewVar = " + Bdd.numNewVarBd +"  "+ pc);
    List<Atom> res = new ArrayList<>(oldList.size());
    List<Atom> atoms = new ArrayList<>(oldList.size());
    List<Atom> src = pc.getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];

    int i = 0;

    while (!fini) {
      //logger.info(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = (Substitution) substitution.clone();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      //Modif pour transaction, on cherche dans la BD et dans les atomes ajoutés aux itérations précédentes
      atoms.clear();
      if (oldList.containsPredicate(src.get(i).getName())) {
        atoms.addAll(oldList.getAtomsWithPredicate(src.get(i).getName()));
      }
      if (bd.containsPredicate(src.get(i).getName())) {
        atoms.addAll(bd.getAtomsWithPredicate(src.get(i).getName()));
      }

      if (atoms.size() > 0) {
        tabPos[i] = src.get(i).mapAll(atoms, tabSub[i], tabPos[i], stopCond);
      } else {
        tabPos[i] = -1;
      }

            /*if(oldList.asMap().containsKey(src.get(i).getName())){ // ICI c'est utile car dans une règle il êut y avoir des atomes qui ne sont pas encore dans la BD
                //ATTENTION: modifie aussi tabSub[i] !
                //tabPos[i] augmente au fur et a mesure si l'atome trouvé est égal à l'atom Core(..) que l'on a deja
                tabPos[i]=src.get(i).mapBdd(oldList.asMap().get(src.get(i).getName()), tabSub[i], tabPos[i], stopCond);
            }
            else if(trg.asMap().containsKey(src.get(i).getName())){ // ICI c'est utile car dans une règle il êut y avoir des atomes qui ne sont pas encore dans la BD
                //ATTENTION: modifie aussi tabSub[i] !
                //tabPos[i] augmente au fur et a mesure si l'atome trouvé est égal à l'atom Core(..) que l'on a deja
                tabPos[i]=src.get(i).mapBdd(trg.asMap().get(src.get(i).getName()), tabSub[i], tabPos[i], stopCond);
            }
            else{
                tabPos[i]=-1;
            }*/

      if (tabPos[i]
          == -1) { //si le nom ne correspond pas a ce qu'il y a dans la bd OU si aucun map ne fonctionne
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead(), nbProf);

        //logger.info("Atome instancie : "+a);
        long tempsDebutPut = System.currentTimeMillis();
        a.putNotRedOldNullVar(BddMemoire.numNewVarBd);
        //Modif pour transaction
        if ((!bd.containsNew(a, restricted) && !oldList.containsNew(a, restricted)
            && !Query.containsNew(a, res)
            && !newList.containsNew(a, restricted)) || useInDeletion) {
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);
          // on va ajouter l'instance du corps de la règle qui a produit cet atome
          List<Atom> leBody = pc.getBody();
          for (Atom b : leBody) {
            Atom bi = tabSub[i].applySub(b, nbProf);
            if (bi.containsNullValues()) {
              res.add(bi);
            }
          }

        }
        long tempsFinPut = System.currentTimeMillis();
        QS.putRedOldNewVarTime += (tempsFinPut - tempsDebutPut) / 1000F;
      } else {
        i = i + 1;
      }

      if (fini) { // Quand ce sera fini...
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(newList);
            newList.add(b);
          }
          //fini=false;
          res.clear();
        }
      }

    }

    //logger.info(result);
    return result;
  }

  public static boolean updateWithOnePosConstraintn_1(Rule pc, int stopCond, BddMemoire bd,
      BddMemoire oldList, BddMemoire newList, boolean restricted, int nbProf) {
    return BddMemoire.updateWithOnePosConstraintn_1(pc, stopCond, bd, oldList, newList, restricted,
        nbProf, false);
  }

  public static boolean updateWithOnePosConstraintn_1ForTransaction(Rule pc, int stopCond,
      BddMemoire bd, BddMemoire oldList, BddMemoire newList, boolean restricted, int nbProf,
      Substitution substitution) {
    return BddMemoire.updateWithOnePosConstraintn_1ForTransaction(pc, stopCond, bd, oldList,
        newList, restricted, nbProf, false,
        substitution);
  }

  @Override
  public Iterator<Atom> iterator() {
    return this.stream().iterator();
  }

  @Override
  public Stream<Atom> stream() {
    return this.bdd.values().parallelStream().flatMap(Collection::stream);
  }

  /**
   * Clears the database by deleting everything
   */
  @Override
  public void clear() {
    this.bdd.clear();
  }

  @Override
  public Map<String, List<Atom>> asMap() {
    return this.bdd;
  }

  /**
   * Ajoute a la bdd l'Atome a. Si le predicat de l'Atome existe deja, ajoute l'Atome au HashSet
   * d'Atome deja present. Sinon cree un nouvel HashSet d'Atom dont la clef est le nom de l'Atome
   *
   * @param a l'Atome a ajoute
   */
  @Override
  public boolean add(Atom a) {
    String pred = a.getName();

    if (this.containsPredicate(pred)) {
      List<Atom> atoms = this.bdd.get(pred);
      if (atoms.contains(a)) {
        return false;
      } else {
        atoms.add(a);
      }

    } else {
      List<Atom> atoms = new ArrayList<>();
      atoms.add(a);

      this.bdd.put(pred, atoms);
    }

    return true;
  }

  @Override
  public boolean contains(Object o) {
    if (o instanceof Atom) {
      Atom a = (Atom) o;
      String pred = a.getName();

      if (this.bdd.containsKey(pred)) {
        return this.bdd.get(pred).contains(a);
      }
    }

    return false;
  }

  @Override
  public Collection<String> getAllPredicates() {
    return this.bdd.keySet();
  }

  @Override
  public boolean containsPredicate(String pred) {
    return this.bdd.containsKey(pred);
  }

  @Override
  public Collection<Atom> getAtomsWithPredicate(String pred) {
    return this.bdd.getOrDefault(pred, Collections.emptyList());
  }

  /**
   * Return all atom in db which are isomorphic to atom a
   *
   * @param a      An atom we want to suppresse from the database
   * @param delete is true atom is suppressed from database is false atom is kept in database
   */
  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    Set<Atom> result = new HashSet<>();

    for (Atom b : this.getAtomsWithPredicate(a.getName())) {
      if (a.equalsModuloNull(b)) {
        result.add(b);

        if (delete) {
          this.remove(b);
        }
      }
    }

    return result;
  }

  @Override
  public boolean remove(Object o) {
    if (o instanceof Atom) {
      Atom a = (Atom) o;
      String pred = a.getName();
      if (bdd.containsKey(pred) && bdd.get(pred).contains(o)) {
        bdd.get(pred).remove(a);
        if (bdd.get(pred).size() == 0) {
          bdd.remove(pred);
        }

        return true;
      }
    }

    return false;
  }

  /**
   * Retourne le nombre d'Atomes de la Bdd.
   *
   * @return int le nombre d'Atomes de la bdd.
   */
  @Override
  public int size() {
    int res = 0;
    for (Collection<Atom> c : this.bdd.values()) {
      res += c.size();
    }
    return res;
  }

  /**
   * associe à chaque Atome de la bdd un niveau de profondeur
   *
   * @param prof le niveau de profondeur à mettre à chaque Atome
   */
  public void putRank(int prof) {
    for (Atom a : this) {
      a.setRank(prof);
    }
  }

  /**
   * Méthode qui verifie si l'Atome 'a' est dans la Bdd sans prendre en compte les renommages
   * "New".
   *
   * @param a     Atome. l'Atome recherche.
   * @param restr Boolean. Indique si la recherche doit se faire en mode restreint
   * @return boolean retourne un booleen indiquant si l'Atome a est present dans la Bdd.
   */
  public boolean containsNew(Atom a, boolean restr) {
    // return true si a est dans la Bdd liste modulo le renomage des variables New reductibles
    // System.out.print("Test si "+a+ " est dans "+bd+" return ");
    Collection<Atom> atoms = this.getAtomsWithPredicate(a.getName());

    if (a.containsNullValues()) {
      for (Atom b : atoms) {
        //if(a.identiqueModuloNew(b)){
        if (restr) {
          if (a.isLessInstanciate(b)) { //restricted (moins precis)
            //if(a.equals(b)){ //unrestricted

            //logger.info("True");
            return true;
          }
        } else {
          if (a.equals(b)) {
            return true;
          }
        }
      }
      //logger.info("False");
      return false;

    } else {
      return atoms.contains(a);
    }
  }

  /**
   * Méthode qui verifie si l'Atome 'a' est dans la Bdd sans prendre en compte les valeurs nulles.
   *
   * @param a Atome. l'Atome recherche.
   * @return boolean retourne un booleen indiquant si l'Atome a est present dans la Bdd.
   */
  @Override
  public boolean containsModuloNull(Atom a) {
    // return true si a est dans la Bdd liste modulo le renomage des variables New reductibles
    // System.out.print("Test si "+a+ " est dans "+bd+" return ");
    Collection<Atom> atoms = this.getAtomsWithPredicate(a.getName());

    if (a.containsNullValues()) {
      for (Atom b : atoms) {
        if (a.equalsModuloNull(b)) {
          return true;

        }
      }
      //logger.info("False");
      return false;

    } else {
      return atoms.contains(a);
    }
  }

  /**
   * Retire de 'this' les elements en commun que 'this' a avec le paramètre.
   *
   * @param thingsToAddAfterChase Bdd. La bdd avec laquelle on verifie l'intersection
   */
  public void destroyIntersectionWith(Bdd thingsToAddAfterChase) {
    Map<String, List<Atom>> map = thingsToAddAfterChase.asMap();
    for (String key : map.keySet()) {
      if (this.bdd.containsKey(key)) {
        for (Atom a : map.get(key)) {
          this.bdd.get(key).remove(a);
          if (this.bdd.get(key).isEmpty()) {
            this.bdd.remove(key);
          }
        }
      }
    }
  }

  /**
   * Retire de 'this' les elements en commun que 'this' a avec le parametre sans prendre en compte
   * les noms des éléments null (ex: _N1 peut etre égale à _N8)
   *
   * @param otherBdd Bdd. La bdd avec laquelle on verifie l'intersection
   */
  public void destroyIntersectionWithModuloNull(Bdd otherBdd) {
    Map<String, List<Atom>> otherMap = otherBdd.asMap();

    for (String key : otherMap.keySet()) {
      //si this contient la clé (ex: B,A,E...)
      for (Atom a : otherMap.get(key)) {
        if (this.bdd.containsKey(key)) {
          //si a possède des éléments nulles alors on regarde pour tous les atomes de this
          //dans l'ensemble s'il n'y en a pas un qui serai égale modulo le nulle
          if (a.containsNullValues()) {
            List<Atom> toDel = new ArrayList<>();
            for (Atom b : this.bdd.get(key)) {
              if (a.equalsModuloNull(b)) {
                toDel.add(b);
              }
            }
            for (Atom b : toDel) {
              this.bdd.get(key).remove(b);
            }
          } else if (this.bdd.containsKey(key)) {  // add JAC 8/04/19
            this.bdd.get(key).remove(a);
          }
          //si l'ensemble est vide après la supression, alors on le supprime
          if (this.bdd.containsKey(key) && this.bdd.get(key).isEmpty()) {
            this.bdd.remove(key);
          }
        }
      }
    }
  }

  /**
   * garde dans 'this' SEULEMENT les elements en commun que 'this' a avec le parametre sans prendre
   * en compte les noms des éléments nulls (ex: _N1 peut etre égale à _N8)
   *
   * @param otherBdd Bdd. La bdd avec laquelle on verifie l'intersection
   */
  public void keepIntersectionWithModuloNull(Bdd otherBdd) {
    Map<String, List<Atom>> otherMap = otherBdd.asMap();
    Bdd toDel = new BddMemoire();
    //on verifie que la clé est bien présente dans this (ex: B, A, E, ...)
    // et on ne garde que ce qui est commun à otherBdd et this
    for (String key : otherMap.keySet()) {
      if (this.bdd.containsKey(key)) {
        for (Atom a : this.bdd.get(key)) {
          int cpt = 0;
          for (Atom b : otherMap.get(key)) {
            //logger.info(a + " et " + b + " sont-ils identique ?");
            if (a.equalsModuloNull(b)) {
              //logger.info("OUI");
              cpt++;
            }
          }
          //si l'atome a n'est pas au moins égale modulo new à un atome de l'autre bdd
          if (cpt == 0) {
            toDel.add(a);
          }
        }
      }
    }
    this.destroyIntersectionWith(toDel);
    //logger.info("A SUPPR DANS KEEP : " + toDel);
    //on ne garde dans thisKeySet que les clé qui ne sont pas dans otherBdd,
    // et ensuite on les supprime de this.
    Set<String> thisKeySet = new HashSet<>(this.getAllPredicates());
    thisKeySet.removeAll(otherMap.keySet());
    this.bdd.keySet().removeAll(thisKeySet);
  }

  /**
   * Applique une liste de regles de type "N atomes -> 1 atomes" a la Bdd et compte le nomrbre
   * d'Atome crees.
   *
   * @param posConstraintSet LinkedListRules. La liste des regles a appliquer.
   * @param stopCond         int. La condition d'arret.
   * @param restricted       boolean.
   * @return int le nombre d'Atome crees.
   */
  public int update2(LinkedListRules posConstraintSet, int stopCond, boolean restricted) {
    boolean cont = true;
    int nbprof = 0;
    int nbAtomesAdd = 0;

    BddMemoire bd = this;
    BddMemoire oldList;
    BddMemoire newList = this;

    while (cont) {
      nbprof += 1;
      cont = false;
      nbAtomesAdd += newList.size();
      newList.putRank(nbprof);
      bd.addAll(
          newList); // AJOUTE, à chaque tour de boucle, la nouvelle List d'atome à la bd (si t'y touche ça donne pas le bon résultat)
      oldList = newList;
      newList = new BddMemoire();
      LOGGER.info(
          "bd size : " + bd.size() + "\nCompletion prof2 : " + nbprof + " on " + oldList.size()
              + " atomes ");

      for (Rule rule : posConstraintSet) {
        if (updateWithOnePosConstraintn_1(rule, stopCond, bd, oldList, newList, restricted,
            nbprof)) {
          cont = true;
        }
      }
      //ajout ensuite la nexList qui n'a pas été ajouté
      bd.addAll(newList);

      LOGGER.info(
          " produce : " + newList.size() + " atomes  STOP :" + stopCond + " PROF:" + nbprof);
      //logger.info("LA BDD A AJOUTER : " + newList);
      //logger.info(" "+newList);
      if (stopCond > 0 && nbprof >= stopCond) {
        cont = false;
      }
    }
    return nbAtomesAdd;
  }

  /**
   * Applique une liste de regles de type "N atomes -> 1 atomes" a la Bdd et compte le nomrbre
   * d'Atome crees.
   *
   * @param posConstraintSet LinkedListRules. La liste des regles a appliquer.
   * @param stopCond         int. La condition d'arret.
   * @param restricted       boolean.
   * @return int le nombre d'Atome crees.
   */
  public Bdd updateForTransaction(LinkedListRules posConstraintSet, int stopCond,
      boolean restricted) {
    boolean cont = true;
    int nbprof = 0;

    BddMemoire transaction = new BddMemoire();
    BddMemoire newList;// = this;

    while (cont) {
      nbprof += 1;
      cont = false;
      //newList.putRank(nbprof);
      transaction.putRank(nbprof);
      //this.addAll(newList); // AJOUTE, à chaque tour de boucle, la nouvelle List d'atome à la bd (si t'y touche ça donne pas le bon résultat)
      //oldList = newList;
      newList = new BddMemoire();
      LOGGER.info(
          "bd size : " + this.size() + "Completion prof2 : " + nbprof + " on " + transaction.size()
              + " atomes ");
      for (Rule rule : posConstraintSet) {
        LOGGER.info("this " + this);
        LOGGER.info("oldlist " + transaction);
        if (BddMemoire.updateWithOnePosConstraintn_1ForTransaction(rule, stopCond, this,
            transaction, newList, restricted, nbprof,
            new Substitution())) {
          LOGGER.info("hallo");
          cont = true;
          LOGGER.info("One completion");
        }
      }
      //ajout ensuite la nexList qui n'a pas été ajouté
      transaction.addAll(newList);
      //this.addAll(newList);

      LOGGER.info(
          " produce : " + newList.size() + " atomes  STOP :" + stopCond + " PROF:" + nbprof);
      LOGGER.info("LA BDD A AJOUTER : " + newList);
      //logger.info(" "+newList);
      if (stopCond > 0 && nbprof >= stopCond) {
        cont = false;
      }
    }
    return transaction;
  }

  /**
   * simplification de la Bdd par l'algo Core
   *
   * @return boolean
   */
  @Override
  public boolean core() {
    return core(new BddStats());
  }

  private boolean core(BddStats bddStats) {
    LOGGER.info("*****************************************************");
    // initialisation map pour chaque var nulle, lensemble des atomes contenant cette var nulle
    Map<Set<Variable>, Set<Atom>> K = new HashMap<>();
    Map<Variable, Set<Variable>> KNull = new HashMap<>();

    bddStats.startPartition();
    for (Collection<Atom> hs : bdd.values()) {
      for (Atom a : hs) {
        Set<Variable> varNullLink = new HashSet<>();
        Set<Atom> partRes = new HashSet<>();
        partRes.add(a);
        for (Variable v : a.getNulls()) {
          varNullLink.add(v);
          if (KNull.containsKey(v)) {
            varNullLink.addAll(KNull.get(v));
            if (K.containsKey(KNull.get(v))) {
              partRes.addAll(K.get(KNull.get(v)));
              K.remove(KNull.get(v));
            }
          }
        }
        for (Variable v : varNullLink) {
          KNull.put(v, varNullLink);
        }
        K.put(varNullLink, partRes);
      }
    }
    bddStats.stopPartition();

    // mettre Variable à la place de String dans la suite
    LOGGER.info("K apres merge " + K.size() + " elements");
    LOGGER.info(K.toString());
    // simplification
    boolean simplif = false;

    bddStats.startSimplification();
    for (Set<Variable> s : K.keySet()) {
      List<Element> tuple = new ArrayList<>();
      for (Variable s2 : s) {
        tuple.add(new Variable(s2.getName(), Element.NULL));
      }
      Atom h = new Atom("Core", tuple);//tete de la règle
      Rule pc = new Rule(h, K.get(s), true);//corps de la règles
      // logger.info("RULE : " + pc);

      Substitution sub = findMGInstance(pc);
      // logger.info("Après FindMGInstance");
      if (sub != null) {
        simplif = true;
        //logger.info("Ok sub " + sub);
        for (Atom a : pc.getBody()) {
          if (!a.equals(sub.applySub(a))) {
            remove(a);
          }
        }
      }
    }
    bddStats.stopSimplification();

    // logger.info("Ensemble des atomes supprimés : " + remBd);
    // return simplif;
		/* if(simplif) return core();
		else */
    return simplif;
  }

  /**
   * Retourne une substitution sigma différente de l'identité telle que sigma(body(pc)) soit inclu
   * dans la Bdd (this) et il n'existe pas de substitution qui font la même chose mais qui instancie
   * plus de variable vers des constantes ("remplace" les nulles de la règle par des constantes si
   * possibles. Le lien constantes-nulles est stocké dans la substitution)
   *
   * @param pc Rule. La regle à appliquer pour créer la substitution.
   * @return Substitution la substitution en fonction de la règle.
   */
  public Substitution findMGInstance(Rule pc) {
    //logger.info("Try to find instance from : " + pc.getBody() +" on "+this);

    List<Atom> src = pc.getBody();

    boolean fini = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];

    Substitution res = null;

    int i = 0;

    while (!fini) {
      //logger.info(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      //if(this.asMap().containsKey(src.get(i).getName())){
      //ATTENTION: modifie aussi tabSub[i] !
      //tabPos[i] augmente au fur et a mesure sir l'atom trouvé est égal à l'atom Core(..) que l'on a deja
      tabPos[i] = src.get(i)
          .mapAll(this.getAtomsWithPredicate(src.get(i).getName()), tabSub[i], tabPos[i], 0);
      //}
      //else{
      //	tabPos[i]=-1; //INUTILE ???
      //}

      if (tabPos[i]
          == -1) { //si le nom ne correspond pas a ce qu'il y a dans la bd OU si aucun map ne fonctionne
        if (i == 0) {
          fini = true;
        } else {
          i--;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead());
        if (pc.getHead().isLessInstanciate(
            a)) {// on a une substitution différente de l'identité qui filtre le corps de la regle avec la Bdd
          if (res == null) {
            res = (Substitution) tabSub[i].clone();
          } else if (res.nbRealInstances() < tabSub[i].nbRealInstances()) {
            res = (Substitution) tabSub[i].clone();
          }
        }
      } else {
        i++;
      }
    }
    return res;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    return Collections.emptyMap();
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    return Stream.empty();
  }

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    Set<Element> nullBucket = atoms.getAllPredicates().parallelStream()
        .map(this::getAtomsWithPredicate).flatMap(
            Collection::parallelStream).flatMap(Atom::stream).filter(Element::isNullValue)
        .collect(Collectors.toSet());

    nullBucket.addAll(atoms.getNulls());
    return nullBucket;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {

  }

  /**
   * Ajoute un atome (ou un groupe d'atomes) à la base de données en prenant en compte les règles
   *
   * @param depSet                LinkedListRules. La liste des regles à prendre en compte.
   * @param toAdd                 Bdd. La base de données d'atomes que l'ont VEUT ajouter.
   * @param stopCondition         int. La condition d'arret (profondeur...)
   * @param thingsToAddAfterChase Bdd. La base de données d'atomes que l'on DOIT ajouter (pour
   *                              respecter les règles).
   * @param restricted            boolean. Si le Chase doit etre lancé en mode restreint ou pas.
   * @return int le nombre d'Atome crees.
   */
  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> toAdd, LinkedListRules depSet,
      int stopCondition, Bdd thingsToAddAfterChase, boolean restricted) {
    BddStats bddStats = new BddStats();
    bddStats.start();

    // on copie la bdd source et on ajoute à cette dernière le toAdd et ensuite on fait le chase dessus
    bddStats.startInsert();
    BddMemoire copieBddSource = new BddMemoire(toAdd);

    // Enleve de l'ensemble a ajouter les Atomes deja presents dans la Bdd complete.
    copieBddSource.destroyIntersectionWithModuloNull(this);
    bddStats.stopInsert();
    copieBddSource.addAll(this);

    // Applique le chase
    // copieBddSource est nettoyé de la BddSource (il ne reste donc plus que les nouveaux atomes à ajouter)
    // stocke tout les nouveaux elements (ceux de toAdd compris) dans thingsToAddAfterChase
    bddStats.startChase();
    copieBddSource.update2(depSet, stopCondition, restricted);
    copieBddSource.destroyIntersectionWith(this);
    bddStats.stopChase();

    thingsToAddAfterChase.addAll(copieBddSource);
    bddStats.addAtoms(thingsToAddAfterChase.size());

    // Enleve de la bdd complete, par l'ajout et l'application du chase de cet ajout,
    // les Atomes presents dans thingsToAddAfterChase.
    // this.destroyIntersectionWith(thingsToAddAfterChase);

    bddStats.startCommit();
    this.addAll(thingsToAddAfterChase);
    bddStats.stopCommit();

    this.core(bddStats);

    bddStats.stop();
    return bddStats;
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    BddStats bddStats = new BddStats();
    bddStats.start();

    //supressionVersionHalfeld
    LOGGER.info(
        "----------------------------MI NOUVELLE SUPPRESSION-----------------------------------");
    BddMemoire bddTemporaire = new BddMemoire(this);
    BddMemoire atomsToDelete = new BddMemoire(dRequest);

    // on supprime de la bddTemporaire les atomes de toDel (en faisant attention aux éléments null...)
    bddStats.delAtoms(dRequest.size());
    bddStats.startDelete();
    bddTemporaire.destroyIntersectionWithModuloNull(atomsToDelete);
    bddStats.stopDelete();

    // on applique le Chase sur bddTemporaire
    bddStats.startChase();
    bddTemporaire.update2(rules, 0, true);
    bddStats.stopChase();

    // on lui applique le core
    bddTemporaire.core(bddStats);

    LOGGER.info("BDD TEMP: " + bddTemporaire.size() + "atomes");

    // on rassemble les atomes qui ont une profondeur supérieur à celle souhaité
    bddStats.startNullDegree();
    Bdd unsuitable = new BddMemoire();
    for (Atom a : this) {
      if (a.getRank() > deltaMax && deltaMax > 0) {
        unsuitable.add(a);
      }
    }
    bddStats.stopNullDegree();

    // on rassemble les atomes qui ont pu réapparaitre après le Chase
    bddStats.startChase();
    BddMemoire back = new BddMemoire();
    back.addAll(bddTemporaire);
    back.keepIntersectionWithModuloNull(atomsToDelete);

    // on regroupe unsuitable et back ensemble dans la Bdd unsuitable
    unsuitable.addAll(back);
    bddStats.stopChase();

    LOGGER.info("BDD UNSUITABLE: " + unsuitable);

    //on regarde s'il y a des éléments dans unsuitable
    //s'il y en a, alors on enlève à BddTemporaire le résultat de la fonction chaseInverse, on lui applique le core
    // et enfin on applique le résulat à la Bdd result
    //s'il n'y en a pas, alors on applique directement le résulat à la Bdd result
    if (unsuitable.size() != 0) {
      bddStats.startDelete();
      Bdd toDel = deleteOperator(rules, bddTemporaire, unsuitable, deltaMax, restricted);
      bddTemporaire.destroyIntersectionWith(toDel);
      bddStats.stopDelete();
      bddStats.delAtoms(toDel.size());
      bddTemporaire.core(bddStats);
    }

    bddStats.startCommit();
    this.initFromDB(bddTemporaire);
    bddStats.stopCommit();

    bddStats.stop();
    return bddStats;
  }

  /**
   * Operateur de suppression qui supprime tous les atomes souhaité ainsi que les atomes qui leur
   * sont liés
   *
   * @param depSet                  LinkedListRules. La liste des regles à prendre en compte.
   * @param bddSourceApresDelSimple Bdd. La base de données source après lui avoir supprimé les
   *                                éléments souhaités.
   * @param toDel                   Bdd. La Bdd des atomes que l'on veut supprimer.
   * @param cycleCond               int. la condition de boucle choisie par l'utilisateur.
   * @param restreint               boolean. Si le Chase doit etre lancé en mode restreint ou pas.
   * @return Bdd les atomes qui doivent etre supprimer.
   */
  protected Bdd deleteOperator(LinkedListRules depSet, Bdd bddSourceApresDelSimple, Bdd toDel,
      int cycleCond, boolean restreint) {
    // logger.info("DELETE OPERATOR toDel " + toDel);
    bddSourceApresDelSimple.removeAll(toDel);
    // logger.info("DELETE OPERATOR bddSourceApresDelSimple " + bddSourceApresDelSimple.nbFact() + " atomes");
    //on supprime de la base de données
    //on enregistre ici les atomes qu'il y a à virer en plus des atomes de bases
    BddMemoire toDelPlus = new BddMemoire();
    //on enregistre ici les atomes sur lequels on veut boucler pour verifier leurs dépendances
    BddMemoire aVerif = new BddMemoire();
    aVerif.addAll(toDel);
    boolean finish = false;
    BddMemoire copieBddSourceApresDelSimple = new BddMemoire();
    copieBddSourceApresDelSimple.addAll(bddSourceApresDelSimple);

    while (!finish) {
      //logger.info("Averif = " + aVerif);
      Substitution s2 = new Substitution();
      for (Atom a : aVerif) {
        for (Rule r : depSet) {
          //logger.info(r.getHead().getName() +" et "+ a.getName());
          if (r.getHeadNames().equals(a.getName())) {

            //on cré une Substitution et on map la/les variables de l'atome de la tete de la règle aux éléments de
            //l'atome instancié 'a'.
            Substitution s = new Substitution();
            r.getHead().map(a, s);
            //logger.info("LA SUBSTITUTION EN FCT DE L'ATOME A EFFACER: " + s);

            //pour tous les atomes dans le corps de la règle (et dans la tete), on va les instanciés grace
            //à la substitution précédente
            //logger.info("LA QUERY AAVNT INSTANCE: " + r);
            Rule instanciatedRule = s.applySub(r);
            //logger.info("LA QUERY SEMI INSTANCIE: " + instanciatedRule);

            //on trouve la substitution tel que le corp de la QUERY soit présent dans la bdd source
            // après la suppression des atomes voulu
            //(s'il n'y a pas de substitution possible, alors la règle ne s'applique pas à notre atome et
            //on passe à la règle suivante)
            Substitution subForSaveS2 = s2 != null ? (Substitution) s2.clone() : new Substitution();
            //logger.info("la BDD avec LAQUELLE ON TROUVE LA SUB: " + copieBddSourceApresDelSimple);
            s2 = copieBddSourceApresDelSimple.findInstance(instanciatedRule);
            //if(s2!=null) { logger.info("Au moins une instance: " + s2);}
            //else {logger.info("Pas d'instance: ");}

            // new implementation jac april 2019
            while (s2 != null) {
              //on complete notre substitution actuel avec la sub d'avant car il se peut que les
              //à vérifier soit liées entre eux (ex : A(N1,j) et B(t,N1) sont liés par l'élément N1)
              s2.putAll(subForSaveS2);
              //logger.info("LA SUBSTITUTION EN FCT DE LA QUERY: " + s2);

              //une fois la substitution s2 trouvé (cela veut dire que le corps de la règle est instancialble)
              //alors on instancie la query précédente (qui avait été SEMI instancié par l'atome de la tete de
              //la règle), pour que le corps de la règle soit enfin TOTALEMENT instancié
              Rule instanciatedRule2 = s2.applySub(instanciatedRule);
              //logger.info("LA QUERY INSTANCIE: " + instanciatedRule2);

              // on supprime les atomes marqué
              for (Atom b : instanciatedRule2.getAtomesToDelInBody()) {
                toDelPlus.add(b);
                copieBddSourceApresDelSimple.remove(b);
              }

              // on tente de trouver une nouvelle substitutions
              s2 = copieBddSourceApresDelSimple.findInstance(instanciatedRule);

            }
/*
							if (s2 != null){
								//on complete notre substitution actuel avec la sub d'avant car il se peut que les
								//à vérifier soit liées entre eux (ex : A(N1,j) et B(t,N1) sont liés par l'élément N1)
								s2.completeWith(subForSaveS2);
								logger.info("LA SUBSTITUTION EN FCT DE LA QUERY: " + s2);

								//une fois la substitution s2 trouvé (cela veut dire que le corps de la règle est instancialble)
								//alors on instancie la query précédente (qui avait été SEMI instancié par l'atome de la tete de
								//la règle), pour que le corps de la règle soit enfin TOTALEMENT instancié
								Rule instanciatedRule2 = s2.applySub(instanciatedRule);
								logger.info("LA QUERY INSTANCIE: " + instanciatedRule2);

								boolean stop = false;
								for (Atome b : instanciatedRule2.getBody())
								{
									if (toDel.contains(b) && !instanciatedRule2.getAtomesToDelInBody().contains(b)){
										stop = true;
									}
								}

								if (!stop){
									//ensuite on cré la TETE de la règle en partant du corps instancié (comme si le corps
									// était la Bdd
									Bdd newBdd = new BddMemoire();
									Bdd bddCorpsQuery = new BddMemoire();
									bddCorpsQuery.addAll(instanciatedRule2.getBody());
									updateWithOnePosConstraintn_1(r,cycleCond,bddCorpsQuery,bddCorpsQuery,newBdd,restreint,0,true);
									Atome tetePattern = new Atome(" ");
									for (String k : newBdd.asMap().keySet()){
										for (Atome b : newBdd.asMap().get(k)){
											tetePattern = b;
										}
									}
									logger.info("LE PATTERN A COMPARER: " + tetePattern);

									//on regarde si cette tete de règle peut etre mapper avec un atome de la BddSource
									// et si ce dernier n'est pas censé etre supprimé
									// s'il peut etre mapper alors cpt est au moins égal à un
									int cpt = 0;
									for (String k : copieBddSourceApresDelSimple.asMap().keySet()){
										for (Atome b : copieBddSourceApresDelSimple.asMap().get(k)){
											logger.info("LA CONDITION QUI FOIRE : " + toDel.contains(b));
											logger.info("LA CONDITION QUI FOIRE 2: " + (tetePattern.isLessInstanciate(b) || tetePattern.isEqual(b)));
											//si la tete est présente dans la bdd et qu'elle n'est pas censé etre supprimé, alors on fait cpt++
											if ((tetePattern.isLessInstanciate(b) || tetePattern.isEqual(b)) && !toDel.contains(b)){ //ATTENTION ICI SEUL LE PREMIER PATTERN EST VERIF
												cpt++;
												logger.info("PASSE DEDANS");
											}
										}
									}
									if (cpt ==0){
										logger.info("L'ATOME VA ETRE EFFACE " + instanciatedRule2.getAtomesToDelInBody());
										for (Atome b : instanciatedRule2.getAtomesToDelInBody()){
											toDelPlus.add(b);
										}
									}
								}

							}
*/

          }
        }
      }
      // logger.info("TODELPLUS 1 : " + toDelPlus);
      if (toDelPlus.isEmpty()) {
        finish = true;
      }
      //on nettoie ce qu'on avait à vérifier (puisque c'est fait)
      //on ajoute les éléments à supprimer en plus dans la Bdd aVerif afin qu'ils soient vérifier
      // on détruit les atomes en commun entre toDel et aVerif Dans la Bdd aVerif pour ne pas
      //vérifier plusieurs fois un meme atome
      //on ajoute les éléments à 'supprimer en plus' dans la Bdd toDel pour garder un ensemble où on a tous les atomes
      // à supprimer réuni
      //on supprime de la copie de la bddSource les atomes qu'on veut supprimer, ainsi cela évite que si des règles
      // sont cycliques, il y ait une infinité de boucle
      //on nettoie la Bdd toDelPlus puisqu'on a sauvegarder ailleurs les atomes
      aVerif.clear();
      aVerif.addAll(toDelPlus);
      aVerif.destroyIntersectionWith(toDel);
      toDel.addAll(toDelPlus);
      copieBddSourceApresDelSimple.destroyIntersectionWith(
          toDel); //on utilise ici toDel et non toDelPlus au cas où au départ dans toDel on a un atome qui est revenu et qui doit donc etre supprimer ensuite
      toDelPlus.clear();
      // logger.info("TODELPLUS 2 : " + toDelPlus);
    }
    LOGGER.info("VOICI CE QUE NOUS DEVONS SUPPRIMER: " + toDel);
    return toDel;
  }

  /**
   * Véridie si les atomes de this sont liés entre eux par des nulles
   *
   * @return boolean Retourne vrai si au moins deux atomes présent dans this sont lié par des nulles
   */
  public boolean atomesAreLinkedByNull() {
    for (Atom a : this) {
      for (Atom b : this) {
        if (a.containsNullValues() && b.containsNullValues() && !a.equals(b)) {
          Collection<String> names = a.getNullsName();
          names.retainAll(b.getNullsName());
          if (!names.isEmpty()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  /**
   * Modifie le numéro minimum d'une nouvelle variable nulle (afin qu'il n'y ait pas de nouvelle
   * variable nulle de meme nom
   */
  public void putStartNullValue() {
    int max = 0;
    //pour tous les atomes de la bdd
    for (Atom a : this) {
      //si l'atome contient des éléments nuls
      if (a.containsNullValues()) {
        //pour tous les noms des éléments nuls
        for (String s : a.getNullsName()) {
          //on trouve le numéro du nuls
          int num;
          int debut = s.indexOf("New");
          if (debut != -1) {
            int fin = s.indexOf("_");
            num = Integer.parseInt(s.substring(debut + 3, fin));
          } else {
            num = 1;
          }
          //on teste si on enregistre le numéro  ou pas
          if (max < num) {
            max = num;
          }
        }
      }
    }
    BddMemoire.numNewVarBd = max;
    LOGGER.info("LE NUMERO DE NULL EST : " + BddMemoire.numNewVarBd);
  }

  /**
   * Applique une liste de regles de type "1 atome -> 1 atome" a la Bdd et compte le nomrbre d'Atome
   * crees.
   *
   * @param posConstraintSet LinkedListRules. La liste des regles a appliquer.
   * @param stopCond         int. ???
   * @return int le nombre d'Atome crees.
   * @deprecated
   */
  int update1(LinkedListRules posConstraintSet, int stopCond, boolean restricted) {
    boolean cont = true;
    int nbprof = 0;
    BddMemoire oldList;
    BddMemoire newList = this;
    BddMemoire bd = new BddMemoire();
    int nbAtomesAdd = 0;

    while (cont) {
      nbprof += 1;
      cont = false;
      oldList = newList;
      newList = new BddMemoire();
      System.out.print(
          "bd size : " + bd.size() + "Completion prof : " + nbprof + " on " + oldList.size()
              + "atomes ");

      for (Rule rule : posConstraintSet) {
        if (update1WithOnePosConstraint1_1(rule, stopCond, bd, oldList, newList, restricted)) {
          cont = true;
        }
      }
      if (nbprof == 1) {
        bd = oldList;
      } else {
        nbAtomesAdd += oldList.size();
        bd.addAll(oldList);
      }
      LOGGER.info(" produce : " + newList.size() + " atomes");
      //logger.info(" "+newList);
      if (stopCond > 0 && nbprof >= stopCond) {
        cont = false;
      }
    }
    return nbAtomesAdd;
  }

  /**
   * Applique une liste de regles a la Bdd et compte le nombre d'Atome crees.
   *
   * @param posConstraintSet      LinkedListRules. La liste des regles a appliquer.
   * @param toAdd                 Bdd. les atomes que l'on veut ajouter
   * @param stopCond              int. en fonction du traitement voulu pour les valeurs nulles
   * @param thingsToAddAfterChase Bdd.
   * @param restricted            boolean.
   * @return int le nombre d'atomes que l'on doit ajouter pour respecter les inférences.
   * @deprecated
   */
  int updateAdd(LinkedListRules posConstraintSet, BddMemoire toAdd, int stopCond,
      Bdd thingsToAddAfterChase, boolean restricted) {
    boolean cont = true;
    int nbprof = 0;
    int nbAtomesAdd = 0;

    BddMemoire bd = this;
    BddMemoire oldList;
    BddMemoire newList = toAdd;

    while (cont) {
      nbprof += 1;
      cont = false;
      nbAtomesAdd += newList.size();
      newList.putRank(nbprof);
      bd.addAll(newList);
      //buff = newList.clone();
      thingsToAddAfterChase.addAll(newList);
      oldList = newList;
      newList = new BddMemoire();
      System.out.print(
          "bd size : " + bd.size() + "Completion prof : " + nbprof + " on " + oldList.size()
              + " atomes ");

      for (Rule rule : posConstraintSet) {
        if (updateAddWithOnePosConstraintn_1(rule, stopCond, nbprof, bd, oldList, newList,
            restricted)) {
          cont = true;
          //logger.info("One completion");
        }
        //logger.info("num dep : "+i);
      }

      LOGGER.info(" produce : " + newList.size() + " atomes");
      if (stopCond > 0 && nbprof >= stopCond) {
        cont = false;
      }
      //logger.info(" "+newList);
    }
    return nbAtomesAdd;

  }

  /**
   * garde dans 'this' SEULEMENT les elements en commun que 'this' a avec le parametre.
   *
   * @param otherBdd Bdd. La bdd avec laquelle on verifie l'intersection
   * @deprecated
   */
  @Deprecated
  public void keepIntersectionWith(Bdd otherBdd) {
    Map<String, List<Atom>> otherMap = otherBdd.asMap();
    Set<String> otherkeySet = otherMap.keySet();
    //on verifie que la clé est bien présente dans this (ex: B, A, E, ...)
    // et on ne garde que ce qui est commun à otherBdd et this
    for (String key : otherkeySet) {
      if (this.bdd.containsKey(key)) {
        this.bdd.get(key).retainAll(otherMap.get(key));
      }
    }
    //on ne garde dans thisKeySet que les clé qui ne sont pas dans otherBdd,
    // et ensuite on les supprime de this.
    Set<String> thisKeySet = new HashSet<>(this.getAllPredicates());
    thisKeySet.removeAll(otherkeySet);
    this.bdd.keySet().removeAll(thisKeySet);
  }

  /**
   * Retourne une substitution sigma différente de l'identité telle que sigma(body(pc)) soit inclu
   * dans la Bdd (this) (retourne la première solution trouvée ?)
   *
   * @param pc Rule.
   * @return Substitution Une substitution...
   */
  public Substitution findInstance(Rule pc) {
    //logger.info("Try to find instance from : " + pc.getBody() +" on "+this);

    List<Atom> src = pc.getBody();
    Bdd trg = this;

    boolean fini = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    // tabSub[0]=new Substitution(); // fait dans le while

    int i = 0;

    while (!fini) {
      //logger.info(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      if (trg.containsPredicate(src.get(i).getName())) {
        tabPos[i] = src.get(i)
            .mapAll(trg.getAtomsWithPredicate(src.get(i).getName()), tabSub[i], tabPos[i], 0);
      } else {
        tabPos[i] = -1;
      }

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        // je ne comprends pas la suite : commenté april 2019
				/* Atome a= tabSub[i].applySub(pc.getHead());
				if(!a.isEqual(pc.getHead())){
					// on a une substitution différente de l'identité qui filtre le corps de la regle avec la Bdd
					return tabSub[i];
				} */
        return tabSub[i];
      } else {
        i = i + 1;
      }
    }
    //logger.info(result);
    return null;
  }

  /**
   * Supprime un atome (ou une liste d'atomes) de la base de données en prenant en compte les
   * règles
   *
   * @param depSet    LinkedListRules. La liste des regles à prendre en compte.
   * @param toDel     Bdd. La base de données d'atomes que l'ont VEUT supprimer.
   * @param bddResult Bdd. La base de données résultat (ce à quoi ressemble la Bdd Source après la
   *                  suppression).
   * @deprecated
   */
  void suppressionVersionChabin(LinkedListRules depSet, BddMemoire toDel, BddMemoire bddResult,
      int cycleCond, boolean restreint) {
    LOGGER.info(
        "----------------------------LA NOUVELLE SUPPRESSION-----------------------------------");
    BddMemoire bddTemporaire = new BddMemoire();
    bddTemporaire.addAll(this);

    // Enleve de l'ensemble a supprimer les Atomes qui ne sont PAS presents dans la Bdd source.
    // Mais garde les éléments qui sont présent dans la Bdd modulo le nom du "New" qu'ils possèdent
    //ex: A(r,N8) est dans la Bdd et A(r,N1) est dans les atomes à supprimer, on va considerer ici
    //que N1 == N8 (car ils ne sont as dans la même Bdd donc ils peuvent très bien etre égaux)
    //toDel.keepIntersectionWith(this);

    //on regarde si les atomes que l'on veut supprimer sont liés par des nulles
    if (toDel.atomesAreLinkedByNull()) {
      LOGGER.info("LES ATOMES SONT LIEES PAR DES NULLES");
      //s'ils sont liés par des nulles et qu'ils sont présent dans la Bdd source modulo null
      //alors on ne fait rien de spéciale, sinon, si au moins un atome qui est lié par un null
      //n'est pas dans la bdd source (modulo null) alors on vide la bdd toDel car on ne peut pas effacer
      // un seul atome lié par un null si l'autre n'est pas présent
      // La condition avec first permet de définir une substitution qui servira ensuite à vérifier que les
      //deux atomes liés dans la Bdd toDel sont aussi liés dans la Bdd source
      boolean present = true;
      boolean first = true;
      Substitution sub = new Substitution();
      Set<String> groupeNullToDel = new HashSet<>();
      Collection<Atom> tmp;
      for (Map.Entry<String, List<Atom>> entry : toDel.asMap().entrySet()) {
        for (Atom a : entry.getValue()) {
          if (a.containsNullValues() && first) {
            first = false;
            //le -1 est bizard mais c'est comme ça...
            tmp = bddTemporaire.getAtomsWithPredicate(entry.getKey());
            a.mapAll(tmp, sub, -1, cycleCond);
            LOGGER.info("Bdd temp : " + tmp + " " + a);
            LOGGER.info("SUB : " + sub);
          }
          LOGGER.info("ATOME APPLY SUB : " + sub.applySub(a));
          if (!bddTemporaire.contains(sub.applySub(a))) {
            //au moins un atome lié n'est pas présent dans la Bdd source
            //on enregistre le null qui fait le lien avec l'autre atome pour savoir quel
            //groupe d'atomes on supprime (il peut y avoir plusieurs groupe d'atomes liés par
            //des nulles différent, on ne peut donc pas se contenter d'effacer tous les atomes
            //avec des nulles.
            for (Element e : a) {
              if (e.isNullValue()) {
                groupeNullToDel.add(e.getName());
              }
            }

            present = false;
            break;
          }
        }
      }
      if (!present) {
        LOGGER.info("LES ATOMES LIES A SUPPR NE SONT PAS DANS LA BDD SOURCE : " + groupeNullToDel);
        Bdd useless = new BddMemoire();
        for (Atom a : toDel) {
          for (String nameNullVar : a.getNullsName()) {
            if (groupeNullToDel.contains(nameNullVar)) {
              useless.add(a);
            }
          }
        }
        toDel.destroyIntersectionWith(useless);
      }
    }
    //on supprime de la bddTemporaire les atomes de toDel (en faisant attention aux éléments null...)
    bddTemporaire.destroyIntersectionWithModuloNull(toDel);
    //on applique le Chase sur bddTemporaire
    bddTemporaire.update2(depSet, 0, true);
    //on lui applique le core
    bddTemporaire.core();

    LOGGER.info("BDD TEMP: " + bddTemporaire);

    //on rassemble les atomes qui ont une profondeur supérieur à celle souhaité
    BddMemoire unsuitable = new BddMemoire();
    for (Atom a : bddTemporaire) {
      //logger.info("ATOME : " + a.getRank());
      if (a.getRank() > cycleCond && cycleCond > 0) {
        unsuitable.add(a);
      }
    }

    LOGGER.info("BDD UNSUITABLE: " + unsuitable);

    //on rassemble les atomes qui ont pu réapparaitre après le Chase
    BddMemoire back = new BddMemoire();
    back.addAll(bddTemporaire);
    back.keepIntersectionWithModuloNull(toDel);
    //on regroupe unsuitable et back ensemble dans la Bdd unsuitable
    unsuitable.addAll(back);

    //on regarde s'il y a des éléments dans unsuitable
    //s'il y en a, alors on enlève à BddTemporaire le résultat de la fonction chaseInverse, on lui applique le core
    // et enfin on applique le résulat à la Bdd result
    //s'il n'y en a pas, alors on applique directement le résulat à la Bdd result
    if (!unsuitable.isEmpty()) {
      bddTemporaire.destroyIntersectionWith(
          deleteOperator(depSet, bddTemporaire, unsuitable, cycleCond, restreint));
      bddTemporaire.core();
    }
    bddResult.addAll(bddTemporaire);
  }

  @Override
  public boolean isConnected() {
    return true;
  }

  @Override
  public void close() {
  }

  @Override
  public Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    return null;
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    try (Stream<Atom> stream = this.stream()) {
      stream.flatMap(Atom::stream).filter(nulls::contains)
          .forEach(nullElement -> ((Variable) nullElement).setProf(degree));
    }
  }

}

