package fr.lifo.updatechase.model.db.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ConnexionMySQL gere le chargement du driver MySQL pour JAVA et de l'initialisation de la
 * connexion a la base de donee.
 */
public class ConnexionMySQL {

  Connection mysql = null;
  boolean connecte = false;

  /**
   * Constructeur de ConnexionMySQL. Si le chargement du driver ou la connexion echoue, l'attribut
   * mysql est set a null.
   *
   * @param String nomServeur le nom du serveur auquel on veut se connecte.
   * @param String nomBase le nom de la base de donne auquelle on veut se connecte.
   * @param String nomLogin le login de connexion au serveur.
   * @param String motDePasse le mot de passe de connexion au serveur.
   */
  public ConnexionMySQL(String nomServeur, String nomBase, String nomLogin, String motDePasse) {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      System.out.println("Driver MySQL non trouvé?");
      mysql = null;
      return;
    }
    try {
      mysql = DriverManager.getConnection("jdbc:mysql://" + nomServeur + ":3306/" + nomBase,
          nomLogin, motDePasse);
      System.out.println("OK connecté");
      connecte = true;
    } catch (SQLException e) {
      System.out.println("Echec de connexion!");
      System.out.println(e.getMessage());
      mysql = null;
    }
  }

}
