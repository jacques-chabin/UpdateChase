package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Schema;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.driver.QueryRunner;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.exceptions.Neo4jException;
import org.neo4j.driver.summary.SummaryCounters;

public class BddNeo4jV2 extends BddNeo4j {

  protected final Schema schema;
  private final boolean isAttrGraph;

  public BddNeo4jV2(String address, int port) {
    this(address, port, new Schema());
  }

  public BddNeo4jV2(String address, int port, Schema schema) {
    this(address, port, null, null, schema);
  }

  public BddNeo4jV2(String address, int port, String user, String password) {
    this(address, port, user, password, new Schema());
  }

  public BddNeo4jV2(String address, int port, String user, String password, Schema schema) {
    this(address, port, user, password, schema, false);
  }

  public BddNeo4jV2(String address, int port, String user, String password, Schema schema,
      boolean isAttrGraph) {
    super(address, port, user, password);
    this.isAttrGraph = isAttrGraph;
    this.schema = schema;
  }

  private static Atom buildAtomFromRecord(String pred, Map<String, Object> attr) {
    Element[] elements = new Element[attr.size()];
    int i;

    if (attr.containsKey("_id")) {
      elements[0] = Element.stringToElement(attr.get("_id").toString());
      i = 1;

    } else {
      elements[0] = Element.stringToElement(attr.get("from").toString());
      elements[1] = Element.stringToElement(attr.get("to").toString());
      i = 2;
    }

    for (; i < elements.length; i++) {
      elements[i] = Element.stringToElement(attr.get("rank" + i).toString());
    }

    return new Atom(pred, elements);
  }

  private static Atom recordToAtom(Record record) {
    String pred = record.get(0).asString();
    Map<String, Object> attr = record.get(1).asMap();

    return buildAtomFromRecord(pred, attr);
  }

  private static Set<Atom> recordToListAtom(Record record) {
    Set<Atom> atoms = new HashSet<>();

    for (int i = 0; i < record.size(); i += 2) {
      String pred = record.get(i).asString();
      Map<String, Object> attr = record.get(i + 1).asMap();

      atoms.add(buildAtomFromRecord(pred, attr));
    }

    return atoms;
  }

  @Override
  protected void postInit(QueryRunner runner) throws Neo4jException {
    if (this.isAttrGraph) {
      for (String nodeLabel : this.schema.getNodesLabels()) {
        runner.run("CREATE CONSTRAINT " + nodeLabel + " IF NOT EXISTS ON (n:" + nodeLabel
            + ") ASSERT n._id IS UNIQUE");
      }
    }
  }

  private Map<String, Object> getQueryParamsFromAtom(Atom atom) {
    Map<String, String> atomAttributes = new HashMap<>();
    for (int i = 0; i < atom.size(); i++) {
      Element element = atom.get(i);
      String name = element.isConstant() ? element.getName() : getNullName((Variable) element);

      atomAttributes.put("rank" + i, name);
    }

    Map<String, Object> atomDefinition = new HashMap<>();
    atomDefinition.put("name", atom.getName());
    atomDefinition.put("attr", atomAttributes);

    if (this.schema.isEdge(atom)) {
      atomDefinition.put("from", atomAttributes.remove("rank0"));
      atomDefinition.put("to", atomAttributes.remove("rank1"));
    } else {
      atomAttributes.put("_id", atomAttributes.remove("rank0"));
    }

    return atomDefinition;
  }

  private Map<String, Object> getQueryParamsFromAtomsSchema(Collection<? extends Atom> atoms) {
    Collection<Map<String, Object>> nodes = new ArrayList<>(atoms.size());
    Collection<Map<String, Object>> edges = new ArrayList<>(atoms.size());

    for (Atom atom : atoms) {
      Map<String, Object> atomDefinition = this.getQueryParamsFromAtom(atom);
      if (this.schema.isEdge(atom)) {
        edges.add(atomDefinition);
      } else {
        nodes.add(atomDefinition);
      }
    }

    Map<String, Object> params = new HashMap<>();
    params.put("nodes", nodes);
    params.put("edges", edges);

    return params;
  }

  private String getQueryModulloNull(Collection<? extends Atom> atoms) {
    StringBuilder match = new StringBuilder("MATCH ");
    StringBuilder where = new StringBuilder(" WHERE ");
    StringBuilder output = new StringBuilder(" RETURN ");
    boolean firstAtom = true;
    int i = 0;

    // Map of attributes sharing a null
    Map<String, String> nullAttrMap = new HashMap<>();

    for (Atom atom : atoms) {
      Map<String, Object> atomDefinition = this.getQueryParamsFromAtom(atom);
      Map<String, String> attributes = (Map<String, String>) atomDefinition.get("attr");
      String atomVar, fromVar, toVar, fromValue, toValue;

      if (!firstAtom) {
        match.append(", ");
        where.append(" AND ");
        output.append(", ");
      }

      // Atoms
      if (this.schema.isEdge(atom)) {
        atomVar = "`x" + atomDefinition.get("from") + "_" + atomDefinition.get("to") + "`";
        fromVar = "`x" + atomDefinition.get("from") + "`";
        toVar = "`x" + atomDefinition.get("to") + "`";
        fromValue = atomDefinition.get("from").toString();
        toValue = atomDefinition.get("from").toString();

        match.append("(").append(fromVar).append(")-[").append(atomVar).append("]->(").append(toVar)
            .append(")");
        where.append("type(").append(atomVar).append(") = '").append(atomDefinition.get("name"))
            .append("'");
        output.append("'").append(atomDefinition.get("name")).append("' AS out").append(++i)
            .append(", ").append(atomVar).append(
                "{.*, from: ").append(fromVar).append("._id, to: ").append(toVar).append("._id} AS out")
            .append(++i);

        // From attribute
        if (!fromValue.startsWith("_")) {
          where.append(" AND ").append(fromVar).append("._id").append(" = '").append(fromValue)
              .append("'");

        } else if (nullAttrMap.containsKey(fromValue)) {
          String originalNullVar = nullAttrMap.get(fromValue);
          where.append(" AND ").append(fromVar).append("._id").append(" = ")
              .append(originalNullVar);

        } else {
          nullAttrMap.put(fromValue, fromVar + "._id");
        }

        // To attribute
        if (!toValue.startsWith("_")) {
          where.append(" AND ").append(toVar).append("._id").append(" = '").append(toValue)
              .append("'");

        } else if (nullAttrMap.containsKey(toValue)) {
          String originalNullVar = nullAttrMap.get(toValue);
          where.append(" AND ").append(toVar).append("._id").append(" = ").append(originalNullVar);

        } else {
          nullAttrMap.put(toValue, toVar + "._id");
        }

      } else {
        atomVar = "`x" + atomDefinition.get("name") + attributes.get("_id") + "`";
        match.append("(").append(atomVar).append(")");
        where.append("'").append(atomDefinition.get("name")).append("' IN labels(").append(atomVar)
            .append(")");
        output.append("'").append(atomDefinition.get("name")).append("' AS out").append(++i)
            .append(", ").append(atomVar).append(
                "{.*} AS out").append(++i);
      }

      // Attributes
      for (Map.Entry<String, String> attr : attributes.entrySet()) {
        if (!attr.getValue().startsWith("_")) {
          where.append(" AND ").append(atomVar).append(".").append(attr.getKey()).append(" = '")
              .append(attr.getValue()).append(
                  "'");

        } else if (nullAttrMap.containsKey(attr.getValue())) {
          String originalNullVar = nullAttrMap.get(attr.getValue());
          where.append(" AND ").append(atomVar).append(".").append(attr.getKey()).append(" = ")
              .append(originalNullVar);

        } else {
          nullAttrMap.put(attr.getValue(), atomVar + "." + attr.getKey());
        }
      }

      firstAtom = false;
    }

    return match.append(where).append(output).toString();
  }

  private String generateChaseQuerySchema(Rule rule) {
    System.out.println(rule);
    StringBuilder matchBody = new StringBuilder("MATCH ");
    StringBuilder whereBody = new StringBuilder(" WHERE ");
    StringBuilder matchHead = new StringBuilder(" AND NOT EXISTS { MATCH ");
    StringBuilder whereHead = new StringBuilder(" WHERE ");
    StringBuilder output = new StringBuilder(" } RETURN { ");
    boolean firstAtom = true;
    boolean firstOut = true;

    // Map of attributes sharing a null
    Map<String, List<String>> nullAttrMap = new HashMap<>();

    // Rule Body
    for (Atom atom : rule.getBody()) {
      Map<String, Object> atomDefinition = this.getQueryParamsFromAtom(atom);
      Map<String, String> attributes = (Map<String, String>) atomDefinition.get("attr");
      String atomVar, fromVar, toVar, fromValue, toValue;

      if (!firstAtom) {
        matchBody.append(", ");
        whereBody.append(" AND ");
      }

      // Atoms
      if (this.schema.isEdge(atom)) {
        atomVar = "`x" + atomDefinition.get("from") + "_" + atomDefinition.get("to") + "`";
        fromVar = "`x" + atomDefinition.get("from") + "`";
        toVar = "`x" + atomDefinition.get("to") + "`";
        fromValue = atomDefinition.get("from").toString();
        toValue = atomDefinition.get("from").toString();

        matchBody.append("(").append(fromVar).append(")-[").append(atomVar).append("]->(")
            .append(toVar).append(")");
        whereBody.append("type(").append(atomVar).append(") = '").append(atomDefinition.get("name"))
            .append("'");
        //output.append(atomDefinition.get("name")).append(", ").append(atomVar).append("{.*, from: ").append(fromVar).append("._id, to: ").append(toVar).append("._id}");

        // From attribute
        if (!fromValue.startsWith("_")) {
          whereBody.append(" AND ").append(fromVar).append("._id").append(" = '").append(fromValue)
              .append("'");

        } else {
          if (!nullAttrMap.containsKey(fromValue)) {
            nullAttrMap.put(fromValue, new ArrayList<>());
          }

          nullAttrMap.get(fromValue).add(fromVar + "._id");
        }

        // To attribute
        if (!toValue.startsWith("_")) {
          whereBody.append(" AND ").append(toVar).append("._id").append(" = '").append(toValue)
              .append("'");

        } else {
          if (!nullAttrMap.containsKey(toValue)) {
            nullAttrMap.put(toValue, new ArrayList<>());
          }

          nullAttrMap.get(toValue).add(toVar + "._id");
        }

      } else {
        atomVar = "`x" + atomDefinition.get("name") + attributes.get("_id") + "`";
        matchBody.append("(").append(atomVar).append(")");
        whereBody.append("'").append(atomDefinition.get("name")).append("' IN labels(")
            .append(atomVar).append(")");
        //output.append(atomDefinition.get("name")).append(", ").append(atomVar).append("{.*}");
      }

      // Attributes
      for (Map.Entry<String, String> attr : attributes.entrySet()) {
        if (!attr.getValue().startsWith("_")) {
          whereBody.append(" AND ").append(atomVar).append(".").append(attr.getKey()).append(" = '")
              .append(
                  attr.getValue()).append("'");

        } else {
          if (!nullAttrMap.containsKey(attr.getValue())) {
            nullAttrMap.put(attr.getValue(), new ArrayList<>());
          }

          nullAttrMap.get(attr.getValue()).add(atomVar + "." + attr.getKey());
        }
      }

      firstAtom = false;
    }

    // Add null atoms link
    for (Map.Entry<String, List<String>> entry : nullAttrMap.entrySet()) {
      String value = entry.getKey().substring(0, entry.getKey().lastIndexOf("#"));
      List<String> variables = entry.getValue();

      for (int i = 1; i < variables.size(); i++) {
        whereBody.append(" AND ").append(variables.get(0)).append(" = ").append(variables.get(i));
      }

      if (!firstOut) {
        output.append(", ");
      }

      output.append("`").append(value).append("`: ").append(variables.get(0));
      firstOut = false;
    }

    // Rule Head
    Map<String, Object> atomDefinition = this.getQueryParamsFromAtom(rule.getHead());
    Map<String, String> attributes = (Map<String, String>) atomDefinition.get("attr");
    String atomVar, fromVar, toVar;

    if (this.schema.isEdge(rule.getHead())) {
      atomVar = "`x" + atomDefinition.get("from") + "_" + atomDefinition.get("to") + "`";
      fromVar = "`x" + atomDefinition.get("from") + "`";
      toVar = "`x" + atomDefinition.get("to") + "`";

      matchHead.append("(").append(fromVar).append(")-[").append(atomVar).append("]->(")
          .append(toVar).append(")");
      whereHead.append("type(").append(atomVar).append(") = '").append(atomDefinition.get("name"))
          .append("'");

      if (!atomDefinition.get("from").toString().startsWith("_")) {
        whereHead.append(" AND ").append(fromVar).append("._id").append(" = '")
            .append(atomDefinition.get("from")).append("'");
      }

      if (!atomDefinition.get("to").toString().startsWith("_")) {
        whereHead.append(" AND ").append(toVar).append("._id").append(" = '")
            .append(atomDefinition.get("to")).append("'");
      }

    } else {
      atomVar = "`x" + atomDefinition.get("name") + attributes.get("_id") + "`";
      matchHead.append("(").append(atomVar).append(")");
      whereHead.append("'").append(atomDefinition.get("name")).append("' IN labels(")
          .append(atomVar).append(")");
    }

    // Attributes
    for (Map.Entry<String, String> attr : attributes.entrySet()) {
      if (!attr.getValue().startsWith("_")) {
        whereHead.append(" AND ").append(atomVar).append(".").append(attr.getKey()).append(" = '")
            .append(attr.getValue()).append(
                "'");

      } else if (nullAttrMap.containsKey(attr.getValue())) {
        String originalNullVar = nullAttrMap.get(attr.getValue()).get(0);
        whereHead.append(" AND ").append(atomVar).append(".").append(attr.getKey()).append(" = ")
            .append(originalNullVar);

      } else {
        nullAttrMap.put(attr.getValue(), new ArrayList<>());
        nullAttrMap.get(attr.getValue()).add(atomVar + "." + attr.getKey());
      }
    }

    return matchBody.append(whereBody).append(matchHead).append(whereHead).append(output)
        .append("} AS sub").toString();
  }

  ////// Collection //////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  protected boolean addAll(QueryRunner runner, Collection<? extends Atom> atoms, BddStats stats) {
    Map<String, Object> params = this.getQueryParamsFromAtomsSchema(atoms);

    String nodeQ = "UNWIND $nodes AS nodeDef CALL apoc.merge.node([nodeDef.name], nodeDef.attr) YIELD node RETURN node";
    String edgeQ = "UNWIND $edges AS edgeDef MERGE (from {_id: edgeDef.from}) MERGE (to {_id: edgeDef.to}) WITH edgeDef, from, to CALL apoc.merge.relationship(from, edgeDef.name, edgeDef.attr, {}, to, {}) YIELD rel RETURN rel";

    Result result;
    SummaryCounters summary;
    int numberOfCreation = 0;

    if (stats != null) {
      stats.addQueries(2);
    }

    result = runner.run(nodeQ, params);
    summary = result.consume().counters();
    numberOfCreation += summary.nodesCreated();

    result = runner.run(edgeQ, params);
    summary = result.consume().counters();
    numberOfCreation += summary.relationshipsCreated();

    return numberOfCreation > 0;
  }

  @Override
  protected boolean removeAll(QueryRunner runner, Collection<?> c, BddStats stats) {
    Map<String, Object> params = this.getQueryParamsFromAtomsSchema((Collection<? extends Atom>) c);

    String nodeQ = "UNWIND $nodes AS node MATCH (x) WHERE properties(x) = node.attr AND node.name IN labels(x) DETACH DELETE x";
    String edgeQ = "UNWIND $edges AS edge MATCH ({_id: edge.from})-[x]->({_id: edge.to}) WHERE properties(x) = edge.attr AND type(x) = edge.name DELETE x";

    Result result;
    SummaryCounters summary;
    int numberOfDeletion = 0;

    if (stats != null) {
      stats.addQueries(2);
    }

    result = runner.run(nodeQ, params);
    summary = result.consume().counters();
    numberOfDeletion += summary.nodesDeleted();

    result = runner.run(edgeQ, params);
    summary = result.consume().counters();
    numberOfDeletion += summary.relationshipsDeleted();

    return numberOfDeletion > 0;
  }

  @Override
  public int size() {
    try (Session session = driver.session()) {
      return session.run(
              "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} RETURN count(x) AS count")
          .single().get(
              0).asInt();
    }
  }

  @Override
  public void clear() {
    try (Session session = driver.session()) {
      session.run("MATCH (n) DETACH DELETE n");
    }
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Stream<Atom> stream() {
    return this.getAllPredicates().stream().flatMap(x -> this.getAtomsWithPredicate(x).stream());
  }

  ////// Bdd /////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public void initFromDB(Collection<Atom> init) {
    this.clear();
    this.addAll(init);
  }

  @Override
  public Set<String> getAllPredicates() {
    try (Session session = driver.session()) {
      return session.run(
              "CALL db.labels() YIELD label RETURN label AS x UNION CALL db.relationshipTypes() YIELD relationshipType RETURN relationshipType AS x")
          .stream().map(
              record -> record.get(0).asString()).collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsPredicate(String predicate) {
    return this.getAllPredicates().contains(predicate);
  }

  @Override
  public Set<Atom> getAtomsWithPredicate(String predicate) {
    try (Session session = driver.session()) {
      Map<String, Object> params = new HashMap<>();
      params.put("pred", predicate);

      return session.run(
          "MATCH (x) WHERE $pred IN labels(x) RETURN labels(x)[0] AS pred, x {.*} AS attr"
              + " UNION MATCH (from)-[x]->(to) WHERE type(x) = $pred RETURN type(x) AS pred, x {.*, from: from._id, to: to._id} AS attr",
          params).stream().map(BddNeo4jV2::recordToAtom).collect(Collectors.toSet());
    }
  }

  @Override
  protected boolean containsAnyNulls(QueryRunner runner, Collection<? extends Element> nulls,
      BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Map<String, Object> params = new HashMap<>();
    params.put("nulls", nulls);

    return runner.run(
        "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} UNWIND keys(x) AS key WITH DISTINCT x[key] AS elem WHERE elem IN $nulls RETURN true LIMIT 1",
        params).hasNext();
  }

  @Override
  protected Set<Element> getNulls(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    return runner.run(
            "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} UNWIND keys(x) AS key WITH DISTINCT x[key] AS elem WHERE elem STARTS WITH '_' RETURN elem")
        .stream().map(
            record -> record.get(0).asString()).map(Element::stringToElement)
        .collect(Collectors.toSet());
  }

  @Override
  protected int getNbNulls(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    return runner.run(
            "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} UNWIND keys(x) AS key WITH DISTINCT x[key] AS elem WHERE elem STARTS WITH '_' RETURN count(elem)")
        .single().get(
            0).asInt();
  }

  @Override
  protected int getNbConst(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    return runner.run(
            "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} UNWIND keys(x) AS key WITH DISTINCT x[key] AS elem WHERE NOT elem STARTS WITH '_' RETURN count(elem)")
        .single().get(
            0).asInt();
  }

  @Override
  protected void setNullsDegree(QueryRunner runner, Collection<Element> nulls, int degree,
      BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Map<String, String> mappings = new HashMap<>();
    for (Element element : nulls) {
      Variable variable = (Variable) element;

      if (variable.getProf() != degree) {
        Variable renamed = (Variable) variable.clone();
        renamed.setProf(degree);

        mappings.put(getNullName(variable), getNullName(renamed));
      }
    }

    Map<String, Object> params = new HashMap<>();
    params.put("mappings", mappings);
    params.put("values", mappings.keySet());

    runner.run(
        "CALL {MATCH (x) RETURN x UNION MATCH ()-[x]->() RETURN x} UNWIND keys(x) AS key WITH x, key, x[key] AS elem WHERE elem IN $values CALL apoc.create.setProperty(x, key, $mappings[elem]) YIELD node RETURN node",
        params);
  }

  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    try (Session session = driver.session()) {
      Map<String, Object> params = this.getQueryParamsFromAtom(a);
      Map<String, String> attributes = (Map<String, String>) params.get("attr");

      // Map of attributes sharing a null
      Map<String, List<String>> nullAttrMap = new HashMap<>();

      StringBuilder attrWhere = new StringBuilder();
      for (Map.Entry<String, String> attr : attributes.entrySet()) {
        attrWhere.append(" AND x.");

        if (attr.getValue().startsWith("_")) { // Null
          attrWhere.append(attr.getKey()).append(" STARTS WITH '_'");
          if (!nullAttrMap.containsKey(attr.getValue())) {
            nullAttrMap.put(attr.getValue(), new ArrayList<>());
          }

          nullAttrMap.get(attr.getValue()).add("x." + attr.getKey());

        } else { // Const
          attrWhere.append(attr.getKey()).append(" = '").append(attr.getValue()).append("'");
        }
      }

      // Add null atoms link
      for (List<String> variables : nullAttrMap.values()) {
        for (int i = 1; i < variables.size(); i++) {
          attrWhere.append(" AND ").append(variables.get(0)).append(" = ").append(variables.get(i));
        }
      }

      // Build query
      String query;
      if (this.schema.isEdge(a)) {
        if (!params.get("from").toString().startsWith("_")) {
          attrWhere.append(" AND from._id = '").append(params.get("from")).append("'");
        }

        if (!params.get("to").toString().startsWith("_")) {
          attrWhere.append(" AND to._id = '").append(params.get("to")).append("'");
        }

        attrWhere.append(" WITH x, type(x) AS pred, x {.*, from: from._id, to: to._id} AS attr");

        if (delete) {
          attrWhere.append(" DELETE x");
        }

        query = "MATCH (from)-[x]->(to) WHERE type(x) = $name" + attrWhere + " RETURN pred, attr";

      } else {
        attrWhere.append(" WITH x, $name AS pred, x {.*} AS attr");

        if (delete) {
          attrWhere.append(" DETACH DELETE x");
        }
        query = "MATCH (x) WHERE $name IN labels(x)" + attrWhere + " RETURN pred, attr";
      }

      return session.run(query, params).stream().map(BddNeo4jV2::recordToAtom)
          .collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsModuloNull(Atom a) {
    try (Session session = driver.session()) {
      Map<String, Object> params = this.getQueryParamsFromAtom(a);
      Map<String, String> attributes = (Map<String, String>) params.get("attr");

      // Map of attributes sharing a null
      Map<String, List<String>> nullAttrMap = new HashMap<>();

      StringBuilder attrWhere = new StringBuilder();
      for (Map.Entry<String, String> attr : attributes.entrySet()) {
        if (!attr.getValue().startsWith("_")) { // Const
          attrWhere.append(" AND x.").append(attr.getKey()).append(" = '").append(attr.getValue())
              .append("'");

        } else {
          if (!nullAttrMap.containsKey(attr.getValue())) {
            nullAttrMap.put(attr.getValue(), new ArrayList<>());
          }

          nullAttrMap.get(attr.getValue()).add("x." + attr.getKey());
        }
      }

      // Add null atoms link
      for (List<String> variables : nullAttrMap.values()) {
        for (int i = 1; i < variables.size(); i++) {
          attrWhere.append(" AND ").append(variables.get(0)).append(" = ").append(variables.get(i));
        }
      }

      String query;
      if (this.schema.isEdge(a)) {
        if (!params.get("from").toString().startsWith("_")) {
          attrWhere.append(" AND from._id = '").append(params.get("from")).append("'");
        }

        if (!params.get("to").toString().startsWith("_")) {
          attrWhere.append(" AND to._id = '").append(params.get("to")).append("'");
        }

        query = "MATCH (from)-[x]->(to) WHERE type(x) = $name" + attrWhere + " RETURN true";

      } else {
        query = "MATCH (x) WHERE $name IN labels(x)" + attrWhere + " RETURN true";
      }

      return session.run(query, params).hasNext();
    }
  }

  @Override
  protected void chaseStep(QueryRunner runner, Rule rule, Collection<Atom> newToIns,
      Collection<Atom> newToDel, Bdd ToDel, int deltaMax, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    // Execute query
    String query = this.generateChaseQuerySchema(rule);
    System.out.println("QUERY: " + query);
    Result rs = runner.run(query);

    // Get substitutions
    rs.forEachRemaining((Record record) -> {
      Substitution sub = new Substitution();
      Map<String, Object> subMap = record.get("sub").asMap();

      // Generate substitution
      for (Map.Entry<String, Object> entry : subMap.entrySet()) {
        sub.put((Variable) Element.stringToElement(entry.getKey()),
            Element.stringToElement(entry.getValue().toString()));
      }

      // Apply substitution
      Rule r = sub.applySub(rule);
      Atom head = r.getHead();
      int prof = r.getMaxProf() + 1;

      // Set prof
      for (Variable v : head.getVariables()) {
        if (!r.getVariablesInBody().contains(v)) {
          v.setProf(prof);
        }
      }

      //System.out.println("RULE " + r);

      // Do chase step
      if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
        System.out.println("DEL " + r.getAtomesToDelInBody());
        newToDel.addAll(r.getAtomesToDelInBody());

      } else {
        System.out.println("ADD " + head);
        newToIns.add(head);
      }
    });
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(QueryRunner runner,
      Collection<Atom> partition, Integer limit, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    // TODO
    String query = this.getQueryModulloNull(partition);

    if (limit != null) {
      query += " LIMIT " + limit;
    }

    System.out.println(query);
    return Stream.empty();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  protected Set<Element> getNullBucket(QueryRunner runner, Bdd atoms, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Map<String, Object> params = new HashMap<>();
    params.put("predicates", atoms.getAllPredicates());

    Set<Element> nullBucket = atoms.getNulls();
    nullBucket.addAll(runner.run(
            "CALL {MATCH (x) WHERE ANY(label IN labels(x) WHERE label IN $predicates) RETURN x UNION MATCH ()-[x]->() WHERE type(x) IN $predicates RETURN x} UNWIND keys(x) AS key WITH DISTINCT x[key] AS elem WHERE elem STARTS WITH '_' RETURN elem",
            params).stream().map(record -> record.get(0).asString()).map(Element::stringToElement)
        .collect(Collectors.toSet()));

    return nullBucket;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(QueryRunner runner,
      Collection<Element> nullBucket, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    final Map<Set<Element>, Set<Atom>> partition = new HashMap<>();
    final Collection<Element> computed = new HashSet<>();
    final Collection<Element> toCompute = new HashSet<>(nullBucket);

    while (!toCompute.isEmpty()) {
      // Params
      Map<String, Object> params = new HashMap<>();
      params.put("bucket", toCompute.stream().map(element -> getNullName((Variable) element))
          .collect(Collectors.toList()));

      // Prepare next iteration
      computed.addAll(toCompute);
      toCompute.clear();

      // Exec query
      runner.run(
              "MATCH (x) WHERE ANY(key IN keys(x) WHERE x[key] IN $bucket) RETURN labels(x)[0] AS pred, x {.*} AS attr"
                  + " UNION MATCH (from)-[x]->(to) WHERE from._id IN $bucket OR to._id IN $bucket OR ANY(key IN keys(x) WHERE x[key] IN $bucket) RETURN type(x) AS pred, x {.*, from: from._id, to: to._id} AS attr",
              params).stream().map(BddNeo4jV2::recordToAtom)
          .forEach(atom -> { // Merge partitions (from MySQL)
            Set<Set<Element>> keysToMerge = new HashSet<>();

            // Search linked partitions
            for (Element ti : atom.getNulls()) {
              if (!computed.contains(ti)) {
                toCompute.add(ti);
              }
              for (Set<Element> k : partition.keySet()) {
                if (k.contains(ti)) {
                  keysToMerge.add(k);
                }
              }
            }

            // Merge partitions
            Set<Element> newKey = new HashSet<>(atom.getNulls());
            Set<Atom> newPartition = new HashSet<>();
            newPartition.add(atom);

            for (Set<Element> oldKey : keysToMerge) {
              newKey.addAll(oldKey);
              newPartition.addAll(partition.get(oldKey));
              partition.remove(oldKey);
            }

            partition.put(newKey, newPartition);
          });
    }

    return partition;
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    // In-Memory chase forward for edges as they can't be added to the database without nodes attached to it
    BddMemoire insertCopy = new BddMemoire();
    for (Atom a : iRequest) {
      if (this.schema.isEdge(a)) {
        insertCopy.add(a);
      }
    }
    insertCopy.update2(rules, 1, false); // Chase forward
    insertCopy.addAll(iRequest);

    return super.addWithConstraints(insertCopy, rules, deltaMax, thingsToAddAfterChase, restricted);
  }
}