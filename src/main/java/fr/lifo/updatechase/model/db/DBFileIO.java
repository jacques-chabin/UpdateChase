package fr.lifo.updatechase.model.db;

import fr.lifo.updatechase.model.SyntaxError;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

public class DBFileIO {

  private final static char LINE_DELIMITER = '.';
  private final static char LINE_COMMENT = '%';

  //// READ

  public static <T, C extends Collection<T>> C readFromFile(Path path, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    return DBFileIO.readFromFile(path.toFile(), collection, lineParser);
  }

  public static <T, C extends Collection<T>> C readFromFile(URL url, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    InputStream inputStream = url.openStream();
    InputStreamReader reader = new InputStreamReader(inputStream);

    C result = DBFileIO.readFromFile(reader, collection, lineParser);

    reader.close();
    inputStream.close();

    return result;
  }

  public static <T, C extends Collection<T>> C readFromFile(URI uri, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    return DBFileIO.readFromFile(new File(uri), collection, lineParser);
  }

  public static <T, C extends Collection<T>> C readFromFile(File file, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    try (FileReader reader = new FileReader(file)) {
      return DBFileIO.readFromFile(reader, collection, lineParser);
    }
  }

  public static <T, C extends Collection<T>> C readFromFile(String path, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    ClassLoader classLoader = DBFileIO.class.getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream(path);

    Reader reader;
    if (inputStream != null) {
      reader = new InputStreamReader(inputStream);
    } else {
      reader = new FileReader(path);
    }

    C result = DBFileIO.readFromFile(reader, collection, lineParser);

    reader.close();
    if (inputStream != null) {
      inputStream.close();
    }

    return result;
  }

  private static <T, C extends Collection<T>> C readFromFile(Reader reader, C collection,
      ParserFunction<String, T> lineParser)
      throws SyntaxError, IOException {
    StringBuilder line = new StringBuilder();
    int i = 0;

    try (BufferedReader bufferedReader = new BufferedReader(reader)) {
      String fileLine = bufferedReader.readLine();
      while (fileLine != null) {
        fileLine = delSpace(fileLine);

        if (fileLine.length() != 0 && fileLine.charAt(0) != LINE_COMMENT) {
          if (fileLine.charAt(fileLine.length() - 1) != LINE_DELIMITER) {
            line.append(fileLine);

          } else {
            line.append(fileLine, 0, fileLine.length() - 1);
            T output = lineParser.apply(line.toString());
            collection.add(output);
            i++;
            if (i % 1000 == 0) {
              System.out.println(i + " atoms loaded");
            }
            line = new StringBuilder();
          }
        }
        fileLine = bufferedReader.readLine();
      }
    }

    return collection;
  }

  //// WRITE

  public static <T, C extends Collection<T>> void writeToFile(Path path, C collection)
      throws IOException {
    DBFileIO.writeToFile(path.toFile(), collection);
  }

  public static <T, C extends Collection<T>> void writeToFile(URI uri, C collection)
      throws IOException {
    DBFileIO.writeToFile(new File(uri), collection);
  }

  public static <T, C extends Collection<T>> void writeToFile(File file, C collection)
      throws IOException {
    try (FileWriter writer = new FileWriter(file)) {
      DBFileIO.writeToFile(writer, collection);
    }
  }

  public static <T, C extends Collection<T>> void writeToFile(String path, C collection)
      throws IOException {
    try (FileWriter writer = new FileWriter(path)) {
      DBFileIO.writeToFile(writer, collection);
    }
  }

  private static <T, C extends Collection<T>> void writeToFile(Writer writer, C collection)
      throws IOException {
    try (BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
      for (T element : collection) {
        bufferedWriter.write(element.toString());
        bufferedWriter.write(LINE_DELIMITER);
        bufferedWriter.newLine();
      }
    }
  }

  //// Utils

  /**
   * Retire tout les espaces en dehors des guillemets.
   *
   * @param s String. la chaine de caractère a traite.
   * @return String le String traite.
   */
  public static String delSpace(String s) {
    StringBuilder res = new StringBuilder();
    boolean ok = true;

    for (int i = 0; i < s.length(); ++i) {
      if (s.charAt(i) == '"' || s.charAt(i) == '\'') {
        ok = !ok;
      }
      if (s.charAt(i) == ' ') {
        if (!ok) {
          res.append(s.charAt(i));
        }
      } else {
        res.append(s.charAt(i));
      }
    }

    return res.toString();
  }

  @FunctionalInterface
  public interface ParserFunction<T, R> {

    R apply(T t) throws SyntaxError;
  }
}
