package fr.lifo.updatechase.model.db;

import com.github.freva.asciitable.AsciiTable;
import com.github.freva.asciitable.Column;
import com.github.freva.asciitable.HorizontalAlign;
import java.time.Duration;
import java.time.Instant;

public final class BddStats {

  /**
   * Count of addition done.
   */
  private long numberAdd = 0;

  /**
   * Count of deletion done.
   */
  private long numberDel = 0;

  /**
   * Count of queries.
   */
  private long numberQueries = 0;

  /**
   * Total elapsed time for the insertion.
   */
  private Duration insertTime = Duration.ZERO;

  /**
   * Total elapsed time for the deletion.
   */
  private Duration deleteTime = Duration.ZERO;

  /**
   * Total elapsed time for the chase computation.
   */
  private Duration chaseTime = Duration.ZERO;

  /**
   * Total elapsed time for the null bucket retrieval.
   */
  private Duration nullBucketTime = Duration.ZERO;

  /**
   * Total elapsed time for the partition retrieval.
   */
  private Duration partitionTime = Duration.ZERO;

  /**
   * Total elapsed time for the simplifications.
   */
  private Duration simplificationTime = Duration.ZERO;

  /**
   * Total elapsed time for to compute the more specific homomorphism.
   */
  private Duration moreSpecificTime = Duration.ZERO;

  /**
   * Total elapsed time for the computation of the null degree.
   */
  private Duration nullDegreeTime = Duration.ZERO;

  /**
   * Total elapsed time to commit changes.
   */
  private Duration commitTime = Duration.ZERO;

  /**
   * Total elapsed time.
   */
  private Duration totalTime = Duration.ZERO;

  /**
   * Start time of the insertion (used to measure elapsed time).
   */
  private Instant insertInstant;

  /**
   * Start time of the deletion (used to measure elapsed time).
   */
  private Instant deleteInstant;

  /**
   * Start time of the chase computation (used to measure elapsed time).
   */
  private Instant chaseInstant;

  /**
   * Start time of the null bucket computation (used to measure elapsed time).
   */
  private Instant nullBucketInstant;

  /**
   * Start time of the partition retrieval (used to measure elapsed time).
   */
  private Instant partitionInstant;

  /**
   * Start time of the simplification (used to measure elapsed time).
   */
  private Instant simplificationInstant;

  /**
   * Start time of the more specific homomorphism computation (used to measure elapsed time).
   */
  private Instant moreSpecificInstant;

  /**
   * Start time of the null degree computation (used to measure elapsed time).
   */
  private Instant nullDegreeInstant;

  /**
   * Start time of the commit (used to measure elapsed time).
   */
  private Instant commitInstant;

  /**
   * Start time of the algorithm (used to measure elapsed time).
   */
  private Instant totalInstant;

  public BddStats() {
  }

  //// Getter

  /**
   * Get the number of additions done in the database.
   *
   * @return The current number of atoms inserted in the database
   */
  public long getNumberAdd() {
    return numberAdd;
  }

  /**
   * Get the number of deletions done in the database.
   *
   * @return The current number of atoms deleted from the database
   */
  public long getNumberDel() {
    return numberDel;
  }

  /**
   * Get number of queries send to the database.
   *
   * @return The current number of queries
   */
  public long getNumberQueries() {
    return numberQueries;
  }

  /**
   * Get the duration of the insertions into the database.
   *
   * @return The elapsed time during insertion
   */
  public Duration getInsertTime() {
    return insertTime;
  }

  /**
   * Get the duration of the deletions into the database.
   *
   * @return The elapsed time during deletion
   */
  public Duration getDeleteTime() {
    return deleteTime;
  }

  /**
   * Get the duration of the chase computation.
   *
   * @return The elapsed time during computation of the chase
   */
  public Duration getChaseTime() {
    return chaseTime;
  }

  /**
   * Get the duration of the null bucket computation.
   *
   * @return The elapsed time during computation of the null bucket
   */
  public Duration getNullBucketTime() {
    return nullBucketTime;
  }

  /**
   * Get the duration of the partition computation.
   *
   * @return The elapsed time during computation of the partitions
   */
  public Duration getPartitionTime() {
    return partitionTime;
  }

  /**
   * Get the duration of the simplification computation.
   *
   * @return The elapsed time during computation of the simplification
   */
  public Duration getSimplificationTime() {
    return simplificationTime;
  }

  /**
   * Get the duration of the more specific homomorphism computation.
   *
   * @return The elapsed time during computation of the more specific homomorphism
   */
  public Duration getMoreSpecificTime() {
    return moreSpecificTime;
  }

  /**
   * Get the duration of the null degree computation.
   *
   * @return The elapsed time during computation of the null degree
   */
  public Duration getNullDegreeTime() {
    return nullDegreeTime;
  }

  /**
   * Get the duration of the changes commit.
   *
   * @return The elapsed time during committing the changes
   */
  public Duration getCommitTime() {
    return commitTime;
  }

  /**
   * Get the duration of the core computation.
   *
   * @return The elapsed time during computation of the core
   */
  public Duration getCoreTime() {
    return getPartitionTime().plus(getSimplificationTime());
  }

  /**
   * Get the remaining duration not categorized. It corresponds to the total time elapsed minus the
   * duration of all the computations;
   *
   * @return The elapsed time difference between the total and all computations
   */
  public Duration getOtherTime() {
    return getTotalTime().minus(getInsertTime()).minus(getDeleteTime()).minus(getChaseTime())
        .minus(getNullBucketTime()).minus(
            getCoreTime()).minus(getNullDegreeTime()).minus(getCommitTime());
  }

  /**
   * Get the total time elapsed for all the computations.
   *
   * @return The total elapsed time
   */
  public Duration getTotalTime() {
    return totalTime;
  }

  /**
   * Get the statistic summary report.
   *
   * @return A string representation of the summary report
   */
  public String getSummary() {
    Column[] columns = {
        new Column().header("Measure").footer("Total").dataAlign(HorizontalAlign.LEFT),
        new Column().footer(
            getTotalTime().toMillis() + " ms").dataAlign(HorizontalAlign.RIGHT).footerAlign(
            HorizontalAlign.RIGHT), new Column().header(
        "Percent").dataAlign(HorizontalAlign.RIGHT)};

    String[][] data = {{"Number of Addition", getNumberAdd() + " atoms"}, {"Number of Deletion",
        getNumberDel() + " atoms"}, {"Number of Queries", getNumberQueries() + " queries"}, {},
        {"Insert",
            getInsertTime().toMillis() + " ms", getPercent(getInsertTime()) + "%"}, {"Delete",
        getDeleteTime().toMillis() + " ms", getPercent(getDeleteTime()) + "%"}, {"Chase",
        getChaseTime().toMillis() + " ms", getPercent(getChaseTime()) + "%"}, {"Null Bucket",
        getNullBucketTime().toMillis() + " ms", getPercent(getNullBucketTime()) + "%"}, {"Core",
        getCoreTime().toMillis() + " ms"}, {" ├─ Partitions",
        getPartitionTime().toMillis() + " ms", getPercent(getPartitionTime()) + "%"},
        {" └─ Simplification",
            getSimplificationTime().toMillis() + " ms", getPercent(getSimplificationTime()) + "%"},
        {"     └─ More specific",
            getMoreSpecificTime().toMillis() + " ms", getPercent(getMoreSpecificTime()) + "%"},
        {"Null Degree",
            getNullBucketTime().toMillis() + " ms", getPercent(getNullBucketTime()) + "%"},
        {"Commit",
            getCommitTime().toMillis() + " ms", getPercent(getCommitTime()) + "%"}, {"Other",
        getOtherTime().toMillis() + " ms", getPercent(getOtherTime()) + "%"}};

    return AsciiTable.getTable(AsciiTable.BASIC_ASCII_NO_DATA_SEPARATORS, columns, data);
  }

  //// Internal methods

  /**
   * Get percent of the given duration over the total time.
   *
   * @param duration The duration to measure the percentage from
   * @return A percentage between 0 and 1
   */
  private float getPercent(Duration duration) {
    return Math.round(((float) duration.toMillis()) / getTotalTime().toMillis() * 1000) / 10f;
  }

  /**
   * Add atoms insertions.
   *
   * @param number The number of insertions to add
   */
  public void addAtoms(int number) {
    numberAdd += number;
  }

  /**
   * Add atoms deletions.
   *
   * @param number The number of deletions to add
   */
  public void delAtoms(int number) {
    numberDel += number;
  }

  /**
   * Add queries.
   *
   * @param number The number of queries to add
   */
  public void addQueries(int number) {
    numberQueries += number;
  }

  public void startInsert() {
    insertInstant = Instant.now();
  }

  public void stopInsert() {
    insertTime = insertTime.plus(Duration.between(insertInstant, Instant.now()));
  }

  public void startDelete() {
    deleteInstant = Instant.now();
  }

  public void stopDelete() {
    deleteTime = deleteTime.plus(Duration.between(deleteInstant, Instant.now()));
  }

  public void startChase() {
    chaseInstant = Instant.now();
  }

  public void stopChase() {
    chaseTime = chaseTime.plus(Duration.between(chaseInstant, Instant.now()));
  }

  public void startNullBucket() {
    nullBucketInstant = Instant.now();
  }

  public void stopNullBucket() {
    nullBucketTime = nullBucketTime.plus(Duration.between(nullBucketInstant, Instant.now()));
  }

  public void startPartition() {
    partitionInstant = Instant.now();
  }

  public void stopPartition() {
    partitionTime = partitionTime.plus(Duration.between(partitionInstant, Instant.now()));
  }

  public void startSimplification() {
    simplificationInstant = Instant.now();
  }

  public void stopSimplification() {
    simplificationTime = simplificationTime.plus(
        Duration.between(simplificationInstant, Instant.now()));
  }

  public void startMoreSpecific() {
    moreSpecificInstant = Instant.now();
  }

  public void stopMoreSpecific() {
    moreSpecificTime = moreSpecificTime.plus(Duration.between(moreSpecificInstant, Instant.now()));
  }

  public void startNullDegree() {
    nullDegreeInstant = Instant.now();
  }

  public void stopNullDegree() {
    nullDegreeTime = nullDegreeTime.plus(Duration.between(nullDegreeInstant, Instant.now()));
  }

  public void startCommit() {
    commitInstant = Instant.now();
  }

  public void stopCommit() {
    commitTime = commitTime.plus(Duration.between(commitInstant, Instant.now()));
  }

  public void start() {
    totalInstant = Instant.now();
  }

  public void stop() {
    totalTime = totalTime.plus(Duration.between(totalInstant, Instant.now()));
  }
}
