package fr.lifo.updatechase.model.db.cypher;

import com.redislabs.redisgraph.Record;
import com.redislabs.redisgraph.RedisGraph;
import com.redislabs.redisgraph.RedisGraphContext;
import com.redislabs.redisgraph.ResultSet;
import com.redislabs.redisgraph.Statistics;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.cypher.query.LocalElementIndexQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.strategy.NeoQueryStrategyInterface;
import fr.lifo.updatechase.model.db.cypher.strategy.NullDegreeConstantNeoQueryStrategy;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class BddRedis extends Bdd {

  protected final String graphId;
  protected com.redislabs.redisgraph.impl.api.RedisGraph graph;

  protected static final int CHUNK_SIZE = 25000;
  protected static final int TIMEOUT = 2 * 3600 * 1000;

  protected NeoQueryStrategyInterface queryStrategy = new NullDegreeConstantNeoQueryStrategy();

  public BddRedis(String host, int port, String graphId) {
    this.graphId = graphId;

    JedisPoolConfig config = new JedisPoolConfig();
    JedisPool pool = new JedisPool(config, host, port, TIMEOUT);

    try (Jedis jedis = pool.getResource()) {
      jedis.configSet("timeout", "" + TIMEOUT);

      this.graph = new com.redislabs.redisgraph.impl.api.RedisGraph(pool);

      // Dummy query to initialize the graph
      this.graph.query(this.graphId, "CREATE (a) DELETE a", TIMEOUT);
      this.graph.query(this.graphId, "CREATE INDEX ON :Atom(name)");
      this.graph.query(this.graphId, "CREATE INDEX ON :Element(name)");
      this.graph.query(this.graphId, "CREATE INDEX ON :Element(isConst)");
      //this.graph.query(this.graphId, "CREATE CONSTRAINT ON (n:Element) ASSERT n.name IS UNIQUE");

      LOGGER.config("Redis connected");

    } catch (JedisConnectionException error) {
      LOGGER.severe(error.getMessage());
      this.graph = null;
    }
  }

  @Override
  public boolean isConnected() {
    return this.graph != null;
  }

  private static Atom recordToAtom(Record record) {
    String pred = record.getString("a");
    List<Object> elementNames = record.getValue("e");
    return BddNeo4j.buildAtom(pred, elementNames);
  }

  private static Set<Atom> recordToListAtom(Record record) {
    Set<Atom> instance = new HashSet<>();

    for (int j = 0; j < record.size() / 3; ++j) {
      String pred = record.getString("a" + j);
      List<Object> elementNames = record.getValue("e" + j);
      instance.add(BddNeo4j.buildAtom(pred, elementNames));
    }

    return instance;
  }

  public static String prepareQuery(String query, Map<String, Object> params) {
    StringBuilder sb = new StringBuilder("CYPHER ");
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      String key = entry.getKey();
      Object value = entry.getValue();
      sb.append(key).append('=');
      sb.append(valueToString(value));
      sb.append(' ');
    }
    sb.append(query);
    return sb.toString();
  }

  private static String quoteString(String str) {
    return '"' + str.replace("\"", "\\\"") + '"';
  }

  private static String valueToString(Object value) {
    if (value == null) {
      return "null";
    }

    if (value instanceof String) {
      return quoteString((String) value);
    }

    if (value instanceof Character) {
      return quoteString(((Character) value).toString());
    }

    if (value instanceof Object[]) {
      return arrayToString((Object[]) value);

    }

    if (value instanceof Collection) {
      Collection<Object> collection = (Collection<Object>) value;
      return arrayToString(collection.toArray());
    }

    if (value instanceof Map) {
      Map<Object, Object> map = (Map<Object, Object>) value;
      return mapToString(map);
    }

    return value.toString();
  }

  private static String arrayToString(Object[] arr) {
    String strArray = Arrays.stream(arr).map(BddRedis::valueToString)
        .collect(Collectors.joining(", "));
    return '[' + strArray + ']';
  }

  private static String mapToString(Map<Object, Object> map) {
    String strMap = map.entrySet().stream().map(entry -> {
      String value = BddRedis.valueToString(entry.getValue());
      return entry.getKey().toString() + ':' + value;
    }).collect(Collectors.joining(", "));
    return '{' + strMap + '}';
  }

  public void setQueryStrategy(NeoQueryStrategyInterface strategy) {
    this.queryStrategy = strategy;
  }

  /**
   * Generates a Cypher query to get all the atoms matching an atom set in which the null values
   * become variables.
   *
   * @param atoms The collection of atoms to match in the database.
   * @return The generated query
   */
  private String cypherQueryMatchSetModuloNull(Collection<Atom> atoms, Integer limit) {
    StringBuilder query = new StringBuilder();
    StringBuilder queryEnd = new StringBuilder(" RETURN ");

    List<Atom> atomsAtDepth = new ArrayList<>(); // Need to keep order
    Set<Element> nullsAtDepth = new HashSet<>();
    Set<Element> nullsAtNextDepth = new HashSet<>();
    Map<Element, Integer> variableIndex = new HashMap<>();

    int index;
    int atomIndex = 0;
    boolean moreDepth = true;

    // Sort by null number
    List<Atom> atomsSorted = new ArrayList<>(atoms);
    atomsSorted.sort(Comparator.comparingLong(Atom::getNbNulls));
    Collections.reverse(atomsSorted);

    // Search starting atom (i.e. the one with maximum nulls but with at least one constant to reduce search space)
    Atom root = this.queryStrategy.getBFSRoot(atomsSorted);
    if (root != null) {
      atomsSorted.remove(root);
      atomsAtDepth.add(root);
    }

    // If no atoms found take the higher null degree
    if (atomsAtDepth.isEmpty()) {
      atomsAtDepth.add(atomsSorted.remove(atomsSorted.size() - 1));
    }

    // Compute Breadth First Search
    while (moreDepth) {
      for (Atom atom : atomsAtDepth) {
        StringBuilder elements = new StringBuilder();
        StringBuilder relationships = new StringBuilder();
        StringBuilder nullMatch = new StringBuilder(" MATCH ");
        boolean firstConst = true;
        boolean firstNull = true;
        int rank = 0;

        // Build MATCH clause
        query.append(" MATCH (a").append(atomIndex).append(":Atom {name:'").append(atom.getName())
            .append("'})");

        // Add constants to WHERE clause and nulls to next MATCH
        for (Element element : atom) {
          if (variableIndex.containsKey(element)) {
            index = variableIndex.get(element);
          } else {
            index = variableIndex.size();
            variableIndex.put(element, index);
          }

          if (!firstConst || !firstNull) {
            elements.append(", ");
            relationships.append(", ");
          }

          // Add elements to RETURN
          elements.append("e").append(index).append(".name");
          relationships.append(rank);

          // Build WHERE and MATCH
          if (element.isConstant() || nullsAtDepth.contains(element)) {
            query.append(", (a").append(atomIndex).append(")-[:Contains {rank:").append(rank)
                .append("}]->(e").append(index);

            if (element.isConstant()) {
              query.append(":Element {name:'").append(element.getName()).append("'})");
            } else {
              query.append(":Element)");
            }

            firstConst = false;

          } else {
            if (!firstNull) {
              nullMatch.append(", ");
            }
            nullMatch.append("(a").append(atomIndex).append(")-[:Contains {rank:").append(rank)
                .append("}]->(e").append(
                    index).append(":Element)");
            nullsAtNextDepth.add(element);
            firstNull = false;
          }

          rank++;
        }

        if (atomIndex > 0) {
          queryEnd.append(", ");
        }

        if (!firstNull) {
          query.append(nullMatch);
        }

        queryEnd.append("a").append(atomIndex).append(".name AS a").append(atomIndex)      // Atom
            .append(", [").append(relationships).append("] as r")
            .append(atomIndex)    // Relationships
            .append(", [").append(elements).append("] as e").append(atomIndex);        // Elements

        atomIndex++;
      }

      // Compute next depth
      atomsAtDepth.clear();
      nullsAtDepth.addAll(nullsAtNextDepth);

      for (Atom atom : atomsSorted) {
        if (atom.hasAnyElement(nullsAtDepth)) {
          atomsAtDepth.add(atom);
        }
      }

      // Edge case when we have two tree (can't appear in CORE but the query could be call somewhere else)
      if (atomsAtDepth.isEmpty() && !atomsSorted.isEmpty()) {
        atomsAtDepth.addAll(atomsSorted);
      }

      atomsSorted.removeAll(atomsAtDepth);
      moreDepth = !atomsAtDepth.isEmpty();
    }

    return query.toString() + queryEnd + (limit == null ? "" : " LIMIT " + limit);
  }

  @Override
  public void close() {
    this.graph.close();
  }

  @Override
  public boolean add(Atom atom) {
    return this.add(atom);
  }

  @Override
  public boolean addAll(Collection<? extends Atom> atoms) {
    boolean res = false;
    List<Atom> atomList = new ArrayList<>(atoms);

    try (RedisGraphContext context = this.graph.getContext()) {
      for (int i = 0; i < atoms.size(); i += CHUNK_SIZE) {
        res = this.addAll(context, atomList.subList(i, Math.min(i + CHUNK_SIZE, atoms.size())))
            || res;
      }
    }

    return res;
  }

  protected boolean addAll(RedisGraph graph, Collection<? extends Atom> atoms) {
    if (atoms == null || atoms.isEmpty()) {
      return false;
    }

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms(atoms);

    // ---- Execute queries ----
    ResultSet result;
    Statistics statistics;
    int nbNodesCreated = 0;

    // Create elements
    result = this.sendQuery(this.graph,
        "UNWIND $atoms AS a " + "UNWIND a.elems AS e "
            + "MERGE (en:Element {name: e.name, isConst: e.isConst}) ", params);
    statistics = result.getStatistics();
    LOGGER.info(statistics.nodesCreated() + " elements added");

    // Create predicates
    result = this.sendQuery(graph, "UNWIND $atoms AS a "

        // Check atom existence
        + "OPTIONAL MATCH (an:Atom {name: a.name}) " + "UNWIND a.elems AS e "
        + "OPTIONAL MATCH (an)-[:Contains {rank: e.rank}]->(en:Element {name: e.name, isConst: e.isConst}) "

        // Create atom if not exist
        + "WITH a, an, count(en) AS elemNodes " + "WITH a, COLLECT(elemNodes) AS candidates "
        + "WHERE ALL(c IN candidates WHERE c <> size(a.elems)) "
        + "CREATE (new:Atom {name: a.name}) "

        // Create links with elements
        + "WITH a, new " + "UNWIND a.elems AS e "
        + "MATCH (en:Element {name: e.name, isConst: e.isConst}) "
        + "CREATE (new)-[:Contains {rank: e.rank}]->(en);", params);
    statistics = result.getStatistics();
    LOGGER.info(statistics.nodesCreated() + " atoms added with " + statistics.relationshipsCreated()
        + " relations");

    return (nbNodesCreated + statistics.nodesCreated() + statistics.relationshipsCreated()) > 0;
  }

  @Override
  public boolean remove(Object o) {
    return this.removeAll(Collections.singleton(o));
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean res = false;
    List<?> list = new ArrayList<>(c);

    try (RedisGraphContext context = this.graph.getContext()) {
      for (int i = 0; i < list.size(); i += CHUNK_SIZE) {
        res =
            this.removeAll(context, list.subList(i, Math.min(i + CHUNK_SIZE, list.size()))) || res;
      }
    }

    return res;
  }

  protected boolean removeAll(RedisGraph graph, Collection<?> c) {
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms((Collection<Atom>) c);
    ResultSet result = this.sendQuery(graph,
        "UNWIND $atoms AS a " + "MATCH (an:Atom {name: a.name}) " + "UNWIND a.elems AS e "
            + "OPTIONAL MATCH (an)-[:Contains {rank: e.rank}]->(en:Element {name: e.name, isConst: e.isConst}) "
            + "WITH a, an, count(en) AS elemNodes " + "WHERE elemNodes = size(a.elems) "
            + "DETACH DELETE an", params);
    Statistics statistics = result.getStatistics();
    LOGGER.info(
        statistics.nodesDeleted() + " atoms deleted with " + statistics.relationshipsDeleted()
            + " relations");

    return (statistics.nodesDeleted() + statistics.relationshipsDeleted()) > 0;
  }

  @Override
  public int size() {
    ResultSet result = this.sendReadOnlyQuery(this.graph, "MATCH (:Atom) RETURN COUNT(*) LIMIT 1");
    return result.hasNext() ? ((Long) result.next().getValue(0)).intValue() : 0;
  }

  ////// Bdd /////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public void clear() {
    this.graph.deleteGraph(this.graphId);

    // Dummy query to initialize the graph
    this.sendQuery(this.graph, "CREATE (a) DELETE a");
  }

  @Override
  public boolean contains(Object o) {
    return this.contains(o);
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms((Collection<Atom>) c);
    ResultSet result = this.sendReadOnlyQuery(this.graph,
        "UNWIND $atoms AS a " + "OPTIONAL MATCH (an:Atom {name: a.name}) "
            + "WHERE ALL(e IN a.elems WHERE (an)-[:Contains {rank:e.rank}]->(:Element {name: e.name, isConst: e.isConst})) "
            + "RETURN an IS NOT NULL AS isExist", params);
    return StreamSupport.stream(result.spliterator(), true).allMatch(record -> record.getValue(0));
  }

  @Override
  public Iterator<Atom> iterator() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.collect(Collectors.toList()).iterator();
    }
  }

  /**
   * Get database as stream. It need to be close by calling close() or by using try-resource
   *
   * @return A stream of the database
   */
  @Override
  public Stream<Atom> stream() {
    RedisGraphContext context = this.graph.getContext();
    ResultSet result = this.sendReadOnlyQuery(context,
        "MATCH (a:Atom)-[r:Contains]->(e:Element) "
            + "WITH a, collect(r.rank) as r, collect(e.name) as e "
            + "RETURN a.name as a, r, e");
    return StreamSupport.stream(result.spliterator(), false).map(BddRedis::recordToAtom)
        .onClose(context::close);
  }

  @Override
  public void initFromDB(Collection<Atom> init) {
    this.clear();
    Map<String, Object> params = BddNeo4j.getQueryParamsFromAtoms(init);

    // ---- Execute queries ----
    ResultSet result;
    Statistics statistics;

    // Create elements
    result = this.sendQuery(this.graph,
        "UNWIND $atoms AS a " + "UNWIND a.elems AS e "
            + "MERGE (en:Element {name: e.name, isConst: e.isConst}) ", params);
    statistics = result.getStatistics();
    LOGGER.info(statistics.nodesCreated() + " elements added");

    // Create predicates
    result = this.sendQuery(this.graph,
        "UNWIND $atoms AS a " + "CREATE (new:Atom {name: a.name}) " + "WITH a, new "
            + "UNWIND a.elems AS e "
            + "MATCH (en:Element {name: e.name, isConst: e.isConst}) "
            + "CREATE (new)-[:Contains {rank: e.rank}]->(en);",
        params);
    statistics = result.getStatistics();
    LOGGER.info(statistics.nodesCreated() + " atoms added with " + statistics.relationshipsCreated()
        + " relations");
  }

  @Override
  public Set<String> getAllPredicates() {
    ResultSet result = this.sendReadOnlyQuery(this.graph, "MATCH (a:Atom) RETURN DISTINCT a.name");
    return StreamSupport.stream(result.spliterator(), true).map(record -> record.getString(0))
        .collect(Collectors.toSet());
  }

  @Override
  public boolean containsPredicate(String predicate) {
    Map<String, Object> params = new HashMap<>();
    params.put("name", predicate);

    ResultSet result = this.sendReadOnlyQuery(this.graph,
        "OPTIONAL MATCH (an:Atom {name: $name}) " + "RETURN an IS NOT NULL AS isExist LIMIT 1",
        params);
    return result.hasNext() ? result.next().getValue(0) : false;
  }

  @Override
  public Set<Atom> getAtomsWithPredicate(String predicate) {
    Map<String, Object> params = new HashMap<>();
    params.put("name", predicate);

    ResultSet result = this.sendReadOnlyQuery(this.graph,
        "MATCH (a:Atom {name: $name})-[r:Contains]->(e:Element) "
            + "WITH a, collect(DISTINCT r.rank) as r, collect(e.name) as e "
            + "RETURN a.name as a, r, e", params);

    return StreamSupport.stream(result.spliterator(), true).map(BddRedis::recordToAtom)
        .collect(Collectors.toSet());
  }

  @Override
  public boolean containsAnyNulls(Collection<? extends Element> nulls) {
    return this.containsAnyNulls(this.graph, nulls);
  }

  protected boolean containsAnyNulls(RedisGraph graph, Collection<? extends Element> nulls) {
    Map<String, Object> params = new HashMap<>();
    params.put("nulls",
        nulls.stream().map(el -> getNullName((Variable) el)).collect(Collectors.toSet()));

    ResultSet result = this.sendReadOnlyQuery(graph,
        "UNWIND $nulls AS nullName " + "OPTIONAL MATCH (e:Element {name: nullName}) "
            + "RETURN e IS NOT NULL AS isExist", params);
    return StreamSupport.stream(result.spliterator(), true).anyMatch(record -> record.getValue(0));
  }

  @Override
  public Set<Element> getNulls() {
    return this.getNulls(graph);
  }

  protected Set<Element> getNulls(RedisGraph graph) {
    ResultSet result = this.sendReadOnlyQuery(graph,
        "MATCH (n:Element {isConst: false}) RETURN DISTINCT n.name");
    return StreamSupport.stream(result.spliterator(), true).map(record -> record.getString(0))
        .map(Element::stringToElement).collect(
            Collectors.toSet());
  }

  @Override
  public int getNbNulls() {
    return this.getNbNulls(this.graph);
  }

  protected int getNbNulls(RedisGraph graph) {
    ResultSet result = this.sendReadOnlyQuery(graph,
        "MATCH (:Element {isConst: false}) RETURN count(*) LIMIT 1");
    return result.hasNext() ? ((Long) result.next().getValue(0)).intValue() : 0;
  }

  @Override
  public int getNbConst() {
    return this.getNbConst(this.graph);
  }

  protected int getNbConst(RedisGraph graph) {
    ResultSet result = this.sendReadOnlyQuery(graph,
        "MATCH (:Element {isConst: true}) RETURN count(*) LIMIT 1");
    return result.hasNext() ? ((Long) result.next().getValue(0)).intValue() : 0;
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    this.setNullsDegree(graph, nulls, degree);
  }

  protected void setNullsDegree(RedisGraph graph, Collection<Element> nulls, int degree) {
    List<String[]> mappings = nulls.stream().map(element -> {
      Variable renamed = (Variable) element.clone();
      renamed.setProf(degree);

      return new String[]{getNullName((Variable) element), getNullName(renamed)};
    }).collect(Collectors.toList());

    Map<String, Object> params = new HashMap<>();
    params.put("mappings", mappings);

    this.sendQuery(graph, "UNWIND $mappings AS mapping " + "MATCH (e:Element {name: mapping[0]}) "
        + "SET e.name = mapping[1]", params);
  }

  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    try (RedisGraphContext context = this.graph.getContext()) {

      //TODO: replace with parameter query (like add OPTIONAL MATCH clause)
      StringBuilder match = new StringBuilder("MATCH (an:Atom {name:'" + a.getName() + "'})");
      String end = "\nMATCH (an)-[r:Contains]->(e:Element)";
      end += "\nWITH an, an.name AS a, COLLECT(r.rank) AS r, COLLECT(e.name) AS e";
      if (delete) {
        end += "\nDETACH DELETE an";
      }
      end += "\nRETURN a, r, e";

      Element e;
      Map<Element, Integer> variables = new HashMap<>();
      int varCount = 0;

      for (int i = 0; i < a.size(); i++) {
        e = a.get(i);

        if (e.isConstant()) {
          match.append(
              String.format(", (an)-[:Contains {rank:%d}]->(:Element {name: '%s', isConst: true})",
                  i, e.getName()));

        } else {
          if (!variables.containsKey(e)) {
            variables.put(e, varCount++);
          }
          match.append(
              String.format(", (an)-[:Contains {rank:%d}]->(x%d:Element {isConst: false})", i,
                  variables.get(e)));
        }
      }

      String query = match + end;
      ResultSet result = this.sendQuery(context, query);
      return StreamSupport.stream(result.spliterator(), true).map(BddRedis::recordToAtom)
          .collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsModuloNull(Atom a) {
    String query = cypherQueryMatchSetModuloNull(Collections.singleton(a), 1);
    return this.sendReadOnlyQuery(this.graph, query).hasNext();
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    try (RedisGraphContext context = this.graph.getContext()) {
      BddStats bddStats = new BddStats();
      bddStats.start();

      Bdd sideEffects = this.chaseForInsertion(context, iRequest, rules, deltaMax, restricted,
          bddStats);
      thingsToAddAfterChase.initFromDB(sideEffects);

      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(context, sideEffects);
      bddStats.stopNullBucket();

      this.coreMaintenance(context, nullBucket, bddStats);

      bddStats.startNullDegree();
      Collection<Element> invalidNulls = sideEffects.getNulls().stream().map(x -> (Variable) x)
          .filter(
              x -> deltaMax > 0 && x.getProf() > deltaMax).collect(Collectors.toSet());
      boolean cantCommit = this.containsAnyNulls(context, invalidNulls);
      bddStats.stopNullDegree();

      if (cantCommit) {
        //tx.rollback();

      } else {
        bddStats.startNullDegree();
        this.setNullsDegree(context, sideEffects.getNulls(), 0);
        bddStats.stopNullDegree();

        bddStats.startCommit();
        //tx.commit();
        bddStats.stopCommit();
      }

      bddStats.stop();
      return bddStats;
    }
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    try (RedisGraphContext context = this.graph.getContext()) {
      BddStats bddStats = new BddStats();
      bddStats.start();

      bddStats.startDelete();
      Collection<Atom> toDel = this.isomorphicAtom(dRequest, true);
      bddStats.startDelete();
      bddStats.delAtoms(toDel.size());

      Bdd sideEffects = this.chaseForDeletion(context, toDel, rules, deltaMax, restricted,
          bddStats);

      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(context, sideEffects);
      bddStats.stopNullBucket();

      this.coreMaintenance(context, nullBucket, bddStats);

      bddStats.startCommit();
      //tx.commit();
      bddStats.stopCommit();

      bddStats.stop();
      return bddStats;
    }
  }

  @Override
  protected Bdd chaseForInsertion(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    return this.chaseForInsertion(this.graph, iRequest, rules, deltaMax, restricted, bddStats);
  }

  protected Bdd chaseForInsertion(RedisGraph graph, Collection<? extends Atom> iRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsInsertedBefore = new BddMemoire(iRequest); // Atoms inserted at previous step
    Bdd ToIns = new BddMemoire(iRequest);               // All atoms inserted

    bddStats.addAtoms(iRequest.size());
    bddStats.startInsert();
    this.addAll(graph, iRequest);
    bddStats.stopInsert();

    while (!atomsInsertedBefore.isEmpty()) {
      Bdd atomsToInsert = new BddMemoire(); // Atoms to insert during the current step

      bddStats.startChase();
      for (Rule rule : rules) {

        // Get substitution candidates
        substitutionCandidates.clear();
        if (rule.bodyContainsOne(atomsInsertedBefore.getAllPredicates())) {
          for (Atom aRule : rule.getBody()) {
            for (Atom a : atomsInsertedBefore.getAtomsWithPredicate(aRule.getName())) {
              Substitution substitution = new Substitution();
              for (int i = 0; i < a.size(); i++) {
                if (a.get(i).isConstant()) {
                  substitution.put((Variable) aRule.get(i), a.get(i));
                }
              }
              substitutionCandidates.add(substitution);
            }
          }
        }

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE INS " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(graph, r, atomsToInsert, null, null, deltaMax);
        }
      }
      bddStats.stopChase();

      LOGGER.info("CHASE INS " + step + ": " + atomsToInsert + "\n");
      bddStats.addAtoms(atomsToInsert.size());
      bddStats.startInsert();
      this.addAll(graph, atomsToInsert);
      bddStats.stopInsert();

      ToIns.addAll(atomsToInsert);

      atomsInsertedBefore.clear();
      for (Atom a : atomsToInsert) {
        if ((deltaMax < 1 || a.getMaxProf() < deltaMax) && (!restricted
            || a.containsConstantValues())) {
          atomsInsertedBefore.add(a);
        }
      }

      ++step;
    }

    return ToIns;
  }

  @Override
  protected Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    return this.chaseForDeletion(this.graph, dRequest, rules, deltaMax, restricted, bddStats);
  }

  protected Bdd chaseForDeletion(RedisGraph graph, Collection<? extends Atom> dRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsDeletedBefore = new BddMemoire(dRequest);  // Atoms deleted at previous step
    Bdd ToIns = new BddMemoire();                       // All atoms inserted
    Bdd ToDel = new BddMemoire(dRequest);               // All atoms deleted

    while (!atomsDeletedBefore.isEmpty()) {
      Bdd atomsToDelete = new BddMemoire(); // Atoms to delete during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        Atom head = rule.getHead();

        // Get substitution candidates
        substitutionCandidates.clear();
        for (Atom a : atomsDeletedBefore.getAtomsWithPredicate(head.getName())) {
          Substitution substitution = new Substitution();
          for (int i = 0; i < a.size(); i++) {
            Variable var = (Variable) head.get(i);
            if (rule.getVariablesInBody().contains(var)) {
              substitution.put(var, a.get(i));
            }
          }
          substitutionCandidates.add(substitution);
        }

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE DEL " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(graph, r, ToIns, atomsToDelete, ToDel, deltaMax);
        }
      }
      bddStats.stopChase();

      ToIns = this.chaseForInsertion(graph, ToIns, rules, deltaMax, restricted, bddStats);
      for (Atom a : new BddMemoire(ToIns)) {
        if ((deltaMax > 0 && a.getMaxProf() > deltaMax) || ToDel.containsModuloNull(a)) {
          atomsToDelete.add(a);
          ToIns.remove(a);
        }
      }

      LOGGER.info("CHASE DEL " + step + ": " + atomsToDelete);
      bddStats.delAtoms(atomsToDelete.size());
      bddStats.startDelete();
      this.removeAll(graph, atomsToDelete);
      bddStats.stopDelete();

      ToDel.addAll(atomsToDelete);
      atomsDeletedBefore = atomsToDelete;

      ++step;
    }

    Bdd sideEffects = new BddMemoire(ToDel);
    sideEffects.addAll(ToIns);

    return sideEffects;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {
    this.chaseStep(this.graph, rule, newToIns, newToDel, ToDel, deltaMax);
  }

  protected void chaseStep(RedisGraph graph, Rule rule, Collection<Atom> newToIns,
      Collection<Atom> newToDel, Bdd ToDel, int deltaMax) {
    // Execute query
    String query = new LocalElementIndexQueryBuilder().chaseQuery(rule);
    ResultSet rs = this.sendReadOnlyQuery(graph, query);

    // Get substitutions
    rs.forEachRemaining((Record record) -> {
      Substitution sub = new Substitution();
      Map<String, Object> subMap = record.getValue("sub");

      // Generate substitution
      for (Map.Entry<String, Object> entry : subMap.entrySet()) {
        sub.put((Variable) Element.stringToElement(entry.getKey()),
            Element.stringToElement(entry.getValue().toString()));
      }

      // Apply substitution
      Rule r = sub.applySub(rule);
      Atom head = r.getHead();
      int prof = r.getMaxProf() + 1;

      // Set prof
      for (Variable v : head.getVariables()) {
        if (!r.getVariablesInBody().contains(v)) {
          v.setProf(prof);
        }
      }

      // Do chase step
      if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
        LOGGER.info("DEL " + r.getAtomesToDelInBody());
        newToDel.addAll(r.getAtomesToDelInBody());

      } else {
        LOGGER.info("ADD " + head);
        newToIns.add(head);
      }
    });
  }

  @Override
  protected Collection<Set<Atom>> getInstancesModuloNull(Collection<Atom> atoms) {
    return this.getInstancesModuloNull(this.graph, atoms, null).collect(Collectors.toList());
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    return Stream.empty();
  }

  protected Stream<Set<Atom>> getInstancesModuloNull(RedisGraph graph, Collection<Atom> atoms,
      Integer limit) {
    String query = cypherQueryMatchSetModuloNull(atoms, limit);
    LOGGER.info("Instance modulo null query: " + query);
    ResultSet result = this.sendReadOnlyQuery(graph, query);

    return StreamSupport.stream(result.spliterator(), true).map(BddRedis::recordToListAtom);
  }

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    return this.getNullBucket(this.graph, atoms);
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  protected Set<Element> getNullBucket(RedisGraph graph, Bdd atoms) {
    Map<String, Object> params = new HashMap<>();
    params.put("predicates", atoms.getAllPredicates());

    //We build a query (Q2) to find every null which can match with the atoms of the partition
    //and then we build a query (Q3) to find the partitions linked to those null values in the database
    ResultSet result = this.sendReadOnlyQuery(graph,
        "UNWIND $predicates AS p "
            + "MATCH (a:Atom {name: p})-[:Contains]->(n:Element {isConst: false}) "
            + "RETURN DISTINCT n.name", params);

    Set<Element> nullBucket = StreamSupport.stream(result.spliterator(), true)
        .map(record -> record.getString(0)).map(
            Element::stringToElement).collect(Collectors.toSet());
    nullBucket.addAll(atoms.getNulls());

    return nullBucket;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    return this.getPartitions(this.graph, nullBucket);
  }

  protected Map<Set<Element>, Set<Atom>> getPartitions(RedisGraph graph,
      Collection<Element> nullBucket) {
    //A set of null values (partition)
    //is associated with a set of elements belonging to this partition
    Map<Set<Element>, Set<Atom>> partitions = new HashMap<>();

    List<Element> toCompute = new ArrayList<>(nullBucket);
    Set<Element> computed = new HashSet<>();

    // Max path length (traversal of all null-atom pair)
    int maxPathLength = 1; //(this.getNbNulls(graph) * 2) + 1;

    while (!toCompute.isEmpty()) {
      Variable nullElement = (Variable) toCompute.get(0);
      toCompute.remove(0);
      computed.add(nullElement);

      // Prepare params
      Map<String, Object> params = new HashMap<>();
      params.put("null", getNullName(nullElement));

      //We match a combination of an atom (a) and all the nulls is linked directly or indirectly to by other nulls (n).
      //(we are recursively traveling Contains relationships in any direction)
      String cypherQuery = //"UNWIND $nulls AS n\n" +
          "MATCH p = (x:Element {name: $null, isConst: false})-[:Contains*1.." + maxPathLength
              + "]-(y)\n"
              + "WHERE x <> y AND ALL(n IN nodes(p) WHERE n.isConst IS null OR n.isConst = false)\n"
              + "WITH x, COLLECT(DISTINCT y) AS nodes\n"
              + "WITH [n IN nodes WHERE n.isConst IS null] AS atoms, [x.name] + [n IN nodes WHERE n.isConst = false | n.name] AS partition\n"
              + "UNWIND atoms AS a\n" + "MATCH (a)-[r:Contains]->(e:Element)\n"
              + "WITH a, collect(r.rank) as r, collect(e.name) as e, partition\n"
              + "RETURN a.name as a, r, e, partition";

      //We get all partitions in the database which corresponds to our null values in nullBucket
      LOGGER.info("Run partition query for " + nullElement + " " + toCompute.size() + " remaining");
      ResultSet rs = this.sendReadOnlyQuery(graph, cypherQuery, params);

      rs.forEachRemaining(record -> {
        Atom atom = recordToAtom(record);
                /*List<Object> partition = record.getValue("partition");

                Set<Element> foundNulls = partition.stream()
                        .map(Object::toString)
                        .map(Element::stringToElement)
                        .collect(Collectors.toSet());

                if (!partitions.containsKey(foundNulls)) {
                    partitions.put(foundNulls, new HashSet<>());
                }
                partitions.get(foundNulls).add(atom);*/

        Set<Set<Element>> keysToMerge = new HashSet<>();

        // Search linked partitions
        for (Element ti : atom.getNulls()) {
          if (!computed.contains(ti)) {
            toCompute.add(ti);
          }
          for (Set<Element> k : partitions.keySet()) {
            if (k.contains(ti)) {
              keysToMerge.add(k);
            }
          }
        }

        // Merge partitions
        Set<Element> newKey = new HashSet<>(atom.getNulls());
        Set<Atom> newPartition = new HashSet<>();
        newPartition.add(atom);

        for (Set<Element> oldKey : keysToMerge) {
          newKey.addAll(oldKey);
          newPartition.addAll(partitions.get(oldKey));
          partitions.remove(oldKey);
        }

        partitions.put(newKey, newPartition);
      });
    }

    return partitions;
  }

  @Override
  public boolean coreMaintenance(Collection<Element> nullBucket, BddStats bddStats) {
    return this.coreMaintenance(this.graph, nullBucket, bddStats);
  }

  protected boolean coreMaintenance(RedisGraph graph, Collection<Element> nullBucket,
      BddStats bddStats) {
    boolean simplification = false;

    bddStats.startPartition();
    Map<Set<Element>, Set<Atom>> partitions = this.getPartitions(graph, nullBucket);
    bddStats.stopPartition();

    int i = 1;
    for (Set<Atom> partition : partitions.values()) {
      LOGGER.info("Search simplification for partition " + (i++) + "/" + partitions.size());
      bddStats.startSimplification();
      Bdd partSimp = new BddMemoire(partition);
      partSimp.coreMaintenance(
          partition.stream().flatMap(a -> a.getNulls().stream()).collect(Collectors.toSet()),
          new BddStats());
      Optional<Set<Atom>> instance = this.getInstancesModuloNull(graph, partSimp, null)
          .filter(x -> !x.containsAll(partSimp)).sorted(
              (x, y) -> {
                int nbX = 0;
                int nbY = 0;
                for (Atom ap : partSimp) {
                  for (Atom a : x) {
                    for (Element e : a) {
                      if (ap.contains(e)) {
                        nbX++;
                      }
                    }
                  }
                  for (Atom a : y) {
                    for (Element e : a) {
                      if (ap.contains(e)) {
                        nbY++;
                      }
                    }
                  }
                }
                return Integer.compare(nbX, nbY);
              }).findAny();
      bddStats.stopSimplification();

      if (instance.isPresent()) {
        LOGGER.info("**** Simplify " + partition + " with " + instance.get());
        bddStats.delAtoms(partition.size());
        bddStats.addAtoms(instance.get().size());

        bddStats.startDelete();
        this.removeAll(graph, partition);
        bddStats.stopDelete();

        bddStats.startInsert();
        this.addAll(graph, instance.get());
        bddStats.stopInsert();

        simplification = true;
      }
    }

    return simplification;
  }

  private ResultSet sendQuery(RedisGraph graph, String query) {
    return this.sendQuery(graph, query, Collections.emptyMap());
  }

  private ResultSet sendQuery(RedisGraph graph, String query, Map<String, Object> params) {
    if (!params.isEmpty()) {
      query = prepareQuery(query, params);
    }
    return graph.query(this.graphId, query, TIMEOUT);
  }

  private ResultSet sendReadOnlyQuery(RedisGraph graph, String query) {
    return this.sendReadOnlyQuery(graph, query, Collections.emptyMap());
  }

  private ResultSet sendReadOnlyQuery(RedisGraph graph, String query, Map<String, Object> params) {
    if (!params.isEmpty()) {
      query = prepareQuery(query, params);
    }
    return graph.readOnlyQuery(this.graphId, query, TIMEOUT);
  }
}