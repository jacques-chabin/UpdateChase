package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Schema;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseManagementServiceBuilder;
import org.neo4j.graphdb.Entity;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;

public class BddNeo4jCore extends Bdd {

  private final static String NODE_ID_KEY = "_id";

  private final GraphDatabaseService graphDB;
  private final DatabaseManagementService managementService;

  private final Schema schema;

  private final Map<Element, Set<Atom>> nullIndex;

  public BddNeo4jCore() {
    this(new Schema());
  }

  public BddNeo4jCore(Schema schema) {
    Path directory;

    try {
      directory = Files.createTempDirectory("neo4j");

    } catch (IOException exception) {
      exception.printStackTrace();
      directory = Path.of("neo4j");
    }

    this.schema = schema;
    this.managementService = new DatabaseManagementServiceBuilder(directory).build();
    this.managementService.createDatabase("nulls");
    this.graphDB = this.managementService.database("nulls");

    this.nullIndex = new HashMap<>();
  }

  @Override
  public boolean isConnected() {
    return this.graphDB.isAvailable(100);
  }

  private static Atom entityToAtom(Entity entity) {
    Map<String, Object> properties = entity.getAllProperties();

    String predicate;
    Element[] elements;
    int i;

    if (entity instanceof Node) {
      predicate = ((Node) entity).getLabels().iterator().next().name();
      elements = new Element[properties.size()];
      elements[0] = Element.stringToElement(properties.get(NODE_ID_KEY).toString());
      i = 1;

    } else {
      predicate = ((Relationship) entity).getType().name();
      elements = new Element[properties.size() + 2];
      elements[0] = Element.stringToElement(
          ((Relationship) entity).getStartNode().getProperty(NODE_ID_KEY).toString());
      elements[1] = Element.stringToElement(
          ((Relationship) entity).getEndNode().getProperty(NODE_ID_KEY).toString());
      i = 2;
    }

    for (; i < elements.length; i++) {
      elements[i] = Element.stringToElement(properties.get("rank" + i).toString());
    }

    return new Atom(predicate, elements);
  }

  private Map<String, Object> getEntityDefinitionOf(Atom atom, boolean iso) {
    Map<String, Object> entityProperties = new HashMap<>();
    for (int i = 0; i < atom.size(); i++) {
      Element element = atom.get(i);
      String name = element.isConstant() ? element.getName() : getNullName((Variable) element);

      if (!iso || element.isConstant()) {
        entityProperties.put("rank" + i, name);
      }
    }

    if (this.schema.isEdge(atom)) {
      entityProperties.remove("rank0");
      entityProperties.remove("rank1");

    } else {
      if (entityProperties.containsKey("rank0")) {
        entityProperties.put(NODE_ID_KEY, entityProperties.remove("rank0"));
      }
    }

    return entityProperties;
  }

  private boolean atomIsoFilter(Entity entity, Atom atom) {
    boolean ok = true;
    int i = 0;

    Map<String, Object> nullVars = new HashMap<>();
    if (this.schema.isEdge(atom)) {
      i = 2;
    }

    for (; ok && i < atom.size(); i++) {
      if (atom.get(i).isNullValue()) {
        String key = "rank" + i;
        if (i == 0) {
          key = NODE_ID_KEY;
        }

        if (nullVars.containsKey(key)) {
          ok = entity.getProperty(key).equals(nullVars.get(key));

        } else {
          ok = entity.getProperty(key).toString().startsWith("_");
          nullVars.put(key, entity.getProperty(key));
        }
      }
    }

    return ok;
  }

  private Stream<Node> getAtomNodes(Transaction transaction, Atom atom, boolean iso) {
    Label label = Label.label(atom.getName());
    Map<String, Object> properties = this.getEntityDefinitionOf(atom, iso);

    return transaction.findNodes(label, properties).stream()
        .filter(node -> !iso || atomIsoFilter(node, atom));
  }

  private Stream<Relationship> getAtomRelationships(Transaction transaction, Atom atom,
      boolean iso) {
    RelationshipType type = RelationshipType.withName(atom.getName());
    Map<String, Object> properties = this.getEntityDefinitionOf(atom, iso);

    return transaction.getAllRelationships().stream()
        .filter(relationship -> relationship.isType(type)).filter(
            relationship -> relationship.getStartNode().getProperty(NODE_ID_KEY)
                .equals(atom.get(0))).filter(
            relationship -> relationship.getEndNode().getProperty(NODE_ID_KEY).equals(atom.get(1)))
        .filter(
            relationship -> relationship.getProperties().equals(properties))
        .filter(node -> !iso || atomIsoFilter(node, atom));
  }

  private void addToNullIndex(Atom atom) {
    for (Variable nullElement : atom.getNulls()) {
      if (!this.nullIndex.containsKey(nullElement)) {
        this.nullIndex.put(nullElement, new HashSet<>());
      }

      this.nullIndex.get(nullElement).add(atom);
    }
  }

  private void removeFromNullIndex(Atom atom) {
    for (Variable nullElement : atom.getNulls()) {
      if (this.nullIndex.containsKey(nullElement)) {
        Set<Atom> index = this.nullIndex.get(nullElement);
        index.remove(atom);

        if (index.isEmpty()) {
          this.nullIndex.remove(nullElement);
        }
      }

    }
  }

  @Override
  public void close() {
    this.managementService.shutdown();
  }

  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    try (Transaction tx = this.graphDB.beginTx()) {
      Stream<? extends Entity> entityStream;

      if (this.schema.isEdge(a)) {
        entityStream = getAtomRelationships(tx, a, true);

      } else {
        entityStream = getAtomNodes(tx, a, true);

      }

      return entityStream.map(BddNeo4jCore::entityToAtom).collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsModuloNull(Atom a) {
    try (Transaction tx = this.graphDB.beginTx()) {
      Stream<? extends Entity> entityStream;

      if (this.schema.isEdge(a)) {
        entityStream = getAtomRelationships(tx, a, true);

      } else {
        entityStream = getAtomNodes(tx, a, true);

      }

      return entityStream.count() > 0;
    }
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    try (Transaction tx = this.graphDB.beginTx()) {
      BddStats bddStats = new BddStats();
      bddStats.start();

      Bdd sideEffects = this.chaseForInsertion(tx, iRequest, rules, deltaMax, restricted, bddStats);
      thingsToAddAfterChase.initFromDB(sideEffects);

      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(tx, sideEffects);
      bddStats.stopNullBucket();

      this.coreMaintenance(tx, nullBucket, bddStats);

      bddStats.startNullDegree();
      Collection<Element> invalidNulls = sideEffects.getNulls().stream().map(x -> (Variable) x)
          .filter(
              x -> x.getProf() > deltaMax).collect(Collectors.toSet());
      boolean canCommit = this.containsAnyNulls(tx, invalidNulls);
      bddStats.stopNullDegree();

      if (canCommit) {
        tx.rollback();

      } else {
        bddStats.startNullDegree();
        this.setNullsDegree(tx, sideEffects.getNulls(), 0);
        bddStats.stopNullDegree();

        bddStats.startCommit();
        tx.commit();
        bddStats.stopCommit();
      }

      bddStats.stop();
      return bddStats;
    }
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    try (Transaction tx = this.graphDB.beginTx()) {
      BddStats bddStats = new BddStats();
      bddStats.start();

      bddStats.startDelete();
      Collection<Atom> toDel = this.isomorphicAtom(dRequest, true);
      bddStats.startDelete();
      bddStats.delAtoms(toDel.size());

      Bdd sideEffects = this.chaseForDeletion(tx, toDel, rules, deltaMax, restricted, bddStats);

      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(tx, sideEffects);
      bddStats.stopNullBucket();

      this.coreMaintenance(tx, nullBucket, bddStats);

      bddStats.startCommit();
      tx.commit();
      bddStats.stopCommit();

      bddStats.stop();
      return bddStats;
    }
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    try (Transaction tx = this.graphDB.beginTx()) {
      this.setNullsDegree(tx, nulls, degree);
    }
  }

  protected void setNullsDegree(Transaction transaction, Collection<Element> nulls, int degree) {
    nulls.stream().flatMap(
            element -> this.nullIndex.getOrDefault(element, Collections.emptySet()).stream()).distinct()
        .parallel().flatMap(atom -> {
          if (this.schema.isEdge(atom)) {
            return this.getAtomRelationships(transaction, atom, false);
          } else {
            return this.getAtomNodes(transaction, atom, false);
          }
        }).map(Entity::getAllProperties).forEach(properties -> {
          for (Map.Entry<String, Object> property : properties.entrySet()) {
            Element element = Element.stringToElement(property.getValue().toString());
            if (!element.isConstant()) {
              Variable variable = (Variable) element;
              variable.setProf(degree);
              properties.put(property.getKey(), getNullName(variable));
            }
          }
        });
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    return Stream.empty();
  }

  private Collection<Set<Atom>> getInstancesModuloNull(Transaction transaction,
      Collection<Atom> atoms) {
    Map<String, Object> nullVars = new HashMap<>();
    return null;
  }

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    try (Transaction tx = this.graphDB.beginTx()) {
      return this.getNullBucket(tx, atoms);
    }
  }

  protected Set<Element> getNullBucket(Transaction transaction, Bdd atoms) {
    Set<Element> nullBucket = atoms.getNulls();

    for (String predicate : atoms.getAllPredicates()) {
      transaction.findNodes(Label.label(predicate)).forEachRemaining(node -> {
        for (Map.Entry<String, Object> property : node.getAllProperties().entrySet()) {
          Element element = Element.stringToElement(property.getValue().toString());

          if (element.isNullValue()) {
            nullBucket.add(element);
          }
        }
      });
    }

    return nullBucket;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    try (Transaction tx = this.graphDB.beginTx()) {
      return this.getPartitions(tx, nullBucket);
    }
  }

  protected Map<Set<Element>, Set<Atom>> getPartitions(Transaction transaction,
      Collection<Element> nullBucket) {
    final Map<Set<Element>, Set<Atom>> partition = new HashMap<>();
    final Collection<Element> computed = new HashSet<>();
    final Collection<Element> toCompute = new HashSet<>(nullBucket);

    while (!toCompute.isEmpty()) {
      // Params
      Collection<Element> bucket = new ArrayList<>(toCompute);

      // Prepare next iteration
      computed.addAll(toCompute);
      toCompute.clear();

      // Build partitions
      bucket.stream().flatMap(
              nullElement -> this.nullIndex.getOrDefault(nullElement, Collections.emptySet()).stream())
          .forEach(
              atom -> {
                Set<Element> key = null;
                for (Element ti : atom.getNulls()) {
                  for (Set<Element> k : new HashSet<>(partition.keySet())) {
                    if (k.contains(ti)) {
                      if (key == null) {
                        key = k;
                        partition.get(key).add(atom);
                      } else {
                        Set<Atom> oldSet = partition.get(key);
                        partition.remove(key);
                        key.addAll(k);
                        partition.put(key, oldSet);
                        partition.get(key).addAll(partition.get(k));
                        partition.remove(k);
                      }
                    }
                  }
                  if (key == null) {
                    key = new HashSet<>();
                    key.add(ti);
                    partition.put(key, new HashSet<>());
                    partition.get(key).add(atom);
                  }
                  if (!computed.contains(ti)) {
                    toCompute.add(ti);
                  }
                }
              });
    }

    return partition;
  }

  @Override
  public int size() {
    int count = 0;

    try (Transaction tx = this.graphDB.beginTx()) {
      for (Node ignored : tx.getAllNodes()) {
        count++;
      }

      for (Relationship ignored : tx.getAllRelationships()) {
        count++;
      }
    }

    return count;
  }

  @Override
  public Iterator<Atom> iterator() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.collect(Collectors.toList()).iterator();
    }
  }

  @Override
  public Stream<Atom> stream() {
    Transaction tx = this.graphDB.beginTx();

    return Stream.concat(tx.getAllNodes().stream().map(BddNeo4jCore::entityToAtom),
        tx.getAllRelationships().stream().map(BddNeo4jCore::entityToAtom)).onClose(tx::close);
  }

  @Override
  public boolean add(Atom atom) {
    return this.add(atom);
  }

  @Override
  public boolean addAll(Collection<? extends Atom> atoms) {
    try (Transaction tx = this.graphDB.beginTx()) {
      boolean res = this.addAll(tx, atoms);
      tx.commit();
      return res;
    }
  }

  protected boolean addAll(Transaction transaction, Collection<? extends Atom> atoms) {
    for (Atom atom : atoms) {
      Entity entity = null;
      Map<String, Object> properties = this.getEntityDefinitionOf(atom, false);
      this.addToNullIndex(atom);

      if (this.schema.isEdge(atom)) {
        RelationshipType type = RelationshipType.withName(atom.getName());
        Node from = null;
        Node to = null;

        for (Node node : transaction.getAllNodes()) {
          if (from == null && node.getProperty(NODE_ID_KEY).equals(atom.get(0))) {
            from = node;
          }

          if (to == null && node.getProperty(NODE_ID_KEY).equals(atom.get(1))) {
            to = node;
          }

          if (from != null && to != null) {
            break;
          }
        }

        if (from != null && to != null) {
          Optional<Relationship> relationship = StreamSupport.stream(
              from.getRelationships(type).spliterator(), false).filter(
              rel -> rel.getProperties().equals(properties)).findFirst();

          if (!relationship.isPresent()) {
            entity = from.createRelationshipTo(to, type);
          }
        }

      } else {
        Label label = Label.label(atom.getName());
        if (!transaction.findNodes(label, properties).hasNext()) {
          entity = transaction.createNode(label);
        }
      }

      if (entity != null) {
        for (Map.Entry<String, Object> property : properties.entrySet()) {
          entity.setProperty(property.getKey(), property.getValue());
        }
      }
    }

    return true;
  }

  @Override
  public boolean remove(Object o) {
    return this.removeAll(Collections.singleton(o));
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    try (Transaction tx = this.graphDB.beginTx()) {
      boolean res = this.removeAll(tx, c);
      tx.commit();
      return res;
    }
  }

  protected boolean removeAll(Transaction transaction, Collection<?> c) {
    for (Object a : c) {
      Atom atom = (Atom) a;
      this.removeFromNullIndex(atom);

      if (this.schema.isEdge(atom)) {
        this.getAtomRelationships(transaction, atom, false).forEach(Relationship::delete);

      } else {
        this.getAtomNodes(transaction, atom, false).forEach(Node::delete);
      }
    }

    return true;
  }

  @Override
  public Collection<String> getAllPredicates() {
    Collection<String> predicates = new HashSet<>();

    try (Transaction tx = this.graphDB.beginTx()) {
      for (Label label : tx.getAllLabels()) {
        predicates.add(label.name());
      }

      for (RelationshipType type : tx.getAllRelationshipTypes()) {
        predicates.add(type.name());
      }
    }

    return predicates;
  }

  @Override
  public boolean coreMaintenance(Collection<Element> nullBucket, BddStats bddStats) {
    try (Transaction tx = this.graphDB.beginTx()) {
      boolean result = this.coreMaintenance(tx, nullBucket, bddStats);
      tx.commit();
      return result;
    }
  }

  protected boolean coreMaintenance(Transaction transaction, Collection<Element> nullBucket,
      BddStats bddStats) {
    boolean simplification = false;

    bddStats.startPartition();
    Map<Set<Element>, Set<Atom>> partitions = this.getPartitions(transaction, nullBucket);
    bddStats.stopPartition();

    int i = 1;
    for (Set<Atom> partition : partitions.values()) {
      System.out.println("Search simplification for partition " + (i++) + "/" + partitions.size());
      bddStats.startSimplification();
      Optional<Set<Atom>> instance = this.getInstancesModuloNull(transaction, partition).stream()
          .filter(
              x -> !x.containsAll(partition)).findAny();
      bddStats.stopSimplification();

      if (instance.isPresent()) {
        System.out.println("**** Simplify " + partition + " with " + instance.get());
        bddStats.delAtoms(partition.size());
        bddStats.addAtoms(instance.get().size());

        bddStats.startDelete();
        this.removeAll(transaction, partition);
        bddStats.stopDelete();

        bddStats.startInsert();
        this.addAll(transaction, instance.get());
        bddStats.stopInsert();

        simplification = true;
      }
    }

    return simplification;
  }

  @Override
  protected Bdd chaseForInsertion(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    try (Transaction tx = this.graphDB.beginTx()) {
      return this.chaseForInsertion(tx, iRequest, rules, deltaMax, restricted, bddStats);
    }
  }

  protected Bdd chaseForInsertion(Transaction transaction, Collection<? extends Atom> iRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;

    Bdd atomsInsertedBefore = new BddMemoire(iRequest); // Atoms inserted at previous step
    Bdd ToIns = new BddMemoire(iRequest);               // All atoms inserted

    bddStats.addAtoms(iRequest.size());
    bddStats.startInsert();
    this.addAll(transaction, iRequest);
    bddStats.stopInsert();

    while (!atomsInsertedBefore.isEmpty() && (deltaMax < 1 || ToIns.getMaxProf() <= deltaMax)) {
      Bdd atomsToInsert = new BddMemoire(); // Atoms to insert during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        this.chaseStep(transaction, rule, atomsToInsert, null, null, deltaMax);
      }
      bddStats.stopChase();

      System.out.println("CHASE INS " + step + ": " + atomsToInsert + "\n");
      bddStats.addAtoms(atomsToInsert.size());
      bddStats.startInsert();
      this.addAll(transaction, atomsToInsert);
      bddStats.stopInsert();

      ToIns.addAll(atomsToInsert);

      atomsInsertedBefore.clear();
      for (Atom a : atomsToInsert) {
        if (a.getMaxProf() < deltaMax && (!restricted || a.containsConstantValues())) {
          atomsInsertedBefore.add(a);
        }
      }

      ++step;
    }

    return ToIns;
  }

  @Override
  protected Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    try (Transaction tx = this.graphDB.beginTx()) {
      return this.chaseForDeletion(tx, dRequest, rules, deltaMax, restricted, bddStats);
    }
  }

  protected Bdd chaseForDeletion(Transaction transaction, Collection<? extends Atom> dRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;

    Bdd atomsDeletedBefore = new BddMemoire(dRequest);  // Atoms deleted at previous step
    Bdd ToIns = new BddMemoire();                       // All atoms inserted
    Bdd ToDel = new BddMemoire(dRequest);               // All atoms deleted

    while (!atomsDeletedBefore.isEmpty()) {
      Bdd atomsToDelete = new BddMemoire(); // Atoms to delete during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        this.chaseStep(transaction, rule, ToIns, atomsToDelete, ToDel, deltaMax);
      }
      bddStats.stopChase();

      ToIns = this.chaseForInsertion(transaction, ToIns, rules, deltaMax, restricted, bddStats);
      for (Atom a : new BddMemoire(ToIns)) {
        if (a.getMaxProf() > deltaMax || ToDel.containsModuloNull(a)) {
          atomsToDelete.add(a);
          ToIns.remove(a);
        }
      }

      System.out.println("CHASE DEL " + step + ": " + atomsToDelete);
      bddStats.delAtoms(atomsToDelete.size());
      bddStats.startDelete();
      this.removeAll(transaction, atomsToDelete);
      bddStats.stopDelete();

      ToDel.addAll(atomsToDelete);
      atomsDeletedBefore = atomsToDelete;

      ++step;
    }

    Bdd sideEffects = new BddMemoire(ToDel);
    sideEffects.addAll(ToIns);

    return sideEffects;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {
    try (Transaction tx = this.graphDB.beginTx()) {
      this.chaseStep(tx, rule, newToIns, newToDel, ToDel, deltaMax);
    }
  }

  protected void chaseStep(Transaction transaction, Rule rule, Collection<Atom> newToIns,
      Collection<Atom> newToDel, Bdd ToDel, int deltaMax) {
    // Execute query

    // Get substitutions
        /*rs.forEachRemaining((Record record) -> {
            Substitution sub = new Substitution();
            Map<String, Object> subMap = record.get("sub").asMap();

            // Generate substitution
            for (Map.Entry<String, Object> entry : subMap.entrySet()) {
                sub.put(
                        (Variable) Element.stringToElement(entry.getKey()),
                        Element.stringToElement(entry.getValue().toString())
                );
            }

            // Apply substitution
            Rule r = sub.applySub(rule);
            Atom head = r.getHead();
            int prof = r.getMaxProf() + 1;

            // Set prof
            for (Variable v : head.getVariables()) {
                if (!r.getVariablesInBody().contains(v)) {
                    v.setProf(prof);
                }
            }

            //System.out.println("RULE " + r);

            // Do chase step
            if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
                System.out.println("DEL " + r.getAtomesToDelInBody());
                newToDel.addAll(r.getAtomesToDelInBody());

            } else {
                System.out.println("ADD " + head);
                newToIns.add(head);
            }
        });*/
  }

  @Override
  public boolean containsAnyNulls(Collection<? extends Element> nulls) {
    try (Transaction tx = this.graphDB.beginTx()) {
      return this.containsAnyNulls(tx, nulls);
    }
  }

  protected boolean containsAnyNulls(Transaction transaction, Collection<? extends Element> nulls) {
    return nulls.stream().anyMatch(this.nullIndex::containsKey);
  }

}
