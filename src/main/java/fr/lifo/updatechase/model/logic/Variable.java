package fr.lifo.updatechase.model.logic;

/**
 * L'objet Variable represente un Element variable. Il herite de Element.
 *
 * @see Element
 */
public class Variable extends Element {

  private int prof; //profondeur de la varaible créé (seulement si cette variable est nulle)

  /**
   * Constructeur de Variable. Fait uniquement appel au constructeur de Element.
   */
  public Variable(String name) {
    this(name, Element.VARIABLE);
  }

  public Variable(String name, byte type) {
    this(name, type, 0);
  }

  public Variable(String name, byte type, int nbProf) {
    this(name, type, nbProf, true);
  }

  public Variable(String name, byte type, int nbProf, boolean isReductible) {
    super(name, type, isReductible);
    this.prof = nbProf;

    if (type < Element.VARIABLE || type > Element.STARED_NULL) {
      throw new IllegalArgumentException();
    }
  }

  public int getProf() {
    return prof;
  }

  public void setProf(int prof) {
    this.prof = prof;
  }

  @Override
  protected void setType(byte type) throws IllegalArgumentException {
    if (type < Element.VARIABLE || type > Element.STARED_NULL) {
      throw new IllegalArgumentException();
    }

    super.setType(type);
  }

  /**
   * Redefinition du toString de Object. Si la variable est existenciel, prefixe "_" a son nom.
   *
   * @return String Le nom d'un Element.
   */
  @Override
  public String toString() {

    String res = super.toString();

    if (this.isNullValue()) {
      res += "#" + this.prof;

      if (this.isReductible()) {
        res += "R";
      }
    }

    return res;
  }

  @Override
  public Object clone() {
    return new Variable(this.getName(), this.getType(), this.getProf(), this.isReductible());
  }
}
