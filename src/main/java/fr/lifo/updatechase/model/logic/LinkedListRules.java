package fr.lifo.updatechase.model.logic;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.DBFileIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * <b>LinkedListRules herite de LinkedList<Rule></b>
 * Une LinkedListRule permet de gerer une Collection de Rule. Toutes les fonction ajoutees sont
 * statiques</p>
 */
public class LinkedListRules extends LinkedList<Rule> {

  /**
   * Initialise une LinkedListRule a partir d'un fichier texte de regles en .dlp.
   *
   * @param path String. Le nom du fichier source
   * @return LinkedListRule La liste chainee des Rule du fichier.
   * @throws SyntaxError
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static LinkedListRules fromFile(String path) throws SyntaxError, IOException {
    return DBFileIO.readFromFile(path, new LinkedListRules(), Rule::stringToRule);
  }

  public void toFile(String path) throws IOException {
    DBFileIO.writeToFile(path, this);
  }

  /**
   * Initialise un fichier de reponses a partir d'un fichier Sparql en .dlp. par des Constante et
   * les sauvegarde dans le fichier AnsAlgo1.dlp
   *
   * @param nameFile String. Le nom du fichier source
   * @return int Le nombre de solutions ecrites.
   */
  //	static LinkedListRules initFromAnsSparqlFile(String nameFile, Rule q){
  static int initFromAnsSparqlFile(String nameFile, Rule q) {
    //LinkedListRules res = new LinkedListRules();
    Rule r;
    int nbsol = 0;

    try {
      // open the file named nameFile.dlp in the repertory ./Constraint/
      //File f = open("./Constraint/"+nameFile+".dlp");
      FileReader fr = new FileReader("./" + nameFile + ".dlp");
      BufferedReader br = new BufferedReader(fr);

      // trouver la première ligne non vide qui ne commence pas par %
      boolean ok = false;
      String line = " ";
      List<Variable> vars = new ArrayList<>();
      try {
        while (!ok) {
          line = br.readLine();
          if (line == null || line.length() == 0 || line.charAt(0) == '%') {
          } else {
            ok = true;
          }
        }
      } catch (IOException e1) {
        System.out.println("Error reading " + e1.getMessage() + " in file " + nameFile);
      }
      if (ok) {
        //System.out.println(line);
        // initialiser la liste des variables
        line = line.replace(" ", "");
        Variable v2;
        Element e2;
        for (String s2 : line.split(",")) {
          v2 = new Variable(s2);
          vars.add(v2);
        }
        String s3 = "XUnknown1";
        v2 = new Variable(s3);
        vars.add(v2);
        s3 = "XUnknown2";
        v2 = new Variable(s3);
        vars.add(v2);
        //System.out.println("Liste des variables : "+vars);

        try {
          // open the file named ansAlgo1.dlp in the current directory
          FileWriter fw = new FileWriter("./ansAlgo1.dlp");

          Substitution sub;
          try {
            line = br.readLine();
            while (line != null) {

              if (line.length() > 0 && line.charAt(0) != '%') {
                // on supprime le numero du début
                int pos = 0;
                while (line.charAt(pos) >= '0' && line.charAt(pos) <= '9') {
                  //System.out.print(line.charAt(pos));
                  pos++;
                }
                //System.out.println(".");
                line = line.substring(pos + 1);
                //System.out.println("une rep : " + line);
                sub = new Substitution();
                int numv = 0;

                for (String s2 : line.split(",")) {
                  //System.out.println(" constant : " + s2 + " pour " + vars.get(numv));
                  e2 = new Constant(s2);
                  sub.put(vars.get(numv), e2);
                  numv++;
                }
                //System.out.println("nbconstants : " + numv);
                //System.out.println("La substitution : " + sub);
                r = sub.applySub(q);
                fw.write(r.toString() + ".\n");
                nbsol++;
                //res.add(r);
              }
              line = br.readLine();
            }
          } catch (IOException e1) {
            System.out.println("Error reading " + e1.getMessage());
          }
          fw.flush();
          fw.close();
        } catch (IOException e3) {
          System.out.println("Can't create file " + e3.getMessage());
        }
      }
    } catch (FileNotFoundException e2) {
      System.out.println("File not found " + e2.getMessage());
    }
    //return res;
    return nbsol;
  }

  /**
   * Initialise un fichier de reponses a partir d'un fichier de resultat MapRed en .dlp. par des
   * Constante et les sauvegarde dans le fichier AnsAlgo1.dlp
   *
   * @param nameFile String. Le nom du fichier source
   * @return int Le nombre de solutions ecrites.
   */
  // initialisation d'un fichier de réponses à partir d'un fichier de résultat MapRed
  public static int initFromAnsMapRedFile(String nameFile, Rule q) {
    //LinkedListRules res = new LinkedListRules();
    Rule r;
    int nbsol = 0;

    try {
      // open the file named nameFile.dlp in the repertory ./Constraint/
      //File f = open("./Constraint/"+nameFile+".dlp");
      FileReader fr = new FileReader("./" + nameFile + ".dlp");
      BufferedReader br = new BufferedReader(fr);

      // trouver la première ligne non vide qui ne commence pas par %
      boolean ok = false;
      String line = " ";
      List<Variable> vars = new ArrayList<>();
      try {
        while (!ok) {
          line = br.readLine();
          ok = (line != null && line.length() > 0 && line.charAt(0) != '%');
        }
      } catch (IOException e1) {
        System.out.println("Error reading " + e1.getMessage() + " in file " + nameFile);
      }
      if (ok) {
        //System.out.println(line);
        // initialiser la liste des variables
        line = line.replace(" ", "");
        Variable v2;
        Element e2;
        for (String s2 : line.split(",")) {
          v2 = new Variable(s2);
          vars.add(v2);
        }
        String s3 = "XUnknown1";
        v2 = new Variable(s3);
        vars.add(v2);
        s3 = "XUnknown2";
        v2 = new Variable(s3);
        vars.add(v2);
        //System.out.println("Liste des variables : "+vars);

        try {
          // open the file named ansAlgo1.dlp in the current directory
          FileWriter fw = new FileWriter("./ansAlgo1.dlp");

          Substitution sub;
          try {
            line = br.readLine();
            while (line != null) {

              if (line.length() > 0 && line.charAt(0) != '%') {
                int pos = 0;
                //System.out.println("une rep : " + line);
                sub = new Substitution();
                int numv = 0;

                for (String s2 : line.split(",")) {
                  //System.out.println(" constant : " + s2 + " pour " + vars.get(numv));
                  e2 = new Constant(s2);
                  sub.put(vars.get(numv), e2);
                  numv++;
                }
                //System.out.println("nbconstants : " + numv);
                //System.out.println("La substitution : " + sub);
                r = sub.applySub(q);
                fw.write(r.toString() + ".\n");
                nbsol++;
                //res.add(r);
              }
              line = br.readLine();
            }
          } catch (IOException e1) {
            System.out.println("Error reading " + e1.getMessage());
          }
          fw.flush();
          fw.close();
        } catch (IOException e3) {
          System.out.println("Can't create file " + e3.getMessage());
        }
      }
    } catch (FileNotFoundException e2) {
      System.out.println("File not found " + e2.getMessage());
    }
    //return res;
    return nbsol;
  }

  /**
   * Repère le risque de boucle infinie si aucune condition n'est définie (stopCond == 0). En
   * particulier, cette fonction repère les règles du type A(X,Y) -> A(Y,Z) qui sont problématiques
   * si aucune condition d'arret n'est définie. ATTENTION: cette fonction ne marche que pour les
   * règles de type n -> 1.
   *
   * @param stopCond int. La condition d'arret
   * @param llr      LinkedListRules. La liste des règles à appliquer.
   * @return boolean Vrai s'il y a un risque, faux sinon.
   */
  public static boolean risqueBoucleInfinie(LinkedListRules llr, int stopCond) {
    if (stopCond == 0) {
      for (Rule r : llr) {
        // Si le corp de la règle n'a qu'un atome et que le nom de cet atome est égal au nom de l'atome
        //contenu dans la tête de la règle alors...
        if (r.getBody().size() == 1 && r.getBody().get(0).getName().equals(r.getHead().getName())) {
          return true;
        }
      }
    }
    return false;
  }

  public void destroyIntersectionWith(LinkedListRules llr) {
    if (llr != null) {
      for (Rule r : llr) {
        this.remove(r);
      }
    }
  }
}
