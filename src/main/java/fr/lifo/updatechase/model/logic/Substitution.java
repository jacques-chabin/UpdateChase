package fr.lifo.updatechase.model.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p><b>Substitution associe a un ensemble de variables nommees, une valeur respective par
 * laquelle on peut les remplacer.</b>/<p>
 * <p>Une Substitution est composee d'une Map<Variable,Element></p>
 *
 * @see Element
 * @see Variable
 * @see Constant
 * @see Map
 */
public class Substitution extends HashMap<Variable, Element> {

  /**
   * Construit la representation en String d'une Substitution.
   *
   * @return String La representation en String d'une Substitution.
   */
  public String toString() {
    StringBuilder res = new StringBuilder();
    for (Map.Entry<Variable, Element> entry : this.entrySet()) {
      res.append(entry.getKey()).append("-->").append(entry.getValue()).append(" ");
    }
    return res.toString();
  }

  /**
   * Apply substitution on a set of atom
   *
   * @param atoms The set of atoms where variables need to be substituted
   * @return A set of atoms where variables are substituted
   */
  public Set<Atom> applySub(Collection<Atom> atoms) {
    return atoms.stream().map(this::applySub).collect(Collectors.toSet());
  }

  /**
   * Substitue toutes les Variables possible d'un Atome avec leur valeur associees.
   *
   * @param a Atome L'Atome dont les Variables vont etre substituees.
   * @return Atome Une nouvel Atome dont les Variable ont ete remplacee par leur valeurs.
   */
  public Atom applySub(Atom a) {
    return this.applySub(a, 0);
  }

  /**
   * Substitue toutes les Variables possible d'un Atome avec leur valeur associees. PREND EN COMPTE
   * LA PROFONDEUR DE RECHERCHE
   *
   * @param a Atome, L'Atome dont les Variables vont etre substituees.
   * @return Atome Une nouvel Atome dont les Variable ont ete remplacee par leur valeurs.
   */
  public Atom applySub(Atom a, int nbProf) {
    List<Element> r = new ArrayList<>();
    boolean stared = containsNullValues();
    for (Element e : a) {
      if (e.isVariable()) {
        Element e2 = this.get(e);
        if (e2 != null) {
          if (e2.isVariable()) {

            r.add(new Variable(e2.getName(), e2.getType(), ((Variable) e2).getProf(),
                e2.isReductible()));
          } else {
            r.add(e2);
          }
        } else {
          Element e3 = new Variable("N" + UUID.randomUUID(),
              stared ? Element.STARED_NULL : Element.NULL, nbProf);
          r.add(e3);
          put((Variable) e, e3);
        }
      } else {
        r.add(e);
      }
    }
    return new Atom(a.getFullName(), r);
  }

  /**
   * Substitue toutes les Variables possibles des Atomes du body, et de l'Atome du head d'une Rule
   * avec leur valeur associees.
   *
   * @param r Rule La Rule dont on veut substituer les Variables de chacun de ses Atomes.
   * @return Query Une Query composee de valeur.
   */
  public Query applySub(Rule r) {
    Atom h = applySub(r.getHead());
    List<Atom> b = new ArrayList<>();
    for (Atom a : r.getBody()) {
      b.add(applySub(a));
    }
    return new Query(h, b);
  }

  /**
   * Substitue toutes les Variables possibles des Atomes du body, et de l'Atome du head d'une Rule
   * avec leur valeur associees.
   *
   * @param r Rule La Rule dont on veut substituer les Variables de chacun de ses Atomes.
   * @return Rule Une Rule composee de valeur.
   */
  public Rule applySub2(Rule r) {
    Atom h = applySub(r.getHead());
    List<Atom> b = new ArrayList<>();
    for (Atom a : r.getBody()) {
      b.add(applySub(a));
    }
    return new Rule(h, b);
  }

  /**
   * @return true si la substitution instancie une variable vers une var nulle
   **/
  public boolean containsNullValues() {
    return this.values().stream().anyMatch(Element::isNullValue);
  }

  /**
   * @return le nombre de variables instanciées vers des non variable
   */
  public long nbRealInstances() {
    return this.values().stream().filter(x -> !x.isVariable()).count();
  }

  public long nbNonIdentityNull(Substitution identity) {
    Set<Element> identityNulls = identity.values().stream().filter(Element::isNullValue)
        .collect(Collectors.toSet());
    return this.values().stream().filter(element -> !identityNulls.contains(element)).count();
  }

  public boolean isLessSpecificThan(Substitution substitution) {
    assert this.keySet().equals(substitution.keySet());

    Variable[] vars = this.keySet().toArray(new Variable[0]);
    boolean lessSpecific = true;

    for (int i = 0; i < vars.length; i++) {
      Variable vari = vars[i];
      Element current = this.get(vari);
      if (current.isConstant()) {
        if (current.equals(substitution.get(vari))) {
          lessSpecific = false;
        }
      } else {
        for (int j = 0; j < vars.length; j++) {
          if (j != i) {
            Variable varj = vars[j];
            if (!current.equals(this.get(varj)) && substitution.get(vari)
                .equals(substitution.get(varj))) {
              lessSpecific = true;
            }
          }
        }
      }
    }

    return lessSpecific;
  }

  /**
   * Get the most specific substitution in a list from an identity
   *
   * @param substitutions
   * @return
   */
  public static Substitution getMostSpecific(List<Substitution> substitutions,
      Substitution identity) {
    Variable[] vars = identity.keySet().toArray(new Variable[0]);

    int row_max = -1;
    long count_max = 0;

    for (int i = 0; i < substitutions.size(); i++) {
      long count_curr = substitutions.get(i).nbNonIdentityNull(identity);
      if (count_curr > count_max) {
        row_max = i;
        count_max = count_curr;
      }
    }

    List<Integer> selected_rows = new ArrayList<>();
    if (count_max > 0) {
      for (int i = 1; i < substitutions.size(); i++) {
        for (Variable var : vars) {
          if (substitutions.get(i).get(var).isConstant() && !substitutions.get(i).get(var).equals(
              substitutions.get(row_max).get(var))) {
            selected_rows.add(i);
          }
        }
      }
    }

    int row_spec = row_max;
    for (int i = 0; i < substitutions.size(); i++) {
      if (!selected_rows.contains(i) && i != row_max) {
        Substitution spec = row_spec < 0 ? identity : substitutions.get(row_spec);
        Substitution current = substitutions.get(i);
        if (spec.isLessSpecificThan(current) && !current.isLessSpecificThan(spec)) {
          row_spec = i;
        }
      }
    }

    return row_spec < 0 ? identity : substitutions.get(row_spec);
  }
}
