package fr.lifo.updatechase.model.logic;

import fr.lifo.updatechase.model.SyntaxError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <b>Atome est la classe representant un membre du corps ou de la tete d'une regle.</b>
 * <p>Un Atome est caracterise par :
 * <ul><li>name : le nom de l'atome.</li>
 * <li>tuple : un ensemble d'Element</li>
 * <li>rank : profondeur pour la deduction </li></ul></p>
 *
 * @see Element
 * @see Variable
 * @see Constant
 */
public class Atom implements Collection<Element>, Cloneable {

  private final Element[] tuple;
  private String fullName;
  private String name;
  private int rank = 0;

  /**
   * Constructeur Atome Affecte un nom a un atome.
   *
   * @param name le nom de l'Atome
   */
  public Atom(String name) {
    this(name, new Element[0]);
  }

  /**
   * Constructeur Atome Affecte un nom et les element d'une ArrayList d'Element a un atome.
   *
   * @param name  le nom de l'Atome
   * @param tuple la collection d'Element qui composeront l'Atome. Si l'ArrayList est nulle, une
   *              ArrayList vide lui sera associee.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public Atom(String name, Collection<Element> tuple) {
    this(name, tuple.toArray(new Element[0]));
  }

  /**
   * Constructeur Atome Affecte un nom et les element d'un tableau d'Element a un atome.
   *
   * @param name  le nom de l'Atome
   * @param tuple le tableau d'Element qui composeront l'Atome. Si le tableau est nul, une ArrayList
   *              vide lui sera associee.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public Atom(String name, Element[] tuple) {
    this.fullName = name;
    this.name = this.fullName.endsWith("-") ? this.fullName.substring(0, this.fullName.length() - 1)
        : this.fullName;
    this.tuple = tuple.clone();
  }

  /**
   * Parse le String a pour en retirer le(s) parametre(s) d'un des constructeurs de Atome. Un Atome
   * peut etre "!", "?". Ou bien alors <nomAtome(XnomElemVariable, "nomElemConstant", ... )> Toute
   * variable qui commence par _ est consideree existentielle.
   *
   * @param a String contenant les valeurs de l'Atome suivant le modele de la desciption.
   * @return Un nouvel Atome instancie grace a la valeur du String 'a'.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public static Atom stringToAtom(String a) throws SyntaxError {
    if (a.equals("!") || a.equals("?")) {
      return new Atom(a);
    }

    int pos1 = a.indexOf("(");
    int pos2 = a.indexOf(")");
    if (pos1 == -1 || pos2 == -1) {
      throw new SyntaxError("Syntax error in atome " + a);
    }

    String n = a.substring(0, pos1); // on récupère le nom de l'Atome
    String l = a.substring(pos1 + 1, pos2); // on récupère les éléments de l'Atome
    List<Element> t = new ArrayList<>();

    //l = l.replace(" ", "");
    for (String s : l.split(",")) {
      //if(s.charAt(0)>='A' && s.charAt(0)<='Z'){

      if (s.charAt(0) == '"' || s.charAt(0) == '\'') {
        s = s.substring(1, s.length() - 1);
      }

      t.add(Element.stringToElement(s));
    }

    return new Atom(n, t);
  }

  /**
   * Retourne la profondeur maximale d'une valeur nulle ou d'une variable dans une collection
   * d'atomes
   *
   * @param atoms Les atomes à tester
   * @return La profondeur maximale trouvée dans ces atomes.
   */
  public static int getMaxProf(Collection<? extends Atom> atoms) {
    int max = 0;

    for (Atom a : atoms) {
      int prof = a.getMaxProf();
      if (prof > max) {
        max = prof;
      }
    }

    return max;
  }

  /**
   * Retourne la profondeur maximale d'une valeur nulle ou d'une variable dans l'atome
   *
   * @return La profondeur maximale trouvée dans l'atome.
   */
  public int getMaxProf() {
    int max = -1;

    for (Variable e : this.getVariables()) {
      int eProf = e.getProf();
      if (max < eProf) {
        max = eProf;
      }
    }

    return max;
  }

  /**
   * Getteur afin d'acceder au nom d'un Atome.
   *
   * @return name, le nom d'un Atome.
   */
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
    this.fullName = this.fullName.endsWith("-") ? this.name + "-" : this.name;
  }

  public String getFullName() {
    return this.fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
    this.name = this.fullName.endsWith("-") ? this.fullName.substring(0, this.fullName.length() - 1)
        : this.fullName;
  }

  public int getRank() {
    return this.rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  /**
   * Retourne toutes les variables d'un atome.
   *
   * @return Set<Variable> Un ensemble de valeurs nulles
   * @see Element
   * @see Variable
   * @see Constant
   */
  public Set<Variable> getVariables() {
    return this.stream().filter(Element::isVariable).map((Element e) -> (Variable) e)
        .collect(Collectors.toSet());
  }

  /**
   * Retourne toutes les valeurs nulles d'un atome.
   *
   * @return Set<Variable> Un ensemble de valeurs nulles
   * @see Element
   * @see Variable
   * @see Constant
   */
  public Set<Variable> getNulls() {
    return this.stream().filter(Element::isNullValue).map(e -> (Variable) e)
        .collect(Collectors.toSet());
  }

  /**
   * Verifie pour chaque Element d'un Atome si il est de type Variable caracterise comme 'new' et
   * ajoute son nom a un ArrayList de String.
   *
   * @return res un ArrayList de String qui contient les nom des atomes nouveaux et variables.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public Set<String> getNullsName() {
    return this.stream().filter(Element::isNullValue).map(Element::getName)
        .collect(Collectors.toSet());
  }

  public Set<Constant> getConstants() {
    return this.stream().filter(Element::isConstant).map(e -> (Constant) e)
        .collect(Collectors.toSet());
  }

  /**
   * Compte le nombre de valeures nulles dans l'Atome
   *
   * @return le nombre de valeures nulles dans l'Atome
   */
  public long getNbNulls() {
    return this.stream().filter(Element::isNullValue).count();
  }

  /**
   * Check if the atom contains any of the elements in a collection
   *
   * @param elements the collection of elements to check
   * @return 'true' if the atom contains an element of the collection else 'false'
   */
  public boolean hasAnyElement(Collection<Element> elements) {
    return this.stream().anyMatch(elements::contains);
  }

  /**
   * Verifie pour chaque Element d'un Atome si il represente une valeur nulle.
   *
   * @return retourne un booleen indiquant si l'Atome contient une valeur nulle.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean containsNullValues() {
    return this.stream().anyMatch(Element::isNullValue);
  }

  /**
   * Verifie pour chaque Element d'un Atome si il represente une valeur nulle stared (marquée)
   *
   * @return retourne un booleen indiquant si l'Atome contient une valeur nulle marquée.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean containsNullValuesStared() {
    return this.stream().anyMatch(Element::isStaredNullValue);
  }

  public boolean containsConstantValues() {
    return this.stream().anyMatch(Element::isConstant);
  }

  /**
   * Recupere le nom de tout les Elements "New" de l'Atome this. Recupere les noms de tout les
   * Elements nouveaux de tout les Atomes de 'liste'. Verifie ensuite si un des noms d'Element de
   * this est deja contenu dans 'liste'.
   *
   * @param atoms une ArrayList d'Atome contenant tout les Atomes variables precedemment genere.
   * @return un booleen indiquant si this contient une Element qualifie variable deja cree
   * auparavant.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean containsOldNewVar(Collection<? extends Atom> atoms) {
    // retourne Vrai si l'atome contient une variable New qui est déjà dans la liste
    Collection<String> newVara = this.getNullsName();

    if (newVara.size() == 0) {
      return false;

    } else {
      List<String> newVarListe = new ArrayList<>();
      for (Atom atom : atoms) {
        newVarListe.addAll(atom.getNullsName());
      }

      for (String v : newVara) {
        if (newVarListe.contains(v)) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Renomme l'Element ayant pour nom 'vo' en la valeur contenu en 'vn'. Ne fait rien si le nom 'vo'
   * n'existe pas.
   *
   * @param vo String : le nom original de l'element dont on veut remplace le nom.
   * @param vn String : le nouveau nom.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean rename(String vo, String vn) {
    boolean res = false;

    for (Element e : this) {
      if (e.getName().equals(vo)) {
        res = true;
        e.setName(vn);
      }
    }

    return res;
  }

  /**
   * Recupere tout les nom des Element <Variable New> present dans l'Atome 'this'. Pour chacun de
   * ces noms, on essaie de le renommer dans 'liste'. Si ce renommage est effectue, on le renomme
   * alors dans l'Atome 'this'.
   *
   * @param atoms Une ArrayList d'Atome. Elle contient touts les Atomes ayant des Elements Variable
   *              precedemment cree.
   * @see Element
   */
  public void renameOldNewVar(Collection<? extends Atom> atoms) {
    // renome les variables New de la liste qui sont aussi dans l'atome (ce ne sont plus des New)
    for (String v : this.getNullsName()) {
      int pos = v.indexOf("New");
      boolean ok = false;
      String vn = v.substring(0, pos) + "Old" + v.substring(pos + 3);
      //System.out.println(" renomage v "+v+ " en "+vn);

      for (Atom atom : atoms) {
        if (atom.rename(v, vn)) {
          ok = true;
        }
      }

      if (ok) {
        this.rename(v, vn);
      }
    }
  }

  public void putNotRedOldNullVar(int num) {
    // rend non "reductible"(pour la comparaison) les variables new dont le numero est inférieur à num
    int pos, pos2;
    String nom, sNum;

    for (Element e : this) {
      nom = e.getName();
      pos = nom.lastIndexOf("New");
      pos2 = nom.lastIndexOf("_");

      if (pos != -1) {
        if (pos2 != -1) {
          //System.out.println(nom+"   "+ pos + "  "+pos2);
          sNum = nom.substring(pos + 3, pos2);
        } else {
          sNum = nom.substring(pos + 3);
        }

        int vNum = Integer.parseInt(sNum);
        //System.out.println(sNum + " ********** " + vNum);
        if (vNum < num) {
          e.setReductible(false);
        }
      }
    }
  }

  /**
   * Rend non "reductible"(pour la comparaison) les variables new de l'atome qui sont déjà dans la
   * liste.
   *
   * @param atoms La liste d'Atomes a rendre non reductible.
   */
  public void putRedOldNewVar(Collection<? extends Atom> atoms) {
    // rend non "reductible"(pour la comparaison) les variables new de l'atome qui sont déjà dans la liste
    int pos;
    String nom;

    for (Element e : this) {
      nom = e.getName();
      pos = nom.indexOf("New");

      if (pos != -1 && atoms.stream().anyMatch((Atom a) -> a.contains(e))) {
        e.setReductible(false);
      }
    }
  }

  /**
   * Rajoute le String "New" + un entier d'identification au nom d'une Variable.
   *
   * @param num un entier
   * @return Une nouvelle instance d'Atome qui a le meme nom et ses Element <Variable> renomme.
   */
  public Atom renameVar(int num) {
    List<Element> elements = new ArrayList<>();
    for (Element e : this) {
      if (e.isVariable()) {
        elements.add(new Variable(e.getName() + "New" + num, Element.NULL));

      } else {
        elements.add(e);
      }
    }
    return new Atom(this.getFullName(), elements);
  }

  /**
   * Met a jour la Substitution 's' afin qu'elle contienne composition des substitutions des Element
   * Variable de l'Atome 'this' et de l'Atome 'b' uniquement si ces deux Atome ont le meme nom.
   *
   * @param other        un Atome avec lequel on fait l'union des substittion.
   * @param substitution Substitution aui contient un HashMap de toutes les substitutions deja
   *                     existantes.
   * @return la Substitution apres sa mise a jour. Une valeur null si le tuple n'est pas Variable.
   * null si b!=this null si l'Element que l'on traite existe deja et n'est pas dan b.
   * @see Element
   * @see Variable
   * @see Constant
   * @see Substitution
   */
  public Substitution map(Atom other, Substitution substitution) {
    // return s'°s  such that s'(s(this))==b null if not possible
    // System.out.println("MAP " +this+"      "+b);
    Substitution res = null;

    if (this.getName().equals(other.getName()) && this.size() == other.size()) {
      res = new Substitution();

      for (int i = 0; i < this.size(); ++i) {
        Element e1 = this.get(i);
        Element e2 = other.get(i);

        if (e1.isVariable()) {
          if (!substitution.containsKey(e1) && !res.containsKey(e1)) {
            res.put((Variable) e1, e2);

          } else if (substitution.containsKey(e1) && !substitution.get(e1).equals(e2)) {
            res = null;
            break;
          }

        } else if (!e1.equals(e2)) {
          res = null;
          break;
        }
      }
    }

    return res;
  }

  /**
   * Recherche une instance possible de l'Atome 'this' dans la liste a partir de la position pos tel
   * que la substitution de cette Atome soit present dans l'ArrayList target.
   *
   * @param atoms        ArrayList<Atome> dans laquelle on fait la recherche.
   * @param substitution Substitution.
   * @param pos          Entier. Le numero du premier l'Atome a regarder
   * @param stopCond     Entier. Traitement des valeurs nulles.
   * @return le numero de l'intance possible. -1 si il y en a pas.
   * @see Substitution
   * @see Element
   * @see Variable
   */
  public int mapAll(Collection<? extends Atom> atoms, Substitution substitution, int pos,
      int stopCond) {
    // return the position after pos in trg such that s(this) = trg[res]
    // -1 if not exists
    int i = pos + 1;
    boolean ok = false;
    Substitution newSubstitution;
    Atom[] atomsList = atoms.toArray(new Atom[0]);

    while (!ok && i < atomsList.length) {
      if (atomsList[i].containsNullValues() && stopCond == -1
          || atomsList[i].containsNullValuesStared() && stopCond == -2) {
        i++;
      } else {
        newSubstitution = (Substitution) substitution.clone();
        newSubstitution = this.map(atomsList[i], newSubstitution);
        if (newSubstitution != null) {
          ok = true;
          substitution.putAll(newSubstitution);
          //System.out.println("La substitution trouvee dans mapList : "+s);
        } else {
          i++;
        }
      }
    }

    return ok ? i : -1;
  }

  /**
   * Recherche une instance possible de l'Atome 'this' deja substitue, dans la liste a partir de la
   * position pos tel que cet Atome soit deja substitue dans la liste.
   *
   * @param atoms        Collection<Atome> dans laquelle on fait la recherche.
   * @param substitution Substitution.
   * @param pos          Entier. Le numero du premier l'Atome a regarder
   * @return le numero de l'intance possible. -1 si il y en a pas.
   * @see Substitution
   * @see Element
   * @see Variable
   */
  public int mapAll2(Collection<? extends Atom> atoms, Substitution substitution, int pos) {
    // return the position after pos in src such that this = s(src[res])
    // -1 if not exists
    int i = pos + 1;
    boolean ok = false;
    Substitution newSubstitution;
    Atom[] atomsList = atoms.toArray(new Atom[0]);

    while (!ok && i < atomsList.length) {
      newSubstitution = (Substitution) substitution.clone();
      newSubstitution = atomsList[i].map(this, newSubstitution);
      if (newSubstitution != null) {
        ok = true;
        substitution.putAll(newSubstitution);
        //System.out.println("La substitution trouvee dans mapList : "+s);
      } else {
        i++;
      }
    }

    return ok ? i : -1;
  }

  /**
   * Permet de connaitre la position dans un tuple de la premiere occurence d'un Element apres une
   * certain position dans l'ArrayList d'Element de l'Atome 'this' si cette occurrence existe.
   *
   * @param element l'Element recherche.
   * @param pos     l'entier qui represente la position a partir de laquelle la recherche
   *                s'effectue.
   * @return La position de la premiere occurrence de l'Element recherche, a partir de la position
   * 'pos'. -1 si l'Element n'existe pas apres la position donnee.
   */
  public int findElement(Element element, int pos) {
    // retourne la première position après pos qui contient l'element e
    // -1 s'il elle n'existe pas
    while (++pos < this.size()) {
      if (this.get(pos).equals(element)) {
        return pos;
      }
    }

    return -1;
  }

  public boolean isDeletableInRuleBody() {
    return this.fullName.contains("-");
  }

  public boolean isLessInstanciate(Atom other) {
    // retourne true si il existe une substitution s telle que s(this)=b en considérant comme des constantes les variables non reductibles
    boolean res = this.getName().equals(other.getName()) && this.size() == other.size();
    Substitution s = new Substitution();

    if (res) {
      for (int i = 0; i < this.size(); ++i) {
        Element e1 = this.get(i);
        Element e2 = other.get(i);

        if (e1.isVariable() && e1.isReductible()) {
          if (!s.containsKey(e1)) {
            s.put((Variable) e1, e2);

          } else if (!s.get(e1).equals(e2)) {
            res = false;
            break;
          }

        } else if (!e1.equals(e2)) {
          res = false;
          break;
        }
      }
    }

    return res;
  }

  public Element get(int index) {
    return this.tuple[index];
  }

  public Element set(int index, Element element) {
    return this.tuple[index] = element;
  }

  /**
   * Verifie que l'Atome 'this' et l'atome 'b' ont le meme nom. Puis verifie pour chaque Element i
   * des deux Atomes que leur nom soient egaux sans prendre en compte la sous-chaine potentielle
   * "new" du nom.
   *
   * @param other L'Atome avec lequel on compare 'this'
   * @return boolean Renvoie une valeur booleenne indiquant si les deux Atomes sont bien identique.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean equalsModuloNew(Atom other) {
    // retourne true si les deux atomes sont identiques modulo le renomage des variables nouvelles
    boolean res = this.getName().equals(other.getName()) && this.size() == other.size();

    if (res) {
      for (int i = 0; i < this.size(); ++i) {
        if (!this.get(i).identiqueModuloNew(other.get(i))) {
          res = false;
          break;
        }
      }
    }

    return res;
  }

  /**
   * Verifie que l'Atome 'this' a le meme nom que l'Atome b et que les Element qui les composent ont
   * le meme nom et le meme type SAUF pour les éléments nulles (ex: _N8 peut etre égale à _N1 donc
   * ici  A(r,_N7) == A(r,_N1) ) En d'autre terme que leurs Element sont identique.
   *
   * @param other Un Atome que l'on souhaite comparer a 'this'
   * @return boolean Valeur qui indique si les deux Atomes sont bien identique.
   * @see Element
   * @see Variable
   * @see Constant
   */
  public boolean equalsModuloNull(Atom other) {
    boolean res = this.getName().equals(other.getName()) && this.size() == other.size();

    if (res) {
      for (int i = 0; i < this.size(); ++i) {
        if (!this.get(i).equalsModuloNull(other.get(i))) {
          res = false;
          break;
        }
      }
    }

    return res;
  }

  /**
   * Redefinition de la methode equals. Verifie si 'x' est une instance de Atome, si leur nom sont
   * identique et si leur Exemples respectif sont identique en i.
   *
   * @param other De type Object. c'est l'instqnce aue l'on veut comparer avec x.
   * @return boolean Valeur booleenne indiquant si les deux Atomes sont bien identique.
   * @see Element
   * @see Variable
   * @see Constant
   */
  @Override
  public boolean equals(Object other) {
    boolean res = other instanceof Atom && this.getName().equals(((Atom) other).getName())
        && this.size() == ((Atom) other).size();

    if (res) {
      Atom otherAtom = (Atom) other;
      for (int i = 0; i < this.size(); ++i) {
        if (!this.get(i).equals(otherAtom.get(i))) {
          res = false;
          break;
        }
      }
    }

    return res;
  }

  /**
   * Gere l'affichage d'un Atome en texte.
   *
   * @return le String d'affichage d'un Atome.
   */
  @Override
  public String toString() {
    StringBuilder res = new StringBuilder(this.getFullName());
    if (this.size() > 0) {
      res.append("(");
      int cpt = 0;
      for (Element s : this) {
        if (cpt == 0) {
          res.append("'").append(s).append("'");
        } else {
          res.append(", ").append("'").append(s).append("'");
        }
        cpt++;
      }
      res.append(")");
    }
    return res.toString();
  }

  /**
   * Redefinition de la methode hashCode. Concatene le hashCode de l'attribut name de 'this' avec
   * celui de chacun de ses Element.
   *
   * @return int L'entier qui represente le hashCode de l'instance de Atome.
   */
  @Override
  public int hashCode() {
    int res = name.hashCode();
    for (Element element : this) {
      res += element.hashCode();
    }
    return res;
  }

  @Override
  public int size() {
    return this.tuple.length;
  }

  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  @Override
  public boolean contains(Object other) {
    return other instanceof Element && this.findElement((Element) other, -1) != -1;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return c.stream().allMatch(this::contains);
  }

  @Override
  public Iterator<Element> iterator() {
    return Arrays.stream(this.tuple).iterator();
  }

  @Override
  public Object[] toArray() {
    return this.tuple;
  }

  @Override
  public <T> T[] toArray(T[] a) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean add(Element element) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(Collection<? extends Element> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Object clone() {
    Element[] tuple = new Element[this.size()];
    for (int i = 0; i < this.size(); i++) {
      tuple[i] = (Element) this.get(i).clone();
    }

    return new Atom(this.getFullName(), tuple);
  }
}

			
		
