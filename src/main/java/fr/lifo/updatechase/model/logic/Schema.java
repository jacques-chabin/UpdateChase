package fr.lifo.updatechase.model.logic;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.DBFileIO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Schema {

  private final static char EDGE_TOKEN = '$';

  private final Map<String, String[]> nodes = new HashMap<>();
  private final Map<String, String[]> edges = new HashMap<>();

  public static Schema fromFile(String path) throws SyntaxError, IOException {
    Collection<Atom> atoms = DBFileIO.readFromFile(path, new ArrayList<>(), Atom::stringToAtom);
    Schema schema = new Schema();

    for (Atom atom : atoms) {
      String pred = atom.getName();
      String[] attributes = atom.stream().sequential().map(Element::getName).toArray(String[]::new);

      if (pred.charAt(0) == EDGE_TOKEN) {
        pred = pred.substring(1);
        schema.edge(pred, attributes);

      } else {
        schema.node(pred, attributes);
      }
    }

    return schema;
  }

  public static Schema fromBdd(Bdd bdd) {
    return new Schema();
  }

  public void toFile(String path) throws IOException {
    Collection<Atom> atoms = new HashSet<>();

    for (Map.Entry<String, String[]> node : this.nodes.entrySet()) {
      atoms.add(new Atom(node.getKey(),
          Arrays.stream(node.getValue()).sequential().map(Element::stringToElement)
              .toArray(Element[]::new)));
    }

    for (Map.Entry<String, String[]> edge : this.edges.entrySet()) {
      atoms.add(new Atom(EDGE_TOKEN + edge.getKey(),
          Arrays.stream(edge.getValue()).sequential().map(Element::stringToElement)
              .toArray(Element[]::new)));
    }

    System.out.println(atoms);

    DBFileIO.writeToFile(path, atoms);
  }

  public Schema node(String label, String... attributes) {
    this.nodes.put(label, attributes);
    return this;
  }

  public Schema edge(String label, String... attributes) {
    this.edges.put(label, attributes);
    return this;
  }

  public Collection<String> getNodesLabels() {
    return Collections.unmodifiableSet(this.nodes.keySet());
  }

  public Collection<String> getNodeAttributes(String label) {
    return Arrays.asList(this.nodes.get(label));
  }

  public Collection<String> getEdgesLabels() {
    return Collections.unmodifiableCollection(this.edges.keySet());
  }

  public Collection<String> getEdgeAttributes(String label) {
    return Arrays.asList(this.edges.get(label));
  }

  public boolean isNode(String label) {
    return !this.isEdge(label);
  }

  public boolean isNode(Atom atom) {
    return this.isNode(atom.getName());
  }

  public boolean isEdge(String label) {
    return this.edges.containsKey(label);
  }

  public boolean isEdge(Atom atom) {
    return this.isEdge(atom.getName());
  }

  /**
   * Permet d'obtenir le nom d'un attribut d'un predicat donne.
   *
   * @param predicat String Le nom du predicat dont on veut obtenir le nom de l'attribut.
   * @param i        int Le numero de l'attribut dans le predicat.
   * @return String Le nom de l'attribut i dans le predicat.
   * @throws SyntaxError Si le nom du predicat n'existe pas ou si l'attribut i dans le predicat
   *                     n'existe pas.
   */
  String getAttributName(String predicat, int i) throws SyntaxError {
    // return name of the attribute number i of predicat
    String[] attributes = null;

    if (this.nodes.containsKey(predicat)) {
      attributes = this.nodes.get(predicat);

    } else if (this.edges.containsKey(predicat)) {
      attributes = this.edges.get(predicat);

    } else {
      throw new SyntaxError("Predicat '" + predicat + "' not in schema");
    }

    if (i >= attributes.length) {
      throw new SyntaxError(
          "Predicat '" + predicat + "' has not " + (i + 1) + " attributes in schema");
    }

    return attributes[i];
  }
}
