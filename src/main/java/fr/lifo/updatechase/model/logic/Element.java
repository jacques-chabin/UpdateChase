package fr.lifo.updatechase.model.logic;

/**
 * <b>Un Element est soit Variable soit Constant.</b>
 * Un Element compose un Atome.
 * <p>Un Element est caracterise par :
 * <ul><li>String name : son nom</li>
 * <li>int typ : son type</li>
 * <li>boolean red : si il est reductible ???</li>
 *
 * @see Constant
 * @see Variable
 */
public abstract class Element implements Comparable<Element>, Cloneable {

  public final static byte CONSTANT = 1;
  public final static byte VARIABLE = 2;
  public final static byte NULL = 3;
  public final static byte STARED_NULL = 4;
  private String name;
  private byte type;  // 1 for constant, 2 for variable, 3 for null value, 4 for stared null value
  private boolean isReductible;

  /**
   * Constructeur de Element.
   *
   * @param name le nom d'un Element.
   * @param type le type d'une Element ???.
   */
  protected Element(String name, byte type) {
    this(name, type, true);
  }

  protected Element(String name, byte type, boolean isReductible) {
    this.name = name;
    this.setType(type);
    this.isReductible = isReductible;
  }

  /**
   * Permet de construire un Element à partir d'une chaîne de caractères qui le représente.
   *
   * @param s Chaîne de caractère qui représente l'Element
   * @return Une Element qui peut être une constante, variable, ou une valeur nulle
   */
  public static Element stringToElement(String s) {
    Element e;
    s = s.trim();

    switch (s.charAt(0)) {
      case '_':
        int prof = 0;
        boolean reductible = false;
        String name = s.substring(1);

        int profIndex = name.lastIndexOf('#');
        if (profIndex > 0) {
          if (name.endsWith("R")) {
            name = name.substring(0, name.length() - 1);
            reductible = true;
          }
          prof = Integer.parseInt(name.substring(profIndex + 1));
          name = name.substring(0, profIndex);
        }
        e = new Variable(name, Element.NULL, prof, reductible);
        break;

      case 'X':
        e = new Variable(s);
        break;

      default:
        e = new Constant(s);
        break;
    }

    return e;
  }

  public String getName() {
    return this.name;
  }

  /**
   * Setteur de l'attribut name.
   *
   * @param name Le nom qu'aura l'Element.
   */
  public void setName(String name) {
    this.name = name;
  }

  public byte getType() {
    return this.type;
  }

  protected void setType(byte type) throws IllegalArgumentException {
    if (type < CONSTANT || type > STARED_NULL) {
      throw new IllegalArgumentException();
    }

    this.type = type;
  }

  /**
   * Verifie si L'Element est Variable
   *
   * @return boolean Un booleen indiquant si l'Element est Variable.
   */
  public boolean isVariable() {
    return this.getType() >= Element.VARIABLE;
  }

  /**
   * Verifie si L'Element est Constant
   *
   * @return boolean Un booleen indiquant si l'Element est Constant.
   */
  public boolean isConstant() {
    return this.getType() == Element.CONSTANT;
  }

  public boolean isNullValue() {
    return this.getType() >= Element.NULL;
  }

  public boolean isStaredNullValue() {
    return this.getType() == Element.STARED_NULL;
  }

  /**
   * Getteur de l'attribut red.
   *
   * @return Le booleen indiquant si l'Element est reductible ???.
   */
  public boolean isReductible() {
    return this.isReductible;
  }

  /**
   * Setteur de l'Attribut red
   *
   * @param isReductible le booleen indiquant si l'Element sera reductible
   */
  public void setReductible(boolean isReductible) {
    this.isReductible = isReductible;
  }

  //public String toString(){if(red){return name+'-';} else{return name+'+';}}
  //public String toString(){if(red) return name+"*r"; else return name;}
  //public String toString(){if(typ!=4) return name; else return name+'*';}

  /**
   * Redefinition du toString de Object. Si l'Element est existenciel, prefixe "_" a son nom.
   *
   * @return Le nom d'un Element.
   */
  @Override
  public String toString() {
    String res = name;

    switch (this.getType()) {
      case Element.STARED_NULL:
        res += '*';
      case Element.NULL:
        res = '_' + res;
        break;
    }

    return res;
  }

  /**
   * Teste l'egalite entre 'this' et un autre Element en comparant leur nom et leur type SANS
   * prendre en compte le nom des éléments nulles (ici _N8 == _N7 == _N42 ...)
   *
   * @param other L'Element avec leauel on veut tester l'egalite.
   * @return le booleen indiquant si this est egal a l'Element passe en parametre.
   */
  boolean equalsModuloNull(Element other) {
    return (this.getName().equals(other.getName()) && this.getType() == other.getType()) || (
        this.isNullValue() && other.isNullValue());
  }

  /**
   * Teste l'identite entre 'this' et un autre Element en comparant leur nom sans prendre en compte
   * la chaine de caractere "New", et leur type.
   *
   * @param other L'Element avec leauel on veut tester l'identite.
   * @return le booleen indiquant si this est identique a l'Element passe en parametre.
   */
  boolean identiqueModuloNew(Element other) {
    boolean res;
    int pos1 = this.isReductible() ? this.getName().indexOf("New") : -1;
    int pos2 = other.isReductible() ? other.getName().indexOf("New") : -1;

    if (pos1 != -1 && pos2 != -1) {
      String name = getName().substring(0, pos1);
      String otherName = other.getName().substring(0, pos2);
      res = name.equals(otherName);

    } else {
      res = this.getName().equals(other.getName()) && this.getType() == other.getType();
    }

    return res;
  }

  /**
   * Override de la methode equals de Object. Verifie si l'Element 'this' a le meme nom et type que
   * l'Element passe en parametre, si c'est un Element.
   *
   * @param other Un Object, preferablement une instance de Element.
   * @return Booleen indiquant si l'Object passe en parametre a le meme nom et type que L'instance
   * 'this' de Element.
   */
  @Override
  public boolean equals(Object other) {
    if (other instanceof Element) {
      Element e = (Element) other;
      return this.getType() == e.getType() && this.getName().equals(e.getName());
    }

    return false;
  }

  /**
   * Override de la methode hashCode de Object. applique la fonction hashCode a (name+typ).
   *
   * @return int le hashCode d'un Element.
   */
  @Override
  public int hashCode() {
    return (this.getName() + this.getType()).hashCode();
  }

  /**
   * Implementation de la fonction compareTo de Comparable. Compare le type, puis, le name si les
   * type sont egaux.
   *
   * @param other Un Element a comparer.
   * @return int
   */
  @Override
  public int compareTo(Element other) {
    int res = other.getType() - this.getType();

    if (res == 0) {
      res = this.getName().compareTo(other.getName());
    }

    return res;
  }

  @Override
  public abstract Object clone();
}
