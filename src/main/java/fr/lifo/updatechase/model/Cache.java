package fr.lifo.updatechase.model;

import fr.lifo.updatechase.model.db.DBFileIO;
import fr.lifo.updatechase.model.logic.Query;
import fr.lifo.updatechase.model.logic.Rule;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Cache est l'Objet qui represente toute les requetes a faire sur la base de donnee et leur
 * reponses???
 */
public class Cache {

  Map<Query, Integer> queries;

  /**
   * Constructeur de Cache. Initialise l'Attribut query avec une nouvelle HashMap.
   */
  public Cache() {
    queries = new HashMap<Query, Integer>();
  }

  /**
   * Retourne le nombre de requetes.
   *
   * @return int le nombre de requetes.
   */
  public int size() {
    return queries.size();
  }

  /**
   * Effectue le passage en String du cache.
   *
   * @return String Le String d'affichage du cache.
   */
  public String toString() {

    String res = ""; //+queries;
    for (Query q : queries.keySet()) {
      Integer ans = queries.get(q);
      res += q + " answer : ";
      if (ans == null) {
        res += "unkown";
      } else {
        if (ans == 1) {
          res += "True";
        } else {
          res += "False";
        }
      }
      res += "\n";
    }
    return res;
  }

  /**
   * Verifie si la Query est dans le Cache.
   *
   * @param Query q La Query recherche.
   * @return Integer L'Integer associe a la Query ou null si la Query n'existe pas.
   */
  public Integer isInCache(Query q) {
    //System.out.println("Suis la avec queries = "+ queries + "***" + q + "===");
    return queries.get(q);
  }

  /**
   * Ajoute le couple (Query, reponse) dans le Map<Query, Integer> de Cache.
   *
   * @param Query q la Query a ajoute.
   * @param int   ans l'entier associe a la query.
   */
  public void addInCache(Query q, int ans) {
    queries.put(q, ans);
  }

  /**
   * Creer un Cache a partir d'un fichier.
   *
   * @param String nameFile le nom du fichier a partir duquel un Cache est cree.
   * @return Cache un Cache initialise.
   */
  public static Cache initFromAns2File(String nameFile) {
    Cache res = new Cache();
    Query r = null;

    try {
      // open the file named nameFile.dlp in the repertory ./Constraint/
      //File f = open("./Constraint/"+nameFile+".dlp");
      FileReader fr = new FileReader("./" + nameFile + ".dlp");
      BufferedReader br = new BufferedReader(fr);

      try {
        StringBuilder rule = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
          //line = line.replace(" ", "");
          line = DBFileIO.delSpace(line);
          if (line == null) {
          } else if (line.length() == 0) {
          } else if (line.charAt(0) == '%') {
          } else if (line.charAt(line.length() - 1) != '.') {
            rule.append(line);
          } else {
            int pos = 0;
            int ans;
            // on supprime le point à la fin
            rule.append(line, 0, line.length() - 1);
            try {
              //System.out.println("String to transform in query : "+rule);
              // on récupère la réponse Yes ou No
              if (rule.charAt(0) == 'N') {
                ans = -1;
                pos = 4;
              } else if (rule.charAt(0) == 'Y') {
                ans = 1;
                pos = 5;
              } else {
                throw new SyntaxError("No. or No missing before BCquery");
              }
              // supprime Yes or No
              rule = new StringBuilder(rule.substring(pos));
              // remet la query dans le bon sens ? :-
              pos = rule.indexOf("?");
              String tete = rule.substring(pos);
              rule = new StringBuilder(rule.substring(0, pos - 3));
              rule.insert(0, tete + " :- ");
              //System.out.println("String to transform in query : "+rule);
              r = (Query) Rule.stringToRule(rule.toString());
              res.addInCache(r, ans);
              rule = new StringBuilder();
            } catch (SyntaxError e) {
              System.out.println(e.getMessage());
              rule = new StringBuilder();
            }
          }
          line = br.readLine();
        }
      } catch (IOException e1) {
        System.out.println("Error reading " + e1.getMessage());
      }
    } catch (FileNotFoundException e2) {
      System.out.println("File not found " + e2.getMessage());
    }
    return res;
  }
}

