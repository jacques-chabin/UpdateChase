# UpdateChase

This project is a consistency management tool for incomplete databases with linked nulls.
Two different implementations are proposed: an in-memory version and an incremental, query-driven version that deals with data stored in the DBMS.
Ensuring consistent and irredundant data sets remains essential to minimise errors, promote maintainable application code, and achieve reliable results in data analysis.
A major challenge in achieving consistency is dealing with incomplete data, i.e. missing information that may be provided later.
This often requires the use of marked (or linked) nulls in many applications to express unknown but connected information.
Keeping data consistent and avoiding redundancy is challenging.

The tools provide the `fr.lifo.updatechase.model.db.Bdd` as a wrapper arround different DBMS:
- In-Memory
- MySQl
- Neo4J

This work is licensed under CC BY-NC 4.0.

## Dataset

The project contains multiple datasets located in `src/test/resources`.
Each dataset is composed of multiple dlp files:
- `BddInit.dlp`, the initial consistent database
- `Constraints.dlp`, the rules applied to the database
- `InsertFacts.dlp`, an exemple of insertion
- `InsertResults.dlp`, the database resulting from the exemple of insertion
- `SuppFacts.dlp`, an exemple of deletion
- `SuppResults.dlp`, the database resulting from the exemple of deletion

Here the list of available datasets, multiple examples used for tests are also availables:

| Datase     | Size (facts) | Path                             | Link                                                       |
| :--------- | -----------: | :------------------------------- | :--------------------------------------------------------- |
| Movie      |          604 | `neo4j-examples/movies`          | https://github.com/neo4j-graph-examples/movies             |
| GOT        |       24,818 | `neo4j-examples/got`             | https://github.com/neo4j-graph-examples/graph-data-science |
| LDCB-0N    |       16,559 | `benchs/ldbc_nulls/ldbc-0N`      | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-50N   |       16,559 | `benchs/ldbc_nulls/ldbc-50N`     | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-100N  |       16,559 | `benchs/ldbc_nulls/ldbc-100N`    | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-500N  |       16,559 | `benchs/ldbc_nulls/ldbc-500N`    | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-1000N |       16,559 | `benchs/ldbc_nulls/ldbc-1000N`   | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-1K    |        2,248 | `benchs/ldbc_size/ldbc-1k`       | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-10K   |       16,559 | `benchs/ldbc_size/ldbc-10k`      | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-50K   |      133,952 | `benchs/ldbc_size/ldbc-50k`      | https://ldbcouncil.org/benchmarks/snb/                     |
| LDCB-100K  |      190,631 | `benchs/ldbc_size/ldbc-100k`     | https://ldbcouncil.org/benchmarks/snb/                     |
| Yeast      |       13,828 | `benchs/yeast`                   | https://networkrepository.com/bio-yeast.php                |
| Yeast-10N  |       13,819 | `benchs/yeast-10N`               | https://networkrepository.com/bio-yeast.php                |
| Yeast-50N  |       13,779 | `benchs/yeast-50N`               | https://networkrepository.com/bio-yeast.php                |
| Yeast-100N |       13,725 | `benchs/yeast-100N`              | https://networkrepository.com/bio-yeast.php                |

## Installation

### Prerequisites

The project is based on Maven, before you begin, ensure you have met the following requirements:
- Java Development Kit (JDK) installed on your machine
- Maven installed on your machine

### Instructions

1. Navigate to the project directory

2. Install the project:

```bash
mvn install
```

## Tests

You can run tests with:
```bash
mvn test
```

## Usage

A GUI is avaible at `fr.lifo.updatechase.ui.Exec`.

## Citations

> CHABIN, Jacques, HALFELD-FERRARI, Mirian, et LAURENT, Dominique. Consistent updating of databases with marked nulls. Knowledge and Information Systems, 2020, vol. 62, no 4, p. 1571-1609. DOI: [10.1007/s10115-019-01402-w](https://doi.org/10.1007/s10115-019-01402-w)

> CHABIN, Jacques, HALFELD-FERRARI, Mirian, HIOT, Nicolas, et LAURENT, Dominique. Managing Linked Nulls in Property Graphs: Tools to Ensure Consistency and Reduce Redundancy. In : European Conference on Advances in Databases and Information Systems. Cham : Springer Nature Switzerland, 2023. p. 180-194. DOI: [10.1007/978-3-031-42914-9_13](https://doi.org/10.1007/978-3-031-42914-9_13)
